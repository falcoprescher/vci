// Clear screen and print title
var title = process.env.NODE_ENV ? 'VCI - ' + process.env.NODE_ENV : 'VCI';
var indent = '';
for (var c = 0; c < process.stdout.columns / 2 - title.length / 2; c++)
indent = indent + ' ';
var fill = '                                                                                                                               ';
process.stdout.write('\u001B[2J\u001B[0;0f\u001B[1;44;37m' + (indent + title + fill).substr(0, process.stdout.columns - 1) + '\u001B[0m\n\n');
// -------------------------------------------------------------------------------------------- //


// global vars
rootdirname = __dirname;   // set rootdir; global(in all scripts)
sessionStore = undefined;  // session management and data only
socket = undefined;
db = undefined;
server = undefined;    // web server
vci_sessions = new Array();   // linking between http and socket session and session data
vcis = new Array();   // all active vcis
fsCache = undefined;   // file cache and minifying


// db models
// ModelName: collection_name
MODELS = [{
	Users: 'users'
}, {
	Sessions: 'sessions'
}, {
	VCIs: 'vcis'
}, {
	ChatMessages: 'chat_messages'
}, {
	VCIData: 'vci_data'
}];


// load modules
console.log('[LOAD] modules');

colors = require('colors');
vsprintf = require('sprintf').vsprintf;

async = require('async');
util = require('util');
os = require('os');
uuid = require('node-uuid');

fs = require("fs");
path = require('path');
mime = require('mime');
uglifyjs = require("uglify-js");
//uglifyjs = require("uglify-js2");

crypt = require('crypto');
mongodb = require('mongodb');
bson = mongodb.BSONPure;

express = require('express');
app_primary = express();
app_secondary = express();
http = require('http');
https = require('https');
i18n = require("i18n");
cookie = require('cookie');
connect = require('connect');
socketio = require('socket.io');

jsdom = require("jsdom").jsdom;

uaParser = require('ua-parser');


// load libs
console.log('[LOAD] libs');

logger = require(rootdirname + '/libs/logger');
config = require(rootdirname + '/libs/config');
core = require(rootdirname + '/libs/core');


// load classes
console.log('[LOAD] classes');

FSCache = require('./classes/fscache');
Server = require('./classes/server');
DB = require('./classes/db');
Socket = require('./classes/socket');

VCI = require('./classes/vci');
Chat = require('./classes/chat');


// ############################################################################################ //
// ############################################################################################ //


// configure uglify(maximum compress)
uglify = function (str) {
	var ast = uglifyjs.parser.parse(str);
	ast = uglifyjs.uglify.ast_mangle(ast);
	ast = uglifyjs.uglify.ast_squeeze(ast);
	return uglifyjs.uglify.gen_code(ast);
}


// configure i18n
i18n.configure({
	locales: config.LOCALS,
	register: global
});


// set process name
if (config.PROCESS_NAME) process.title = config.PROCESS_NAME;


// startup all application components in a queue
async.series({
	prepare: function (callback) {
		// prepare sessionStore
		logger.setup('sessionStore');
		sessionStore = new connect.middleware.session.MemoryStore();

		// create file cache
		logger.setup('fileCache');
		fsCache = new FSCache();

		callback(null, 'prepare');
	},
	db: function (callback) {
		// connect db server and init collections(models)
		logger.setup('database');
		new DB(function (err, result) {
			db = result;
			callback(err, result);
		});
	},
	preCache: function (callback) {
		// precache files
		fsCache.precache(config.PRECACHE, callback);
	},
	server: function (callback) {
		// configure and run webserver
		logger.setup('webserver');
		new Server(function (err, result) {
			server = result;
			callback(err, result);
		});
	},
	socket: function (callback) {
		// create socket server
		logger.setup('socket');
		new Socket(function (err, result) {
			socket = result;
			callback(err, result);
		});
	}
},
// final callback
function (err, results) {
	logger.system("Server running ...");
});


// ############################################################################################ //
// ############################################################################################ //


//base methods
clients = function () {
	for (key in vcis) {
		var vci = vcis[key];

		console.log(key);
	};
};

//system methods
info = function () {
	logger.log('hostname =', os.hostname());
	logger.log('networkInterfaces =', os.networkInterfaces());
	logger.log('uptime =', os.uptime());
	logger.log('cpus =', os.cpus());
	logger.log('type =', os.type());
	logger.log('release =', os.release());
	logger.log('platform =', os.platform());
	logger.log('arch =', os.arch());
	logger.log('loadavg =', os.loadavg());
	logger.log('totalmem =', os.totalmem());
	logger.log('freemem =', os.freemem());
};

process.on('uncaughtException', function (err) {
	if (err instanceof ReferenceError) {
		console.log('>>> ref error')
	}

	logger.error(err.stack);
});

process.stdin.resume();
process.stdin.setEncoding('utf8');

process.stdin.on('data', function (chunk) {
	try {
		eval(chunk + '();');
	} catch (e) {
		eval(chunk);
	}
});

process.stdin.on('end', function () {
	process.stdout.write('end');
});

process.on('exit', function () {
	process.nextTick(function () {
		logger.system('This will not run');
	});
	logger.system("Server shutdown");
});