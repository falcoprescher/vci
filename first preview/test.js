// Konstruktoren
function Superclass (name) {
   this.name = name;
   this.superprop = true;
}

function Subclass (name) {
   Superclass.call(this, name);
}





// Vererbung
Subclass.prototype = Object.create(Superclass.prototype);
Subclass.prototype.constructor = Subclass;







var superobject = new Superclass("Hello");
var subobject = new Subclass("World");



console.log("superobject", superobject);
console.log("subobject", subobject);