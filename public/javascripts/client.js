// looks if object o is empty by looking if it has any properties
var isEmpty = function(o) {
	for (var i in o) {return false;}
	return true;
};

// gets all snapshot objects which will be sent to the server in a broadcast interval
// datastructure must be build like this VCI.layers[layerID].objects[objectID]
var SnapShot = function() {
	this.take = function(callback) {
		if (VCI) {
			var _returnVal = {};

			for(var layerID in VCI.layers) {
				var vciLayer = VCI.layers[layerID];

				if (!vciLayer[layerID])
					_returnVal[layerID] = {};

				for(var propertyKey in vciLayer) {
					if (propertyKey != "objects"){
						if (_returnVal[layerID])
							_returnVal[layerID][propertyKey] = vciLayer[propertyKey];

						delete vciLayer[propertyKey];
					}
				}

				for(var vciObjectID in vciLayer.objects) {
					var object = vciLayer.objects[vciObjectID];


					for(var propertyKey in object) {
						if (!_returnVal[layerID].o)
							_returnVal[layerID].o = {};
						if (!_returnVal[layerID].o[vciObjectID])
							_returnVal[layerID].o[vciObjectID] = {};
						_returnVal[layerID].o[vciObjectID][propertyKey] = object[propertyKey];

						delete object[propertyKey];
					}
				}
				if (isEmpty(_returnVal[layerID])) {delete _returnVal[layerID];}
			};

			if (!isEmpty(_returnVal)) {callback(_returnVal);}
		}
	};
};

var last_chat_origin = undefined;

// configurates the client side connection to the socket on the server
sio = io.connect(host, {
	'reconnect': true,
	'reconnection delay': RETRY_DELAY,
	'max reconnection attempts': RETRY_ATTEMPTS
});

// Emitting the SessionId
sio.emit('ssid', ssid);

sio.on('sys', function (data) {
	console.log("server:" + data);
});

// initialization method while connecting to the socket
sio.on('connect', function () {

	var chat_send_btn = document.getElementById("vci-chat-send-btn");
	var chat_send_text = document.getElementById("vci-chat-send-text");

	chat_send_btn.onclick = function() {
		if (chat_send_text.value != '') {
			sio.emit('chat', chat_send_text.value);
			chat_send_text.value = '';
		}
	};

});

// processes some data on disconnection of the socket connection
sio.on('disconnect', function () {
	// ...
});

// shows an error message when connection to the socket failed
sio.on('connect_failed', function () {
	alert('can\'t connect to to server ...');
});

// shows an error message when reconnection to the socket failed
sio.on('reconnect_failed', function () {
	alert('can\'t reconnect to to server ...');
});

// shows an error message when error appears on the socket connection
sio.socket.on('error', function (reason){
  alert('socket error: ', reason);
});

// initiate method after establishing the socket connection
// sets the broadcast intervall
// loads the initiating data for the VCI for showing the Shapes which are saved on the server database
sio.on('init', function (data) {
	console.log(data);
	$('#vci-chat-text').innerHTML = '';
	for (var i in data.chat){
		var chat_message = data.chat[i];

		var content = '<div>';

		if (chat_message.name != last_chat_origin)
			content = content + '<strong>' + chat_message.name + '</strong>'
		last_chat_origin = chat_message.name;

		content = content + '</div>[' + chat_message.ts + ']<div>' + chat_message.msg + '</div>';

		try{
				$(content).appendTo("#vci-chat-text").hide().fadeIn(2000);
		} catch (e) {
			$('#vci-chat-text').append(content);
		}
	}

	$("#vci-chat-text").scrollTop($("#vci-chat-text")[0].scrollHeight);

	if (data.vciData) {
		VCI.Stage.initialVciDataLoading(data.vciData);
	}

    $.mobile.loading('hide');
	broadcaster = setInterval(broadcast, 100);
});

// gets the chat data from the server and processes it to the chat control
sio.on('chat', function (data) {
	var content = '<div>';

	if (data.name != last_chat_origin)
		content = content + '<strong>' + data.name + '</strong>'
	last_chat_origin = data.name;

	content = content + '</div>[' + data.ts + ']<div>' + data.msg + '</div>';

		last_chat_origin = data.name;

	try{
			$(content).appendTo("#vci-chat-text").hide().fadeIn(2000);
	} catch (e) {
		$('#vci-chat-text').append(content);
	}

	$("#vci-chat-text").scrollTop($("#vci-chat-text")[0].scrollHeight);
});

// receives the snapshot sent from the server
sio.on('ss', function (data) {
	console.log(data);
	for (user in data)
		var snapShot = data[user];
		if (user != sio.socket.sessionid) {
			for (layerID in snapShot) {
				var layer = snapShot[layerID];
				var layerObjects;

				if (layer.o) {
					layerObjects = VCI.Common.clone(layer.o);
					delete layer['o'];
				}

				var layerObjectChanges = VCI.Common.clone(layer);

				if (Object.keys(layerObjectChanges).length > 0) {
					var clientSnapshot = VCI.Stage.getClientSnapshot();
					layerObjectChanges.objectServerId = layerID;
					clientSnapshot.pushObjectChangesList(layerObjectChanges);
				}

				if (layerObjects && layerObjects != null) {
					var clientLayer = VCI.Stage.getLayer(layerID);

					if (clientLayer && clientLayer != null) {
						var clientSnapshot = clientLayer.getClientSnapshot();
						clientSnapshot.pushObjectChangesList(layerObjects);
					}
				} 
			}
		} else {
			// applying server generated id to the creator of the object/layer
			for (layerID in snapShot) {
				var uniqueLayerKey = layerID;
				var tempLayer = snapShot[uniqueLayerKey];

				if (tempLayer.command && tempLayer.command == 'Create') {
					var stagePersistance = VCI.Stage.getPersistance();
					stagePersistance.resetId(tempLayer.originID, uniqueLayerKey);

					if (VCI.Stage.getSelectedLayerId() == tempLayer.originID) {
						VCI.Stage.setSelectedLayerId(uniqueLayerKey)
					}

					VCI.UI.LayerManager.initLayerManagementControl();
				}

				if (tempLayer.o) {
					var tempLayerObjects = tempLayer.o;
					for (uniqueObjectKey in tempLayerObjects) {
						var tempObject = tempLayerObjects[uniqueObjectKey];
						if (tempObject.command && tempObject.command == 'Create') {
							var clientVciLayer = VCI.Stage.getLayer(uniqueLayerKey);
							var layerPersistance = clientVciLayer.getPersistance();
							layerPersistance.resetId(tempObject.originID, uniqueObjectKey);
						}
					}
				}
			}
		}
});

// emits the snapshot broadcast method
var snapShot = new SnapShot();
var broadcast = function() {
	snapShot.take( function(ss) {sio.emit('ss', ss)} );
};

// responsible object for broadcasting the data to the client
var broadcaster;