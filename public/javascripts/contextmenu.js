function radial_menu() {
	document.getElementById("radial_container").innerHTML = "<ul class='list'>\
			<li class='item'>	<div class='my_class'>  0  </div>	</li>\
			<li class='item'>	<div class='my_class'>  1  </div>	</li>\
			<li class='item'>	<div class='my_class'>  2  </div>	</li>\
			<li class='item'>	<div class='my_class'>  3  </div>	</li>\
			<li class='item'>	<div class='my_class'>  4  </div>	</li>\
			<li class='item'>	<div class='my_class'>  5  </div>	</li>\
			<li class='item'>	<div class='my_class'>  6  </div>	</li>\
			<li class='item'>	<div class='my_class'>  7  </div>	</li>\
			<li class='item'>	<div class='my_class'>  8  </div>	</li>\
			<li class='item'>	<div class='my_class'>  9  </div>	</li>\
		</ul>";

	jQuery("#radial_container").radmenu({
		listClass: 'list', // the list class to look within for items
		itemClass: 'item', // the items - NOTE: the HTML inside the item is copied into the menu item
		radius: 100, // radius in pixels
		animSpeed:400, // animation speed in millis
		centerX: 30, // the center x axis offset
		centerY: 100, // the center y axis offset
		selectEvent: "click", // the select event (click)
		onSelect: function($selected){ // show what is returned 
			alert("you clicked on .. " + $selected.index());
		},
		angleOffset: 0 // in degrees
	});
}