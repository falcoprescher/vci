// Visual Collaboration Interface
// Empty VCI Object which stores all working area components of the whiteboard
var VCI = {};

// Layers to synchronize with server
VCI.layers = {};

// Selected Tool as String
VCI.selectedTool = '';

// Selected Points as Array of Integers
VCI.points = [];

// Initialisation of the Stage and the UI
VCI.init = function() {
    var stage = VCI.Stage.init();
    //var testLayer = stage.addLayer();
    //testLayer.processObjectChangesForCreatingLayer();
    //VCI.Stage.setSelectedLayerId(testLayer.getId());

    // Initialize Controls
    VCI.UI.init();
    $.mobile.loading('hide');
    // dirty hack to equalize layerIds
    // var stagePersistance = stage.getPersistance();
    // stagePersistance.resetId(testLayer.getId(), 2);
    // stage.setSelectedLayerId(2);
};

// VCI Common Function
VCI.Common = {
    // Function for Deep Cloning Objects 
    clone: function (obj) {
        if (null == obj || "object" != typeof obj) return obj;
        var copy = obj.constructor();
        for (var attr in obj) {
            if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
        }
        return copy;
    },
    // Function for extending a object with anothers object properties over prototyping
    extend: function(c1, c2) {
        for(var key in c2.prototype) {
            if(!( key in c1.prototype)) {
                c1.prototype[key] = c2.prototype[key];
            }
        }
    }
};

// Contains generic methods to attach to objects
VCI.Mixin = {};

// Mixins for shape objects
VCI.Mixin.Shape = {
    // Initializes ObjectChanges for the server when selectedLayer exists
    processObjectChangesForCreatingShape: function (shapeClientId, shapeObjectType, layerId, vciShapeConfig) {
        var vciLayer = VCI.Stage.getLayer(layerId);

        var objectChanges = new VCI.DataLayering.ObjectChanges();
        objectChanges.objectClientId = shapeClientId;
        objectChanges.command = "Create";
        objectChanges.objectAttributes = VCI.Common.clone(vciShapeConfig);
        objectChanges.objectType = shapeObjectType;
        objectChanges.objectServerId = undefined;
        objectChanges.layerId = layerId;

        var serverSnapshot = vciLayer.getServerSnapshot();
        serverSnapshot.pushObjectChanges(objectChanges);
        serverSnapshot.processToServer();
    },
    // calculates 2D controlpoints for beziercurves
    // returns it in this form [cx1,cy1,cx2,cy2,cx3,cy3,cx4,cy4,...]
    calculate2DControlPointsForBezierCurve: function (xpoints, ypoints) {
        if (xpoints.length == ypoints.length) {
            cx = this.calculate1DControlPointsForBezierCurve(xpoints);
            cx1 = cx[0];
            cx2 = cx[1];
            cy = this.calculate1DControlPointsForBezierCurve(ypoints);
            cy1 = cy[0];
            cy2 = cy[1];
            cxy = [];
            var calculatedControlPointsLength = cx1.length;
            for (var i = 0; i < calculatedControlPointsLength; i++) {
                cxy.push(cx1[i], cy1[i], cx2[i], cy2[i]);
            }

            return cxy;
        }
    },
    // computes control points with predefined points for x or y --> points is a array of x or y points */ through splines and solution of Ax=b through Thomas algorithm
    // returns array for c1 and c2
    calculate1DControlPointsForBezierCurve: function (points) {
        var p1 = [];
        var p2 = [];
        var n = points.length - 1;
        
        var a = [];
        var b = [];
        var c = [];
        var r = [];
        
        a[0] = 0;
        b[0] = 2;
        c[0] = 1;
        r[0] = points[0] + 2 * points[1];
        
        for (var i = 1; i < n - 1; i++)
        {
            a[i] = 1;
            b[i] = 4;
            c[i] = 1;
            r[i] = 4 * points[i] + 2 * points[i+1];
        }
                
        a[n-1] = 2;
        b[n-1] = 7;
        c[n-1] = 0;
        r[n-1] = 8 * points[n-1] + points[n];
        
        for (var i = 1; i < n; i++)
        {
            var m = a[i] / b[i-1];
            b[i] = b[i] - m * c[i - 1];
            r[i] = r[i] - m * r[i-1];
        }
     
        p1[n-1] = r[n-1] / b[n-1];
        for (var i = n - 2; i >= 0; --i) {
            p1[i] = (r[i] - c[i] * p1[i+1]) / b[i];
        }

        for (var i = 0; i < n - 1; i++) {
            p2[i] = 2 * points[i+1] - p1[i+1];
        }
        
        p2[n-1] = 0.5*(points[n] + p1[n-1]);
        
        return [p1,p2];
    },
    // douglasPeucker Algorithm for reducing Points on a Array of Points (= Line)
    // points should be like this: points[0] = x1; points[1] = y1; points[2] = x2; points[3] = y2; ....
    // tolerance = epsilon
    douglasPeucker: function( points, tolerance ) {
        if ( points.length <= 4 ) {
            return points;
        }

        var startAndEndPoint = [ points[0], points[points.length - 1] ];
        var returnPoints = [];
        // make line from start to end

        var maxDistance = 0;
        var maxDistanceIndex = [];
        var returnPoints;

        for( var i = 1; i <= points.length - 2; i += 2 ) {
            var distance = VCI.Line.distanceToPoint( startAndEndPoint, [ points[i], points[i+1] ] );
            if ( distance > maxDistance ) {    
                maxDistance = distance;
                maxDistanceIndex = i + 1;
            }   
        }

        // check if the max distance is greater than our tollerance allows
        var tempPoints = [ points[maxDistanceIndex - 1], points[maxDistanceIndex] ];
        if ( maxDistance >= tolerance ) {
            // include this point in the output
            returnPoints = returnPoints.concat( VCI.Mixin.Shape.douglasPeucker( points.slice( 0, maxDistanceIndex ), tolerance ) );

            returnPoints = returnPoints.concat( VCI.Mixin.Shape.douglasPeucker( points.slice( maxDistanceIndex, points.length ), tolerance ) );
        } else {
            // ditching this point
            returnPoints = [ points[0], points[1] ];
        }

        return returnPoints;
    }
}

// Mixins for container objects
VCI.Mixin.Container = {

}

// Standard configurations for creating a shape
VCI.Standard = {
    stage: {
        container: 'stage',
        width: 1200,
        height: 800
    },
    rectangle: {
        fill: 'green',
        stroke: 'black',
        strokeWidth: 0,
        draggable: false
    },
    circle: {
        fill: 'red',
        stroke: 'black',
        strokeWidth: 4,
        draggable: false
    },
    line: {
        stroke: "green",
        strokeWidth: 4,
        lineJoin: "round",
        draggable: false
    },
    text: {
        text: 'Simple Text',
        fontSize: 30,
        fontFamily: 'Calibri',
        textFill: 'blue',
        draggable: false
    }
};

// Object for initialize toolchanges
VCI.Selection = {
    // moves the selective layer according to the toolname
    toolChoise: function(tool){
        VCI.selectedTool = tool;
        var layers = VCI.Stage.getLayers();

        switch (tool){
            case 'select':
                for (layerKey in layers) {
                    layers[layerKey].getSelectiveArea().moveToBottom();
                }
                break;
            default:
                for (layerKey in layers) {
                    layers[layerKey].getSelectiveArea().moveToTop();
                }
                break;
        }

        VCI.Stage.draw();
    },
    // unsubscribes all previous eventListeners and subscribe the actual eventListeners according to toolChosen
    switchTools: function (toolChosen, eventListenerCollection) {
        var eventListenerCollection = eventListenerCollection;
        var selectedLayer = VCI.Stage.getSelectedLayer();

        if (selectedLayer && selectedLayer != null) {
            selectedLayer.offAll();
            for (event in eventListenerCollection) {
                selectedLayer.on(event, eventListenerCollection[event]);
            }

            VCI.Selection.toolChoise(toolChosen);
        }
    },
    // tool for selecting objects
    select: function () {
        VCI.selectedTool = 'select';
        VCI.Selection.switchTools(VCI.selectedTool, {});
        VCI.UI.DrawingTools.hideAllProperties();
    },
    // tool for doing nothing
    none: function () {
        VCI.selectedTool = 'none';
        VCI.Selection.switchTools(VCI.selectedTool, {});
        VCI.UI.DrawingTools.hideAllProperties();
    },
    // tool for using the pen
    pen: function () {
        VCI.selectedTool = 'pen';
        var eventListenerCollection = VCI.Pen.eventListenerCollection;
        VCI.Selection.switchTools(VCI.selectedTool, eventListenerCollection);
        VCI.UI.DrawingTools.showPenProperties();
    },
    // tool for creating a rectangle
    rectangle: function () {
        VCI.selectedTool = 'rectangle';
        var eventListenerCollection = VCI.Rectangle.eventListenerCollection;
        VCI.Selection.switchTools(VCI.selectedTool, eventListenerCollection);
        VCI.UI.DrawingTools.showRectangleProperties();
    },
    // tool for creating a circle
    circle: function () {
        VCI.selectedTool = 'circle';
        var eventListenerCollection = VCI.Circle.eventListenerCollection;
        VCI.Selection.switchTools(VCI.selectedTool, eventListenerCollection);
        VCI.UI.DrawingTools.showCircleProperties();
    },
    // tool for creating a line
    line: function () {
        VCI.selectedTool = 'line';
        var eventListenerCollection = VCI.Line.eventListenerCollection;
        VCI.Selection.switchTools(VCI.selectedTool, eventListenerCollection);
        VCI.UI.DrawingTools.showLineProperties();
    },
    // tool for creatin a text
    text: function () {
        VCI.selectedTool = 'text';
        var eventListenerCollection = VCI.Text.eventListenerCollection;
        VCI.Selection.switchTools(VCI.selectedTool, eventListenerCollection);
        VCI.UI.DrawingTools.showTextProperties();
    },
};

// VCI Stage
VCI.Stage = (function () {
    var privateInstance;
    var privateLayerSelectedId = 0;

    var privatePen = {};
    var privateStage;
    var privateLastMouseMove;
    var privateObjectType = "Stage";
    var privateTempData = {};
    var privateObjectAttributes;

    var privateServerSnapshot;
    var privateClientSnapshot;
    var privatePersistance;

    return {
        // Initiates the Stage for drawing and returns itself (singleton)
        init: function (config) {
            var tempConfig = config;

            if ( !tempConfig ) {
                tempConfig = VCI.Standard.stage;
            }

            if ( !privateInstance ) {
                privateInstance = new Kinetic.Stage(tempConfig);
                privateObjectAttributes = tempConfig;

                privateServerSnapshot = new VCI.DataLayering.ServerSnapshot(this);
                privateClientSnapshot = new VCI.DataLayering.ClientSnapshot(this);
                privatePersistance = new VCI.DataLayering.Persistance(this);
            }

            return this;
        },
        // Getting predefined ObjectType "Stage"
        getObjectType: function () {
            return privateObjectType;
        },
        // Gets all Attributes of the Stage
        getObjectAttributes: function () {
            return privateAttributes;
        },
        // Gets the underlying Stage (Kinetic.Stage)
        getStage: function () {
            return privateInstance;
        },
        // Adds layer to the current Stage
        addLayer: function (config) {
            var vciLayer = new VCI.Layer();
            var kineticLayer = vciLayer.init(config);

            privatePersistance.addObject(vciLayer);
            privateInstance.add(kineticLayer);
            
            //this.setSelectedLayerId(vciLayer.getId());
            vciLayer.draw();

            return vciLayer;
        },
        // Removes an object from the current Stage over the id of the object
        remove: function (id) {
            privatePersistance.remove(id);
        },
        // Deletes exclusively a Layer over a specific ID
        deleteLayer: function (id) {
            var o = privatePersistance.getObject(id);

            if (o && o != null && o.getObjectType() == "Layer") {
                privatePersistance.remove(id);

                var privateChild = privateInstance.get('#' + id);

                if (privateChild && privateChild.length > 0) {
                    privateChild[0].remove();
                }
            }
        },
        // Gets the ID of the current selected Layer
        getSelectedLayerId: function () {
            return privateLayerSelectedId;
        },
        // Sets the ID for the current selected Layer
        setSelectedLayerId: function (layerId) {
            var o = privatePersistance.getObject(layerId);

            if (o && o != null && o.getObjectType() == "Layer") {
                privateLayerSelectedId = layerId;

                var layers = this.getLayers();

                for (layerKey in layers) {
                    var selectiveArea = layers[layerKey].getSelectiveArea();
                    selectiveArea.setAttrs( { visible: false } );
                }

                var selectiveArea = this.getSelectedLayer().getSelectiveArea();
                selectiveArea.setAttrs( { visible: true } );

                this.draw();
            }
        },
        // Gets the selected Layer
        getSelectedLayer: function () {
            return privatePersistance.getObject(privateLayerSelectedId);
        },
        // Gets all Layer Objects as a Colleciton
        getLayers: function () {
            var objectList = privatePersistance.getObjects();
            var layerList = {};

            for (objectListKey in objectList) {
                var objectType = objectList[objectListKey].getObjectType();
                var objectId = objectList[objectListKey].getId();

                if (objectType == "Layer") {
                    layerList[objectId] = objectList[objectListKey];
                }
            }

            return layerList;
        },
        // Gets the layer by its ID
        getLayer: function (layerId) {
            var o = privatePersistance.getObject(layerId);

            if (o && o != null && o.getObjectType() == "Layer") {
                return o;
            }

            return null;
        },
        // Gets the Order of all zIndexes of the Layers in the Stage
        // Object Contains layerId, zIndex and ObjectType "Layer"
        getLayerZIndexOrder: function () {
            var zIndexObjectIdCollection = {};
            var layers = this.getLayers();
            
            for (layerId in layers) {
                var layer = layers[layerId];

                var objectChanges = new VCI.DataLayering.ObjectChanges();
                objectChanges.objectClientId = layerId;
                objectChanges.command = undefined;
                objectChanges.zIndex = layer.getZIndex();
                objectChanges.objectAttributes = undefined;
                objectChanges.objectType = "Layer";
                objectChanges.objectServerId = undefined;
                objectChanges.layerId = undefined;

                zIndexObjectIdCollection[objectChanges.zIndex] = objectChanges;
            }

            return zIndexObjectIdCollection;
        },
        // Gets the Top zIndex which was allocated
        getTopZIndex: function () {
            var layers = this.getLayers();
            var maxZIndex = 0;

            for (layerId in layers) {
                var layer = layers[layerId];
                var zIndex = layer.getZIndex();
                maxZIndex = maxZIndex > zIndex ? maxZIndex : zIndex;
            }

            return maxZIndex;
        },
        // Gets the current mouseposition on the stage
        getMousePosition: function (evt) {
            return privateInstance.getMousePosition(evt);
        },
        // Clears the stage
        clear: function () {
            privateInstance.clear();
        },
        // Draws current object changes on the stage
        draw: function () {
            privateInstance.draw();
        },
        // Resets the attribute of the stage to default values
        reset: function () {
            privateInstance.reset();
        },
        // Creates a Image of the current Stage
        toImage: function (config, callback) {
            return privateInstance.toImage(config, callback);
        },
        // Gets all childobjects of the Kinetic.Stage
        getChildren: function () {
            return privateInstance.getChildren();
        },
        // gets the pen object of the stage
        getPen: function () {
            return privatePen;
        },
        // Sets the pen object of the stage
        setPen: function (pen) {
            privatePen = pen;
        },
        // Gets the last mousemove on the stage as time
        getLastMouseMove: function () {
            return privateLastMouseMove;
        },
        // Sets the last mousemove on the stage as time
        setLastMouseMove: function (mouseMove) {
            privateLastMouseMove = mouseMove;
        },
        // Gets temporarily data on the stage
        getTempData: function () {
            return privateTempData;
        },
        // Resets the temporarily data to a empty object
        resetTempData: function () {
            privateTempData = {};
            return privateTempData;
        },
        // Gets the persistant object of the stage
        getPersistance: function () {
            return privatePersistance;
        },
        // Gets the serversnapshot object of the stage
        getServerSnapshot: function () {
            return privateServerSnapshot;
        },
        // gets the clientsnapshot object of the stage
        getClientSnapshot: function () {
            return privateClientSnapshot;
        },
        // draws all persistant object by adding them on the Kinetic.Stage from the persistant object
        drawPersistantObjects: function () {
            var layerPersistantObjects = privatePersistance.getObjects();

            for (objectKey in layerPersistantObjects) {
                var tempObject = layerPersistantObjects[objectKey];

                if (tempObject && tempObject != null) {
                    var tempObjectType = tempObject.getObjectType();

                    switch (tempObjectType) {
                        case 'Layer':
                            privateInstance.add(tempObject.init());
                            break;
                        default:
                            break; 
                    }
                }
            }

            this.draw();
        },
        // Processes Object Changes which are sent from the server
        processObjectChangesListFrame: function (stageObjectChange) {
            // here its possible to draw with animations or to draw in an instance
            // also this method should add/create/edit/delete objects here befor drawing
            var tempObjectChange = stageObjectChange;
            var command = tempObjectChange.command;
            var objectServerId = tempObjectChange.objectServerId;
            var objectType = tempObjectChange.objectType;
            switch (command) {
                case 'Create':
                    switch (objectType) {
                        case 'Layer':
                            var tempLayer = this.addLayer(tempObjectChange.objectAttributes);
                            var oldId = tempLayer.getId();
                            privatePersistance.resetId(oldId, objectServerId);
                            this.setSelectedLayerId(privateLayerSelectedId);

                            if (tempObjectChange.zIndex) {
                                tempLayer.setZIndex(tempObjectChange.zIndex);
                            }

                            break;
                        default:
                            break;
                    }
                    break;
                case 'Delete':
                    switch (objectType) {
                        case 'Layer':
                            this.deleteLayer(objectServerId);
                            VCI.UI.LayerManager.initLayerManagementControl();
                            break;
                        default:
                            this.remove(objectServerId);
                            break;
                    }
                    break;
                case 'Begin':
                case 'End':
                default:
                    switch (objectType) {
                        case 'Layer':
                            var tempLayer = this.getLayer(objectServerId);

                            if (tempObjectChange.objectAttributes) {
                                tempLayer.setObjectAttributes(tempObjectChange.objectAttributes);
                            }

                            if (tempObjectChange.zIndex) {
                                tempLayer.setZIndex(tempObjectChange.zIndex);
                            }

                            VCI.UI.LayerManager.initLayerManagementControl();

                            break;
                        default:
                            break;
                    }

                    // A Animation can be created here and applied to the container
                    break;
            }

            this.draw();
        },
        // Method for the initialization of the vciData on its startup
        initialVciDataLoading : function (vciData) {
            // empty all data in the stage
            privateInstance.clear();
            privateInstance.removeChildren();
            var zIndexOrderForLayers = [];

            for (layerUniqueKey in vciData) {
                var initLayer = vciData[layerUniqueKey];
                var vciLayer = new VCI.Layer();
                var kineticLayer = vciLayer.init(initLayer.objectAttributes);
                privatePersistance.addObjectWithPreDefinedId(vciLayer, layerUniqueKey);
                privateInstance.add(kineticLayer);

                zIndexOrderForLayers[initLayer.zIndex] = layerUniqueKey;

                var layerObjects = initLayer.o;
                if (layerObjects) {
                    var layerPersistance = vciLayer.getPersistance();

                    for (layerObjectUniqueKey in layerObjects) {
                        var layerObject = layerObjects[layerObjectUniqueKey];
                        switch (layerObject.objectType) {
                            case 'Rectangle':
                                var tempRectangle = new VCI.Rectangle(layerObject.objectAttributes);
                                layerPersistance.addObjectWithPreDefinedId(tempRectangle, layerObjectUniqueKey);
                                break;
                            case 'Circle':
                                var tempCircle = new VCI.Circle(layerObject.objectAttributes);
                                layerPersistance.addObjectWithPreDefinedId(tempCircle, layerObjectUniqueKey);
                                break;
                            case 'Line':
                                var tempLine = new VCI.Line(layerObject.objectAttributes);
                                layerPersistance.addObjectWithPreDefinedId(tempLine, layerObjectUniqueKey);
                                break;
                            case 'BezierCurve':
                                var tempLine = new VCI.BezierCurve(layerObject.objectAttributes);
                                layerPersistance.addObjectWithPreDefinedId(tempLine, layerObjectUniqueKey);
                                break;
                            case 'Text':
                                var tempText = new VCI.Text(layerObject.objectAttributes);
                                layerPersistance.addObjectWithPreDefinedId(tempText, layerObjectUniqueKey);
                            default:
                                break;
                        }
                    }

                    vciLayer.drawPersistantObjects();
                }
            }

            // set zIndex for Layers here
            var zIndexCounter = 0;
            for (zIndexOrderKey in zIndexOrderForLayers) {
                var layerId = zIndexOrderForLayers[zIndexOrderKey];

                if (layerId) {
                    var layer = VCI.Stage.getLayer(layerId);
                    layer.setZIndex(zIndexCounter);
                    zIndexCounter++;
                }
            }
        },
        applyAttributeOnEachShape: function (layerId, attributeName, value) {
            var tempLayer = VCI.Stage.getLayer(layerId);

            if (!tempLayer || tempLayer == null) {
                return;
            }

            var persistantShapes = tempLayer.getShapes();

            for (persistantShapeKey in persistantShapes) {
                var persistantShape = persistantShapes[persistantShapeKey];
                var attribute = {};
                attribute[attributeName] = value;
                persistantShape.setAttrs( attribute );
            }
        }
    }
})();

// Defines the Container for Shapes
VCI.Layer = function (config) {
    var privateInstance;
    var privateEventListenedCollection = {};
    var privateObjectType = "Layer";
    var privateObjectAttributes = {};
    var privateSkipAnimation = true;

    // reacts to events --> so its selective area
    var privateSelectiveArea = {};
    var privateServerSnapshot;
    var privateClientSnapshot;
    var privatePersistance;
    var privateAnimation;

    return {
        // initializes the layer object
        init: function (config) {
            if (!privateInstance || privateInstance == null) {
                privateInstance = new Kinetic.Layer(config);
                privateSelectiveArea = new Kinetic.Shape({
                    id: -1,
                    drawFunc: function(context) {
                        context.beginPath();
                        context.rect(0, 0, VCI.Standard.stage.width, VCI.Standard.stage.height);
                        this.fill(context);
                    },
                    fill: '#000',
                    listening: true,
                    opacity: 0.01
                });

                privateInstance.add(privateSelectiveArea);

                privateAnimation = new VCI.Animation(privateInstance); 
                privateObjectAttributes = config;

                privateServerSnapshot = new VCI.DataLayering.ServerSnapshot(this);
                privateClientSnapshot = new VCI.DataLayering.ClientSnapshot(this);
                privatePersistance = new VCI.DataLayering.Persistance(this);
            }

            return privateInstance;
        },
        // gets the predefined object type of the object ("Layer")
        getObjectType: function () {
            return privateObjectType;
        },
        // gets all current object attributes of the layer
        getObjectAttributes: function () {
            return privateInstance.getAttrs();
        },
        getId: function () {
            return privateInstance.getId();
        },
        // should just be used from persistance
        setId: function (id) {
            return privateInstance.setId(id);
        },
        // gets the zIndex of the Layer
        getZIndex: function () {
            return privateInstance.getZIndex();
        },
        // sets the zIndex of the Layer
        setZIndex: function (zIndex) {
            privateInstance.setZIndex(zIndex);
        },
        // gets the Name of the Layer
        getName: function () {
            return privateInstance.getName();
        },
        // sets the Name of the Layer
        setName: function (layerName) {
            privateInstance.setName(layerName);
        },
        // applies new configurations the the current layer
        setObjectAttributes: function (config) {
            if (!privateObjectAttributes || privateObjectAttributes == null) {
                privateObjectAttributes = {};
            }

            for (configKey in config) {
                privateObjectAttributes[configKey] = config;
            }

            privateInstance.setAttrs(config);
        },
        // adds a groupcontainer through a collection of shapeids
        addGroup: function (shapeIds) {
            // VCI.Group will be added later
            var group = new Kinetic.Group();

            for (shapeId in shapeIds){
                var shape = privateObjects[shapeId];
                group.add(shape);
            }

            privateInstance.add(group);

            this.draw();

            return privatePersistance.addObject(group);
        },
        // adds a shape to the layer
        addShape: function(shape) {
            privateInstance.add(shape);

            VCI.Selection.toolChoise(VCI.selectedTool);

            this.draw();

            return privatePersistance.addObject(shape);
        },
        // gets all shapes of the persistant object of a layer
        getShapes: function () {
            var persistantObjects = privatePersistance.getObjects();
            var persistantShapes = {};

            for (persistantObjectKey in persistantObjects) {
                var persistantObject = persistantObjects[persistantObjectKey];
                var objectId = persistantObject.getId();
                var objectType = persistantObject.getObjectType();

                switch (objectType) {
                    case 'Rectangle':
                    case 'Circle':
                    case 'Line':
                    case 'BezierCurve':
                    case 'Text':
                    case 'Shape':
                        persistantShapes[objectId] = persistantObject;
                        break;
                    default:
                        break;
                }
            }

            return persistantShapes;
        },
        // removes a object of the layer by its id
        remove: function (id) {
            var actualObject = privatePersistance.getObject(id);

            if (actualObject) {
                actualObject.remove();
            }

            privatePersistance.remove(id);
        },
        // clears the layer of all drawings
        clear: function () {
            privateInstance.clear();
        },
        // draws current object changes made to the layer
        draw: function () {
            privateInstance.draw();
        },
        // gets a eventListenerCollection which is applied to the selectiveArea of the Layer
        getEventListenedCollection: function () {
            return privateEventListenedCollection;
        },
        // allows to subscribe a eventlistener through a eventSelector
        on: function (eventSelector, eventListener) {
            privateEventListenedCollection[eventSelector] = eventListener;
            privateSelectiveArea.on(eventSelector, eventListener);
        },
        // allows to unsubscribe all eventListeners through a eventSelector
        off: function (eventSelector) {
            if (privateEventListenedCollection[eventSelector] && privateEventListenedCollection[eventSelector] != null)
            {
                privateSelectiveArea.off(eventSelector);
                delete privateEventListenedCollection[eventSelector];
            }
        },
        // allows to unsubscribe all eventListeners to any event
        offAll: function() {
            for (privateEventListenedKey in privateEventListenedCollection) {
                this.off(privateEventListenedKey);
                delete privateEventListenedCollection[privateEventListenedKey];
            }
        },
        // allows to subscribe a eventlistener through a eventSelector to a layer by its id
        onFromLayer: function (layerId) {
            var tempLayer = VCI.Stage.getLayer[layerId];

            if (tempLayer && tempLayer != null) {
                var tempEventListened = tempLayer.getEventListenedCollection();
                for (privateEventListenedKey in privateEventListenedCollection) {
                    this.on(privateEventListenedKey, privateEventListenedCollection[privateEventListenedKey]);
                }
            }
        },
        // gets the selectiveArea object of the layer
        getSelectiveArea: function () {
            return privateSelectiveArea;
        },
        // gets the persistant object of the layer
        getPersistance: function () {
            return privatePersistance;
        },
        // gets the animation object of the layer
        getAnimation: function () {
            return privateAnimation;
        },
        // gets the serversnapshot object of the layer 
        getServerSnapshot: function () {
            return privateServerSnapshot;
        },
        // gets the clientsnapshot of the layer
        getClientSnapshot: function () {
            return privateClientSnapshot;
        },
        // draws all shapes of the persistant of the layer to the layer by adding it on the Kinetic.Layer
        drawPersistantObjects: function () {
            var layerPersistantObjects = privatePersistance.getObjects();

            for (objectKey in layerPersistantObjects) {
                var tempObject = layerPersistantObjects[objectKey];

                if (tempObject && tempObject != null) {
                    var tempObjectType = tempObject.getObjectType();

                    switch (tempObjectType) {
                        case 'Rectangle':
                        case 'Circle':
                        case 'Line':
                        case 'BezierCurve':
                        case 'Text':
                        case 'Shape':
                            privateInstance.add(tempObject);
                            break;
                        default:
                            break; 
                    }
                }
            }

            this.draw();
        },
        // adds a shape to the persistant object of the layer and resets its id to a new id
        addingShapeAndResetId: function  (object, newId) {
            var tempObject = this.addShape(object);
            var oldId = tempObject.getId();
            privatePersistance.resetId(oldId, newId);
        },
        // processes object changes which are sent by the server
        processObjectChangesListFrame: function (objectChangesListFrame) {
            // here its possible to draw with animations or to draw in an instance
            // also this method should add/create/edit/delete objects here befor drawing
            var tempoclf = VCI.Common.clone(objectChangesListFrame);
            for (tempoclfKey in tempoclf) {
                var tempObjectChanges = tempoclf[tempoclfKey];
                var command = tempObjectChanges.command;
                var objectServerId = tempoclfKey;
                switch (command) {
                    case 'Create':
                        var objectType = tempObjectChanges.objectType;
                        var objectAttributes = tempObjectChanges.objectAttributes;
                        switch (objectType) {
                            case 'Rectangle':
                                var tempRectangle = new VCI.Rectangle(objectAttributes);
                                this.addingShapeAndResetId(tempRectangle, objectServerId);
                                break;
                            case 'Circle':
                                var tempCircle = new VCI.Circle(objectAttributes);
                                this.addingShapeAndResetId(tempCircle, objectServerId);
                                break;
                            case 'Line':
                                var tempLine = new VCI.Line(objectAttributes);
                                this.addingShapeAndResetId(tempLine, objectServerId);
                                break;
                            case 'BezierCurve':
                                var tempLine = new VCI.BezierCurve(objectAttributes);
                                this.addingShapeAndResetId(tempLine, objectServerId);
                                break;
                            case 'Text':
                                var tempText = new VCI.Text(objectAttributes);
                                this.addingShapeAndResetId(tempText, objectServerId);
                            default:
                                break;
                        }
                    break;
                    case 'Delete':
                        this.remove(objectServerId);
                        break;
                    case 'Begin':
                    case 'End':
                    default:
                        // A Animation can be created here and applied to the container
                        // Or the Animation can be skipped
                        if (privateSkipAnimation) {
                            var persistentObject = privatePersistance.getObject(objectServerId);
                            //should use objectServerId later, if its assigned from server

                            if (persistentObject.setAttrs) {
                                persistentObject.setAttrs(tempObjectChanges.objectAttributes);
                            } else {
                                for (objectChangesKey in tempObjectChanges) {
                                    persistentObject[objectChangesKey] = tempObjectChanges[objectChangesKey];
                                }
                            }
                        } else {
                            // Do some Animation
                        }

                        break;
                }
            }

            this.draw();
        },
        // builds a object change containing data of the creation of an layer object for the server
        processObjectChangesForCreatingLayer: function () {
            var layerId = this.getId();
            var layerName = this.getName();

            var objectChanges = new VCI.DataLayering.ObjectChanges();
            objectChanges.objectClientId = layerId;
            objectChanges.command = "Create";
            objectChanges.zIndex = this.getZIndex();
            objectChanges.objectAttributes = { name: layerName };
            objectChanges.objectType = this.getObjectType();
            objectChanges.objectServerId = -1;
            objectChanges.layerId = undefined;

            var serverSnapshot = VCI.Stage.getServerSnapshot();
            serverSnapshot.pushObjectChanges( objectChanges );
            serverSnapshot.processToServer();
        }
    }
};

// name space for structuring the stored data to a container object like layer and stage
VCI.DataLayering = {};

// contains all objects of an container for maintainance
VCI.DataLayering.Persistance = function (vciContainer) {
    var privateObjects = {};
    var privateVciContainer = vciContainer;

    // sets the id of an object
    function privateSetId (object, id) {
        if ( !object.setId ) {
            object['id'] = id;
        } else {
            object.setId(id);
        }
    }

    // gets the id of an object
    function privateGetId (object) {
       if (object != null) {
            if (object.getId) {
                return object.getId();
            } else if (object[id]) {
                return object[id];
            }
        }

        return -1;
    }

    // generates a random client id
    function privateGetRandomId () {
        return Math.round( $.now() * ( Math.random() + 10 ));
    }

    return {
        // adding a object to the persistant object with a randomId
        addObject: function (object) {
            var tempId = privateGetRandomId();
            return this.addObjectWithPreDefinedId(object, tempId)
        },
        // adding a object to the persistant object with a predefinedId
        addObjectWithPreDefinedId : function (object, preDefinedId) {
            privateObjects[preDefinedId] = object;
            privateSetId(object, preDefinedId);
            return object;
        },
        // removes a object by its id
        remove: function (id) {
            if (privateObjects[id] && privateObjects[id] != null){
                delete privateObjects[id];
            }
        },
        // removes all objects in the persistant object
        removeAll: function () {
            privateVciContainer.removeChildren();
            delete privateObjects;
            privateObjects = {};
        },
        // gets a object by its id
        getObject: function (id) {
            return privateObjects[id];
        },
        // gets all objects as a collection
        getObjects: function () {
            return privateObjects;
        },
        // sets a object by a id to the persistant object
        setObject: function (id, object) {
            this.remove(id);
            privateObjects[id] = object;
            privateSetId(object, id);
        },
        // sets a collection of objects in the persistant object
        setObjects: function (objects) {
            if (objects && objects != null) {
                for (objectKey in objects) {
                    var o = objects[objectKey];
                    var id = this.privateGetId(o);

                    setObject(id, o);    
                }
            }
        },
        // resets a object with a old Id to a new Id
        resetId: function (oldId, newId) {
            var tempObject = this.getObject(oldId);
            if (tempObject && tempObject != null) {
                privateSetId(tempObject, newId);
                this.setObject(newId, tempObject);
                delete privateObjects[oldId];
            }
        }
    }
}

// Defines the overall structure of a Object Change object for the server
VCI.DataLayering.ObjectChanges = function () {
    this.objectClientId;
    this.command;
    this.objectAttributes;
    this.objectServerId;
    this.objectType;
    this.layerId;
}

//Contains a structure for processing objects to a object which the server should store
VCI.DataLayering.ServerSnapshot = function (vciContainer) {
    var privateObjectChangesList = [];
    var privateVciContainer = vciContainer;

    var privateProcessToServer;

    // clears all temporarily object changes
    function cleanUpObjectChanges () {
        delete privateObjectChangesList;
        privateObjectChangesList = [];
    }

    if (privateVciContainer && privateVciContainer != null) {
        var objectType = privateVciContainer.getObjectType(); 
        switch (objectType) {
            case 'Stage':
                privateProcessToServer = function () {
                    var snapshotLayers = {};

                    for (objectChangesKey in privateObjectChangesList) {
                        var objectChanges = privateObjectChangesList[objectChangesKey];
                        
                        if (!snapshotLayers[objectChanges.objectClientId]) {
                            snapshotLayers[objectChanges.objectClientId] = {};
                        }

                        for (objectChangeKey in objectChanges) {
                            snapshotLayers[objectChanges.objectClientId][objectChangeKey] = objectChanges[objectChangeKey];
                        }
                    }

                    VCI.layers = snapshotLayers;
                    cleanUpObjectChanges();
                };
                break;
            case 'Layer':
                privateProcessToServer = function () {
                    var snapshotLayers = {};

                    for (objectChangesKey in privateObjectChangesList) {
                        var objectChanges = privateObjectChangesList[objectChangesKey];
                        
                        if (!snapshotLayers[objectChanges.layerId]) {
                            snapshotLayers[objectChanges.layerId] = {};
                            snapshotLayers[objectChanges.layerId].objects = {};
                        }

                        var objectId = objectChanges.objectClientId;
                        snapshotLayers[objectChanges.layerId].objects[objectId] = {};
                        for (objectChangeKey in objectChanges) {
                            snapshotLayers[objectChanges.layerId].objects[objectId][objectChangeKey] = objectChanges[objectChangeKey];
                        }
                    }

                    VCI.layers = snapshotLayers;
                    cleanUpObjectChanges();
                };
                break;
            case 'Group':
                privateProcessToServer = function () {};
                break;
            default:
                privateProcessToServer = function () {};
        }
    }

    return {
        // gets the object changes collection of the serversnapshot object
        getObjectChanges: function () {
            return privateObjectChangesList;
        },
        // pushes a object changes object to the object changes collection
        pushObjectChanges: function (objectChanges) {
            privateObjectChangesList.push(objectChanges);
        },
        // pushes a object changes list to the object changes collection
        pushObjectChangesList: function (objectChangesList) {
            for (objectChangesKey in objectChangesList) {
                this.pushObjectChanges(objectChangesList[objectChangesKey]);
            }
        },
        // initiates the method for processing the objectchanges to the server through a implementation from the vciContainer itself
        processToServer: privateProcessToServer
    }
}

//Contains a structure for processing objects to a object which the client should store
VCI.DataLayering.ClientSnapshot = function (vciContainer) {
    var privateObjectChangesListFramesBuffer = [];
    var privateMaxFrameBufferLength = 3;
    var privateProcessWaitingTime = 100;
    var privateVciContainer = vciContainer;

    var privatePushObjectChangesList;

    if (privateVciContainer && privateVciContainer != null) {
        var objectType = privateVciContainer.getObjectType(); 
        switch (objectType) {
            case 'Stage':
                privatePushObjectChangesList = function (stageObjectChange) {
                    privateObjectChangesListFramesBuffer.push(VCI.Common.clone(stageObjectChange));
                    this.processToClient();
                };
                break;
            case 'Layer':
                privatePushObjectChangesList = function (objectChangesList) {
                    if ( privateObjectChangesListFramesBuffer.length < privateMaxFrameBufferLength) {
                        privateObjectChangesListFramesBuffer.push(VCI.Common.clone(objectChangesList));
                        setTimeout(
                            function () { 
                                vciContainer.getClientSnapshot().processToClient(); 
                            }, privateProcessWaitingTime);
                    } else {
                        for (privateObjectChangesFramesBufferIndex in privateObjectChangesListFramesBuffer) {
                            this.processToClient();
                        }

                        privateObjectChangesListFramesBuffer.push(VCI.Common.clone(objectChangesList));
                        this.processToClient();
                    }
                };
                break;
            case 'Group':
                privateProcessToServer = function () {};
                break;
            default:
                privateProcessToServer = function () {};
        }
    }

    return {
        // pushes a objectChangesList to ObjectChangesListFramesBuffer --> each List is one frame for the Buffer
        pushObjectChangesList: privatePushObjectChangesList,
        // sets the ObjectChangesListFramesBuffer maximum length (when the max is reached, all frames will be flushed in an instance)
        setMaxFrameBufferLength: function (bufferLength) {
            privateMaxFrameBufferLength = bufferLength;
        },
        // gets the maximum length of the ObjectChangesListFramesBuffer
        getMaxFrameBufferLength: function () {
            return privateMaxFrameBufferLength;
        },
        // processes the ObjectChangesList on the ObjectChangesListFramesBuffer to the Client by the implementation of the specified container
        processToClient: function () {
            privateVciContainer.processObjectChangesListFrame(privateObjectChangesListFramesBuffer.shift());
        }
    }
}

// The Animation Object for initiating interpolated object changes to a persistant object on the client
VCI.Animation = function (kineticLayer) {
    var privateProcessWaitingTime = 100;
    var privateAnimationTimeout = 1000;
    var privateKineticLayer = kineticLayer;
    var privateCurrentAnimationObjectIndex = 0;
    var privateAnimationObjects = {};

    // pushes a animation for a object to the animationStack
    function pushAnimationObject (animationObject) {
        privateAnimationObjects[privateCurrentAnimationObjectIndex] = animationObject;
        privateCurrentAnimationObjectIndex++;
    }

    return {
        // functionWithCallbackFrame should have its head declared like this: function (frame) {}
        // adds a animationFunction to the animationStack
        addAnimation: function (functionWithCallbackFrame) {
            var tempConfig = {
                func: functionWithCallbackFrame,
                node: privateKineticLayer
            };
            var tempAnimation = new Kinetic.Animation(tempConfig);

            var animationObject = {
                animation: tempAnimation,
                config: tempConfig,
                hasStarted: false
            };

            privateAnimationObjects.pushAnimationObject(animationObject);
        },
        // starts to process the animationstack
        startDrawingObjectChanges: function () {
            for (animationObjectKey in privateAnimationObjects) {
                var animationObject = privateAnimationObjects[animationObjectKey];

                if (!animationObject.hasStarted)
                {
                    animationObject.hasStarted = true;
                    animationObject.animation.start();
                    setTimeout(function () {
                        privateAnimationObjects[animationObjectKey].animation.stop();
                        delete privateAnimationObjects[animationObjectKey];
                    }, privateAnimationTimeout);
                }
            }
        },
        // stops to process the animationstack
        stopDrawingObjectChanges: function () {
            for (animationObjectKey in privateAnimationObjects) {
                var animationObject = privateAnimationObjects[animationObject];

                if (animationObject.hasStarted)
                {
                    animationObject.hasStarted = false;
                    animationObject.animation.stop();
                }
            }
        },
        // flushes all animation of the animationstack and draws objectchanges in an instant without interpolation
        flushDrawingObjectChanges: function () {
            this.stopDrawingChanges();

            for (animationObjectKey in privateAnimationObjects) {
                var animationObject = privateAnimationObjects[animationObjectKey];

                if (animationObject.config != null && animationObject.config.func) {
                    animationobject.config.func( { time: privateAnimationTimeout } );
                }

                delete privateAnimationObjects[animationObjectKey];
            }

            privateAnimationObjects = {};
            privateCurrentAnimationObjectIndex = 0;

            privateKineticLayer.draw();
        },
    }
}

// Defines a structure for storing a rectangle shape
VCI.Rectangle = function (config) {
    Kinetic.Rect.call(this, config);
    
    // get the object type of the shape "Rectangle"
    this.getObjectType = function () {
        return "Rectangle";
    }

    // gets all attributes of the rectangle
    this.getObjectAttributes = function () {
        return this.getAttrs();
    };
    // calculates all vertices of the rectangle
    // the object structure for the corners are [upperLeft, upperRight, bottomLeft, bottomRight]
    this.getVertices = function () {
        var x = this.getAttribute("x");
        var y = this.getAttribute("y");
        var width = this.getAttribute("width");
        var height = this.getAttribute("height");

        var upperLeft = { x: x, y: y };
        var upperRight = { x: x + width, y: y };;
        var bottomLeft = { x: x, y: y + height };;
        var bottomRight = { x: x + width, y: y + height };

        var cornerArray = [upperLeft, upperRight, bottomLeft, bottomRight];
        return cornerArray;
    };

    //this.anchor = {};
    //this.anchor.upperLeft = new VCI.UI.Anchor(_layer, upperLeft.x, upperLeft.y);
    //this.anchor.upperRight = new VCI.UI.Anchor(_layer, upperRight.x, upperRight.y);
    //this.anchor.bottomLeft = new VCI.UI.Anchor(_layer, bottomLeft.x, bottomLeft.y);
    //this.anchor.bottomRight = new VCI.UI.Anchor(_layer, bottomRight.x, bottomRight.y);
};

VCI.Rectangle.getConfigs = function () {
    var tempConfigs = VCI.UI.DrawingTools.getConfigs();
    var realConfigs = { 
        fill: tempConfigs.fill ? tempConfigs.fill : '#FFFFFF',
        stroke: tempConfigs.stroke,
        opacity: tempConfigs.opacity,
        strokeWidth: tempConfigs.strokeWidth,
    };

    if (tempConfigs.stroke == undefined) {
        delete realConfigs.stroke;
        delete realConfigs.strokeWidth;
    }

    return realConfigs;
};

VCI.Rectangle.eventListenerCollection = {
    // initiates the creation of the shape and sends its information to the server
    // starts to capture the first point of the rectangle
    'mousedown.startPoint': function(evt) {
        var vciStage = VCI.Stage;
        var mousePosition = vciStage.getMousePosition();
        var vciLayer = vciStage.getSelectedLayer();

        if (vciLayer && vciLayer != null) {
            var tempData = VCI.Stage.resetTempData();
            tempData['x'] = mousePosition.x;
            tempData['y'] = mousePosition.y;

            tempData['isActive'] = true;
            vciLayer.on('mousemove.captureMousePoints', VCI.Rectangle.eventListenerCollection['mousemove.captureMousePoints']);
        }
    },
    'touchstart.startPoint': this['mousedown.startPoint'],
    // draws the rectangle by the second point through its current mouselocation on mousemove
    'mousemove.captureMousePoints': function(evt) {
        var vciStage = VCI.Stage;
        var vciLayer = vciStage.getSelectedLayer();
        var tempData = vciStage.getTempData();
        
        if (vciLayer && vciLayer != null) {
            var mousePosition = vciStage.getMousePosition();
            // isActive --> if mousedown was pressed
            if (tempData['isActive'] && tempData['isActive'] != false) {
                if (!tempData['object'] || tempData['object'] == null) {

                    var tempData = VCI.Stage.getTempData();
                    tempData['width'] = Math.max(Math.abs(mousePosition.x - tempData['x']));
                    tempData['height'] = Math.max(Math.abs(mousePosition.y - tempData['y']));

                    var config = VCI.Common.clone(VCI.Rectangle.getConfigs());
                    config['x'] = VCI.Common.clone(tempData['x']);
                    config['y'] = VCI.Common.clone(tempData['y']);
                    config['width'] = VCI.Common.clone(tempData['width']);
                    config['height'] = VCI.Common.clone(tempData['hight']);

                    var vciShape = vciLayer.addShape(new VCI.Rectangle(config));
                    tempData['object'] = vciShape;
                    tempData['config'] = config;
                } else {
                    tempData['width'] = Math.max(Math.abs(mousePosition.x - tempData['x']));
                    tempData['height'] = Math.max(Math.abs(mousePosition.y - tempData['y']));
                    tempData['object'].setAttrs( { width: tempData['width'], height: tempData['height'] });
                }
            } else {
                vciLayer.off('mousemove.captureMousePoints');
                vciLayer.off('touchstart.startPoint');
            }

            vciLayer.draw();
        }
    },
    'touchmove.captureMousePoints': this['mousemove.captureMousePoints'],
    // ends the allocation of the second point of the rectangle and sends the rectangle information to the server
    'mouseup.endPoint': function(evt) {
        var vciStage = VCI.Stage;
        var vciLayer = vciStage.getSelectedLayer();
        var tempData = vciStage.getTempData();
        
        if (vciLayer && vciLayer != null) {
            if (tempData['isActive'] && tempData['isActive'] != false) {
                var vciShape = tempData['object'];
                if (vciShape && vciShape != null) {
                    var mousePosition = vciStage.getMousePosition(evt);
                    tempData['width'] = Math.max(Math.abs(mousePosition.x - tempData['x']));
                    tempData['height'] = Math.max(Math.abs(mousePosition.y - tempData['y']));
                    tempData['object'].setAttrs( { width: tempData['width'], height: tempData['height'] });

                    tempData['config']['width'] = tempData['width'];
                    tempData['config']['height'] = tempData['height'];
                    VCI.Mixin.Shape.processObjectChangesForCreatingShape(vciShape.getId(), vciShape.getObjectType(), vciStage.getSelectedLayerId(), VCI.Common.clone(tempData['config']));
                }
            }

            vciLayer.draw();
            vciLayer.off('mousemove.captureMousePoints');
            vciLayer.off('touchmove.captureMousePoints');
            VCI.Stage.resetTempData();
        }
    },
    'touchend.endPoint': this['mouseup.endPoint']
};

VCI.Rectangle.prototype = Object.create(Kinetic.Rect.prototype);
VCI.Rectangle.prototype.constructor = VCI.Rectangle;

// VCI Circle
VCI.Circle = function(config){
    Kinetic.Circle.call(this, config);

    // get the object type of the shape "Circle"
    this.getObjectType = function () {
        return "Circle";
    }

    // gets all attributes of the circle
    this.getObjectAttributes = function () {
        return this.getAttrs();
    };
};

VCI.Circle.getConfigs = function () {
    var tempConfigs = VCI.UI.DrawingTools.getConfigs();
    var realConfigs = { 
        fill: tempConfigs.fill ? tempConfigs.fill : '#FFFFFF',
        stroke: tempConfigs.stroke,
        opacity: tempConfigs.opacity,
        strokeWidth: tempConfigs.strokeWidth,
    };

    if (tempConfigs.stroke == undefined) {
        delete realConfigs.stroke;
        delete realConfigs.strokeWidth;
    }

    return realConfigs;
};

VCI.Circle.eventListenerCollection = {
    // initiates the creation of the shape and sends its information to the server
    // starts to capture the first point of the circle
    'mousedown.startPoint': function(evt) {
        var vciStage = VCI.Stage;
        var mousePosition = vciStage.getMousePosition();
        var vciLayer = vciStage.getSelectedLayer();

        if (vciLayer && vciLayer != null) {
            var tempData = VCI.Stage.resetTempData();
            tempData['x'] = mousePosition.x;
            tempData['y'] = mousePosition.y;

            tempData['isActive'] = true;
            vciLayer.on('mousemove.captureMousePoints', VCI.Circle.eventListenerCollection['mousemove.captureMousePoints']);
        }
    },
    'touchstart.startPoint': this['mousedown.startPoint'],
    // draws the circle by the second point through its current mouselocation on mousemove
    'mousemove.captureMousePoints': function(evt) {
        var vciStage = VCI.Stage;
        var vciLayer = vciStage.getSelectedLayer();
        var tempData = vciStage.getTempData();
        
        if (vciLayer && vciLayer != null) {
            var mousePosition = vciStage.getMousePosition();
            // isActive --> if mousedown was pressed
            if (tempData['isActive'] && tempData['isActive'] != false) {
                if (!tempData['object'] || tempData['object'] == null) {

                    var tempData = VCI.Stage.getTempData();
                    tempData['radius'] = Math.max(Math.abs(mousePosition.x - tempData['x']), Math.abs(mousePosition.y - tempData['y']));

                    var config = VCI.Common.clone(VCI.Circle.getConfigs());
                    config['x'] = VCI.Common.clone(tempData['x']);
                    config['y'] = VCI.Common.clone(tempData['y']);
                    config['radius'] = VCI.Common.clone(tempData['radius']);

                    var vciShape = vciLayer.addShape(new VCI.Circle(config));
                    tempData['object'] = vciShape;
                    tempData['config'] = config;
                } else {
                    tempData['radius'] = Math.max(Math.abs(mousePosition.x - tempData['x']), Math.abs(mousePosition.y - tempData['y']));
                    tempData['object'].setAttrs( { radius: tempData['radius'] });
                }
            } else {
                vciLayer.off('mousemove.captureMousePoints');
                vciLayer.off('touchstart.startPoint');
            }

            vciLayer.draw();
        }
    },
    'touchmove.captureMousePoints': this['mousemove.captureMousePoints'],
    // ends the allocation of the second point of the circle and sends the line information to the server
    'mouseup.endPoint': function(evt) {
        var vciStage = VCI.Stage;
        var vciLayer = vciStage.getSelectedLayer();
        var tempData = vciStage.getTempData();
        
        if (vciLayer && vciLayer != null) {
            if (tempData['isActive'] && tempData['isActive'] != false) {
                var vciShape = tempData['object'];
                if (vciShape && vciShape != null) {
                    var mousePosition = vciStage.getMousePosition(evt);
                    tempData['radius'] = Math.max(Math.abs(mousePosition.x - tempData['x']), Math.abs(mousePosition.y - tempData['y']));
                    vciShape.setAttrs( { radius: tempData['radius'] } );

                    tempData['config']['radius'] = tempData['radius'];
                    VCI.Mixin.Shape.processObjectChangesForCreatingShape(vciShape.getId(), vciShape.getObjectType(), vciStage.getSelectedLayerId(), VCI.Common.clone(tempData['config']));
                }
            }

            vciLayer.draw();
            vciLayer.off('mousemove.captureMousePoints');
            vciLayer.off('touchmove.captureMousePoints');
            VCI.Stage.resetTempData();
        }
    },
    'touchend.endPoint': this['mouseup.endPoint']
};

VCI.Circle.prototype = Object.create(Kinetic.Circle.prototype);
VCI.Circle.prototype.constructor = VCI.Circle;

// VCI Line
VCI.Line = function(config){
    Kinetic.Line.call(this, config);
    
    // get the object type of the shape "Line"
    this.getObjectType = function () {
        return "Line";
    }

    // gets all attributes of the circle
    this.getObjectAttributes = function () {
        return this.getAttrs();
    };
};

VCI.Line.getConfigs = function () {
    var tempConfigs = VCI.UI.DrawingTools.getConfigs();

    return { 
        stroke: tempConfigs.stroke ? tempConfigs.stroke : '#FFFFFF',
        opacity: tempConfigs.opacity,
        strokeWidth: tempConfigs.strokeWidth,
    };
};

// pointsOfLine[0] = x1
// pointsOfLine[1] = y1
// pointsOfLine[2] = x2
// pointsOfLine[3] = y2
// destPoint[0] = x
// destPoint[1] = y
VCI.Line.distanceToPoint = function(pointsOfLine, destPoint) {
    // slope
    var m = ( pointsOfLine[3] - pointsOfLine[1] ) / ( pointsOfLine[2] - pointsOfLine[0] );
    // y offset
    var b = pointsOfLine[1] - ( m * pointsOfLine[0] );
    var d = [];
    // distance to the linear equation
    d.push( Math.abs( destPoint.y - ( m * destPoint[0] ) - b ) / Math.sqrt( Math.pow( m, 2 ) + 1 ) );
    // distance to p1
    d.push( Math.sqrt( Math.pow( ( destPoint[0] - pointsOfLine[0] ), 2 ) + Math.pow( ( destPoint[1] - pointsOfLine[1] ), 2 ) ) );
    // distance to p2
    d.push( Math.sqrt( Math.pow( ( destPoint[0] - pointsOfLine[2] ), 2 ) + Math.pow( ( destPoint[1] - pointsOfLine[3] ), 2 ) ) );
    // return the smallest distance
    return d.sort( function( a, b ) {
        return ( a - b ); //causes an array to be sorted numerically and ascending
    } )[0];
};

VCI.Line.eventListenerCollection = {
    // starts to capture the first point of the line
    'mousedown.startPoint': function(evt) {
        var vciStage = VCI.Stage;
        var mousePosition = vciStage.getMousePosition();
        var vciLayer = vciStage.getSelectedLayer();

        if (vciLayer && vciLayer != null) {
            var tempData = VCI.Stage.resetTempData();
            tempData['points'] = [];
            tempData['points'][0] = mousePosition.x;
            tempData['points'][1] = mousePosition.y;

            tempData['isActive'] = true;
            vciLayer.on('mousemove.captureMousePoints', VCI.Line.eventListenerCollection['mousemove.captureMousePoints']);
        }
    },
    'touchstart.startPoint': this['mousedown.startPoint'],
    // draws the line by the second point through its current mouselocation on mousemove
    'mousemove.captureMousePoints': function(evt) {
        var vciStage = VCI.Stage;
        var vciLayer = vciStage.getSelectedLayer();
        var tempData = vciStage.getTempData();
        
        if (vciLayer && vciLayer != null) {
            var mousePosition = vciStage.getMousePosition();
            // isActive --> if mousedown was pressed
            if (tempData['isActive'] && tempData['isActive'] != false) {
                if (!tempData['object'] || tempData['object'] == null) {

                    var tempData = VCI.Stage.getTempData();
                    tempData['points'][2] = mousePosition.x;
                    tempData['points'][3] = mousePosition.y;

                    var config = VCI.Common.clone(VCI.Line.getConfigs());
                    config['points'] = VCI.Common.clone(tempData['points']);

                    var vciShape = vciLayer.addShape(new VCI.Line(config));
                    tempData['object'] = vciShape;
                    tempData['config'] = config;

                    tempData['pointAttributes'] = {};
                } else {
                    tempData['points'][2] = mousePosition.x;
                    tempData['points'][3] = mousePosition.y;
                    tempData['object'].setAttrs( { points: tempData['points'] });
                }
            } else {
                vciLayer.off('mousemove.captureMousePoints');
                vciLayer.off('touchstart.startPoint');
            }

            vciLayer.draw();
        }
    },
    'touchmove.captureMousePoints': this['mousemove.captureMousePoints'],
    // ends the allocation of the second point of the line and sends the line information to the server
    'mouseup.endPoint': function(evt) {
        var vciStage = VCI.Stage;
        var vciLayer = vciStage.getSelectedLayer();
        var tempData = vciStage.getTempData();
        
        if (vciLayer && vciLayer != null) {
            if (tempData['isActive'] && tempData['isActive'] != false) {
                var vciShape = tempData['object'];
                if (vciShape && vciShape != null) {
                    var mousePosition = vciStage.getMousePosition(evt);
                    tempData['points'][2] = mousePosition.x;
                    tempData['points'][3] = mousePosition.y;
                    vciShape.setAttrs( { points: tempData['points'] } );

                    tempData['config']['points'] = tempData['points'];
                    VCI.Mixin.Shape.processObjectChangesForCreatingShape(vciShape.getId(), vciShape.getObjectType(), vciStage.getSelectedLayerId(), VCI.Common.clone(tempData['config']));
                }
            }

            vciLayer.draw();
            vciLayer.off('mousemove.captureMousePoints');
            vciLayer.off('touchmove.captureMousePoints');
            VCI.Stage.resetTempData();
        }
    },
    'touchend.endPoint': this['mouseup.endPoint']
};

VCI.Line.prototype = Object.create(Kinetic.Line.prototype);
VCI.Line.prototype.constructor = VCI.Line;

// VCI Brush 
// Defines a structure for adding custom shapes through paths
VCI.Brush = function (config) {
    return {
        getShape: function () {

        }
    }
};

// Defines a structure of a shape for storing beziercurves
VCI.BezierCurve = function(config) {
    this._initBezierCurve(config);

    // get the object type of the shape "BezierCurve"
    this.getObjectType = function () {
        return "BezierCurve";
    };

    // get the current Attributes of the BezierCurve
    this.getObjectAttributes = function () {
        return this.getAttrs();
    };
};

VCI.BezierCurve.prototype = {
    // initiates the beziercurve with its properties
    _initBezierCurve: function(config) {
        this.setDefaultAttrs({
            linepoints: [],
            controlpoints: [],
            lineCap: 'butt',
            detectionType: 'pixel'
        });

        this.shapeType = "BezierCurve";
        config.drawFunc = this.drawFunc;
        // call super constructor
        Kinetic.Shape.call(this, config);
    },
    // defines the draw function to render the line
    drawFunc: function(context) {
        var tempLinepoints = Kinetic.Type._getPoints(this.attrs.linepoints);
        var tempControlpoints = Kinetic.Type._getPoints(this.attrs.controlpoints);

        context.beginPath();
        context.moveTo(tempLinepoints[0].x, tempLinepoints[0].y);

        if (tempControlpoints.length < 4) {
            for(var n = 1; n < tempLinepoints.length; n++) {
                var px = tempLinepoints[n].x;
                var py = tempLinepoints[n].y;
                // draw normal line
                context.lineTo(px, py);
            }
        } else {
            var controlpointCounter = 0;
            for(var n = 1; n < tempLinepoints.length; n++) {
                var cx1 = tempControlpoints[controlpointCounter].x;
                var cy1 = tempControlpoints[controlpointCounter].y;
                var cx2 = tempControlpoints[controlpointCounter + 1].x;
                var cy2 = tempControlpoints[controlpointCounter + 1].y;
                var px = tempLinepoints[n].x;
                var py = tempLinepoints[n].y;
                // draw bezier curve
                context.bezierCurveTo(cx1, cy1, cx2, cy2, px, py);
                controlpointCounter += 2;
            }
        }

        this.stroke(context);
    },
    // sets the linepoints and controlpoints to the beziercurve object
    setPoints: function(linepoints, controlpoints) {
        // linepoints --> storing the points of the actual line
        this.setAttr('linepoints', Kinetic.Type._getPoints(linepoints));
        // controlpoints --> stroing the controlpoints for the actual line
        this.setAttr('controlpoints', Kinetic.Type._getPoints(controlpoints));
    }
};
VCI.Common.extend(VCI.BezierCurve, Kinetic.Shape);

// add getters setters for the linepoints and controlpoints attributes
Kinetic.Node.addGetters(VCI.BezierCurve, ['linepoints']);
Kinetic.Node.addGetters(VCI.BezierCurve, ['controlpoints']);

// VCI Text
VCI.Text = function (config) {
    Kinetic.Text.call(this, config);

    this.getObjectType = function () {
        return "Text";
    };

    this.getObjectAttributes = function () {
        return this.getAttrs();
    };
}

VCI.Text.prototype = Object.create(Kinetic.Text.prototype);
VCI.Text.prototype.constructor = VCI.Text;

VCI.Text.getConfigs = function () {
    var tempConfigs = VCI.UI.DrawingTools.getConfigs();

    return { 
        textFill: tempConfigs.fill ? tempConfigs.fill : '#FFFFFF',
        text: tempConfigs.text,
        fontSize: tempConfigs.fontSize,
        opacity: tempConfigs.opacity,
    };
};

VCI.Text.eventListenerCollection = {
    // initiates the creation of the shape and sends its information to the server
    'dblclick.createShape': function (evt) {
        var vciStage = VCI.Stage;
        var vciLayer = vciStage.getSelectedLayer();

        if (vciLayer && vciLayer != null) {

            var mousePosition = vciStage.getMousePosition();

            var config = VCI.Common.clone(VCI.Text.getConfigs());
            config.x = mousePosition.x;
            config.y = mousePosition.y;

            var vciShape = new VCI.Text(config);
            vciLayer.addShape(vciShape);

            vciLayer.draw();

            VCI.Mixin.Shape.processObjectChangesForCreatingShape(vciShape.getId(), vciShape.getObjectType(), vciStage.getSelectedLayerId(), config);
        }
    },
    'touchmove.createShape': this['dblclick.createShape']
};

// Defines a object for creating a complex line by moving the mousecursor
VCI.Pen = function () {
    // current recorded points
    this.points = [];
    // shows the activity of pen
    this.isActive = false;
};

VCI.Pen.getConfigs = function () {
    var tempConfigs = VCI.UI.DrawingTools.getConfigs();

    return { 
        stroke: tempConfigs.stroke ? tempConfigs.stroke : '#FFFFFF',
        opacity: tempConfigs.opacity,
        strokeWidth: tempConfigs.strokeWidth,
    };
};

VCI.Pen.eventListenerCollection = {
    // records the first point of the pen
    'mousedown.startPoint': function(evt){
        var vciStage = VCI.Stage;
        var mousePosition = vciStage.getMousePosition();
        var vciLayer = vciStage.getSelectedLayer();

        if (vciLayer && vciLayer != null) {
            var tempData = vciStage.getTempData();
            tempData['lastMouseMove'] = $.now();
            tempData['pen'] = new VCI.Pen();
            tempData['xpoints'] = [];
            tempData['ypoints'] = [];
            tempData['pen'].points.push(mousePosition.x, mousePosition.y);
            tempData['xpoints'].push(mousePosition.x);
            tempData['ypoints'].push(mousePosition.y);
            tempData['pen'].isActive = true;
            vciLayer.on('mousemove.captureMousePoints', VCI.Pen.eventListenerCollection['mousemove.captureMousePoints']);
        } else {
            vciLayer.off('mousemove.captureMousePoints');
            vciLayer.off('touchstart.startPoint');
        }
    },
    'touchstart.startPoint': this['mousedown.startPoint'],
    // creates and renders all following points which are created by the mousemovement
    'mousemove.captureMousePoints': function (evt) {
        var stage = VCI.Stage;
        var tempData = stage.getTempData();
        var pen = tempData['pen'];
        var mousePosition = stage.getMousePosition();
        var vciLayer = stage.getSelectedLayer();
        if (vciLayer && vciLayer != null) {
            if (pen && pen != null && pen.isActive) {
                if (!tempData['object'] || tempData['object'] == null) {
                    // get a point in 30-msec intervall
                    var lastMouseMove = tempData['lastMouseMove'];
                    pen.points.push(mousePosition.x, mousePosition.y);
                    tempData['xpoints'].push(mousePosition.x);
                    tempData['ypoints'].push(mousePosition.y);
                    lastMouseMove = $.now();

                    var controlPointsXY = VCI.Mixin.Shape.calculate2DControlPointsForBezierCurve(tempData['xpoints'], tempData['ypoints']);
                    var config = VCI.Common.clone(VCI.Pen.getConfigs());
                    tempData['config'] = config;
                    config['linepoints'] = pen.points;
                    config['controlpoints'] = controlPointsXY;

                    var vciShape = vciLayer.addShape(new VCI.BezierCurve(config));
                    tempData['object'] = vciShape;
                } else {
                    // get a point in 30-msec intervall
                    var lastMouseMove = tempData['lastMouseMove'];
                    if ($.now() - tempData['lastMouseMove'] > 50){
                        pen.points.push(mousePosition.x, mousePosition.y);
                        tempData['xpoints'].push(mousePosition.x);
                        tempData['ypoints'].push(mousePosition.y);
                        lastMouseMove = $.now();

                        var controlPointsXY = VCI.Mixin.Shape.calculate2DControlPointsForBezierCurve(tempData['xpoints'], tempData['ypoints']);
                        tempData['object'].setPoints(pen.points, controlPointsXY);
                    }
                }
            } else {
                vciLayer.off('mousemove.captureMousePoints');
            }
        
            vciLayer.draw();
        }
    },
    'touchmove.captureMousePoints': this['mousemove.captureMousePoints'],
    // stores the last point to be made by the point and creates the beziercurve to the stage
    // sends its information to the server
    'mouseup.endPoint': function (evt) {
        var stage = VCI.Stage;
        var tempData = stage.getTempData();
        var pen = tempData['pen'];
        var mousePosition = stage.getMousePosition();
        var vciLayer = stage.getSelectedLayer();
        if (vciLayer && vciLayer != null) {
            if (pen && pen != null && pen.isActive) {
                var vciShape = tempData['object'];
                if (vciShape && vciShape != null) {
                    pen.points.push(mousePosition.x, mousePosition.y);
                    tempData['xpoints'].push(mousePosition.x);
                    tempData['ypoints'].push(mousePosition.y);
                    lastMouseMove = $.now();

                    var controlPointsXY = VCI.Mixin.Shape.calculate2DControlPointsForBezierCurve(tempData['xpoints'], tempData['ypoints']);
                    tempData['object'].setPoints(pen.points, controlPointsXY);
                    tempData['config']['controlpoints'] = controlPointsXY;
                    tempData['config']['linepoints'] = pen.points;

                    VCI.Mixin.Shape.processObjectChangesForCreatingShape(vciShape.getId(), vciShape.getObjectType(), stage.getSelectedLayerId(), VCI.Common.clone(tempData['config']));
                }

                pen.isActive = false;
            }

            vciLayer.draw();
        }

        vciLayer.off('mousemove.captureMousePoints');
        vciLayer.off('touchmove.captureMousePoints');

        stage.resetTempData();
    },
    'touchend.endPoint': this['mouseup.endPoint']
}

// Defines Methods and Objects for the user interface of the VCI
VCI.UI = {
    // Method for initializing all controls on the user interface
    init : function () {
        // header
        $('#vci-header-home-btn').bind("click", function () {});
        // href="/user/vcis"
        $('#vci-header-chat-btn').bind("click", function () { VCI.UI.Chat.toggleChat(); });
        $('#vci-header-menu-btn').bind("click", function () { VCI.UI.Menu.toggleMenu(); });
        // content

        // stage
        $('#vci-content-stage').disableSelection();

        // menu-navbar
        $('#vci-content-navbar-drawingtools-btn').bind("click", function () { VCI.UI.Menu.showDrawingTools(); VCI.UI.ObjectSelection.exitObjectSelection(); });
        $('#vci-content-navbar-layermanagement-btn').bind("click", function () { VCI.UI.Menu.showLayerManagement(); VCI.UI.LayerManager.initLayerManagementControl(); VCI.UI.ObjectSelection.exitObjectSelection(); });
        $('#vci-content-navbar-objectselection-btn').bind("click", function () { VCI.UI.Menu.showObjectSelection(); VCI.UI.ObjectSelection.initiateObjectSelection(); });

        // content-drawingtools
        VCI.UI.DrawingTools.init();
        $('#vci-content-drawingtools-none-cb').bind("click", function () { VCI.Selection.none(); });
        $('#vci-content-drawingtools-rectangle-cb').bind("click", function () { VCI.Selection.rectangle(); });
        $('#vci-content-drawingtools-circle-cb').bind("click", function () { VCI.Selection.circle(); });
        $('#vci-content-drawingtools-line-cb').bind("click", function () { VCI.Selection.line(); });
        $('#vci-content-drawingtools-pen-cb').bind("click", function () { VCI.Selection.pen(); });
        $('#vci-content-drawingtools-text-cb').bind("click", function () { VCI.Selection.text(); });

        $('#vci-content-drawingtools-properties-color-fillcolor-tb').bind("click", function () { VCI.UI.DrawingTools.activateFillColor(); });
        $('#vci-content-drawingtools-properties-color-strokecolor-tb').bind("click", function () { VCI.UI.DrawingTools.activateStrokeColor(); });

        // content-layermanagement
        $('#vci-content-layermanagement-add-btn').bind("click", function () { VCI.UI.LayerManager.addLayer(); });
        $('#vci-content-layermanagement-delete-btn').bind("click", function () { VCI.UI.LayerManager.deleteSelectedLayer(); });
        $('#vci-content-layermanagement-layerName-tb').bind("keydown", function (e) { VCI.UI.LayerManager.changeNameForSelectedLayer(e); });
        $('#vci-content-layermanagement-layerVisibility-cb').bind("click", function () { VCI.UI.LayerManager.toggleVisibilityOfCurrentLayer(); });
    }
};

VCI.UI.DrawingTools = (function () {
    var mouseDown = false;
    var fillColorActive = false;
    var strokeColorActive = false;

    function publicInit () {
        var colorPickerStage = new Kinetic.Stage ({
            container: "vci-content-drawingtools-properties-color-colorpicker",
            width: 256,
            height: 256
        });

        var colorPickerLayer = new Kinetic.Layer()

        var colorPickerObject = new Image();
        colorPickerObject.onload = function () {
            var colorPickerImage = new Kinetic.Image({
                x: 0,
                y: 0,
                image: colorPickerObject,
                width: 256,
                height: 256
            });

            colorPickerLayer.add(colorPickerImage);
            colorPickerStage.add(colorPickerLayer);

            colorPickerLayer.draw();

            colorPickerImage.on("mousedown", function () { mouseDownListener(); } );
            colorPickerImage.on("mouseup", function () { mouseUpListener(); } );
            colorPickerImage.on("mousemove", function (evt) { mouseMoveListener(evt, colorPickerImage, colorPickerStage); });
        }

        colorPickerObject.src = VCI.Images.colorpicker;
    }

    function mouseDownListener() {
        mouseDown = true;
    }

    function mouseUpListener() {
        mouseDown = false;
    }

    function mouseMoveListener(evt, shape, stage) {
        if (mouseDown)
        {
            var mousePosition = stage.getMousePosition(evt);
            var mousePosX = Math.floor(mousePosition.x);
            var mousePosY = Math.floor(mousePosition.y);

            if (mousePosX > shape.getX() &&
                mousePosX < shape.getWidth() &&
                mousePosY > shape.getY() &&
                mousePosY < shape.getHeight()) {
                var imageData = shape.getContext().getImageData(shape.getX(), shape.getY(), shape.getWidth(), shape.getHeight());
                var rawData = imageData.data;
                var red = rawData[((shape.getWidth() * mousePosY) + mousePosX) * 4];
                var green = rawData[((shape.getWidth() * mousePosY) + mousePosX) * 4 + 1];
                var blue = rawData[((shape.getWidth() * mousePosY) + mousePosX) * 4 + 2];
                var rgbColor = "rgb(" + red + "," + green + "," + blue + ")";

                if (fillColorActive) {
                    $('#vci-content-drawingtools-properties-color-fillcolor-tb').val(rgbColor);
                }

                if (strokeColorActive) {
                    $('#vci-content-drawingtools-properties-color-strokecolor-tb').val(rgbColor);
                }
            }
        }
    }

    function publicGetConfigs() {
        var fillColor = $('#vci-content-drawingtools-properties-color-fillcolor-tb').val();
        var strokeColor = $('#vci-content-drawingtools-properties-color-strokecolor-tb').val();
        var transparency = $('#vci-content-drawingtools-properties-color-transparency-sd').val();

        var strokewidth = $('#vci-content-drawingtools-properties-proportions-strokewidth-sd').val();

        var fontSizeValue = $('#vci-content-drawingtools-properties-proportions-fontsize-sd').val();
        var textValue = $('#vci-content-drawingtools-properties-text-textcontext-tb').val();

        var configObject = {
            fill: fillColor != "" ? fillColor : undefined,
            stroke: strokeColor != "" ? strokeColor : undefined,
            opacity: (transparency / 100),
            strokeWidth: strokewidth,
            fontSize: fontSizeValue, 
            text: textValue
        }

        return configObject;
    }

    function publicHideAllProperties() {
        var colorPickerValue = $('#vci-content-drawingtools-properties-color-colorpicker-container');
        var fillColorValue = $('#vci-content-drawingtools-properties-color-fillcolor-container');
        var strokeColorValue = $('#vci-content-drawingtools-properties-color-strokecolor-container');

        var transparencyValue = $('#vci-content-drawingtools-properties-color-transparency-container');

        var proportionsValue = $('#vci-content-drawingtools-properties-proportions');
        var textValue = $('#vci-content-drawingtools-properties-text');

        colorPickerValue.hide();
        fillColorValue.hide();
        strokeColorValue.hide();
        transparencyValue.hide();
        proportionsValue.hide();
        textValue.hide();
    }

    function publicShowRectangleProperties() {
        publicHideAllProperties();
        var colorPickerValue = $('#vci-content-drawingtools-properties-color-colorpicker-container');
        var fillColorValue = $('#vci-content-drawingtools-properties-color-fillcolor-container');
        var strokeColorValue = $('#vci-content-drawingtools-properties-color-strokecolor-container');

        var transparencyValue = $('#vci-content-drawingtools-properties-color-transparency-container');

        var proportionsValue = $('#vci-content-drawingtools-properties-proportions');

        colorPickerValue.show();
        fillColorValue.show();
        strokeColorValue.show();
        transparencyValue.show();
        proportionsValue.show();
    }

    function publicShowCircleProperties() {
        publicHideAllProperties();
        var colorPickerValue = $('#vci-content-drawingtools-properties-color-colorpicker-container');
        var fillColorValue = $('#vci-content-drawingtools-properties-color-fillcolor-container');
        var strokeColorValue = $('#vci-content-drawingtools-properties-color-strokecolor-container');

        var transparencyValue = $('#vci-content-drawingtools-properties-color-transparency-container');

        var proportionsValue = $('#vci-content-drawingtools-properties-proportions');

        colorPickerValue.show();
        fillColorValue.show();
        strokeColorValue.show();
        transparencyValue.show();
        proportionsValue.show();
    }

    function publicShowLineProperties() {
        publicHideAllProperties();
        var colorPickerValue = $('#vci-content-drawingtools-properties-color-colorpicker-container');
        var strokeColorValue = $('#vci-content-drawingtools-properties-color-strokecolor-container');

        var transparencyValue = $('#vci-content-drawingtools-properties-color-transparency-container');

        var proportionsValue = $('#vci-content-drawingtools-properties-proportions');

        colorPickerValue.show();
        strokeColorValue.show();
        transparencyValue.show();
        proportionsValue.show();
    }

    function publicShowPenProperties() {
        publicHideAllProperties();
        var colorPickerValue = $('#vci-content-drawingtools-properties-color-colorpicker-container');
        var strokeColorValue = $('#vci-content-drawingtools-properties-color-strokecolor-container');

        var transparencyValue = $('#vci-content-drawingtools-properties-color-transparency-container');

        var proportionsValue = $('#vci-content-drawingtools-properties-proportions');

        colorPickerValue.show();
        strokeColorValue.show();
        transparencyValue.show();
        proportionsValue.show();
    }

    function publicShowTextProperties() {
        publicHideAllProperties();
        var colorPickerValue = $('#vci-content-drawingtools-properties-color-colorpicker-container');
        var fillColorValue = $('#vci-content-drawingtools-properties-color-fillcolor-container');

        var transparencyValue = $('#vci-content-drawingtools-properties-color-transparency-container');

        var textValue = $('#vci-content-drawingtools-properties-text');

        colorPickerValue.show();
        fillColorValue.show();
        transparencyValue.show();
        textValue.show();
    }

    return {
        init: publicInit,
        getConfigs: publicGetConfigs,
        activateFillColor: function () { fillColorActive = true; strokeColorActive = false; },
        activateStrokeColor: function () { strokeColorActive = true; fillColorActive = false; },
        hideAllProperties: publicHideAllProperties,
        showRectangleProperties: publicShowRectangleProperties,
        showCircleProperties: publicShowCircleProperties,
        showLineProperties: publicShowLineProperties,
        showPenProperties: publicShowPenProperties,
        showTextProperties: publicShowTextProperties
    };
})();

// Contains methods for the Chat control
VCI.UI.Chat = (function () {
    // toggles the visibility of the chat according to menu and stage
    function publicToggleChat () {
        var chatDisplayStyle = $('#vci-content-chat').css('display');
        var menuDisplayStyle = $('#vci-content-menu').css('display');

        if (chatDisplayStyle == "none" && menuDisplayStyle == "none") {
            $('#vci-content-chat').css('display', "inline-block");
            $('#vci-content-chat').css('height', "100%");
            $('#vci-content-chat').css('width', "40%");
            $('#vci-content-stage').css('width', "60%");
            $('#vci-content-stage').css('display', "inline-block");
        } else if (chatDisplayStyle == "none" && menuDisplayStyle != "none") {
            $('#vci-content-chat').css('display', "inline-block");
            $('#vci-content-chat').css('width', "100%");
            $('#vci-content-chat').css('height', "300px");
        } else if (chatDisplayStyle != "none" && menuDisplayStyle != "none") {
            $('#vci-content-chat').css('display', "none");
            $('#vci-content-chat').css('width', "0%");
        } else {
            $('#vci-content-chat').css('display', "none");
            $('#vci-content-chat').css('width', "0%");
            $('#vci-content-stage').css('width', "100%");
            $('#vci-content-stage').css('display', "auto");
        }
    }

    return {
        toggleChat: publicToggleChat
    };
})();

// Contains methods for using the layermanager control
VCI.UI.LayerManager = (function () {
    var anchorIdSuffix = "_layerName_a";
    var listItemIdSuffix = "_layerAttributesContainer_li";
    var layerManagerListViewId = "vci-content-layermanagement-listview";
    var layerNameTextBoxId = "vci-content-layermanagement-layerName-tb";
    var layerVisibilityCheckBoxId = "vci-content-layermanagement-layerVisibility-cb";

    // Toggles the visibility of the current selected Layer
    function publicToggleVisibilityOfCurrentLayer () {
        var selectedLayer = VCI.Stage.getSelectedLayer();

        if (selectedLayer && selectedLayer != null) {
            var layerVisibility = $('#' + layerVisibilityCheckBoxId).attr('checked');

            switch (layerVisibility) {
                case 'checked':
                case 'true':
                    layerVisibility = true;
                    break
                default:
                    layerVisibility = false;
            }

            selectedLayer.setObjectAttributes( { visible:layerVisibility } );
        }
    }

    // Method for initializing the layermanagement control
    function publicInitializeLayerManagementControl () {
        $('#' + layerManagerListViewId).sortable();
        $('#' + layerManagerListViewId).bind('sortstop', function(event, ui) {
            $('#' + layerManagerListViewId).listview('refresh');

            var sortedLayerIdFromListItem = ui.item.attr('id').split(listItemIdSuffix)[0];
            var selectedListItem = $('#' + sortedLayerIdFromListItem + listItemIdSuffix);
            var maxZIndexOfLayers = VCI.Stage.getTopZIndex();
            var actualZIndex = maxZIndexOfLayers - selectedListItem.index();

            if (actualZIndex < 0) {
                return;
            }

            var vciLayer = VCI.Stage.getLayer(sortedLayerIdFromListItem);
            vciLayer.setZIndex(actualZIndex);

            // refresh zIndex for all layers because of sortChange
            var zIndexLayerIdCollection = VCI.Stage.getLayerZIndexOrder();
            var serverSnapshot = VCI.Stage.getServerSnapshot()

            serverSnapshot.pushObjectChangesList(zIndexLayerIdCollection);
            serverSnapshot.processToServer();

            VCI.Stage.draw();
        });

        publicCreateListItemOnExistingLayers();
    }

    // resets layer listitems with the a newLayerId
    function publicResetListItemOnNewLayerId (oldLayerId, newLayerId) {
        $('#' + oldLayerId + anchorIdSuffix).attr('id', newLayerId + anchorIdSuffix);
        $('#' + oldLayerId + listItemIdSuffix).attr('id', newLayerId + listItemIdSuffix);

        $('#' + newLayerId + listItemIdSuffix).val(newLayerId);
        $('#' + newLayerId + anchorIdSuffix).bind('click', function () {
            VCI.Stage.setSelectedLayerId(newLayerId);
        });

        $('#' + layerManagerListViewId).listview('refresh');
    }

    // creates layer listitems on existing layers in the VCI.Stage sorted by its zIndex
    function publicCreateListItemOnExistingLayers () {
        $('#' + layerManagerListViewId).empty();

        var zIndexLayerIdCollection = VCI.Stage.getLayerZIndexOrder();
        var layers = VCI.Stage.getLayers();

        for (var zIndex = 0; zIndex < Object.keys(zIndexLayerIdCollection).length; zIndex++) {
            var zIndexLayerIdItem = zIndexLayerIdCollection[zIndex];
            var layerId = zIndexLayerIdItem.objectClientId;
            var layerName = layers[layerId].getName();
            privateCreateListItemForLayerIdAndLayerName(layerId, layerName);
        }

        $('#' + layerManagerListViewId).listview('refresh');
    } 

    // Adds a layer to the layerlist and sends the layer data to the server for creating the layer
    function publicAddLayer () {
        var testLayer = VCI.Stage.addLayer();
        var tempLayerId = testLayer.getId();
        var layerName = "layer " + tempLayerId;
        testLayer.setName(layerName);
        VCI.Stage.setSelectedLayerId(tempLayerId);

        privateCreateListItemForLayerIdAndLayerName(tempLayerId, layerName);
        $('#' + layerManagerListViewId).listview('refresh');
        testLayer.processObjectChangesForCreatingLayer();
    }

    // creates a layer list item by its id and name
    function privateCreateListItemForLayerIdAndLayerName (layerId, layerName) {
        var anchorId = layerId + anchorIdSuffix;
        var listItemId = layerId + listItemIdSuffix;

        // value of li contains layerId
        var anchorTagLayerName = "<a href='#' id='" + anchorId + "'> " + layerName + "  </a>"
        var listItem = "<li id=" + listItemId + "> " + anchorTagLayerName + " </li>"

        $('#' + layerManagerListViewId).prepend(listItem);
        $('#' + anchorId).bind('click', function () {
            VCI.Stage.setSelectedLayerId(layerId);
            var selectedLayerName = VCI.Stage.getSelectedLayer().getName();
            var layerName = $('#' + layerNameTextBoxId).val(selectedLayerName);
            $('#' + layerVisibilityCheckBoxId).attr('checked', VCI.Stage.getSelectedLayer().getObjectAttributes()['visible']).checkboxradio("refresh");
            var selectedToolFunction = VCI.Selection[VCI.selectedTool];

            if (selectedToolFunction && selectedToolFunction != null) {
                selectedToolFunction();
            }
        });
    }

    // deleted the selected layer and its listitem
    function publicDeleteSelectedLayer () {
        var selectedLayerId = VCI.Stage.getSelectedLayerId();

        if (selectedLayerId) {
            $('#' + selectedLayerId + listItemIdSuffix).remove();

            var objectChanges = new VCI.DataLayering.ObjectChanges();
            objectChanges.objectClientId = selectedLayerId;
            objectChanges.objectType = "Layer";
            objectChanges.command = "Delete";
            objectChanges.objectAttributes = undefined;
            objectChanges.objectServerId = undefined;
            objectChanges.layerId = undefined;

            var vciStageServerSnapshot = VCI.Stage.getServerSnapshot();
            vciStageServerSnapshot.pushObjectChanges(objectChanges);
            vciStageServerSnapshot.processToServer();

            VCI.Stage.deleteLayer(selectedLayerId);
            VCI.Stage.draw();
        }
    }

    // changes name for the selected layer and sends this object change to the server
    function publicChangeNameForSelectedLayer (e) {
        if (e && e.keyCode && e.keyCode == 13) {
            var selectedLayerId = VCI.Stage.getSelectedLayerId();

            if (selectedLayerId) {
                var layerName = $('#' + layerNameTextBoxId).val();

                var objectChanges = new VCI.DataLayering.ObjectChanges();
                objectChanges.objectClientId = selectedLayerId;
                objectChanges.objectType = "Layer";
                objectChanges.command = undefined;
                objectChanges.objectAttributes = { name: layerName };
                objectChanges.objectServerId = undefined;
                objectChanges.layerId = undefined;

                var vciStageServerSnapshot = VCI.Stage.getServerSnapshot();
                vciStageServerSnapshot.pushObjectChanges(objectChanges);
                vciStageServerSnapshot.processToServer();

                VCI.Stage.getSelectedLayer().setName(layerName);
                $('#' + selectedLayerId + anchorIdSuffix).text(layerName);
            }
        }
    }

    return {
        initLayerManagementControl : publicInitializeLayerManagementControl,
        addLayer: publicAddLayer,
        deleteSelectedLayer: publicDeleteSelectedLayer,
        resetListItemOnNewLayerId : publicResetListItemOnNewLayerId,
        createListItemOnExistingLayers: publicCreateListItemOnExistingLayers,
        changeNameForSelectedLayer: publicChangeNameForSelectedLayer,
        toggleVisibilityOfCurrentLayer : publicToggleVisibilityOfCurrentLayer
    };
})();

// Contains methods for initializing a object selection for shapes
VCI.UI.ObjectSelection = (function () {
    var eventListenerCollection = {
        // Contains a eventlistner for clicking on a shape
        'click.objectSelection': function (evt) {
            var objectSelectionContainer = $('#vci-content-objectselection');
            
            // empties all property-controls on each click of a shape
            objectSelectionContainer.empty();
            var shape = evt.shape;
            var shapeId = shape.getId();
            var attributes = shape.getAttrs();
            var controlIdCollection = [];

            var layer = shape.getLayer();
            var layerId = layer.getId();
            var vciLayer = VCI.Stage.getLayer(layerId);
            var vciLayerServerSnapshot = vciLayer.getServerSnapshot();

            for (attributeName in attributes) {
                if (attributeName == "drawFunc" ||
                    attributeName == "draggable" ) {
                    continue;
                }

                var attributeValue = JSON.stringify(attributes[attributeName]);

                // construct of the textbox id for the attributes
                var controlId = shapeId + '_' + attributeName + '_tb';
                controlIdCollection.push(controlId);

                var attributeLabel = '<label for="basic" class="ui-input-text">' + attributeName + '</label>';
                var attributeTextbox = '<input type="text" id="' + controlId + '" name="' + attributeName + '" data-theme="b" value=' + attributeValue  + ' />';
                objectSelectionContainer.append(attributeLabel, [attributeTextbox]);
            }

            var applyObjectAttributesButtonId = shapeId + '_applyObjectAttributes_btn';
            var deleteObjectButtonId = shapeId + '_deleteObject_btn';

            var applyObjectAttributesButtonElement = '<a id="' + applyObjectAttributesButtonId + '" href="#" data-role="button" data-icon="check" data-iconpos="right">Attribute anwenden</a>';
            var deleteObjectButtonElement = '<a id="' + deleteObjectButtonId + '" href="#" data-role="button" data-icon="delete" data-iconpos="right">Objekt löschen</a>';

            objectSelectionContainer.append(applyObjectAttributesButtonElement, [deleteObjectButtonElement]);

            var applyObjectAttributesButton = $('#' + applyObjectAttributesButtonId);
            var deleteObjectButton = $('#' + deleteObjectButtonId);

            applyObjectAttributesButton.bind('click', function () {
                var objectAttributesForObjectChanges = {};

                for (controlIdKey in controlIdCollection) {
                    controlId = controlIdCollection[controlIdKey];
                    var textBoxWithAttributeDescription = $('#' + controlId);
                    var attributeValueString = textBoxWithAttributeDescription.val()
                    var attributeValue;

                    if (!attributeValueString || 
                        attributeValueString == "undefined" || 
                        attributeValueString == "" ||
                        attributeValueString == "id") {
                        continue;
                    } else {
                        // catch, when its not a json-object and just uses the string value of the textbox
                        try {
                            attributeValue = JSON.parse(attributeValueString);
                        } catch (ex) {
                            attributeValue = attributeValueString;
                        }
                    }

                    var attributeName = textBoxWithAttributeDescription.attr('name');
                    objectAttributesForObjectChanges[attributeName] = attributeValue;
                }

                shape.setAttrs(objectAttributesForObjectChanges);

                var objectChanges = new VCI.DataLayering.ObjectChanges();
                objectChanges.objectClientId = shapeId;
                objectChanges.command = undefined;
                objectChanges.objectAttributes = objectAttributesForObjectChanges;
                objectChanges.objectServerId = undefined;
                objectChanges.layerId = layerId;

                vciLayerServerSnapshot.pushObjectChanges(objectChanges);
                vciLayerServerSnapshot.processToServer();

                vciLayer.draw();
            });

            // defines how to send delete object information
            deleteObjectButton.bind('click', function () {
                vciLayer.remove(shapeId);

                vciLayer.draw();

                var objectChanges = new VCI.DataLayering.ObjectChanges();
                objectChanges.objectClientId = shapeId;
                objectChanges.command = 'Delete';
                objectChanges.objectAttributes = undefined;
                objectChanges.objectType = undefined;
                objectChanges.objectServerId = undefined;
                objectChanges.layerId = layerId;

                vciLayerServerSnapshot.pushObjectChanges(objectChanges);
                vciLayerServerSnapshot.processToServer();

                publicCleanUpObjectSelection();
            });

            // apply jQuery mobile styles in div-container
            objectSelectionContainer.trigger( "create" );
        },
        // Contains a eventlistener for dragging a shape
        'dragmove.objectSelection' : function (evt) {
            var shape = evt.shape;
            var shapeId = shape.getId();
            var shapePosition = shape.getPosition();

            var shapeXCoord = shapePosition.x;
            var shapeYCoord = shapePosition.y;

            var xCoordTextBox = $('#' + shapeId + '_x' + '_tb');
            var yCoordTextBox = $('#' + shapeId + '_y' + '_tb');

            xCoordTextBox.val(shapeXCoord);
            yCoordTextBox.val(shapeYCoord);

            var layer = shape.getLayer();
            var layerId = layer.getId();
            var vciLayer = VCI.Stage.getLayer(layerId);
            var vciLayerServerSnapshot = vciLayer.getServerSnapshot();

            var objectChanges = new VCI.DataLayering.ObjectChanges();
            objectChanges.objectClientId = shapeId;
            objectChanges.command = undefined;
            objectChanges.objectAttributes = {
                x: shapeXCoord,
                y: shapeYCoord
            }
            objectChanges.objectType = shape.getObjectType();
            objectChanges.objectServerId = undefined;
            objectChanges.layerId = layerId;

            vciLayerServerSnapshot.pushObjectChanges(objectChanges);
            vciLayerServerSnapshot.processToServer();
        },
        //'touchmove.objectSelection' : this['dragmove.objectSelection']
    }

    // Contains a method for initialize a object selection for the current selected Layer
    function publicInitiateObjectSelection () {
        var selectedLayer = VCI.Stage.getSelectedLayer();

        if (selectedLayer) {
            selectedLayer.offAll();
            VCI.Selection.select();
            publicSubscribeEventsToShapesInSelectedLayer();
            VCI.Stage.applyAttributeOnEachShape(VCI.Stage.getSelectedLayerId(), "draggable", true);
        }
    }

    // Calls a CleanUp Method for exiting Object Selection
    function publicExitObjectSelection () {
        publicCleanUpObjectSelection();
        VCI.Stage.applyAttributeOnEachShape(VCI.Stage.getSelectedLayerId(), "draggable", false);
    }

    // Subscribes the eventListenerList to all objects on the current selected Layer 
    function publicSubscribeEventsToShapesInSelectedLayer () {
        var selectedLayer = VCI.Stage.getSelectedLayer();

        if (!selectedLayer || selectedLayer == null) {
            return;
        }

        var persistantShapes = selectedLayer.getShapes();

        for (persistantShapeKey in persistantShapes) {
            var persistantShape = persistantShapes[persistantShapeKey];

            for (eventSelector in eventListenerCollection) {
                var eventListener = eventListenerCollection[eventSelector];
                persistantShape.on(eventSelector, eventListener);
            }
        }
    }

    // Unsubscribes the eventListenerList to all objects on the current selected Layer 
    function privateUnSubscribeEventsToShapesInSelectedLayer () {
        var selectedLayer = VCI.Stage.getSelectedLayer();

        if (!selectedLayer || selectedLayer == null) {
            return;
        }

        var persistantShapes = selectedLayer.getShapes();

        for (persistantShapeKey in persistantShapes) {
            var persistantShape = persistantShapes[persistantShapeKey];

            for (eventSelector in eventListenerCollection) {
                var eventListener = eventListenerCollection[eventSelector];
                persistantShape.off(eventSelector, eventListener);
            }
        }
    }

    // Defines a CleanUp Method for exiting Object Selection
    function publicCleanUpObjectSelection () {
        privateUnSubscribeEventsToShapesInSelectedLayer();
        $('#vci-content-objectselection').empty();
    }

    return {
        initiateObjectSelection: publicInitiateObjectSelection,
        exitObjectSelection: publicExitObjectSelection
    }
})();

// Contains methods for manipulating controls of the menu
VCI.UI.Menu = (function () {
    // Toggles the visibility of the menu
    function publicToggleMenu () {
        var menuDisplayStyle = $('#vci-content-menu').css('display');
        var chatDisplayStyle = $('#vci-content-chat').css('display');

        if (menuDisplayStyle == "none" && chatDisplayStyle == "none") {
            $('#vci-content-menu').css('display', "inline-block");
            $('#vci-content-menu').css('width', "40%");
            $('#vci-content-stage').css('width', "60%");
            $('#vci-content-stage').css('display', "inline-block");
        } else if (chatDisplayStyle != "none" && menuDisplayStyle != "none") {
            $('#vci-content-menu').css('display', "none");
            $('#vci-content-menu').css('width', "0%");
            $('#vci-content-chat').css('display', "inline-block");
            $('#vci-content-chat').css('height', "100%");
            $('#vci-content-chat').css('width', "40%");
            $('#vci-content-stage').css('width', "60%");
            $('#vci-content-stage').css('display', "inline-block");
        } else if (chatDisplayStyle != "none" && menuDisplayStyle == "none") {
            $('#vci-content-chat').css('display', "inline-block");
            $('#vci-content-chat').css('width', "100%");
            $('#vci-content-chat').css('height', "300px");
            $('#vci-content-menu').css('display', "inline-block");
            $('#vci-content-menu').css('width', "40%");
            $('#vci-content-stage').css('width', "60%");
            $('#vci-content-stage').css('display', "inline-block");
        } else {
            $('#vci-content-menu').css('display', "none");
            $('#vci-content-menu').css('width', "0%");
            $('#vci-content-stage').css('width', "100%");
            $('#vci-content-stage').css('display', "auto");
        }
    }

    // makes drawingtools control visible
    function publicShowDrawingTools () {
        setAllMenuOptionsInvisible();
        $('#vci-content-drawingtools').css('display', "inline-block");

        var selectedToolFunction = VCI.Selection[VCI.selectedTool];

        if (selectedToolFunction && selectedToolFunction != null) {
            selectedToolFunction();
        }
    }

    // makes layermanagement control visible
    function publicShowLayerManagement () {
        setAllMenuOptionsInvisible();
        $('#vci-content-layermanagement').css('display', "inline-block");
    }

    // makes objectselectiontool control visible
    function publicShowObjectSelection () {
        setAllMenuOptionsInvisible();
        $('#vci-content-objectselection').css('display', "inline-block");
    }

    // makes all menu tools invisible
    function setAllMenuOptionsInvisible () {
        $('#vci-content-drawingtools').css('display', "none");
        $('#vci-content-layermanagement').css('display', "none");
        $('#vci-content-objectselection').css('display', "none");
    }

    return {
        toggleMenu: publicToggleMenu,
        showDrawingTools: publicShowDrawingTools,
        showLayerManagement: publicShowLayerManagement,
        showObjectSelection: publicShowObjectSelection
    };
})();

// Describes anchors for transforming a object
VCI.UI.Anchor = function(layer, x, y){
    this.x = x;
    this.y = y;
    this.layer = layer;

    this.anchor = new VCI.Circle({
        x: x,
        y: y,
        radius: 8,
        stroke: "#666",
        fill: "#ddd",
        strokeWidth: 2,
        draggable: true,
    });
    
    this.anchor.on("mouseover", function () {
        document.body.style.cursor = "pointer";
        this.setStrokeWidth(4);
        layer.draw();
    });
    
    this.anchor.on("mouseout", function () {
        document.body.style.cursor = "default";
        this.setStrokeWidth(2);
        layer.draw();
    });

    layer.add(anchor);
};

VCI.Images = {};

VCI.Images.colorpicker = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQAAAAD+CAYAAADVndu7AAAACXBIWXMAAA7DAAAOwwHHb6hkAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgACWspJREFUeNrs9O97Gkf27/3m7z/cG29mM4NHniFDQkxCIgWHGAcbS5ZsbIwtjJCQkBACgQBVFV1FN900v+T3eUDLlj357vP83Glfr+uz1qqiAZnqb4AxMAP8v/zlL/+vMQPG3wTNX9df11/X//su/68HwF/XX9dfD4C/rr+uv66/HgB/XX9df11/PQD+uv66/rr+egD8df11/XX9//sD4OPNjf/x5oaPHz9y8/EjNzcf+fjxBj7efNF/vPnIx5ubwNf119b3+x/3ffq3fp+PH2/4yM2XMz6u7/ExuNfH9dr6s9ydfYSPt/f5+IWbIPni7jfccLsWfMc73/3m7ozbtZtP+em+wR1vuPt+N5/e879nN58+/83tdw7qm2Dt5s7apxmf8+bTnrufiS9e82nGRz5+hI8f+fR91n3wue78DW7/jusZn9//Jvib3H7P2/72+90Ef/qPcEOQN3f2feXjzd31z/XHm+Az3b337f1vP/vd9/p4w83Nzaf/j0/v+/HP3/f2vrd/m4+f/p8DN3AT3Pv2N8fNnd/E7ee/89n4+Plz3+5f/3Q/cnvDj3df88X5uP3J3q59/j18Pjt8eX4+3jkrH2/uvMfn9Y9fzD5++p638y8+13qf/83H1cq/WS65Wa3WlituVsvAnf7r+Rf115bc3Ky+2PdxteLj7frNreVXPs8+frFnFfRf7/16vuLm4+f7r27+u17dLFl96j/Xn/3fZkF+/FwvP/UrljfLdX/r44rlxyWLINev+XLP8vYef1Kv/mRt7eYrd/fcsPy4nq8+rt3duwp80X/8yOrm46fXru6sLW/nNzesVjcsV7drH1mt1rMv9q8+slp9ZPnJzX9Z3a2XgaBe3N5z9Xn+9b7l6obl8vO9VkH/Kf/M6uN/3feLfnnDzeqO5cc7ax+5+Xp9dcPH5Q03q2Dtk6/61Q0f7+z5+MXeu/e5e/7u7rmdfSWYf7y7vvwTqyU3y6+tuFku+bhc+t98XC78m8WC/9FyEbxo8aXVHf+3tcDHwM1qERzWxWd39wazj3fX/6T/PP/8UFjdLFh9XM9XX1h+1X+9tmR5s/gffb1n9fHL/YuPCxY3f+LjgsXHZWA9W94sWNysvrL8qv5sGTxYvtxzE1gf+v+638ebz/nx69f8iY8fWdx8DOqv14P56msfWQSH8cv1j58tP7JY3qytbj7X/5PFf1sGvph/tX+5CB4Id92dL25YLj5+nt2x+trtA2Px8c7843/tu1kEB3fxkZugX7vzALjdd2f2cbHi5isfFzef+7uHd/Fnlp8tl3y823+19mX932f742Lhf3Mzn/s3M5/VbPbJzfxPzGbczPx1vZhxs/C5mfusFjNWizv7FmurxYybZdAvP1stZqyWPjerGauVz2o542b1P7hZWwVu56vV59maz+pmxjJw269uZiw+8Vne+CyDfrmafarnX+z7uveZB7NZkF/3s6/4d/vVbT9fW82/rG/71YLZzR2rO/n12s3ys9Xyy/5mtbZa4d/WX6zd3Nlzg39zE8xuPr9utWK2vPlcr27WfTCbLQOLz3M/MFsE8/lqXc9v1hZfCeb+7HM9+7qe3TCbrQJf1zfMZzfMZ6vAur5dn89uWMxuWPg3LGarwJ35V5b/P6z+L27+y2rNX7Gard34K25mS25my69mt5bczBb/lavZghv/dva1+adczeas/MX6/N72szk3s9k6/XlwhgP+jBt/5n+zmvn+yp+ymk5Z+VNWvh/klNXsq/52dtf8Lv+/+pu5z83CZ7WY/s+W0/XDYBXUSz+Yfc3/ol+ufJar6X+7mbJY3XHjf9mvpsz/B7ObKbPb+iv+f/G/6Kcrn+lq+tnNbe3/idmdnAdm+J/q/zt/tWD6P1oyXa2CDNws8VerYL4+2Hf7T5Zr/vJzPV2umC4+87+q7/bT+Z+YBeY3+LMV/uzOmr/m+5/rL0xX+NN1fqr9dc78FbPpl/zA7O66/9l8+j/wVyymX/HXltOv3XyqV9MVKz/I/6vln/MXrKaBu/V0wcqfs5oGB3s6/xOzNT9w20/9Nd//XN9xM51yM52ymk79b1bTqb90PVaux2rqspq6LD2P5dT7lCvfZeV7rHyPpe+yDPrlzGMZzFczl6W/7pdzj9XcZRXsXc49VrO15cJjuXBZLlxWC5fVIth7Wy9dlkGuFu46ly6rlcdy6bEKLD9xP9WLlcti5TFfusxXLvOVx2K1ns+XHrPVLXe9Z+mt519becyWLv7SxV95+Mtb69l06TFdukxXHtOVi7f08JYe7sr7VHsrD2/p4q2meMvpV/Mp3tLHW/lBTnFXwWzp4y593NUMbzlb18sZ7srHXc3X9R3ecoa3nK/3ruZ4y8W6Xy1wVwvc1XJtedsv8FZL3OUKd7nEXa3wViu85XLdL5d4ixXe4nN/O3PnK7z5al0vVnizO+Yr3PkS11/hzla4/grPX+LNlrizVZBL3OkSz1/i+uu93vTWes2dLvG81dpt7a4+zVxvxTSop94Sz13iuet66i2Zumu+u2TqrvC9W0tm7oqZt2TmLvHdFb4b1N6SuRusu0sW7pKFu2LmrZi7K+buen3urVh6S5bu8lMu3BULb8XKXbK6nXurNTfgLVl56/WVt2DpLYJ7LFi5C1a36S5YuguW7oylO2Pl+qxcn6U7Z+nNWXozVp7Pypux9HyW3u362nrNZ+l6LN0pS3e6Pteex8qd3hGcbc/zv1lOXH9pOywnDouJveY4LCbBzHVYuHaQDkvPZuHZLDyHpTdh6U5YTO2Aw3LqsPQdFr79ee47n80cFjP7DofF/NaExXzCcm6zuLWwWSxtFgvnv8wXDvOFHeTneraYMFvYzBbOJ/7SwV/csvGX9nq2dJguHaaLwNLBWzh4Czvg4C0d3KWDu7RxFzbuwsFd2EwWDpOFjbOYMFlMcBbOn5gEHJylg7Nwv7T0cBYu9m29dLGXHvZiirOYYi/W9ZqPvfySs/RxFrdmTBYznOUMZzHH/sLiU+0sFkG/xF4scAJ2wJkvceYL7PkCZx70swX2bIEzWzAJOP5/s6cLbH+BM10wma7Tni5xpkvs6RLbW+J4S2x3ge0ucO6wJ2uOE5gsmUwWTJw7M+dzP3EWOPaCib1gYi+ZTJa49pKJvcCzF7j2As9Z4E0WeJMlU3vB1FkwDdankwV+0Pv2uvadBTN7ycy+rW/7JTNnwdxZrk2WzO3FmrNgYS9Y2EuWzpLFZMHi02zB0lmwcOYs7Pk6g3rpzFg4MxaTGQv7Lj8wZWFPWdo+S8dn6UxZBJaOx9LxWNgeSzuoJx4Lx2Nhu3dM1ufZnrBwArczx/G/WTq2vxyPWdpjFrYdGLNwxiwnNgtnzGJyl/2Ze9f484H3xuuHxHT8pdmYhf8n5rcPg/We9QNgvLYYs1gGuRivHwiLMfOvzL5gf8FfjJkubaaLO5bjgI23HOMtxuvD/emQj5ncWtqBMU4wc+6wFzb2p7QZL2zGi3HAxl442AuH8dJhvJgwXtzmhPHSZbyYYC8m2MugX3qMFy7jhYe98BgvPMa3D4Hg4I/vsBdr44XPeDFjvJwxXszX9WKGvZhhL+bB7NaCcfAAsBeLoA/Ml9jzBeNPloxnC8b+f7Nv62nA+8z+1K8P/thbMp6s3R728S1ngX3LXjD+M+MF9nh96B17gTO+axlYMLnLXqc7XuKOF3h3TO0gA/7/aMlsHDwIxmtzO3gAjD9bjBcsx8HBH3+2HM9ZjIMHgD1fH/LxjOUXh91nMfZZjKef2etcjr1P1nOP5dhl+cVBd1mMJ5/ZExZj58/ZzvqM22P/m4UZ+wutmRvN4tZ4bR7kwr7DWZvfmqxzMdHMXYu5q1m4mrmrmXvrXHh3TO/w1+azW1ZAs5jfsdDMFxaLxed6trCYLzSzeWDxmX/H9A5vHlisubfmFu7cYrKwmCw0k7nGmVs4i8BcYy809nxtvLAYL/QnZh5YrOlbc41emE+shUEvxuj5GL2wv+KgFw7WPLBw0PMJeu5+zoWLnnvoxZq18NCL6Z+yFjP03L9jhl7M0fM1az5f94sF1mLxeW22QM8X6/xkjvbn6Ok6rekCy1tgTRfo6QLtBb23QLvzwGLNmaOdBXqyQDsLLGed2lmgxwu0HRjP0WaxNl6gzXxNz7GC1HqB0QuMnmP0grFeMNZzxnqOsda9redfcPQcRy+YBBy9wNULXD3/ZBKkZy3w9ILpF+ZM9Rxfz5npxR1zZnrO/JMFc71gYc1Z6ID1eX09nzG/Y6F95taUhf6KNWWh3c8sl/ndXk+YWxMW1oSF5azpW/Ync20z12MWeszCGrPQhoXWzLW5rf1vFpblz5ViPgpYdxjFwqzzC+MR87H6zFbMHcXcGQWCfhJwv+LdMVXMp6N1+nfMbo2Yz9UXZl/xA9M7vK+4d3Jyh3MnnbnCDozvMF/RAesro4CaK9R8hJpbjIL8TKPmmtHcoL4wDthfce6Y3OF+xfvKNODfMfvKHDWfM5rPGc3mqFv+n5jOUd4d7p+YBJxbi3XagfEdZp0jM0fptZGeo6xAUI9Gc1TAUnNGAesOfYe5Y3yH/ckCRy1w1PyTScD9ijcKqDlTNcf/E/PA7E792ewr/h3TgPcVl7nyWCg3qCcB5yv2HePPRmZNGeZKB6w7aTFXo4Dyv5mNlD+TkrmUzKRgpgTzkWCmJDNLMLMkc31br821ZKYlMyOZG8FsLJiPJTNbMhsHbMHMlswngvlEMp9IfEcwm0jmnmTuCeaeYDYVzDy5NhXMfPE5Z4LZTDLzJbOZZD6X+LdmAn8umM4E05lcm0umc4E7k59MZoLJTODOJe5MMJlJ7JnAngucmcCeCcYzyfi2ngvMTAYpMHOJngv0XGDN5WczgZpL1FwwmgvkTCBn617OBWIuEDOJnEnkfIScK8R8hJiNkDMLOdPImUbMNWJuIWYaOTOImUHMx8jZGDk3iLmNmI8DDnJuI+cOYu4gZrc5Qc48xDww8z73syli5iPnU+TcR8xnyPkMEZDzOXI2R/pz5Gy25s8Q/nomvM+kO0e4M4Q3Q7ozhDNHOHOkM0dOZgh7hrDXvbDnCHuGtH2kPUeYOWI8Q5gZQs+Q1hyp50hrjhjNEaMZ0pqtU82Qah7kDClnCDlDis9GcoaSM5SYYYkZIzHHkgExRweMXBvLOUbMGYvZJ7aY4QgfO6gncoYj50zEnIn0caXPRM5wxQxXzPHkDE/OmIoZUzHHF3N8uc6ZmOGLGTMxYy5mzMWcufSZS5+Z9JmLKTPpMxM+c+ExFx4z4TKXLnM5YS7cz72YMBMOczFhLhzm0mYuHGZizFzYzOSYmTTM5Zi5HDMTmpnQzIVmLgNCMxcj5tJiLkbMAnOh8KXCF5KZFP43Myl9fzjEHw7xrwdrYoAvh/hygK+Ga3KArwb4owG+NcQfDddpDfD1AN8MmZkhMz0M6gH+eIhvD/DtYWCA7wzxJ0P8yWDNHeC7wzVv8Nn01hDf/zMDpgHPH+L5Q1x/iOsPmPjDTxx/EBhi+wNsf8jYH3xB+0O0P8D4A7Q/wAp6K6hH/oCRP0D6Q6Q/RPlD5Kd+gAhc+0Ou/QFDf8DAv2bgXzOcCYb+2sCXDHzJ0FefDPxRwGLoW0GtGX7qDQNfB2kY+mOGvs3AHzPw7YDD0J8wuGM4cxn4LgPfY+B7DH2PoT9l4E+D9Bn4PsO7pgHPZ+AF6X42nPgMAsOJz8BeGwYGts9g7DMc+wzM2jAwsL40HAWUzyAwlD4D6TMUd1yvDa59rgdrw4HP9dBHDH3k4DM19JFDHzXwGQ18rIGPNVzTQx898DGDdd7W4zvs4Zoz8HGGPpNhkIM1d7jmDT6bDn2mgzX/a0MffzhdG3j4Q2+dAzcwwR8GBoGhgz+wvzQc4w/GzAYGf2DwhwZ/qD8bWIER/jAwUGtDiT+Q+APx2VDgD67xh0P/G3947fv9Af6gvzb8ynUfX9yhAqPberCurT6zgG/1mek+vhmsjfufOX9i0sd3A94A3+vjT7/i/7dpwPP7uHc4fj849H3swPgrxu+j/X5w0PufjO6kCsiACFx/MuDaHzDw+wz8Pv0/MxvQ928NA9f0fXGHDKjAKGAx8C36n2j6vrljTP/Og6DvO/T9ydrMpe/f8gLTr8zo+/7a9A4v4AYma7eHv+/49G2f/vgOE9AB60+oO6RPX6wNguwPfQbDdfYHgf5/G9wxDFz3fcQdsh88FPpro4D1Fd33MXeMA/ZgzemvTfo+7le8/ufD/+kB0P/aNOAF3MDkK3ZgfIcJaPy+ZtbX+P3gsPdHAXXHVwd9IPD713cMAv3b8+5/4/f7vt/r4V/1+JRXPfx+D38QGPbwh9010cUXPXx5m4FRj1nAt7r4VpA6YHpr4x6+3cO3u0H28Cdd/EmQbsDrBbr40x7+tMfU7zH1u0z9Ht4nXdzAxO8y8Xs4fg/b72L7PcZ+N9DD+F2M30MHLL+H5Xex/B7K76H8bpA9RODa7wa5rod+j8Edfb9H3+/S83tc+T16fo+e36Xrd+n5V/RmV3T9/rr2+/T8Pl1/+EnPH9LzBd3Aupb0fEnPH9H1FT1/RM+36PkWXd+i62t6vqbnG7r+OGDT8226n0zoBda1S8/36PnTO3x6vk/X9+lOfbreWs+9Y+LTc3y6jk/PvmPs0x37dI1PT3/WtXy6I5/eHV0VED496dMT67p77dMdrvWGPr3BWrcfuPLpXfl0e2u9W12fq65P/1bPZ9j9kuj5XPd8RHdN9tZGPR/V9Rl1faxeoOujuz66t07T9Rn3fMZdHzvgBCa9Nbfr4/Z8vMD0Dr83/VLXw++6az0XvzvB703wuw5+z75jjN8d4/cMftfg9zR+z/rKKCDxuwq/K/B7ge41fne41gt0B/jdPn7v1hV+t4ff6+L3uv43fq/r+502/mWb6WUb/7KD3w302vi9NtOrS6b99if+sLN23cG/bq9TtPFlB1918FUbX3WYjtr4Vpup1WZqdZjqNr7prI3b+OPLddptfKeD77TxJwG3w9TrMPXa+F4H3+sw9dt4n3Rwpx1cv8PEbzOZtnGmbRy/g+23GQfMdE37HbTfxvI7jPwOI7/NyO+gAtLvIPw219POmt9h6LcZ+m0Gfof+tEN/2uZq2uHK79DzL+n6nUCbjt/h0u/Q8Tu0/TbtYNbxL+n4XS6nXdp+l47fo+1f0Z72aftrHX9AZzqk7Q/p+EPa02va02s6vqDtSzq+ouMr2r6i7Y9o+xaXvkXbt2j7ms7U0J4aOtMx7U8cOr5Nx3do+xPa/oSO79KZenR8j44/XZv6tL07XJ+OO6UzCTg+bcenYwfGa23j09ZrHcunbfm0Rz7t0ZSOmnKpfNrSpy2mtK+ndK6ndK59OsMpneGU9sCn3Z/S7vu0r9Y6vSmdnk+7G7j0ubz06bR92m2fdsfnsuPT6fhctn26gV7b56oz5ao9pd+e0m/7DDo+w47PsL123fG5bvuIjo9sBzo+qu0zavtYbR/rcp267WMC486UcXuK3Z7itH2cztqk/ZnbmeJ1fLz2mt+Z4ren+JceftvDb7v4nQl+exKkE7DxOzb+5XitbfDbGr+j8dsW047FtDPCb4/w24ppR+G3JX5H4F8K/PZ1YIjfHtzRZ9q5Ytq5wm/31i67QX2J37nE73TwOx3/m+llx59etPAuWkzbgc6ad7k27V7g9Vp4vRbTXguvf8G0f8F00GI6aOENWnjDFt71BVPR+sSTLaajC6ajFlPVwhu1mFotPKuFZ1pMTYvpuMXUbuGNL/DsFp7TwnO/NHVbTL0WXsD1Lph4LSZeC2d6gT1tYU9bjKctxl4LE6T2WuhpC2vawvJajLwWymshvRbSbyGnLaR3gfBaXHsthtMWg2mLgbfWD1x5LXrTte60Rddrceld0PFadLwWba9Fe9riYtqidZd3Qctrc+G1uZh2aE07tLxLWtMurWmPlvfZhdenNe3T8vq0poM1b0jLE7SmktZUrHmSlqe48Ea0vBEtz+LCs2hNLVpTTWtqaE3H6/TsoLZp+RNa0wmtqcvF1KXlubQ8j9Z0Ssub0nJvebScKS07MPZojae0jLemp7S0R8ua0rK8NeXRUlNackpLelxIjwvh0br2aA09WoMpF4MpFwOPVn+6duXR6k1p9Txa3bWLyymtjvdZ2+Oi5dH6ZEqrNaV9MaXdmtJueXRaHpctj+7FlG5rSi9wdeHRb3n0W1P6rSmDwLA15foO0fJQrWnAQ7U8Rq0pVkBfeJjWFLs1xW55jFsedmuK0/KY3OEGvJaH15qu88Jl2pqsXUzwWg5ey8Zr2UxbY7yWWbvQTC80XssKjPBaimlL4bXkp/RagmnrGq81xLsY4rUGeK0B09aAaesK75MeXquL1+oybV3itS7xLjp4rTbTgNe6YHpx4X8zbV34XrOJd36G17rjoonXPsPrnOEG6V02cbtNvN4dV028fhOvf7bOwRnesIl33cQTZ3iy+Zlq4o3O1qwzPH2Gp5t45gx33MQ1TTy7ies08Zwm3uRszT3D85p43hkTt7nmNbG9JrZ3xtg7Y+w1GXtNjHeG9ppo7wwrMPKajLwm0jtDemeIwLXX5NptMnCbDLy1vnvGlXdGz2sGzuh6Z3TcJh2vyaXb5MJr0vbOuAi0vCYtr8m516TpnQWaNL1zmu7amXcRaNP02py5HZruJU3vkqbXpel1OfO6nHlXNN0rmt4VZ+6AM3dI07t1TdO9pukJzjwZUJx5I5ruiKZr0XQ1TU/T9AxNz3DmGc68MU3P5sxzOPMmnHkTmp5L03M5cz3OJh7NW45L0/Y4G3s0xx5NE9AeTSswcmmOPM6UR1N5nEmPM+HRFB5n14GBRzNw1vc4u/I46wW662xeupx1XJodj2bHW9cXHs2WR/PC46zlcdZ0aX7i0TzzOD/zOG96XATaTZd206Nz5nHZ9Og0PbpnHt2mR+/M46q51m969M/Whs2166aHaHpcn3nIMw/V9FBnHqPmmtX00E0PcxZoeoybHnbTw266OGcek4B75uGeuXhNF+8s0JzgNR28Mwe3OcY7u2XwmhqvaeGd3RrhNRXemcRtStymwGsKvDOB1xzinQ3xzgaBK9zmFd7ZrV7gEq95q4N31sZttnHPWnifNHGbTbyzM/8b76zpu40G3mkD9/QErxk4P8E7b+C2GrgXDdz2CW77BK9zgnvZwO3ecXWCd9XAvWrg9hu4gzXvuoEnGnjiBFec4MoGnjrBG53gjRp4VgPPOsHVjc/GDdzxCa59gjtZ8yYnuF4D12swcT+zvQa2d8LYa2C8BsZds7wTLG+dI/cE6TaQ3gnSO0F4Da69Btdug6F7wsA9oe826LsNrtwGPbdBz2vQDXRuuQ3aboMLt8GF1+DcO6H5SYPTQMNrcOI1OHEbNLwzTtwzTtzmmnceuODEvdWm4XVoeJeceF1OvEsabpeGd0XDvaLh9gMDGt6AE/eaE09w4gkanqDhSU48RcMd3WHR8DQNzwTGNDybE8+m4U2+5Lo0Jp+dOC4nY5eTsUfDuGvao2F9djJyOVEuJ9KlIV0awqNx7dG4djkZeJwMPU76nzV6ga5H49Jd66ydtF1OLtYa5y6N5tpJ0+PkzKVx6tJouJycuDROXE4bLqcNj+aJR7Oxdt5waQUuGh7twGXDXTtx6Z549E48rhou/cDgxGNw4nF94nHdWBMND9nwUCcuo4a3duJhnbiYhhdwGZ8EGh5OYHLi4Z64eI07TiZ4Jw5uY4zbGOM1DN7Jmnti4TYsvMYooHAbCrchcBvXuCeBxgC3McRrDHAbV4FeoIvX6OI1LvEaHbyTDt7JBV5jzW20cE/O1xpN3MZZ4BS30fC/8RoN3z2u457UcRuB0zruWaBZxz2v47YCF3Xcdh23E7is43YDvTruVR23HxjUcYd13Os7ZEDVcUcBq46rA6aOO67j2se4Tn1tsjZxP3PcOuM7jFtHu3WswMito9w6MiDcOteBoVtn4NbpB67cOj23Tjdw6dbpBC4CrcB54Cxw6tZpBE7cOsdunbpbp+4eU3dPqLsN6u5p4CzQpO6eU3dbgQvqbpu62wlcBrrU3R519yrQp+4OqLtD6u71HTKgAiPqrkXd1dRdExhTd23qrhOYBFzqk4DjUrdd6uOAcalrl7oVGLnUlUtdBoRL/TowdKkPXOr9wJVLvRfoutQvXeqdQDvQCpyvHZ+51M9c6qcu9cYdxy71ustx3eWk7tIInNZdzu44r7u07mjXXTqBy0C37tKru1wF+oFh4DogAyowqrtYdRdddzGBcd3FDjh1l0nArbu49UnAwa3buPVxwODWNW7dCoxw6wq3Lu+4Dgxx6wPcej9whVvv4da7gUvceifQDrQC54GzwGmgETjGrdf9b7zjY989rOHWa7jHd5wc4jZquKeHuGeBZg33/BC3dYR7UcO9OMRt19Yua7iXh7jdGm7vEPfqELdfwx0c4Q4OcYdHuOIQV9TWKWu46ghXHeKOarjWIa6u4ZqAfYjr1AKHuJMaE7eG49Zw3ENst8bYrWHcQ4xbQ7tHWG6NkVtj5B6i3COkW0O4hwj3iGv3kGFg4B5y5R5y5R7Rc4/ouodcBjruIW33iAu3Rss9ouUecu7WaLqHnLmHnAYa7iEnbo0T95AT95C6e8She8ihe0jNrVFzD6m5Rxy6dY7cY2ruMTX3hEP3hJrb4NA95cg949BtUnPPOXRbgYtAmyP3kkP3kprbpeb2OHKvOHL71Nw+h+6Amjug5g6puYJDV1BzJTVXUXNH1NwRh67Foas5cg1HruHQHXPo2hy6DjV3sjZxOZy4HDouNcelZrvUTEC71KzAyOVQuRzKtZpwqQ1dDm8NXA77LkdXLodXLrXu2uGlS63jUmu71C4CLZda0+Wo6XJ45lI7dTk6dTlquNROXGrHQdZdDo9cDg9dDmuf1QPHhy4nhy6nhy6NmstZoFlzOa+5nB+6tA5dLmou7ZpLp+ZyebjWrbn0ai5XNZd+YFBzGdZcrgPi0EUeusgjF3XkMjpysQ5d9NGaqbmMD13sQxentjapubiHk4CDe2jj1mzcQ4NbM7hHeu3Qwj0c4dYUbk3i1gTukcA9vF6rDXFrA9yjPu7hFW6th3vYXatd4h52cGtt3MM2bu0Ct9bCPWzh1pq4h2drR6e4h6e4hye4teO1ozru4RHu4SFu7dD/xj068t2DKpNaFbdWxT0K1A9wTqpMTg6YNA5wGgdMTqu4Z1UmzQPc8yru+QFOq4rbqjJpV5l0DnA7VdzLA9zeAZNeFeeqinN1gNuv4g6rTIZVnOEBE1HFFQe4ooojD3DUAZNRlYlVxbWqTEwVd1zFtatM7AMmThXbreJMqownB9iTKmOninYP0G6VkVtlNKmiJlXk5AAxqXIdGEyqDCYHDNwD+u4BfeeAnlOl5x7QnRzQcQ9oTw7oTA64mBzQmhxwPqnSdNfOJlXOJgc03ANOnANOJgccTw6ou1XqkwMOnQNqTpUDt8qBe0B1UqXqVqm6Bxy4NaqTQ6rOEVWnzoFbp+oec+CeUHUbHExOqU6aHLhNDiZNDiYtDpxzDpwLDiZtqpM21cklVadL1e1y4Paouj0O3Cuq7hVVt0/VGXLgDKlOrqlOBNWJpOoqDlzFgTOi6lhUXc2Bazhwxxy4DtWJTXXiUHUmHDgTDsYTquMJVeOuWS7V0Z0cTTgQLgfXDgfXDtXhhGrf4aA/4aDvcnDlctCbrLM7odpxqXYmHLQdqhcO1daE6rlL9XxCtelQPZ1QPZ1wcOpQPXGpnrgcnEyo1idUj1yqhxOqtQnVmstBzeWgOgk4HFYnHFZdjg5c6gcu9arLSdWlETgNnB04NA8czqsureqEi+qE9oFD+2BCp+pyWZ3Qrbr0qhOuqg5X1Qn9qsuw6jKsOoiDCdcHE66rE0R1gjpwGFUdRgcOo+oEqzrBVB1M1cGuTrCrE5yqw+RgwuTAYXJgMzkYM6kaJgcGp6pxqyPc6ojJgcI9kLhVgVO9ZlIVuNUhbnWAezBgUu0HergHXSbVSyYHnbVqG+egzaR6gXvQwq2e41abTA6aTKpnuAcN3IMGk+rJ2sExbvVo7eCQSbWGe3CAWz3wv3FrNd/5UMGpVnA+VHAOKji1Cs7hPs5hBadewTmu4Bzv45xUcBr7OKf7OKeVteY+znkFp1XBuajgXOzjtPdxLu/oVnCu9tcGFZzBPs6wgnO9jyP2ceQ+jqzgqArOaB/HquCYwHgfx97HsSvYToVxwAQsp4JyKihnH+VUkE4F4exzHRg6+wycfa6cCldOhZ6zT8+pcOlUuHT26Tj7XDiVT86dfZrOPmdOhTNnn1Nnn4azz4lT4dipUA8cORUOnQoHToWqU6Hq7FN1KlQC+84HKs4HKk6VfeeAinNAxTmk4hwF6uw7x+w7J1ScRuCUitNk32lScVrsO+dUnBYVp82+06HidKg43UCPfeeKfeeKijMIDNl3rqk4goojA4p9Z8S+Y1FxDBXHsO+MqTj2mu1QsR32xw77xqGiHSqWQ2XksK/WKsphXzjsXzvsDx0qg8BVoBfoOux3HSrtwMXa/rlDpelQOQucru0fO1Ru1R32jxwqtcCBQ6XqsF912P/gsF9xqFQcPlQcqvsOB/sOtX2Hw4rD4b5Dfd/huLJ2csdpxeGs4tCsOJxXHFqBdsXhouLQ2Xe4rDh0A1cVh35guO8wrDhc7zuIfQe576AqDmp/bVRx0PsOpuIwrjjYAWffxqncGuNUDE7FwtkfBRRORQaucfaHgQHO/hVOpXdHF2e/g1Np41QuPttv4VTOcSpNnMpZ4BSncoKzf4xTOcbZr+PsH+FUDnEqtcABTqWKs/8Bp1Lxv5lUKr5TLuO8L+Psl3EqZZwPgWoZp1bGOSzjHAXqZZyTMk7jjtMyTrOMc17GaQUuyjidMs5loFvG6ZVx+mWcQWBYxrku44gyjgyoMs6ojGOVcUxgXMaxy9hOmXHAOGW0U2bklFEB6ZS5vmPolBk4ZfpOmV6g65S5dMp0nDIXgZZT5twp03TKnAYagROnTD1wGKg5ZaqBD06ZilPmfeCdU6b8yTvKzj5lpxL4QNmpUnZqlJ1Dys5RoE7ZOaHsNO44pew0KTvngRZl54Ky06HsXAa6lJ0eZadP2RkEhpSda8qOoOzIgKLsjCg7mrJjAmPKjk3ZdtbGDmXjUNYO5VFAOZSlQ1k4lK8DQ4fywKHcdyj3Al2H8qVDueNQvgi0HMrnDuWmQ/k00AicOJTrgSOH8qFDueZQrgY+OJQrDuV9h/J7h/I7h3J57V3ZYb/sUAl8KDtUyw61ssNh4KjsUC87nJQdGneclh2aZYfzQKvscFF26JQdLgPdskOv7NAvOwwCw7LDdUAGVNlhVHawyg4mMC472GUHp2wHxjhlg1PWOOVRQOGUJU5Z4JSvA0Oc8gCn3Mcp9wJdnPIlTrmDU27jlC9wyi2c8jlOuYlTPsMpn+KUG4ETnPIxTrmOUz7CKR/ilGs45WrgA065glPexym/xym/wymX/W+c9+99+20Ju1zCflfCfl/C3i9hV0rYH0rY1RL2QQm7VsI+DNQDxyXskxJ2o4R9WsI+K2GfBy4C7RJ2p4R9WcLulrB7JeyrEna/hD0IXJewRQlblrBVCXtUwrZK2LqEbUrY4xKOU8K2S4ztEiZg2SVGdglll5B2CWGXGAYGdom+XeLKLtGzS3TtEpfOWtsucWGXaAWadokzu8SpXaJhlzixSxzbJeqBw0DNLnFgl6jaJT7YJSp2iX27xHu7xDu7RNku8dYuUfqkHHhHyX5Pyd6nZH+g5Hyg5FQpOQeU7Bol+5CSXadkHwdOKNmngTNKdpOSfR64oGS3KdkdSvZloEfJvqJk9ynZA0r2kJJ9TckWAUXJHlGyLUq2pmSbwJiSbVMa25SMTUnblCyb0simpAIiMLQpDWxKfZvSlU2pZ1Pq2pQuAx2bUitwblNq2pTObEqngROb0rFNqR44DNRsSgc2pWqgYlPatym9tym9C7y1KZXW3gbelWz2A5WSzYeSTbVkc1CyqZVsDks2RyWbesnmuGRzEjgt2ZyVbJolm/NAq2TTCVyWHC5LNr2SzVXJpl+yGQSuSw6iZCMDqmRjlWx0ycYExiUbu2Rjl8bYJYNd0tglC7s0wi4p7JLELgns0jV2aYhdGmCX+tilq0AXu3QZ6GCX2tilFnbpHLvUxC6dBRrYpRPs0jF2qR44DNSwSweBD9ilCnZpH7v0PvAOu/QWu1Tyv3HevvXt10XsN0XsUhH7bRG7XMR+V8R+X8TeL2JXitgfitjVQK2IfRSoB06K2I0i9mkR+6yI3Sxit4rYF0XsdhG7E+gWsXtF7Ksidr+IPQxcF7FFEVsWsVUR2wroIrYpYttFxnYRE9B2Ecsuouwi0i4iAtd2kYFdpG8XubKL9Owil4GOXaRtF7mwi7TsIud2kaZd5NQu0ggc20XqdpEju8ihXaRmF6kGPthFKoH3dpF3dpGyXeStXaRkF3ljF3ltFyl+8iZQomi/o2iXKdrvA/sU7Q+BKkW7RtE+pGgfUbTrFO1jinYjcErRPuO13aRotyjaFxTtdqBD0e5StHsU7SuKdp+iPaBoDyna1xRtQdGWFG0V0AHDa9tQHNtr2qZoBZRNUQaETfHapjgI9G2KvcBloG1TvLAptmyK5zbFpk3x1KbYCBwH6jbFI5vioU3xwKZYtSl+CFRsiu9tiu9simWb4lubYsmm+Mam+NqmWFx7XbR5U7Qp3/GuaLNftKkUbT4UbaqBWtHmsGhzVLSpF22OizaNwGnR5qxo0yzatIo2F0WbdtGmE+gWbXpFm6uiTb9oMyjaDIs210UbUbSRgVHRxira6KKNKdqMizZ20cYuGuyixi5a2MURdlEFROAauzgIXGEXe4FL7GIHu9jGLl5gF1vYxXPsYhO7eBpoYBePsYt17OJRoIb9+gC7WA1UsIv7gXeBt4ESdvE1drHof+O8eePbey8Zv9plXNxl/GYXU3rJuPSScXkPu7zH+N1LzPs9TGWXcWWPcXUPu7aLXdvDPlwz9T3M8S7mZI9xY5fx6S6m+RLT2sVcvGR8sYt9sYfdeYl9uYfd3cW+2mU82MMevsQe7GEPdxmLXYzaY6z2GI/2MNYeY7PLeLyLMbtos4s13sOydxnZu8jxLtfjXa7HewzHLxnYewzGe1yNX9Id73I53qVtdmmPd7kwL2mZl7TMLudml7PxLqdmjxOzx/F4l+PxLkdmjyOzR228x8F4j+p4j8r4JZXxLu/He7wb7/HOfslbe4+3Zo+S2eW12eWVvUtxvMfL8R6745fsBrk3LrI3fs2uecOuKbE3fsueKbM7fsfueD9QYc+usmcf8NKusWsfsmsfsTc+Zm98wu74hF37lD37jF27ye64xd74gr1xi93xBXvjDrvjbqDHrumzOx6wZ67ZNdfsGsGuLdkbK/bGI16ONS/Hmr2xYXdss6dtdq0xu5ZhVxn2xJg9adi9HrM7tNkdjHnZH7N3NWavN2a3a7N7abPbHq+1bHbPA2eGvdMxuyeG3eO1vbrN3pHN7qHNbs1m98Bm98OY3cqY3f0xu+/H7L6z2Xs75mVpzN6bMbtvbHZfj9l7NWZ3b8ze7pi9XZuXu2OKe2NevzS8eTmmtGdT2rV5uzvm/d6Y/d0xlV2byq5Ndc/mYM9Q2x1zuDvmaHfM8d6Yk5djTnZtGrtjzl6Oab4c09odc/FyTHvPpr27drln09216e2O6e+OGe6ZtZeG670xcneM3Bsz2jNYewa9axjvjjF7hvGuYbxrBUaM9yTjPYG9d814d8h4d4C928fevcLe7WLvXgY62LttxrstzG6L8W4Te++M8e4p472Ttd1j7N0j7L0j7Jc17L0a9l4Vs/uB8W4Fe/c94913mN13mN0y49232Lsl7N032LuvMbtFzO5LzO6e/4396pVvXuxgdncwe9uYV9uY4g7m9Q6mdMe7Hcz7bcz+DqayjaluY6o7mIMdTG0Hc7SDOdrGHO9gTrYxjW3M2Q6muY1p7mDOtzEX25j2Dqazg7ncxnS3Mb0dTD8w2MZcb2PEDkbuYNQOZrSD0dsYs6bNNpbZYWS2UWYbYbYZmm2GZoeB2aEfuDQ7XJptOmabttnmwmxzbnZomh2aZptG4MTscGx2qJttjsw2NbNDzWxTNTtUzQ4fzA77Zof3Zpuy2eFt4I3Z4bXZoWi2eWm22TXb7JodXphtts1O4AU7Zo9t85Jt84pt85od84YdU2LblNg279g279k279kx++yYCjumyrY5YNscsmMO2TZHbJs62+aYbdNg25yxbZrsmCY75pxt02LHtNk2HbbNJdumy7bpsW367JgB22bItrlm2wh2jGTHKHbMiB1jsW0028awow3blmF7ZNhWhh1h2L42bA8N2wPDdt+wc2XY6Rp2Lg3bncCFYfvcsN00bJ8Ztk8N2w3Dzolh+9iwXTdsHxp2Dg07NcP2gWG7atj+YNjeN2y/N2y/M2yXDdtvDDtvDDuvDTuvDNuBnT3D9gvDzo5hZ9uwu214ubP2asdQ3DG83l57u2Mob6/tbxv2dwyVbUN123CwbahtGw53DEc7huNtw8m2obFjONsxNLcN5zuG1o6htW1obxvaO4bLbUN329DbNvR31oYBsW1QO2ujHYPeXjM7GrOtMdsWZnuE2VaYHYHZuV7bHmK2B5jtK8x2N9DBbLcDLcz2OWa7idk+w+w0MNsnmJ1jzE4ds32E2T7E7NQwO9XAB8x2BbP9DrNdxmy/veM1ZrsYeIXZ3sNsv8Bsv/C/Ge++8E3hGWa7gNl5htl9htkrrL18hik+w7x5hikV1t4WMO8KmP0CplLAVJ5hPjzDHBQwhwXMUQFTf4Y5eYY5KWBOA80CpvUMc/EM0w5cPsN0C5irAqYfGBYwooCRBYwqYEYFjFXAmAI6YJkCyhSQpsC1KTA0BQamQN8UuDIFuqbApSnQMQXapsCFeca5KdA0Bc5MgVNToGEKHJsCdVPgyDzj0DyjZgpUTYEPpkDFFNg3Bd6bAmXzjLemQMkUeGMKvDYFXpkCL02BPfOMXfOMHVPgeaDwyXMKZoeC2aVg9gIvKZgiBfPmjrc8M+94ZvYpmH2emQrPzAcK5oCCOQwcUTDHFEyDgjkNnFEwTQqmRcG0Ax0KpkvBXFEw/cCQgrmmYCQFoyiYEQVjUTCagjZrlqGgDAVpKFwHBoZC3/DsylDoBjqGQttQuDAUzg2FpqFwZig0AseGQt1QODIUDg2FA0Phg6FQCewbCu8NhbKh8NZQKBkKbwyFouHZK0PhpaGwZ3j2wvBsx1DYNhSeGwqFtecFw3bBsBvYKxheFgyvnhneFNZKBcPbguFdwbAfqBQMH54ZDgqGw8BRwXBcMDSeGU4La2cFw3nB0CoYLgqGdsFwWTB0C4ZewdAPDJ8Zrp8ZZMGgCoZRwWAVDLpgMAUdsDAFhSlITOF67dkA86yPKVxhCt1AB1NoYwoXmMI5ptDEFM4whVPMswamcIwp1DGFI0zhEFOoYQpVTOEDplDBFPYxhfeYQhlTeIsplDCFN5hCEVN4hSm8xBT2MIVdTGEHU9jGFJ5jCgX/m/HOjm+e5jGFp5jnecx2HvMij9nNY14GinnMm0ApjynnMe/ymPd5zH4eU8ljqnlMLY85DBzlMcd5TCOPOc1jzvKY8zymlcdc5DHtPKaTx1zmMb2nmKs8pp/HDPKYYR4j8hiVx4zyGOspxjxFmzyWyWOZpyiTR5o81ybP0OQZmDx9k6dn8nRNnkuTp23yXJg8LZOnafKcmTynJk/D5Dk2eeomz5HJc2jyHJg8HwIVk+e9yfPO5CmbPCWT543J89rkKZo8L02ePZNn1+R5YZ6yY/JsmzwFk+eZyZM3efLmKXnzjLx5Tt5skzcvyJtdnppd8uYleVMkb14HSuRNmbx5R968J2/2eWoq5E2VvKmRN4fkzSFPzRF5c0zeNMib00CTp6ZF3lyQN23ypkPeXJI3PfLmirzpkzcD8uaavJHkjSJvRuSNRd5onmpD3jLkR4a8MuSlIX9tyA8N+YEhf2XI9wxPLw35S0O+bchfGPItQ75pyJ8Znp4a8g1D/sTwtG7IHxnyh4Z8zZD/EKgY8u8NT98ZnpYN+ZIh/8aQf214WjQ8fWXI7xnyu4b8C0N+x5B/bnj63JB/Zsg/NTzNG57lDc/zhu2nhhd5w25gL28o5g2v84Y3eUMpbyjnDe/yhvd5w37eUMkbqnnDQd5QyxsO84Z63nCSNzTyhtNA86mhlTdc5A3tvKGTN1zmDb2nhqu8oZ83DPKGYd4g8gaZN6i8YZQ36LzB5A0mb2HyI0xeYfISk7/G5IeY/ACTv8Lke5h8F5O/xOTbmPwFJt/C5M8x+SYmfxo4weTrmPwRJn+IeVrDPD3A5D9g8hVM/j0m/y5QxuRLmPxrTL6Iyb/C5Pcw+V1M/gUmv4PJb2PyzzH5Z5j8U/+b8bNnvn6SQ+dz6Kc59LMcupBDP8+ht3PoFzn0Xg79Mod5lUMXc+g3OXQphy4H3ufQlRy6mkMfBGo59FEOfZxDn+TQjRz6NIdu5tDnOfRFDt3OoTs5dDeH7uXQVzn0IIe+zqFFDi1zaJVDj3JoncPSOUY6h9I5pM4hTI6hzjHQOfo6x5XO0dM5LnWOts5xoXO0dI5zk+NM5zjVOU50jmOdo65zHOocNZ3jQOeo6hwVk+O9zvFO5yjrHCWd443OUdQ5XukcL3WOPZ1jV+fY0Tm2dY6CzvFM53iqc/yhczzROXI6R04/IafzgWfkdIGceU7ObJPTu+T0Hjn9kpx+RU4Xyek35HSJnH5LTr8L7JPTH8jpKjl9QE4fktN1cvqYnD4hpxvk9Ck53SSnz8npC3K6TU53yOkuOd0jp/vk9ICcHpLTgpyR5Iwip0fktEVOa3KWJqc0OanJCU1uqMkNNLm+JnelyfU0uUtNrqPJXWhyLU3uXJM70+RONbkTTe5Yk6trcoeaXM2QO9DkqppcRZN7b8i9M+TKmlxJk3ujyRU1uVea3EtNbk+Te6HJ7Why25pcwZB7psk91eT+0OSeaHI5zZOc5o+cJp/TPM1pCjnN85xmO6fZyRn2cpq9nOZlTlPMad7kNKWc5m1O8y6wn9N8yGmqOc1BTlPLaQ5zmuOc5iSnaeQ0pzlNM6c5z2kucpp2TtPJabo5TS+nucpp+jnNIKe5zmlkTqNymlFOY+U0OqfROQudU+icROcEOjdE5wboXB+du0LneuhcF53roHNtdO4CnTtH587QuVN07gSdO0bn6pjcISZXw+QO0LkqOvcBndtH596hc2V0roTOvQkU0bmX6NweOvcCk9tB57bRuQI69wyde4rO5dG5P9C5nP+Nyed9/XsW/TiLfpJF57Pop1n0syy6kEVvZ9EvsujdLHovi36VRRez6NdZdCnwLot+n0VXsugPgWoWXcuij7LoehZ9nEWfZNGnWXQzi24FLrLoThbdy6K7WfRVFj3IoodZ9HUWLbNolUWPsox0FqWzSJ1F6CzXOstAZ7nSWXo6S1dnudRZ2jrLhc5yrrM0dZbTwInOUtdZjnSWQ53lQGep6iwfdJaKzvJeZynrLG91lpLO8lpnKeosL3WWPZ1lV2fZCTzXWZ7pLE91lrzOktNZHussv+ssWZ0lq38nqx+T1U/I6j/I6jxZ/YysLpDV22T1Dln9gqzeI6tfktVFsvoNWV0iq98G3pHV+2R1haz+QFYfkNWHZPURWV0nq4/J6hOyukFWN8nqc7L6ItAhq7tkdY+sviKrB2T1NVktAoqsHpG19JrUZIUme63JDjTZK022p8l2NdlLTbatyV5osueabFOTPdNkTzXZhiZb12SPNNlDTfYg8EGTrWiy7zXZsib7VpMtabKvNdmiJvtKk32pye5psi802W1NtqDJPtNkn2qyeU32D002p8n+rslm1x5nNbmsJh94mtUUsprtrGYnq3mR1exmNS+zmmJW8zqrKWU1bwPvspr9rKaS1XzIaqpZzUFWc5jVHAdOsprTrOYsqznPalpZzUVW085quoGrrGaQ1QyzGhEYBXTWQmdH6KwMCHR2gM5eobM9dLaLzl6isx109gKdbaGz5+jsGTrbQGdP0Nk6OnuEzh4GDtDZD+hsJfAOnX2LzpbQ2dfobDHwEp3dRWdfoLPb6GwBnX2Gzj5FZ/Po7B/o7BN09jE6+7v/jXnyxNe/ZbB+y6B/z6BzGfQfa9bTDLqQwXqewdrOYL3IoHczWK8yWMU1/SaDfptBvwvsZ7AqGawPGfRBBn2YQR9l0PUM1nEG3chgnWWwmhms8wxWK4NuZ9CXGXQ3g77KoAcZ9DCDvs6gZQatMmgrw0hnkNbatZVhqDP0rQy9wKWVoWNluLAytKwMTSvDmZXhVGc4sTLUrQxHVoZDK0NNZ6jqDBWdYV9neKczlHWGkpXhtZWhGHhlZdizMrywMuxYGZ7rtWdWhj8COStD1sqQ1Rl+szI8sjJkrEdkdJaM9TsZ/ZiM/oOMzpPRz8hYz8joAhm9Q0a/IKN3yeg9MtYrMlaRjPWGjFUio8tk9Dsyep+MVSFjfSBjHZCxamSsIzJWnYw+JmM1yFhnZKwmGd1as9pkrEsyVpeM1SNj9clYQzLWNRlLkLEkGctaG2kyUpO51mSGmsxAk7nSZHoWmUuLTMcic2GRObfINC0yZ5rMqSZzYpGpW2SOLDKHFpmaRaZqkalYZPYtMu8tMmWLTMki89oiUwy8tMjsaTIvLDI7FpnnFpmngbxF5g9NJqfJPNZkfrfI/GaRyVhkMppsRvM4Y5HLWDzJWPyR0TwLFDKanYzFi4zFbkbzMqMpBt5kNKWMphzYz2gqGc2HjOYgo6llNEcZTT1jcZyxaGQsTjMWzYzmPKNpZTTtjMVlxqKb0fQymn5GM8xYXGcsREYjM5pRRmNlLKzMCCsjsTLX6MwQK9PHylxhZXpYmUusTAcr08bKtNCZJlbmDCtzipVpYGWO0ZkjrMwhVqaGzhygMx/QmX2szHuszHt0poTOvA4U0ZlXWJmXWJlddGYbK7ONlXmGlcljZf7AyvyBzuTQmcdYmd+xMr+hM4/8b0w26+utTaxft9CPttC/b6Efb6Jzm1hPNtH5TaxnW1iFTaztTfSLTazdLayXa7q4hX69iS5toctb6HdbWPtbWB+20NUtdG0LXdtEH21i1bfQJ1tYp1tYZ1tYzU2s1ia6vYm+3ERfbqF7W+irTfRgE329hRabaLWJtjYZ6U2ktYmwNrm2thjoTa6sTXrWJl1rk461xYW1RcvapGltcWZt0bC2ONab1K1NjqwtDq0tatYWVb1FRW+yrzd5rzcp6y3e6k3eWJsUrU1eWZvsWZvsWlvsWFtsW1sUrC2e6S2e6i3+sDZ5Ym3x2Nrid2uL36wtMnqLX60ttqxNNq1NNnWGLesRWzrLln7Mls6xpZ+waeXZ1M/Y1AU29XO29A6bepdN62XgFZvWazZ1iS1dZlO/Y9N6z6ZVYcuqsmXV2LRqbFpHbOo6m9YJm1aDTeuMTd1kU1+waV2waXXYtDpsWT22rCu2rAFb1jWblmDTkmxZFlsjiy2l2RKazWvN5lCz1ddsXWm2uhZbHYuttsVWy2Lr3GLrzGLrVLPZ0GwdW2zVLbYOLbZqFltVi82Kxda+xeZ7i82yxWbJYut14JXF1kuLrV2LrR3N1rbF1nOLrWcWW08ttv6w2HpisfVYs/W7Zus3zWbGYvNXi80ti80tza9bmkdbFr8HcluaJ5ua/Kbm6Zbm2aZFYdNie1PzYkuzt6V5uaV5taV5vaUpbWrebmnKW5p3m5r9Tc2HTc3Bpqa2qTncsqhvWZxsWjS2LE63NM1NTWtT09606GxZXG5qupuaq03NYMtiuGUhtjRyU6M2NdbmCGtTYW1KrM1r9OYQa7OPtXmFtdnF2rrE2mpjbbWwts7RW2dYm6dYmw2srWOszTp68xBrq4a1VUVvVtBb++jN91ibZaytEnrrdeAVemsPvbWLtbWDtbWN3nyOtfkMa/Mp1tYfWFtPsDYfozez6K3fsDYfYW39it7c8r/Rjx75o5/TWJtprF/TWI/SWL+lGWXTjH5PY+XSWH+kGT1NM3qWxnqextpJY71IY+2lsV6msV6lsV6nsUpprLdprHdprP00ViWN9SGNdZDGOkxj1dNYx2msRhrrNI11lsY6T2NdpLE6a6NumlEvjdVPY12nGYk0I5nGUmmUlUZaaYSVZmilGVhprqw0XStNJ9Cy0pxbaZpWmtNRmpNRmuNRmiMrzaGV5sBKU7XS7Ftp3ltp3llpylaakpXmjZWmOErzcpRmb5Rmd5Rmx0rzfJTmmZUmb6X5w0rzxErz2Erz+yjNb6M0j0ZptkZpNkdpfrbW0tbPpK1fSI+2SFsZ0qNHpEdZ0tbvpEc50tYT0tZT0qNnpEfPSY92SFs7pK1d0tYeaesVaes16dEb0tZb0laZtPWetFUhbVVJWwekrRpp64i0dUzaapC2TkmPzkhbTdKjFulRZ83qkrZ6pK0+6dGQ9OiatCVIj0ZrakRaWKSvR6QHI9JXI9K9EenLEemORfrCIn1ukW5apE8t0g2L9PGI9JFF+tAifTAiXR2R/jAivW+RfmeRLluk31qk31ikixbplxbpPYv07oj0zoj0c4v0sxHppyPS+RHpJxbpxxbprEX6N4v0oxHpX0ekNy3Sv1ikfx7xc3rEZnrEr2mLTNrit7RFNm3xOG3xJG3xR9oinx7xND3ieXrETtriRdpiLz3iZXrEq7RFMW1RSlu8TY8op0e8T1vsp0dU0xYHaYta2uIwbXGUHnGcHtFIW5ymLZppi/O0RTttcRnopS36aYth2uI6bSHTFiptYaUVo7RilBZY6SGj9AArfYWV7mGlL7HSbaz0BaN0i1G6iZU+xUo3sNInWOk6VrqGlT7ASlcZpT9gpStY6feM0mVG6RKj9Bus9Gus9Cus9B5W+gWj9A6j9DZWuoCVfoqV/oNR+gmjdI5R+ndG6Syj9CNG6V8ZpbcYpX/BSv/sf6O3tvzRjw+xfnqI9XMKazOFtZVilEkxepRilE0xyqUYPUkxyqewnj3EKjzE2k5h7aSwXqSw9lKMXqWwXqew3qSw3qawyilG71NYlRTWhxTWQQqr9pDR0UNGxylGjRSjsxSj8xSjVgrrIoXVSTHqphhdpbD6KUbDFKPrFCOZYjR6iLJSSCuFsFIMrBRXVoqeleLSStGxUlxYDzm3HtK0UpyOUjRGKU5GKeqjFIejFDUrRdVKUbFS7I9SvLNSvLVSlKyHvLEeUrRSvByl2B2leDF6yPboIQUrxdPRQ/6wUjyxUuSsFL+PUvw2SvFolOLX0UM2Rw/5ZZQiPUrx4yhFapQiNfqRlPUTqdHPpEabpEZbpEYZUqNHpEZZUtZjUlaOh6M/eDh6SmpUIGU9J2XtkLJekLL2SI1ekRq9JjV6Q8p6S8oq89B6z0OrQsr6QMo64OGoRso6JGUdk7IapEanpKwmqVGLh6M2qVGH1OiS1KhHatQnNRrwcHTNQ0uQGo1IqREpOeKhGPFwOOJhf0SqNyLVHZG6HJFqW6RaFqmmRerMItWwSJ1YpOojUocWqZrFw+qIhx9GpPZHpN6NSL0dkSqNePjG4mHRIvVyRGp3RGpnRGp7RKowIvV0RCo/IvVkRCo34uHvFqnfRqQejUj9OiK1OSL1y4hU2uLhTxap1IgfUyN+So1Ipyw2UxZbKYvMQ4tHDy2yKYvHKYtcasQfqRH51IhnKYvnKYud1IgXqRF7Dy1epUa8To14kxrx9uGI8kOLd6kRlYcjPjwccfBwRC1lcZQacZIa0UiNOE1ZnD20OE9ZtFMWnZTFZcqil7LoP7QYpCyuUyNEaoRMWYweKkYpySglsFJDRqk+VqqHleoySl0ySrWxHrYYpZqMUmeMUg2shydYD+tYqUOsVI1Rqsoo9YFRah8r9Q7r4VtGqRKj1BtGqSJW6iVWahcr9QIrtc3oYYHRw6dYqTxW6glWKsco9Tuj1G+MUo8YpX5llNpklPqFUepnRqmfGKVS/jfWzz/76ock6sck6qck6uck6pckaiuJ+jWJepRE/Z5EPU6iniRR+STqaRJVSKKeJ1HbSdSLJOplElUMlJKot0lUOYl6n0TtJ1EfkqhqElVLoo6SqOMkqpFEnSZRzSTqInCZRHWTqKskqp9EXSdRIolSSaRKIlSSoUrSV0l6gUuVpKOStFWSc5XkTCVpqCTHKkldJTlUSWoqSVUlqQTeqSRvVZKSSvJaJSmqJC9Vkl2VZEcl2VZJCirJU5XkD5Ukp5I8Vkl+V0l+U0kyKsmWSrKpkvyskvykkqRUkocqSVIlSaofSKqHJNWPJNXPJNUvJNUmSfUrSfWIpMqSVL+TVDmS6g+S6ilJVSCpnpNU2yTVC5Jqj6R6RVIVSao3JNVbkuodSbUf+EBSVUmqGklVJ6mOSaoGSXVGUjVJqhZJdUFSdUiqLkl1RVL1SaprkkqQVJKkVCSFInmtSA4Vyb4ieaVIXiqSbUXyQpE8D5wqkieKZF2RPFIka4rkgSJZUST3Fcl3imRZkSwpkq8VyaIi+VKR3FUkdxTJ54pkQZF8qkj+oUg+USQfK5K/K5K/KZIZRfJXRXJTkfxZkfxJkUwpkj8okknFD0nFw6Tix6Ti56Til6RiM6n4Nal4lFRkk4rfk4pcUvEkqcgnFYWk4nlSsZ1UvEgq9pKKV0lFMal4k1SUkoq3ScX7pGI/qfiQVBwkFYdJxVFScZxUnCQVZ0lFM6k4Tyoukop2UtFNKq6Sin5ScR2QSYVKKlRSoJLXqOQAleyjkleo5CUq2UYlL1DJ88AZKnmCStZRySNUsoZKHqCSFVRyH5V8h0q+RSVLqORrVLKISr5EJXdRyR1UchuVLKCST1HJP1DJJ6jkY1Tyd1TyN1Qyg0r+ikpuopI/o5I/oZIpVPIhKpn0v7F+/NFX3yVQyQTqYQL1YwKVTqB+TqB++Q71awL16DvUbwnU7wlULoF6kkDlE6inCdTzBGongXqRQO0lUK8SqNcJVCmBeptAvUug3idQlQTqQwJ1kEAdJVD1BOokgWokUGcJ1HkC1UqgOgnUZQLVS6D636EGCdR1AiUSSJVAqARDlaCvElypBF2VoK0SXKgE5yrBmUpwqhKcqATHKsGRSlBTCaoqQUUl2FcJ3qsEb1WCNyrBa5XglUqwpxK8UAl2VILnKsEzlSCvEjxRCR6rBNnAI5XgV5VgU33HzypBWiX4USV4qBIkVYLvVYKESpBQ35FQ3/OdekhC/ch36icSKk1CbZJQv5JQGRLqEQmVJaEek1BPSKg8CfWUhCqQUDsk1AsSao+EekVCFUmoNyTUWxLqHQn1noSqkFAfSKgDEuqQhKqTUMckVIOEOiOhzkmoFgnVJqEuSageCdUnoQYk1DXfKUFCKhJCkRgqEn1F4krxXVeR6CgSF4pES5FoKhKniu9OFIljReJIkagpvqsqEhVFYl+ReK9IlBWJN4rEa0XilSKxp0i8UCR2FInnikRBkcgrEn8oEjlF4ndF4jfFd78pvssoEluKxM+KRFqR+EmReKhIJBWJ7xSJhOK7hOL7hOKHhOLHhOKnhOLnhGIzofg1ocgkFI8Sit8TiscJxZOE4o+E4mlCUUgothOKFwnFbkLxKqEoJhSvE4pSQvEuoXifUFQSig8JxUFCcZRQ1BOK44SikVCcJRTnCUUroWgnFJffKXoJxVVCMUgorhMKkVCohEIlBOq7ISrRRyWuUIkuKtFBJS5QiRYq0UQlzlCJBipxjEocoRI1VKKKSnxAJfZRifeoRBmVKKESr1GJV6jEHirxApXYQSWeoxLPUIl8IIdK/I5K/BbIoBJbqMQvqEQalfgJlXiISiRRie9Rie/8b6yHD331bRz1XRz1fRz1Qxz1MI76KY76OY76JY76NY7KxFG/xVG/x1GP46gncdQfcdSzOKoQR23HUS/iqN046lUcVYyj3sRR5cB+HFWJo6px1GEcdRRHncRRp3HUWRx1Hke14qh2HNWJo7px1CAg4igVR6o4QsUZqjhXKk5PxblUcdoqzoWK01RxzlSchopzrOLUVZyainOg4nxQcfZVnHcqTlnFKak4r1WcVyrOnorzQsXZUXGeqzjPVJy8ivNExXms4mRVnN9UnIyKs6Xi/KLipFWcn1SclIrzg4rzvYqTUHG+VXHiKk5cfUtc/Ye4+o64+oG4ekhc/UhcpYmrX4irLeLqV+LqEXGVJa4eE1dPiKs/iKunxFWBuNomrl4QV3vE1UviqkhcvSGuysTVO+LqPXFVIa6qxNUhcXVEXNWJqwZxdUZcnRNXLeKqTVx1iKsucdUnrgbE1TVxJYlLRVwo4kNFvK+I9xTxS0W8o4hfKOLniviZIt5QxI8V8boifqiIHyjiHxTxfUX8nSJeVsRLivhrRfyVIr6niL9QxHcU8eeK+DNFPK+IP1HEHyviWUX8N0U8o4hvKeK/KOJpRfwnRTyliD9UxL9XxBOK+LeKeFzxbVzxn7jiu7jih7jiYVzxY1zxU1zxc1yxFVf8Glc8iiuyccXvccWTuOKPuOJpXFGIK7bjihdxxV5c8TKuKMYVb+KKt3FFOa54H1dU4opqXHEYVxzFFfW44iSuOI0rmnFFK65oxxWduKIbV1zFFYO44jqukHGFiitUXKDiQ1S8j4r3UPFLVLyDil+g4ueo+Bkq3kDFj1HxOip+iIofoOIfUPEKKv4OFS+j4iVU/DUq/goV30PFX6DiO6j4c1T8GSqeR8WfoOKPUfEsKv4bKp5BxX9FxX9BxdOo+E+oeAoV/wEV/x4VT6Di36Licf8b6/vvfPWvDdS3G6j/bKC+20AlH6AePkD9uIFKb6B+2UBtbqB+3UA92kBlN1CPN1BPNlB/bKCebaCeP0Btb6BebKB2H6BePUC93kCVNlDlDdS7B6j9B6gPD1C1DdThBupoA3WygTrdQDU3UOcbqIsNVHsDdbmButpADR6ghg9Q8gFSbSDUBgO1wdVog67aoKM2aKsNWmqDptrgVD3gRD2grh5wqDY4UBtU1QYV9YD3aoOy2qCkNnijNiiqDfbUBi/UBjtqg4J6wDO1QV5t8ERtkFMb/K42+E1t8KvaYFNt8Mtog5/UBj+qDR6qDZLqAd+rB/xHbRBXG/xLbfBAbfBA/YsHKs6G+g8b6jseqCQP1A9sqBQb6ic21C9sqE021K9sqEc8UFkeqMdsqBwP1B88UE/ZUAU21A4b6gUbapcN9YoH6jUbqsSGKrOh3rGh9tlQH9hQBzxQhzxQdR6oYzZUgweqyQN1zgN1wcaozYa6ZENdsaH6bKghG0qwIRQb1yM2BooHfcWDnuLBpWKjrdhoKTaaio1TxcaJYqOueHCoeFBTPKgqNiqKjfeKjbJi461i47Vi45ViY0+x8UKxsaPYeK7YeKZ4kFc8eKJ4kFNsZBUPHik2MoqNLcXGL4oHacXGj4qNh4qNHxQb3ys2EoqNbxUb/1JsPFD8a0MR31D8Z0Px3QPF9w8UPzxQpB4oftpQ/LKh2NxQbG0oHm0oshuKxxuK3IbiyQPF0weKwobi+QPFiweKvQeKVxuK1xuK0gPF2weKdw8U+w8UHzYU1Q3F4Ybi6IHi5IHidENxtqE431BcbCjaG4rLB4qrDUV/QzHcUMgNhdqQqA2B2higNvqoBz3UxiVqo43auEBtnKM2TlEPTlAP6qgHh6iNGmqjitr4gNrYR22UURtvURuvURuvUBsvURsvUBs7qAfPURsF1MZT1MYT1IMc6kEWtfEI9SCDerCF2vgFtZFGbfyI2kihNn5AbXyPepBAPfgWtfFv1MYD/5vRt9/68n4M+SCG/FcM+W0MmbiP/D6GTMaQD2PIn2LIdAy5GUP+GkM9iiGzMeTvMeSTGPKPGPLpfeTzGHI7hnxxH7kXQ72KIV/HkKUYsnwf9f4+snIfeRBD1mLIwxjyOIZsxJBnMeR5DHkRQ7ZjyMsY8iqGHNxHDu8jxX2EinEtYwxkjJ6M0ZUxOirGhYxxLmOcyRin8j7HMsaRvE9NxqjKGB9kjH15n7KM8VbGeC1jFFWMlzLGCxljW8Z4LmM8k/fJyxhPZIzHMkZWxngkY/wqY2zKGD/LGGkVIyVj/CBjfC9jJOR9/iNj/FvGeCBj/FPFuK9i3Ff3iakNYvJfxOS33Jf/ISa/IyaTxORDYvInYvIXYuoXYnKLmMpwX2aJyd+5L3PEZJ6YfEZMPiemtonJF8TkHvflK2LyNTFZIibLxOR7YrJCTB5wXx4Sk0fcl8fEZIP78oz78pyYbBGTbWLykpi8Iib7xOSQmLwmJiSxa0VsoLh/JYl1Jfc7itiFJHYuiTUlsVNJ7EQRO1LcP1TcP5DEPkhi+5LYO0nsrSL2RhJ7JYntSWIvJLEdSey5JPZMEstL7j+R3H8siWUlsUeK+79KYpuS2M+K2E+SWEpy/wdJ7HtJLCGJ/UcS+7ck9kAS+6ckdl/yz5hkIyb5V0zy7X3Jd/cl38UkyZjk4X3JTzFJOib5JSbZikkyMUk2Jvk9Jsndl/wRkzyNSZ7fl2zfl+zEJHsxyauY4vV9RSkmKd+XvL8vqcQk1ZikFpMc3pccxySNmOQsJjmPSVoxSTsmubwv6cUk/ZhkGJOImETGJDJ2jYwNkLE+MtZF3u8gY21krIWMNZGxBvL+MfJ+HRmrIWMHyNgHZGwfGXuHjL1Fxt4gY0Vk7CUytouM7aBi26jYM+T9PDL2BBV7jIxlkbFHqFgGeX8LGfsZGfsJGfsRGUshY98jYwlk7D/IWBx5/1/I2AYydt//ZvSvf/ny71Hk/ShyI4r8dxQZjyITUeR3UWQyikxFkT9GkT9Hkb9EkVtR5KMo8rco8nEUmYsi/4gin0WRz6PInSjyRRS5F0W+jiLfRJFvo6hyFLkfRVYDtSjyKIo8jiIbUeTZ35GtKLIdRV5Gkb0osh9FDqPI6yjXMspQRunLKF0ZpSOjtGWUlozSlFHOZJSGjFKXUQ5llJqMUpVR9mWUdzJKSUZ5I6MUZZQ9GWVXRtmRUZ7LKAUZJS+jPJFRHssoWRnlkYzyq4yyKaP8LKP8JKOkZJQfZJTvZZSEjPKtjPJvGeWBjPJPGeUfMkpURonKvxOV94nKDaLyX0RlnKj8D1H5HVGZ5O8yRVT+yN9lmqj8hajcIiofEZW/EZW/E5VPiMo8UfmMqCwQldtE5Qui8iVRWSQqXxOVJaKyTFS+Jyo/8HdZIyprROURUXlMVDaIyjOi8pyobBOVl0Rll7/LK6JyQFReE72WRIeSaF8SvZJELyV/b0v+3pJEzyXRM0n0RBKtS6I1SfRAEv0gie5Lou8k0bIk+kYSfS2JvpREXwSeS6LPJNG8JPpE8vfHkmhWEn0kif4qiW5Koj9Loj9JoilJ9AdJ9HtJNCGJfiuJ/lsSfSCJ/lMS/YckGpX8PSq5H5VsRCX/jkriUcl/opLvopJkVJL6u+THqCQdlfwSlWxFJY+ikt+ikt+jklxU8kdU8iwqeR7YjUpe/l3yKip5E5W8jUreRSX7UUklKqlGJbWo5CgqOY5KTqOSs6jkPCq5iEo6UUkvKrmKSgZRyXVUIqMCGb1GRvvI6BUyeomMtpF/v0BGm8joKTJ6gvx7HRk9QkZryOgHZHQf+fd3yOhbZPQNMvoaGX2FjO4hoy+Q0W1UtICM5pHRJ8joY2Q0i4z+hoz+ioxuIqM/I6M/IaMpZPQHZPR7ZDSBjH6LjP4bGX2AjP4TGY0ho1H/G/XPf/ribxHEPyKI+xHERgTxIIKIRxD/iSC+iyCSEUQqgvgpgkhHED9HEFsRxKMI4rcI8vcI4kkEkY8gnkUQzyOInQhyN4J8FUEWI4g3EcTbCOJdBFGJID5EEAcRxGEEcRJBNCKIs78hzyPIVgTRjiB6EcRVBDGIIK4jXIsIAxGhLyL0RISOiHAhIpyLCGciQkNEOBYRjkSEmohQFRE+yAjvRYSyiPBGRHgtIrwSEXZFhB0Z4bmMUJARnsoIT0SExyJCVkR4JCJsiQi/iAhpEeFHEeGhiJCUEb4XERIiQlxE+JeIsCEixESEf4gI/0dEiIgIEfF/iIh/EBH3iYh/EhH/IiL+TUT+h4j4jr+JJBHxA38TKSLiJyLyZyJii4j4lYh4RET8TkTkiIg8EfGMiCgQETtExC4R8ZKIKBIRr4mIt0TEOyJin7+JKhFxQEQcEhF1IuKEiDgjIs6JyBYR0SEiuvxNXBERAyLimsi1IDIURPqCSE8QuRT8rS3427kgciaInEoix4LIkSBSE0SqgkhFEHkviJQlkZIk8loSeSWJ7AoiO4LItiBSkESeCiJ/CCI5wd+ygsgjQWRLEPlFEPlZEPlJEEkJIj8IIt8LIglBJC6I/EsQ2RBE7gsi/5BE/o8kEhH8LSL4e0QQiwg2IoIHEcG/I4JvI4LvIpLk3wQPI4IfI5J0RPBzRLAVEfwakTyKCLIRQS4i+CMieBaRFCKC7Yhg92+ClxFBMSJ4ExG8jQjKEcF+RPAhIjmISA4jknpEcBIRnEYkzYjkIiLpRASXEcFVRDCICK4jAhERiMgQEekjIj1E5BIRaSP+1kJEzhCRBiJyjPjbESJygIxUkZEKIvIe8bcyIvIGEXmNiBQRkT1k5AUiso2IPENEniIjTxCRHCKSRUQeISK/IiK/ICJpRORHRCSFiPyAiHyPiPwHEYkjIv9GRDYQkfuIyD8QkSgiEvG/Uf/4hy/uhRF/CyOiYcQ/woj79xAPwoh/hxHfhhGJe4jvwogfwohUGPFTGPFLGLEZRmTCyN/CiN/DiCdhxB9hxLN7iOdh5E4YuRdGvgwjXocRpTCiHEa8DyMqYcTBPUQtjDgKIxphROMe8jyMbN1DtMOIXhhxdQ8xCCOuwwgRZiDu0RdhLkWYjghzIcKciTCnIkxDhKmLMIciTFWEqYgw72WYdyLMWxHmtbjHK3GPXXGPFyLMtgzzTN4jL+/xRIZ5LML8JsJkRJhfRZhfRJi0CPOjCPODCJMUYb6TYb4V9/i3CPNAhLkvwvxDhPm7uMffRJj/LcKERZiwuEdY/I17Iso98Q/CIkZYPCAs/0VYxAmLBGHxPWHxA2GRIix/4p74hXtik3viV+6J3wiL37knnhAWfxAWTwmLAvfENvfELvfES8KiSFi8ISzeEhbvCYsKYVElLGqExRFhcUJYNAiLM8KyRVi0CYsuYdEjLPqExZDwteDeUHDvSnCvKwh3BOELQbgpCJ8Jwg1JuC4IHwruHQjuVQT33gvCZUG4JAm/loRfSe7tSe7tCMLPBfeeCcJ5SfiJIPxYEP5NEH4kCG8J7v0iuJcW3PtRcO8HQfh7QTghCP9HEP634N4DQfi+IBwThKOC8N8k4f8tCd8T3LsniNwT/J97gn+EBffDgo2w4N9hwbdhyXdhwXdhwQ9hSSos+Ome4JewYCss+TUseBQW/B4WPLkn+CMseRoWPA8LdsKCvbDgZVhQDAvehAVv7wnehwX7YcmHsKQWltTDgpOw4DQsaYYlrbCkfU9weU/QCwv6YcHwnkCEh4jwABHuI8I9RLiDCF8g7p0jwmeIcAMRriPuHSLuHSDDFWR4HxEuI8IlRPgNIvwKcW8PcW8HGX6OCBcQ4Twi/AQZziHCvyHCGUT4V0R4ExFOI8I/IsIPEeHvEfcSiHvfIsL/Rtx7gLj3T0Q4hghHEeH/gwhHEOGw/42KRn3xv/4/iP8dQkRCiGgI8Y8Q4p8hxEYI8e8Q4tsQIhFCJEOIH0KIVAiRDiF+CSG2QshHIUQ2hMiFEE9CiKchRCGE3A4hd0PIlyHEqxDiTQjxNoR4F0LshxDVEOIghDgMIU5CiNMQ8iyEbIUQ7RCiG0L0QohBCHEdQogQfRHiSoToihBtEaIlQjRFiFMR4kSEOBIhaiJEVYSoiBDvZYi3IsQbEaIoQrwUIV6IEDsiREGGeCpDPJEhHssQv4sQj0SIX0WIX0SIn0WIH0WIhyJEUoT4ToT4jwzxbxHigQhxX4T4hwgRFSH+JkL8bxHif4kQIREiJP4XIXGPkIgQElFC4u+ExH1C8gEh8W9C4ltCIkFIfE9IPCQkfyQk0oTEL4TEJiGRISSyhMRjQuIJIZEnJAqExDYh8YKQ2CMkXhESbwiJt4TEO0Jin5D4QEgcEBKHhMQxIdEgJM4IyRYh0SYkuoREj5DoExLXhK4FoaEgdCUIdQWhjiB0IQg1BaFTQehEEjoShGqCUFUQqghC7wWht4LQG0moKAm9lIR2JaFtQaggCD0VhJ5IQjlBKCsIPRKEtgShXwShnwWhHwWhh4LQ94JQQhD6jyD0b0HogSB0XxD6hyD0d0Hob4LQ/5aE7klC/4/g/wkJ7oUEfwsJoiHBP0KCWEiwERL8KyT5NiRIhATfhyQ/hASpkCAdEvwSkmyFBJmQ4LeQ4HFI8CQkyYcEhZDgeUjwIiTYCwlehQRvQoK3IcG7kGA/JPkQkhyEJIchwXFIcBqSnIUkrZCkHRJchgRXIUE/JLgOCURIIELD/y/L+PWe9pnofdv5m14e7sWNtFQWsxgyhAwTRooUMSYkJIQEBQUbx9jY2GCMbLn33nvvvffeHfderu91/Xi3vfl5dp7zOE7syAfsyDvsyBvsyCvsyAvsyFPsyGPsyEPsyH3syD3cyG3cyE3syDXsyBXsyGXsyAXsyDnsyBncyCnsyAnsyFHsyGHcyAHsyD7syG7syC7syA7syFbsyGbsyEbsyHrsyFrsyCrsyArsyHLsyBLsyCLsyHzsyDzsyBzsyMzPX3jz5n6206fhZjaxs5vYeU3s/GHs4iZ2SRO3fBi3chi3uolb18RtaGI3NbFbm9jtw7hdTeyeJm5fE3ugiT00jD3axB5vYk82cWeGceeGsReb2MtN7NVh3I1h3K1h7O1h7L0m7kET+6iJfTKMfdrEvWhiXzWxb5vY98PYT02smljb5KNt8t4O89YO89oN89I2eWabPLXDPLbDPLTD3LNN7tgmt2yTG7bJVTvMZdfkgm1y3jY5Y5uctE2O2yZHbZPDdpiDtsk+22SPHWanHWa7bbLVNtnkmmyww6yzTdbYJqtsk+W2yVLbZJFrstA1mWebzLZNZtomM2yTYdtk2E5n2M6kaWczbOcybOczbBfSdEsYtssYtitp2tU07VqadgPDdhNNu4Vhu52m3UnT7mbY7mXYHmDYHqJpj9C0xxm2Jxm2pxm252ja8zTtJYbtFYbtdZr2Jk17m6a7R9M+oGkf0bRPaNpnDNsXDLtXDNu3DNv3DNuPDFtDU5Zh42h+cDTfOZqvLc2XluHnluZTS/OxpfnQMnzfMnzX0rxlad6wDF+zDF92NC86hs87mmcdzVOO4eOW5lFL87CledDS3Odo7rE0d1qa2y3NrZbmJsvwBsvwOktzjaW5ytJcbhleahleZGkusDTnWZqzLcMzLc0RS3PYMdx0jDQds5uWeU3L/GHLwmHLkmHHsmHLyqZl9bBlTdOyvmnZ1LRsaVq2NS07mpbdTcvepuVA03KoaTnStBxvWk42LaeblrNNy4Wm41LTcqVpuT5sudm03G5a7jUtD4Ydfw07ngxbng1bXgxbXjUtb5uW98OWj02LaVrssMUOG2zzA7b5Dtt8gx1+hR1+jm0+xTYfY5sPscP3sc272OZtbPMGtnkNN3wZN3wR2zyPbZ7FNk9hm8exw0exw4exzYPY5j7c8B7s8E7s8A7c8DZccxO2uQHbXIdtrsEOr8I2l2ObS7HDi7HDC7DNedjmbOzwLGxzBNsc/vyFmzP7s5oNNH0aGmmg2dPQ3GlowTS0qIGWTMMub2BXNrBrGmhdA21ooM0NtK2BtjfQrgba00D7GuhAA3u4gT3awB5voFMNdGYa9nwDXWygKw10rYFuNNCtBrrTwN5roIcN9FcDPW2g59OwLxvoTQO9a6CPDWQaGDX4qAbv1eCtGrxSgxdq8NQ2eGwbPLIN7ttp3FWDW2pwQw2uqcFlNbioBudsg7O2wSlN47gaHLUNjtgGB9VgvxrsUYNdtsF2NdiqBpvVYIMarFODNWqwUg2Wq8FSNVhsGyxQg3lqMEcNZmoa022Dpho01KChaTTsdBoaYZpmM01zaWg+DS2ioaU0tJyGVtLQahpaR8NuYJo2MU1baWg7De2kYXfT0D4aOkBDh5mmYzR0gmk6xTSdoaFzNHSBhi7T0PX/7y0aukPD3qOhhzT0Fw09oaHnNPSSht7Q0Dum2Q80rKFhLI1PlsZ70XgjGq9F44Wl8czSeCIaj0TjvmjctTRui8YN0bgmGldE46JonBeNs5Zpp8W0E6Jx1NI4YmkcFI39orFXNHaJadtFY6tobBaNDaKxztJYIxqrRGOFaCwVjcWisUA05onGHNGYKRozLI2maDQs0xpiekOMNMTsaWJuQ8xvWBY1xJKGZXlDrGyIVQ2xtiE2NMSmhtjSsGxrWHY2xO6G2DdNHGyIww1xrCFONCwnG+JMQ5xriAsNcXmauNYQNxriZkPcaVjuNcTDaeKvhnjSEM8b4mXD8qZhedcQHxrCNIQaQo1PqPEeNd6ixivstBeo8QzbeIJt/IVt3EeNu6hxGzVuosZ11LiKGhdR4zxqnEWN06hxEjWOosYR1DiIGvuxjT1o2i7U2IGdtg01tqDGBtRYh22sQY1VqLEC21iKpi1GjYWoMQ815qDGTGxjBpo2jBqNz194M0c+a2odTaujGTU0Ukez62huHTu/jl1Yxy6poWV17MoaWlvDrqthN9SwW2rYrTW0vYbdVcPurWH317CHatgjNXSsjj1Zw56uY8/W0Pk69nIde7WOvV7D3qph79Sw92rYh3XsX3Xskzp6Xse+rmPf1LHvatiPdWTqGFvno63x3tZ4bWu8snVe2BpPVOexajy0de7ZOndsjVu2xg1b56qtc8nWuKA6Z22NM7bGSdU5pjpHbJ1Dts4BW2evrbFLdXaoxjbV2GLrbLI11tsaa22dlbbGctVZamsssjUW2BrzVGOOasxSjRm2TtPWmGrr1GyNuqZSt01qmk5dM6nb2dTsXGp2AXW7iJpdQs2uoG5XUrerqWkddW2krk3U7Fbqdgc1u4ua3UPN7qduD1LXEWr2GHV7kro9TV1nqesCNV2ipqvU7HVq9iY13aZu71GzD6jZv6jZJ9Tsc+r2JTW9oaZ31PWBugx1WWofLbX3lvpbS/2Vpf7CUnsq6o9F7ZGldt9Su2up3bLUb1jq1yz1y5b6BVE7K2pnLPVTon5c1I9YaodE7YClttdS322p7RT1baK+WdQ2ito6S22NpbbKUltuqS+11BdZagss9XmW2hxRmyXqMyy1YUutYanVLFPrlmbdMr0mZtbF7JplTs0yv2ZZVLcsrVmW1S0ra5bVdcvaumVDzbK5btlaEzvqYldN7K1b9tctB+uWI3XLsbrlZN1yuibO1sWFurhUE1frlus1y8265U7dcq8mHtTFX3XxpG55Xre8rFne1MS7uvhQs5i6xdaErX3C1t5j62+x9Veo/gJbe4atPcbWHmHr91HtLrZ2G1u7ga1dQ/XL2NoFbO0ctn4GWzuFrR1HtSOodghbO4Ct7cPWd6P6TlTbjq1vxtY2otp6VFuLra3C1pZj60ux9UXY2kJUm4etz8HWZmFr07H1YWy9ga3XP3/hpk//rMmTUL2KGlU0vYpmVNHsKnZuFTu/ihZV0ZIqWlFFq6poTRWtr6KNVbSlirZVsTuq2N1V7N4q9kAVe6iKjlTR8So6WUVnq9jzVXSxir1SxV6tYm9Wsber6G4VPaiiR1Xs0yp6XkUvq9g3Vey7KvpYRaaKUZUPtso7W+WNrfJSVZ6rymNV+UtVHqrKPVW5oyo3bJXrtsplW+WSrXJBVc7YKqdslROqclRVDqvKAVXZpyq7VWWnqmxTlS2qsklV1qvKWltllaosV5WlqrJYVRaoyjxVma0qI6oyXVWmqcpUVanZKlVbparJVG2dqhpUNUxVM6hqJlXNoar5VLWIqpZQ1TKqdiVVraaqdVS1gardTNVuo6odVLWbqvZQ1X6qOkRVR6jqOFWdpKrTVHWOqi5S1RWqukpVN6jqNlXdpaoHVPWIqh5T1TOq9gVVvaaqd1T1gao+UZWofrRU34vqW1F9JaovRPWpqD4W1Yeies9SvWOp3rRUr4vqVVG9JKrnRfWsqJ6yVE+I6lFRPSyqB0R1n6jusVR3WqrbRHWLqG4U1fWiukZUV4nqclFdKqqLRXWBpTrXUp1tqY6I6nRRnSaqU0V1iqhWLZOrol4VjaoYrooZVTGrKuZUxbyqWFi1LK5allUtK6uW1VXLuqplQ1VsroqtVbG9KnZVxd6q2F8VB6viSFUcr4qTVXG6Ks5VxcWquFwV16riRtVyuyruVsWDqnhUFY+rlmdVy8uqeF0Vb6vifVV8qgpVhaqfUPU9qr5F1Veo+hxVn6LqY1R9hKr3sNU72OotbPU6tnoVVS9jq+ex1bOoehpVT6LqcVQ9jKoHUHUfqu7BVneh6jZU3YKtbsJWN6DqGlRdha2uQNWlqLoYVRdiq3NRdTaqjqDqdFQdRtUGqk7BVqufv3DTpn3WpAloSgXVK2haBU2voJkVNLuC5lbQ/ApaVEFLK2h5Ba2qoLUVtL6CNlXQlgraVkE7K2hPBe2roIMVdLiCjlXQiQo6VUFnK+hiBV2uoCsVdKOCblXQnQq6X0EPK+hJBT2roBcV9KaC3lbQxwoyFaQKH1ThnSq8UYWXqvBMFR6rwl+q8EAV7qrCLVW4oQpXVeGyKlxUhXOqcFoVTqjCMVU4rAoHVWG/KuxRhV2qsF0VtqrCJlXYoAprVWGVKqxQhaWqsFgVFqjCXFWYrQozVWG6KjRVYaoqTFGFqipUVKGiSVQ0mYrqVDSNioapaAYVzaKiuVQ0n4oWUdESKlpORSupaA0VraeijVS0hYq2UdFOKtpNRXup6AAVHaaio1R0gopOUdFZKrpARZep6CoVXaeiW1R0h4ruU9FDKnpMRc+o6AUVvaait1T0noo+UfkkKh9E5Z2ovBGVl6LyXFSeiMojUXkgKndF5bao3BCVq6JyWVQuiMo5UTktKidE5ZioHBaVg6KyX1T2iMouUdkuKltEZaOorBeVtaKySlRWiMpSUVksKgtFZZ6ozBaVEVGZLipNUZkqKjVRqYrKRDGxIqZURL0iplXEcEXMqIhZFTGnIuZXxKKKWFIRyytiZUWsqYj1FbGhIjZXxLaK2FkRuytiX0UcqIhDFXG0Ik5UxKmKOFsRFyrickVcqYjrFXGrIu5UxP2KeFgRf1XEs4p4URGvK+JtRbyviE8VoYpQ5ROqvEOVN6jyElWeocoTVPkLVR6gyl1UuY0qN1HlGqpcQZULqHIOVc6gyklUOY4qR1DlEKrsR5U9qLILVbajylZU2YwqG1BlHaqsRpUVqLIUVRajykJUmYcqs1FlJqpMR5UmqkxFlRqqTEaViZ+/cPX6Z40fhyaWUbWM6mXUKKPpZTRSRrPKaG4ZzS+jxWW0tIyWl9GqMlpTRhvKaFMZbSmjHWW0q4z2lNH+MjpcRkfL6HgZnS6js2V0vowuldGVMrpeRrfK6E4Z3S+jh2X0pIyeldGLMnpdRm/L6GMZmTJSmY8q805lXqnMC5V5pjKPVeaRytxTmTsqc0tlrqvMVZW5rDIXVOasypxSmeMqc1RlDqnMAZXZqzK7VWaHymxTmU0qs0Fl1qnMapVZoTLLVGaRyixQmbkqM0tlRlRmWGWmqUxdZSarzCSVmaAyZZUpazxlTaSsKmXVKKtBWcOUNZOyZlHWHMqaT1kLKWsJZa2grFWUtZay1lPWJsraQlnbKWsXZe2hrP2UdYiyjlLWcco6RVlnKes8ZV2irCuUdY2yblLWHcq6T1kPKesxZT2jrBeU9Zqy3lHWe8r6RNmI8gdRfifKb0T5pSg/F+UnovxIlB+I8l1RviXKN0T5qihfFuULonxWlE+L8glRPirKh0T5gCjvFeXdorxDlLeK8iZR3iDK60R5tSivEOWlorxIlBeI8jxRni3KI6I8LMrTRHmqKE8R5UmiPEGUy2J8WUwsi2pZ1MqiURbNsphRFrPKYm5ZzC+LhWWxpCyWl8WqslhTFuvLYmNZbCmL7WWxqyz2lMX+sjhYFkfK4nhZnCyLM2VxriwulsWVsrhWFjfL4k5Z3C+Lh2XxV1k8K4sXZfG6LN6Wxfuy+FQWKguVP6HyO1R+g8ovUfk5Kj9B5Ueo/ACV76LyLVS+gcpXUfkyKl9A5bOofAaVT6DyMVQ+hMoHUHkfKu9G5Z2ovA2VN6HyBlReh8qrUXklKi9F5UWovACV56HybFQeQeXpqDwNlaeicg2Vq6g8AZXLn79wkyd/1p9j0IQSmlRCk0uoXkKNEhouoZklNLuE5pbQwhJaXEJLS2hFCa0qoXUltKGE3VTCbithd5bQrhLaW0IHS+hwCR0bi06W0OkSOltCF0roUgldK2FvlNCtEvZeCfuwhB6V0JMSejEWvS6htyXshxL2UwmjEu9tibe2xGtb4oVKPFWJRyrxQCXuq8QdlbilEtdU4rJKXFSJcypxRiVOqsRxjeWQShxQiX0qsVsldqrENpXYohIbVGKdLbHGllhpSyxTiSW2xAKVmKcSs1ViRCWGVWKaLTHVlphsS0y0JSaoxDiVGKsSJf1JSeMpaSIlTaakGiU1KGmYkmZQ0izGag5jNZ+xWkRJSyhpOSWtoqS1lLSBkjZR0lbGagcl7aakvZR0gJI9QskepaQTlHSaks5R0gVKukRJVynpJiXdoqS7lPSAkh5R0hPG2ueU7CtK9i0l+56S/UTJiNJHS+mdKL0WpRei9EyUHovSQ0vpnqV0W5RuitJ1UboiShdF6ZyldNpSOmkpHRelw6J0UJT2idJuUdopSttEabMobRBj14nSalFaKUrLRGmxKC0QpXmiNFuURkRpWIydJkp1UZoiSpNEaYIYO06USuLPkhhfEpWSqI4VtZKYWhLDJTGjZJlVsswpifklsagklpTE8pJYVRJrS2J9SWwqia1jxY6xYtdYy96S2F+yHC5ZjpbEiZI4VRJnSuJCSVwqiSsly42SuFUSd0viQcnyqCSelMTzknhVEm9Llvcl8akkVDKo9BGV3qHSa1R6iS09R6XHqPQIle6jsbdR6RYqXUelK6h0CZXOodJZVDqFSsdR6QgqHcCW9qHSHlTaiUrbUWkLGrsBldah0hpsaSUqLUelxai0AJXmodIcVBrBlobR2GlobB2VpqBSFY2toLFlbKn0+Qs3ceJnjfkDjSuiCUU0sYgmF1GtiBpFNL2IRopoVhHNK6IFRbSoiJYX0coiWlVEa4toYxFtLqLtRbSziPYU0f4iOlREh0ej40V0qojOFNH5IrpYRFeL6EYR3Sqiu0V0v4j+KqInRfRsNHpVRG+K6EMRfSzyyRT5oCJvVeSlijxXkacq8khFHqjIXRW5pSI3VOSKilxSkQsqclZFTqnIcRU5qtEcUpH9KrJXRXaqyHYV2aIiG1VknYqsUZGVKrJMRRaryEIVmasis1VkREWGVaShInUVmaIiE1VkvIqMU5GSioxWkaLGUNSfFDWeoiZSVJWiahTVoKhhiprBaM1itOYwWvMpahFFLaWoFRS1iqLWUtQGitrMaG2jqB0UtZui9lHUQYo6QlHHKOokRZ2hqPMUdZGirlDUdYq6RVF3KeoBRT2iqCeM1nOKekVRbyjqPUV9pPjJUPwgiu9E8ZUoPhfFp6L4lyg+FMW7onhLFG+I4lVRvCyK50XxjCieEsXjonhEFA+J4n5R3COKO0VxuyhuEcWNorhOjF4jiitFcZkoLhbFhaI4TxRni+KIKA6LYkOMroviZFGcJIoTRHGcGF0SxaIYUxR/FkW5KCqjRbUoakXRKIpmUcwoillFMaco5hfFoqJYWhQrimJVUawtig1FsXm02DZa7BgtdhfFvqI4WBRHiuJYUZwsijNFca4oLhbF5aK4XhS3iuJuUTwoikdF8aQonhXFi6J4UxTviuJTUahoUPEjKr5Dxdeo+BwVn6LiY1R8gIr30OjbqHgDFa+i4iVUvIiKZ1HxFCoeR8WjqHgYFfej4h5U3ImK21FxCypuRKPXo+IaVFyJistQcQkqLkDFuag4GxVnouIwKjbQ6DoaPRkVJ6FiBY0uo9FjUbH4+Qs3Yfxn/VFApSH0ZwFNKKCJBVQtoFoBTSugGQU0UkBzCmheAS0soKUFtKyAVhbQmiG0oYA2FdCWIbS9gHYV0N4COlhAhwvoWAGdLKCzBXR+CF0cQlcL6HoB3SyguwV0v4AeDaEnQ+h5Ab0aQm+G0IcC+lTAqMAHFXinAq9U4LkKPFWBRyrwQAXuqcBtFbihAldU4JIKnFOBMypwSgWOaYgjKnBQBfapwB4V2KECW1VgkwqsV4G1KrBSBZarwBIVWKgh5qvAbBWYqQLTVWCaCtRVYLIKTFKBCRpinAqUVKCoAgUVKKhIQSWG9CdDGk9BEyloMgXVKWgaBc2goBEKms2Q5jGkBQxpMQUtp6CVFLSGgtZT0CYK2kJBOyhoNwXtZUgHKOgwBR1jSCcp6DQFnaOgixR0hYKuU9BNhnSHgu5T0CMKekJBzynoFQW9oaD3FPSJghGFj6LwVhReicJzMfRUDP0lCg9F4a4o3BJDN0ThqihcEoXzonBGFE6KwjFROCIKB0VhnyjsEUM7xdBWUdgsChtEYa0YWiWGlovCUlFYJArzRWGOKMwUhemiME0UpoqhKaIwSRQmiMI4MTRWFEaLQkEUC6JUEH8WxPghMbEgqgVRK4jGkBgeEiNDYlZBzCuIBQWxeEgsK4gVBbGmINYXxKYhsbUgthfEroLYOyQODInDQ+JoQZwsiNMFcbYgLhbE5YK4XhC3CuJOQdwviIcF8aQgnhfEy4J4WxDvC+JTQWjIoMJHVHiLCq/R0AtUeIoKf6HCA1S4hwq30dANVLiKCpfQ0AVUOIMKp1DhOCocQYVDqLAPFfagoV1oaCsqbEaFjaiwFhVWocJyVFiKCotRYT4qzEGFmagwAxWmoUIdFaagwiRUqKDCODRUQoXRqDD0+Qs3btxn/f4b+iOPSnlUHkTjB9GkPJqcR1PzqJlH0wfRrDyak0fz82hRHi3NoxV5tDqP1g2ijXm0eRBtH0Q782hPHu3Po0OD6Ggencij03l0bhBdyKPLeXQtj27k0Z08updHDwfR40H0LI9e5dGbPPqQRx8H+WTyfFCetxrkpfI8V54nyvNIeR4oz10NckuDXFeeK8pzUYOc0yCnNchJ5TmqQQ4pzwHl2atBdinPduXZojwblWedBlmtPCuUZ6nyLFKe+RpkrvLM1CAzNEhTg0xVnikaZJLyVJSnrEHGapDRylNQnkENMqgCeRXJawx5/Ule48lrInlNZlB18ppGXtMZ1EwGNYe85jGoheS1lLyWk9cq8lpLXhsY1Cby2kZeO8lrD4Paz6AOMqgj5HWcvE4xqLPkdYG8LpHXVfK6SV53yOseeT1kUI/J6xl5vSSvN+T1nrw+kv9kyH8Q+Xci/0rkn4vBJyL/SOQfiPxdkb8l8tfF4BWRvyjy50T+tMifFPljIn9Y5A+I/F6R3yXy28XgFpHfKPLrRH61GFwh8ktFfonILxD5uSI/S+RniHxT5KeKfE3kqyI/QeTLIj9W5EeL/JDI58Xvg6I4KEp5MS4vxudFJS8m50V9UEzLi+l5MZIXc/JiXl4szIslebF8UKwaFGvzYn1ebM6LrXmxY1Dszot9eXEwL47kxfG8OJUXZ/PifF5cyoureXEjL27nxb28eJgXj/PiWV68GBRvBsX7vPiYF2bQoPxHlH+LBl+jwRco/wTlH6H8A5S/h/K3UP46yl9B+Ysofx7lz6L8SZQ/hvKH0eABNLgX5Xej/A6U34Lym1B+HRpcg/IrUX4Zyi9G+YUoPw/lZ6H8DJQfRvmpKD8F5SehfAXlyyg/FuVHo/wfaHDw8xe2VPpsfvsFU8hh/shhSjnMuBxmQg4zKYeZksNMzWGaOcyMHGZWDjM3h1mQwyzOYZblMCtzmDU5zPocZlMOszWH2ZHD7M5h9uUwB3OYIznM8RzmVA5zNoc5n8NcymGu5jA3cpg7Ocy9HOZhDvM4h57lMC9zmDc5zPsc5mOOjybHe5PjjfmVVybHc5Pjicnx0OS4b3LcMTlumhzXTI7LJscF8ytnTY5TJsdxk+OI+ZWDJsc+k2O3ybFDObaaHJtMjvUmxxqTY6XJsczkWGxyLDA55pocs0yOGeZXmibHVJNjiskxyeSYYHKMMzlKJkfR5CiYHHmTI2dy5Mxv5Mzv5EyRnCmRM3+SMxPImUnkzBRyZio50yRnZpAzs8iZueTMAnJmMTmzjJxZSc6sIWfW86vZSM5sIWe2kzO7yZl9/GoOkDOHyZlj5MxJcuYMOXOenLlEzlwlZ26QM7fJmXvkzANy5i9y5ik584KceU3OvCNnPpL7ZMh9MOTeGnKvDLnnhtwTkXskcvcNuTuG3E1D7pohd9mQu2DInTXkThlyJwy5o4bcQUNuvyG3x5DbachtM+Q2G3IbDLm1htwqQ265IbfEkFtoyM0z5GYbciOG3LAh1zDkaoZc1ZCrGHJlQ65kyBUNuSFDbtCQ+9XwW87we87wR84wJmf4M2cYnzNMzBkm5wz1nGFazjA9Z5iZM8zJGebnDItyhqU5w4qcYXXOsC5n2JgzbMkZtucMu3KGvTnDgZzhcM5wLGc4mTOcyRnO5wwXc4arOcONnOF2znAvZ3iQMzzOGZ7mDC9yhtc5w7uc4WPOYHIGk/uAyb3F5F5hcs8xuSeY3CNM7j4mdxeTu4XJXcfkrmByFzG5c5jcaUzuBCZ3FJM7hMntx+T2YHI7MbltKLcZk9uAya3F5FZhcssxuSWY3EJMbh4mNxuTG8HkhjG5BiZXw+SqmFwFkxuHyY3F5IqY3BAmN4jJ5T5/4caM+azcz+j3LBrKotFZNDaLyllUyaJqFtWzqJFFw1k0kkVzsmh+Fi3KoiVZtCKLVmfR2izamEWbs2hbFu3Koj1ZtD+LDmXR0Sw6kUWns+h8Fl3OoitZdCOLbmXRvSx6mEWPs+hZFr3MojdZ9D6LPmX5pCwflOWtsrxSlufK8kRZHirLfWW5rSw3lOWqslzUL5xXljPKclJZjinLIWU5oCx7lWWXsmxXls3KskFZ1inLamVZrixLlWWhssxXljnKMqIs0/ULDWWpK8tkZakoy3hlGasso5VlSFkGlSWnLFllySpHVnmyGiKr0WRVIqsyWVXIqkpWNbJqkNUwWY2Q1WyymkdWC8lqCVktJ6tVZLWWrDaQ1Way2kZWu8hqL1kdIKtDZHWUrE6Q1WmyOk9Wl8jqClldJ6tbZHWPrB6S1WOyekZWL8nqDVm9J6tPZD+J7AeRfSuyr0T2ucg+EdlHIntfZG+L7A2RvSqyl0T2gsieFdmTIntMZA+L7EGR3Suyu0R2u8huEdlNIrteZFeL7AqRXSqyi0R2vsjOEdmZIjtdZKeJbF1kJ4vsRJEdL7J/iuxokR0S2UGR/VVkfxG5rMhnRSErillRyopxWVHJimpW1LJialYMZ8WMrJidFfOyYkFWLMmKZVmxKivWZsX6rNicFVuzYmdW7MmK/VlxMCuOZsWJrDidFeey4mJWXMmK61lxKyvuZcWDrHicFc+y4mVWvMmKd1nxKSuUNSj7EWXfouwrlH2Osk9Q9hHK3kfZOyh7A2WvouwllD2PsmdQ9iTKHkPZwyh7AGX3ouwulN2OsltQdiPKrkPZNSi7Ev2yFGUXo+wClJ2LsjNRdgbKTkPZOspOQdmJKDsBZcei7BiU/QNlf0fZX1E2+/kL98cfn83PP2J+y2B+z2CGMpjRGczYDGZ8BjMxg5mcwdQymEYGMz2DmZXBzM1gFmQwizOYZRnMygxmTQazPoPZlMFszWB2ZDC7M5h9GczBDOZoBnM8gzmdwZzNYC5kMJczmGsZzK0M5m4G8yCD+SuDeZrBvMxgXmcw7zOYjxk+mQzvTIY3JsNLk+GZyfDYZHhgMtwzGW6bDDdMhqsmw0XzE+dNhtMmwwmT4ajJcNBk2G8y7DEZdpoM20yGTSbDepNhjcmw0mRYZjIsNhkWmAxzTYZZJsMMk2Ga+YmpJsMUk2GSyTDBZPjTZBhjMvxhMgyaDL+aDFmTIWMyZMzPZEyOjBkkY4bImDFkzFgypkzGVMiYKhlTI2MaZMwwGTNCxswmY+aRMYvImKVkzAoyZjUZs46M2UjGbCFjdpAxu8iYfWTMQTLmCBlznIw5RcacJWMukDGXyZhrZMxNMuYuGfOAjPmLjHlKxrwgY16TMe/ImI9kjCHzwZB5a/jppSHzzJB5bMg8NGTuGTK3DZkbhsxVQ+aSIXPekDltyJwwZI4aMocMmf2GzB5DZqchs82Q2WzIbDBk1hgyKw2Z5YbMEkNmgSEz15CZZcjMMGSahsxUQ2aKITPJkJlgyPxpyIwxZP4wZH43ZH41ZLKGzE+GnzOGXMaQzxgKGUMxYyhlDOWMYWLGMDljqGcMjYxhesYwM2OYkzHMzxgWZQxLM4YVGcPqjGFdxrAxY9iSMWzPGHZlDPsyhgMZw5GM4XjGcCpjOJsxnM8YLmcM1zKGmxnD3YzhfsbwV8bwNGN4kTG8zhjeZgwfMwaTMZjMR0zmLSbzCpN5hsk8xmQeYjL3MZk7mMwNTOYaJnMJkzmPyZzBZE5gMscwmUOYzH5MZg8msxOT2YbJbMZkNmAyazGZVZjMcsxPSzCZhZjMPExmNiYzgsk0MZk6JjMFk5mIyYzHZP7EZEqYzB+YTAGTyWMyv2Aymc9f2N9//2x+/B7zcxrzaxrzexozlMaMSWPGpjHlNGZiGjM5jamnMdPSmOlpzKw0Zm4asyCNWZzGLEtjVqUxa9KYjWnM5jRmWxqzK43Zk8YcSGOOpDHH0phTacyZNOZCGnMljbmWxtxKY+6kMffTmEdpzNM05mUa8zqNeZ/GfEzz0aR5b9K8MWlemjTPTJq/TJr7Js1dk+aWSXPdpLli0lwwac6aNKdMmuMmzRGT5oBJs9ek2WXSbDdptpg0G0yatSbNKpNmuUmzxKRZYNLMM2lmmTQzTJqmSVM3aaaYNBNNmvEmzZ8mzRiT5g+T5neT5jeTJmvSZEyatEmTNj+SNj+TNjnSZpC0GSJtRpM2Y0mbMmlTIW0mkzZ10mYaaTOdtJlJ2swlbRaQNotJm2WkzUrSZg1ps4G02UzabCNtdpI2e0ibA6TNYdLmGGlzkrQ5Q9pcIG0ukzbXSJubpM0d0uY+afOItHlK2rwgbV6TNu9Im4+kPxrS7w3pN4b0S0P6mSH9lyH9wJC+a0jfMqSvG9JXDOmLhvRZQ/qUIX3ckD5iSB80pPca0rsM6e2G9BZDeqMhvdaQXmVILzeklxjSCw3peYb0LEN6hiHdNKSnGtJTDOmJhvR4Q/pPQ3qMIf2HIV0wpH8zpLOGdMaQTht+TBt+ThtyaUM+bRhKG0anDWPThnLaUEkbJqcNtbRhWtowPW0YSRvmpg0L0obFacOytGFl2rAmbVifNmxOG7alDTvThj1pw7604VDacCxtOJk2nEkbzqUNl9KGa2nDzbThTtpwL214lDY8TRtepA2v04a3acPHtMGkP2HSHzDpN5j0K0z6GSb9Fyb9AJO+i0nfxqSvY9JXMemLmPRZTPo0Jn0ckz6KSR/EpPdi0rsx6e2Y9BZMeiMmvQ6TXo1JL8ekl2DSCzHpeZj0bEx6BibdxKSnYtJTMOlJmPR4TPpPTLqESf+BSRcw6d8w6Swm/RMm/cPnL1w+/1npUeinFPplFMqnUCGFiilUSqFxKTQhhammUC2FGik0nEIzU2jOKDQ/hRan0NIUWplCq0ehDSm0KYW2ptDOFNqTQvtT6FAKHUuhU6PQmRQ6n0KXUuhaCt1KoTsp9CCF/kqhpyn0MoVep9D7FPqU4pNSvFeKN0rxUimeKcVfSnFfKe4qxS2luK4Ul5XivFKcVYqTSnFMKQ4rxX6l2KsUO5Vim1JsVooNJsUapVipFMuUYpFSzFeKOUoxUymmK0VDKWpKUdUoKkpRVoqSUhSVoqAUeaXIKcVPGsUPSpFSipTSpJQhpV8YpTwp/U5Kf5DSGFIaR0oVUppESlNIaSopDZPSTFKaTUrzSWkRKS0lpRWktJqU1pPSJkZpKyntIKU9pLSflA6R0lFSOklKZ0jpPCldImWukdJNUrpDSvdJ6REpPSWlF6T0mpTekdJHUsYw6oNIvRGjXorUMzHqsUg9FKm7InVLpK6L1GWRuiBSZ0XqlEgdF6nDInVApPaK1E6R2iZSm0Vqg0itEamVIrVMpBaL1AKRmiNSM0VqukhNE6maSFVFaqJIlUWqJFJFkSoYUoMilROpn0TqB5FKie9TIpMS2ZT4LSV+T4k/UmJMSvyZEhNSYlJKTEmJqSkxnBIjKTE7JealxKKUWJoSK1JidUqsS4lNKbE1JXakxJ6U2JcSh1LiaEqcSIkzKXEuJS6lxLWU4WZK3EmJeynxKCWepsSLlHidEu9S4lNKKGVQ6gNKvUGpVyj1DKUeo9RDTOouSt1GqesodRmlLqDUWZQ6jVLH0ajDKHUApfai1C6U2oZSm1FqA0qtRalVKLUMpZag1AKUmotSs1BqOko1UaqOUpNRaiJKlVFqLEqNRqkhNGoQjcqh1M8o9SNKff/5C5f75bNGDaB0Ev00gHJJ9FsSFZKoOIBKSVROokoSVZOolkTTkmhGEs0cQHOTaGESLU6i5Um0agCtG0Abk2hLEu1Iot0DaN8AOphER5PoxAA6nUTnk+jSALqWRDeT6E4S3U+iR0n0NIleJNHrJHo/gD4N8ElJ3ivJGyV5oSRPleQvJbmvJHeV5KaSXFOSy0pyXknOKMlJJTmmJIc0wD4NsEcD7NAAW5Vkk5KsU5LVSrJcSZYoyUIlmasks5RkhpI0laSuJFUlmagBykoyVklGK0lBSfJKklOSn5XkBw2QUpKkkiQ1iqTSJJVhQFkG9BsD+p0B/UFSJZIqM6AJDKhKUjWSapDUdJKaSVJzSGo+SS0mqeUktYoBrWVAGxjQFpLaTlK7SWofSR1kQEdI6jhJnSapcwzoIgO6SlI3SOo2Sd0nqUck9YSknpPUa5J6x4A+MvDJMPBBDLwRAy/FwDMx8JdIPhDJuyJ5UySviYHLInlBDJwRAydF8phIHhLJ/SK5RyR3iORWMbBJJNeL5GqRXCmSS8XAQpGcJ5KzRHKGSDbFQF0kJ4vkRJEcL5JjRXK0SA6JZF4M/CoGfhbJH0QyJZJJ8Z8BkR4QmaTIJsVvA+L3AfFHUpSSYlxSTEiKSUlRS4pGUgwnxcwBMXtAzE+KxQNiWVKsTIo1SbEhKTYnxbak2JUUe5PiYFIcSYrjSXE6Kc4lxcWkuJoUN5Li9oC4NyAeJcWTpHieFK8HxLsB8TEpzIBByY8o+QYlX6HkMzTwF0o+QMm7aOAWGriOBi6j5AWUPIsGTqGBY2jgMEoeQMk9KLkTJbeh5CY0sB4NrEbJFSi5FCUXouQ8lJyFkiMo2UTJOkpORsmJKDkeJf9EydEoOYQG8mjgVzTwM0r+gJLfo4Hk5y/czz9/1nd9KNWH+aEP81MfJtePGexHQ/2Y0f2Ysf2Y8X2YiX1och+a2o+G+9GMfjS7H83vRwv70dJ+tKIfs6Yfs6Efs6kfs60fs7Mf7e1HB/rR4X50vB+d6kPn+tDFfnSlH93oR7f7Mff7MY/60JM+9KIPve5D7/vQx34+mT7eq5836uOF6eOp6ecv08d9088d08cN08dV9XFJ/ZxTP6fVz3HTzxHTzwH1s8f0s8v0sc30sVn9bFA/a0w/K00fS00fi0w/89XPbPUzoj6GTR8N088U9TFRfYxXP3+qnzGmnyH1M6h+curnZ/Xzo/pJmX6S6qdP/fRpgD4zij79QL9+pl+/0G/y9KtAnxlNv/mTflOmTxPp02T6TZ1+06TPzKDPzKJfc+nXQvq1lD6toE+r6dc6+s0m+s1W+sxO+rSHPu2nT0fo13H6dIo+naVfF+k3V+g31+k3t+nXPfr0kD7zhD7znD7zmn7zjn7zkb5Phv73ou+N6Hsh+p8a+v4SfQ9E3x3Rf0P0XRV9Fw195wx9pw39x0X/EdF3UPTvE/27RN82Q99mQ98GQ/9aQ99KQ99yQ/9iQ/98Q/8cQ9+IoW/Y0N8w9E0x9E0y9E0w9I8z9I8R/UOG/kFD/6+G/qzo+1H0p0RfUvT1Gb7rM4zqM6T7RKZf/NJv+K3fUOgXxX5R6hPlPlHpE5P7Rb3fMK3fMKPfMLPfMLdfLOw3LO03rOgXq/oN6/oNm/rF1n7Djj7D7j7D/j5xuM9wrN9wql+c6RcX+sWVfnG9X9zuN9ztNzzsF0/6DM/7Da/7DG/7xMc+YfoNpu8Dpu8N6n+J6XuK6fsL0/cA03cH9d/E9F9F/ZdR/3lM/2lM/wlM31HUfwj178P078L0b8f0bUH9G1D/WkzfSkzfMkz/YtQ/H/XPwfSPYPqmY/oamL4pmL5JqG8C6huH6RuD6f8D9f+O+n9F/VnU9yOm/3tM/39Qf//nL7wff/ysb3tQsgeletCPPejnXvRrLxrsRUO9qNSLxvWgCT2o2oOt9WKn9WKn96JZvWhuL1rQi5b0ouW9aHUvWteLNvWirb1oRy92Ty/a34sO9aJjvehUD/ZsD/ZCL7rSi270otu96H4vetSDfdKDXvSg1z3ofQ/61ItRD+/Vyxv18EI9PFUvf6mH++rljnq4oR6uqodLtpdztpfTtpfj6uWI7eWAetmjXnaqh222h83qZZ3tZZXtZYV6WKIeFqqXueplpu1luu1hmnqoq5eqephgexhneympl6J6GbS9/KpesurlR/Xyve0lqV761UuveulRHz0aoMem6NWP9OpnevUrvRqkR0P0agy9+pMeTaBHk+hVjV416NEwPZpJr+bQqwX0agk9Wk6PVtOrdfRqI73aQo920KM99Gg/PfYQvTpKj07So7P06gK9ukKvrtNrb9Ore/ToIT16Qo99To9e06t39OojPUb0fhA9b0TPS9H7VPT8JXoeiJ47oveG6Llq6bkoes6JntOi97joPSJ6DojevaJ3p+jZJno2i571oneN6FkhepaK3oWid67onSV6Zoiepuiti56qpaciesqid6zoLVp6fxe9v4nerOjNiJ7vRW/S0tNv6ekV3/aIgR6R6hE/9IqfesUvvSLfK4Z6LWN6LON6LBN6LJN6LbVe0egVw71ipFfM6RULesXiXsvyXsvKXrG2V2zsFVt6xY4esatH7OsRh3vEsV5xslec6bVc6BVXesX1XnG7V9ztFQ97xZMey/Ne8brH8q7H8qlHqFeo5yPqeYN6X6GeZ6jnL9TzAPXcQb23UO811HsJ9Z5HvadQ7wnUcwT1HkC9+1DvTtS7DfVsRr3rUe8a1LMC9SxFvQtR71zUOxv1Tkc9TdRTRz1V1FNBPWVsz1jUU0S9v6Pe31BvFtubwfZ8j3r/g3q/w/b2fv7CS6c/q+cb9F0C/SeB0gn0YwL7SwL9mkCFBHZ0AltKoPEJVElgJyewjQS2mUAjCTQngeYl0KIEWpZAKxNoTQJtSKAtCbQ9gd2VQHsT6GACHU2gEwnsmQT2YgJdTqDrCXQrge4l0MME9kkCvUigVwn0LoE+JTBK8F4J3ijBCyV4ahM8UoJ7SnBHCW4owWUluGgTnLUJTtkEx2yCwzbBfiXYrQQ7lGCLTbBRCdbaBCttguU2wWIlWKAEc5RgxCYYtgmmKsEUJZioBGWbYKxNUFSCghL8ZhNklSCjBGklGGUTfKcEvUqQUIKEekioj4RNktD3JPQjCWVJ6FcS+p2EiiQ0hoTGkVCFhKokVCehaSQ0g4RmkdA8ElpEQktJaCUJrSGhDSS0mYS2k9AuEtpLwh4koaMkdIKEzpDQBRK6TMJeI2FvkdA9EnpIwj4mYZ+TsK9J2Hck7EcSRiQ+iMQbkXgpEs9E4pFI3BeJOyJxQySuWhIXReKcSJwSiRMicVgk9ovEHpHYIRJbRGKjSKwTidUisVwklojEApGYY0nMtCSGLYmpIjFFJCZZEhNEYqxIjBGJIUsiLxI5kfhJJH4QiVEi8Z0l8a0lkRA9CdGfEMmESCXEjwnLzwnxa0L8nrD8kbCUEpZxCcuEhKWaELWEaCYs0xNiVkLMS1gWJixLE5YVCcuahGV9QmxKiO0JsTMh9ibEwYQ4khAnEuJ0wnI+IS4nxLWEuJWw3E2IhwnxJGF5nrC8SljeJiwfE0IJgxIfUOINSrxEiaco8Qgl7qPEHZS4iRJXUeIiSpxDiVMocRybOIwS+1FiL0rsQImt2MRGlFiLEqtRYhlKLMEmFqDEHJSYhRLDKDENJWooMQklJqDEn9jEGJQYQok8SuRQ4ids4gdsIoUSAyjxLTaR+PyFGzXqs775J/o2jr6Lo1FxlI6jn+LolzjKx9FQHI2Ooz/jaHwcTYqjWhw14mh6HM2Mo7lxtDCOlsTRijhaHUfr42hzHG2Lo11xtDeODsbR0Tg6EUdn4uh8HF2Oo+txdDOO7sbRwzh6Ekcv4uh1HL2Po49xPpk47xXnteK8UJynivNIce4pzm3Fua44VxTnguKcUZyTinNUcQ4pzj7F2aU42xVns+JsUJw1irNCcZYqzkLFmac4sxRnuuJMU5ya4lQVZ4Li/Kk4YxRnSHEGFSenOD8pzg+Kk1KcpOJ8qzgJxfmX4sSVIK5e4vqOuEYRV5q4fiKuX4grT1xDxDWauP4krvHENYm4asQ1lbimE9dM4ppLXAuJawlxrSCu1cS1nrg2EddW4tpJXHuJ6yBxHSGu48R1mrjOE9cl4rpGXDeJ6y5xPSSuJ8T1nLheEdc74vpI3BjiH0T8tYi/EPGnIv5IxO+L+G0Rvy7iV0T8goifFfGTIn5UxA+J+D4R3y3i20V8s4hvEPE1Ir5SxJeK+EIRnyfis0V8hohPE/GaiFdFvCLi40R8jIj/IeKDIv6riP8s4j+IeErEkyLeJ+L/FvF/iW/iojcuvouL/8RFOi4ycfFLXOTjohAXxbgYGxfj42JiXEyJi6lxMRwXM+NiblwsiIvFcbE8LlbHxfq42BQX2+JiZ1zsjYuDcXEkLo7Hxam4OBcXl+LialzcjIs7cfEgLh7HxbO4eBUXb+PiY1yYuEHxDyj+BsVfovhTFH+E4vdR/DaK30DxKyh+AcXPovhJFD+G4odQfB+K70bx7Si+BcU3oPgaFF+J4ktRfBGKz0fxOSg+A8WbKD4VxSej+EQUH4fiJRQvovjvKP4biv+M4j+geArFB1C8D8V7UPxfn79wyYHPin+FemKoL4aSMZT6GmViKBtDv8VQIYaKMTQ2hsoxNDGGpsTQ1BgajqGRGJobQwu+RotjaHkMrYqhdTG0OYa2xdCuGNr3NToQQ0di6HgMnY6hCzF0KYauxdCtGLoXQw9j6EkMvYih1zH0PoZMDKMY7xXjjWK8UIynivFIMe4pxm3FuK4YlxTjgmKcUYyTinFUMQ4pxj7F2KUY2/Q1mxRjvWKsVozlirFEMRYoxlzFmKkYw4rR0NdMUYxJijFeMcYqxmjFKChGXjGyivGjYnyvGEnF6FeMHsX4l2J8ra+J6V/E9G9i6iOmJDGliOlHYvqZmH4lpt+JqUhMY4mpTEwVvtZkYppKTMPENEJMc/haC4hpMTEtJ6ZVxLSOr7WJmLYR005i2ktMB4npCDGdIKbTxHSemC4R0zViukVMd4npITE9IabnxPSamN4T0ydiRsQ+iNgbEXshYk9F7JGI3Rex2yJ2XcSuiNgFETsrYidE7KiIHRKxfSK2W8S2i9gWEdsgYqtFbIWILRGxBSI2V8Rmith0EWuIWE3EJonYeBH7U8RGi9iQiP0mYr+I2E8i9r2I/UfE+kWsV8S+EbGvRTwmEjHxbUwMxEQqJn6IiZ9j4teYGIyJP2JiTEyMi4nK12JyTNRjohkTIzExJybmx8TimFgWE6tiYl1MbIyJbTGxIyb2xMSBmDgcE8dj4lRMnIuJSzFxNSZuxsSdmHgQE0++Fs9j4lVMvIuJjzGhr4ViH1DsDYq9RLGnKPYIxe6j2G0Uu4FiV1DsAoqdRbGTKHYMxQ6h2D709W4U245iW1BsA4qtQbGVKLYExRai2DwUm4W+noG+bqBYDcUmoa/Ho9ifKDYaxYZQLI9iv6DYTyiWRrFRKPYd+roHxb5Bsa8/f+F91//Zfv0PbCKK7Y1iv4ti//MV9ocoNhPF5qLYwSh2KIodE8X+GcVOiGKrUWwtim1GsTOi2NlR7LyvsIui2GVR7Ioodk0UuzGK3RLF7ohid3+FPRDFHo5ij0Wxp6LYc1HspSj2WhR7M4q9G8U+iGIfR7HPo9jXUez7KNZEMTbKexvltY3y3EZ5YqM8tFHu2ii3bJRrNsplG+W8jXLaRjlhoxy2UQ7YKHtslJ02yjb7FZtslHU2ykobZamNsshGmW+jzLFRRmyUpo1St19RtVEqNso4G2WMjTJkowzaKDkb5ScbJW2jjLJRvrNRem2UhI3yTxslaqNE7ddE7b+I2h6itp+o/Q9RmyZqM0TtL0RtnqgtELWjidqxRO14vrKTiNoaUTuNqJ1O1M7mKzuPqF1I1C4lalcQtWv4ym4garcQtTuI2t1E7X6i9hBRe5yoPUnUniNqLxG1V4nam0TtXaL2AVH7mKh9TtS+ImrfErWfiBpL9IMl+sYSfWGJPrVEH1mi9y3RW5boNUv0siV63hI9Y4mesESPWKIHLNE9luhOS3SrJbrREl1nia6yRJdZoost0fmW6GxLdMQSHbZEp1qiky3RiiU6zhItWaJ/WKK/W6K/WqI/WaI/WKKjLNEBS/RbSzRhicYt0a8ssajlX1FLT9TSF7Uko5bvo5ZM1JKNWn6LWgpRSzFqGRu1jP/KMjFqmRK1NKKW6VHLrKhlbtSyMGpZErWsiFrWRC3ro5YtUcu2qGVX1LI/ajkctRyLWk5FLeeilotRcSVquRm13IlaHkQtj7+yPItaXkUtb6OWj1GLvjLY6Eds9A02+gobfYaNPsJG72Gjt7DR69joZWz0IjZ6Fhs9iY0ew0YPYaP7sF/txEa3YqObsNF12OgabHQ5NroEG12Ajc7BRkewXw1jv6pjo5Ox0YnYr8rYaAkb/QMb/R0b/RUb/Rkb/REbTWGjSWz0W+xX/8ZG49joV5+/cN/2flb07ygeQYkI+jaCvvsSpSLohwj6OYJ+jaDBCPojgsZE0LgImhhBkyNoagQ1I2gkguZ+iRZE0JIIWh5BqyNoQwRtjqDtEbTrS7Qvgg5F0LEIOhVBZyPoYgRdjaCbEXQ3gh5E0OMIeh5BryPoXQR9jPBJEd4rwhtFeKEITxThoSLcVYRbinBNES4pwjlFOKUIxxXhsCLsV4TdirBdEbboSzYowhpFWKEISxRhoSLMVYSZijCsCFMVYYq+ZKIilBWhpAhFRfhdEX5VhJ8V4QdFGKUIA4rwrSIkFCGuCF8pwpeKEFGUiL4mom+IqJeIviOiUUSUJqKfiChHRINE9AcRjSGicXypChFNJqKpRNQkohG+1BwiWkBES4hoORGt5kutJ6LNRLSNiHYR0T4iOkhEx4joFBGdJaKLRHSViG4S0V0iekBEj4noORG9JqJ3RPSRiDFEPojIGxF5KSJPROShiNwTkVsicl1ELonIORE5LSLHReSIiOwXkd0iskNEtojIRhFZKyIrRGSpiCwUkbkiMlNEhkWkISJTRGSiiJRFpCQiRRH5XUR+E5GfReRHEfleRAZEpE9E/i0icRGJiciX4h8R8XVEfBMRPRHRHxH/iYh0RPwUEb9ExGBEDEXEmIgY96WYEBGTI6IeEc2IGImI2RGxICIWR8TyiFgdEesiYnNEbI2InRGxLyIORsSxiDgREWci4mLEciUibkbE7Yi4HxGPvxTPIuJ1RLyPiI8RYb40KPIBRd6gyEsUeYoij1DkHorcQpHrKHIZRc6hyGkUOY4iR1DkAIrsRl/uQJEtKLIRRdaiyAoUWYYiC1FkHorMQpFh9OU09OUUFJmEIuPRl2NRpIgiv6PIbyiSRZEfUeR7FEmiSD+K/Bt9+S8UiaHIl5+/aP37359t5H+xsTD2X2FsTxjbF8b+J4z9PozNhLG5MDYfxg6FsaPD2D/D2EoYWw1j62HscBg7I4ydHcbOD2MXh7ErwtjVYez6MHZTGLstjN0dxu4LYw+FscfC2FNh7Nkw9mIYezWMvRnG3g1jH4axT8LYF2Hs6zD2fRhrwsiG+WDDvLFhXtgwT22YRzbMPRvmlg1zzYa5ZMOcs2FO2TDHbZjDNsx+G2a3DbPdhtlsw6y3YdbYMCtsmCU2zAIbZo4NM2LDDNswU22YyTZMxYYZZ8OMsWGGbJhBGyZnw/xkw6RtmP/YMP02TI8N840N87UN8w8bJmzDhO2XhG2MsP0XYdtD2PYRtknC9nvCNkPY/kLY5gnbIcJ2NGH7J2E7gbCtErZ1wrZJ2M4gbGcTtvMJ28WE7XLCdjVhu56w3UTYbiNsdxG2+wjbg4TtMcL2FGF7lrC9SNheJWxvErZ3CNuHhO0TwvY5YfuasH1P2H4iLEv4gyX8xhJ+YQk/tYQfWcL3LOFblvB1S/iSJXzOEj5tCR+3hI9Ywvst4d2W8A5LeLMlvNESXmsJr7CEl1rCCyzhOZbwTEt42BJuWMKTLeGJlnDZEh5jCf9hCQ9awjlL+CdLOG0Jj7KE+y3hXkv4G0v4a0s4agmHLZGw5auw5V9hy7/Dlr6wZSBsSYUtmbAlG7bkw5bfw5Zi2PJn2DI+bKmGLbWwZVrYMiNsmRW2zA9bFoUty8KW1WHLurBlU9iyNWzZGbbsC1sOhi3HwpaTYcvZsOVi2HIlbLkZttwJWx6ELU/Cludhy+uw5V3Y8ilssWGLDX/Eht9gwy+x4afY8CNs+D42fAsbvo4NX8aGz2HDZ7Dh49jwEWz4ADa8GxvegQ1vxoY3YsNrseEV2PBSbHgBNjwXG56JDQ9jww1seDI2PBEbLmPDY7DhP7DhQWz4V2z4J2w4jQ2Pwoa/w4Z7seFvsOF/YsNRbPjvn79offPNZxv+H2w0hP1nCJcI4b4NYQdC2FEh3A8hXDaE+zWE/T2ELYZwY0PY8SHsxBC2FsI2QrgZIdzsEG5eCLc4hF0awq0K4daFcBtD2K0h7K4Qbm8IdzCEOxrCnQzhzoSwF0PYqyHsjRD2bgj3IIR9EsK9COFeh3DvQ1gTwtgQ722INzbECxviiQvx0IW460LcciGu2RCXbIhzNsQpG+KYDXHIhdjnQuyyIbbZEJttiPUuxGoXYrkLsdiFmO9CzLYhZtgQ01yImgsxyYUY70KMdSGKNkTBhvjNhsjaED+6EKNciAEX4lsXIuFCxF2IqA3xdxfiby5EyP2dkPsHIfc1IfcNIddLyH1HyI0iZH8g5H4m5H4l5H4nZP8gZMcSsuMJ2YmE3BRCrkHIDRNyswi5uYTsIkJ2KSG3ipBbR8htJOS2EHI7Cbm9hNxBQu4oIXeSkDtDyF4gZK8QsjcIuTuE7ANC9jEh+5yQfU3IvSfkDCFZQh8doTeO0AtH6Kkj9MgRuucI3XKErltClyyhc5bQaUvomCV02BLaZwntsoS2W0KbLKH1ltBqS2i5I7TYEZpvCc2xhEYsoWmWUN0SqjpCExyhPx2h0Y5QwRH6zRH6xRLKWEIpRyhpCfVZQv92hOKO0FeOUMQR+psjHHL8I+T4OuT4V8jRE3L0hxz/CTnSIcdPIUcu5BgMOf4IOUohSzlkmRiyTA45poYcwyHHzJBjbsixMGRZGrKsDFnWhiwbQ5YtIcuOkGVvyHEg5DgacpwIWc6ELBdClishy42Q5U7I8iBkeRyyPAtZXoUc70KOTyGHDQkX+ogLvcGFXuFCz7ChR9jQfVzoFi50HRe6jA2dw4ZO40LHcaHDuNB+bGg3NrQdG9qMDW3AhtbgQitwoSW40AJcaA4uNIINNbGhOi5UxYUm4EJ/4kKjcaEhbCiPDf2CDWVwoTQulMSF+rChHmwojgvFcKEINvS3z1+04vHP9m9d2Eg3NtaF+1cX7t9d2P4ubLIbl+7C/dSF+6UbO9iFHerClbqw5S5spRs7uRvb6MYNd+NmduPmduEWdWGXduFWduHWdeE2dmO3dmN3dOP2duEOduOOduNOduHOdGEvdmGvdmFvdGHvduEedGEfd+Oed+Ned+Hed+FMF8Z2895288Z288J288R189B1cdd1cct1cc12ccl2cc52c8p2c8x2c9B1sc91sct2sc12s8l2sc51scp1scx1s8h1M891M8t2Md120XBdTHFdTHRdlF0XJdfNH7abQdtNznbzk+0i7br4j+ui33XR47r4l+si5rr40nbxv66LbtdFl/sbXS5Cl/uKLheny/2bLtdHt0vSZb+ny2Xocr/Q5fJ02yG67Bi67Di6bIUuN5luN5VuN0y3m0m3m0uXXUiXXUqXW0mXW0uX20i320qX20GX20OXO0iXO0q3O0mXO0OXvUCXvUqXvUGXu0uXfUC3fUy3fUGXfU2Xe0+3M3TL0v3R0f3G0fXC0fXU0fXI0X3X0X3L0X3d0n3Z0nXW0nXK0nXM0n3Y0r3P0rXL0r3N0r3J0r3e0r3a0r3M0b3Y0TXP0jXb0jXD0tWwdNcsXRMdXeMdXWMdXX84un53dOUcXT9bun6wdP/H0fWdpavH0vWNo+trR/eXjq6wo6vbEepy/L3LEe12/LPLkehyfNvlGOhypLodP3Y7fuly/NblKHQ5Rndb/uy2VLos1S5Hvcsx3OUY6XbM7XYs6LIs6bKs7LKs6bZs7LZs6bbs6LLs6XLs73Yc6Xac6LKc6bJc6LJc6bLc6LLc6bI86LI87rY867a86nK863J86nLYbovr+ojreoPrfoXrfobteoTtvofruoXruobruoTtPoftOoXrOobrOoTr2oft2oXt2obt2oztWo/tWo3rXo7rWozrmo/rmo3rmoHtmobtruG6JuG6xuO6x+K6/8B1/47t+hXb9TO26wdc9yhc13e4rl5s1zfYrq9xXf/AdYex3f/z+YvW119/dt0duL934qIduH924L7pwH3bgfuuE5fqwP3Ygfu5E/dbB67QgRvdgfuzAze+EzepE1fvxE3rxI104uZ04BZ04BZ34JZ34NZ04NZ34jZ34rZ34vZ04A504g534o534M504C504K504K534O504B504B534p534l534N534EwHsp18cJ28cZ28cJ08cZ08dB3cdR3cch1cdR1cch2cdZ2ccJ0cdZ0ccB3sdR3scB1sdZ1sdB2scR2sdB0scZ0sdJ3McZ2MuA6aroO666DqOqi4Dv50HYx2nRRcJ7+5TrKukx9dBynXwYDr4FvXQcJ18E/XQdR18HfXQch10Ok66HDddLj/pcP9gw73NR3uX3S4XjpdPx1uFB3uBzrcz3S4X+l0v9PhinS4sXS48XS4Kp2uRqebRqebQaebTYebT4dbTIdbTodbTYdbT6fbTIfbQYfbTYfbT4c7TKc7Toc7TYe7QIe7Qoe7Toe7Q4d7QKd7TKd7Tod7TYd7T6f7RKcsnR8dnW8cHS8dHU8cHQ8dnfccnbccnVcdnRcdHeccHScdHcccnQcdnXsdHTsdnVsdnZscnWscnSsdnUsdnQscHXMcHTMdHcOOjrqjc7KjY4KjY5yjY4yjo+DoyDs6so6OjKPje0fngKOjz9GRcHTEHR1fOTr/7ugIOTo6Hd0djv/tcHzZ6Yh1OP7V4ejpcPR3OP7T6Uh3On7ucOQ6HIMdjj86HaVOx/gOx8QOx5QOx7QOx/ROx+xOx7wOx6IOx/IOx6pOx/pOx6ZOx7YOx+4Ox75Ox+FOx/EOx5kOx/kOx+UOx/UOx+0Ox/0Ox+NOx7NOx+sOx7sOx6cOh+20uI6PuI63uM6XuM6nuI5HuM77uI6buI6ruI6LuM5zuI6TuI5juI5DuI69uI6duI5tuI6NuI61uI6VuM5luI6FuI65uI6ZuI7puI4GrnMKrmMirqOM6xyD6yzgOvO4jl9wHRlcx/e4ziSuow/X8W9cRxzX8RWu4++4zr/hOrs+f9GKRj+7jjbc39pxkTbcV224f7bh/t2G+7Ydl2zDpdtwP7bjfmnDDbbhhtpwpTbcuHZcpR03pR3XaMfNaMfNasPNa8MtbsMta8OtasOta8dtbsdtb8ftbsPtb8cdbscdb8OdbsNdaMNdacPdaMPdacM9aMM9bsc9b8e9bsO9b8N9akOunQ+unTeunReunSeunQeujduujRuujSuujQuujTOunROunSOunQOujT2uje2ujc2unfWujdWujeWujcWunfmundmunemujYZrY4prY6Jro+zaKLk2/nDtDLp2cq6djGvne9dG0rXR59r4t2sj7tr4yrURcW38zbXR5dpod220uQ7a3P/Q5v5Om4vS5v5Jm0vQ7r6lzQ3Q5lK0uQxt7hfaXZ42N0SbG0ObG0ebq9DuptDuGrS76bS7WbS5ebS5RbS5ZbS5VbS5dbS7TbS5bbS5XbS5/bS5w7S747S507S587S5y7S567S5O7S5B7S7x7S757S517S597S7T7TL0v7R0f7G0fbS0fbE0fbQ0X7X0X7D0X7V0X7B0XbG0XbS0XbE0X7Q0b7X0bbD0b7F0b7e0b7G0b7C0b7Y0b7A0Tbb0TbD0TbN0TbF0T7J0VZ2tJUcbX842gYdbTlHW8bRlna0/cfR3udo63G0xR1tMUfbl472vznauhxt7Y6ONsf/tDn+3u74R5vjn22Ob9ocvW2OgXZHqt3xY5sj2+b4rc0x1O4Y3e4Y1+aotDkmtzkabY7hdsfMdsfcNsfCNseyNsfKdse6dsfGdsfWNseuNse+dsfhdsexNsepNsf5NselNsf1NsftNsf9NsfjdsezdsfrNse7NsenNodtF67tI67tDa79Ja79Ka7tIa79Lq7tJq7tKq7tIq79LK7tJK7tCK7tIK5tL65tB65tC65tA65tDa5tBa59Ma5tAa5tNq5tBq5tGq6thmufhGsbj2sbi2sv4toHce2/4tp+wrWlcW3/wbX349p6cG3/wrXFcG1f4tr+hmvvxrX/9+cvWl9++dn99//F/U8QFw7iokHc10HcN0Hct0HcQBD3fRD3YxCXDeLyQVwhiBsTxI0L4ipB3JQgbmoQNxzEzQzi5gVxi4O4ZUHc6iBufRC3OYjbHsTtDuL2B3GHg7jjQdzpIO5CEHc1iLsRxN0J4h4EcU+CuJdB3Jsg7kMQpyDWBfnogrx1QV66IE9dkIcuyB0X5KYLctUFueiCnHFBjrsgR1yQAy7IHhdkuwuy2QXZ4IKsdkGWuyCLXZD5LsgsF2S6C9JwQaa4IBNdkLILMsYFGXJB8i7ILy5IxgX53gUZcEG+dUESLsg/XZCoC/J3F+R/XJAOFyToggTdfxN03QRdmKD7B0H3NUH3DUHXS9B9R9ClCLofCbosQfcbQVcg6EYTdOMIugpBN5mgm0rQDRN0Mwm6eQTdIoJuGUG3iqBbR9BtIui2E3S7Cbr9BN1hgu44QXeaoDtP0F0h6G4QdHcIugcE3WOC7jlB94ag+0DQGYLWEfzoCL51BF86gk8cwYeO4F1H8KYjeNURvOgInnUETzqCRxzBg47gHkdwhyO4xRFc7wiucQRXOIKLHcEFjuBsR3CGIzjNEZziCE5yBMuOYMkR/MMRzDuCOUcw4wh+7wgOOILfOoL/dgT/6Qh+5QhGHMGQI9jpCAYd7UFHV9Dxv0HHl0HH10HHv4KOnqDju6BjVNDxY9Dxc9Dxa9BRCDqKQce4oGNC0FENOqYGHc2gY2bQMTfoWBh0LAs6VgYd64KOTUHHtqBjd9CxL+g4HHQcCzpOBR3ng47LQceNoONO0PEg6HgcdDwPOt4EHe+DDhN0uKDDBT/hgm9xwVe44DNc8BEueA8XvIkLXsMFL+GCZ3HBk7jgEVzwIC64FxfcgQtuxQU34IJrcMEVuOBiXHABLjgbF5yBC07DBafggpNwwTIuOBYXLOKCg7hgDhfM4IJpXDCJC36LC/4bF/wnLvgVLhjBBUO4YCcu2Pb5i1Yk8tlr+y+8zgDe3wJ4Xwbwvgrg/SuA1xPA6w/gjQrgpQN4Pwfwfg3gFQJ4owN4YwN4EwJ41QDe1ADecABvJIA3N4C3IIC3NIC3KoC3NoC3KYC3NYC3K4C3P4B3KIB3PIB3OoB3IYB3JYB3PYB3J4D3IID3OID3PID3OoD3IYCnANYF+OgFeOMFeOEFeOIFeOAFuOMFuOkFuOoFuOAFOOMFOOEFOOIF2O8F2O0F2O4F2OwFWOcFWOUFWOYFWOQFmOcFmOkFGPYCTPUCTPYCVLwAf3oBRnsBCl6AX70AP3sBfvACjPIC9HsBerwA//ICxLwAX3oB/uYF6PICtHsBAl6AgNdGwOsk4IUIeH8n4H1FwIsT8P5NwOsn4P2H//K+57+8nwh4OQLe7wS8IgGvRMAbT8CbRMCrE/CaBLwRAt5cAt4CAt4SAt5KAt5aAt5GAt5WAt5OAt4+At4hAt4xAt4pAt45At5lAt51At5tAt4DAt5jAt5zAt5rAt4HAp4IWEfgk0fgrUfgpUfgqUfgkUfgrkfgpkfgqkfggkfgjEfgpEfgiEfggEdgt0dgu0dgi0dgvUdgtUdguUdgkUdgvkdglkdgukeg4RGY7BGoeATGeQTGeASGPAK/eQSyHoEfPAKjPALfeQR6PQLfeARiHoEvPQL/6xHo8gj8t0cg4BH8L4+O//IIBTz+HvCIBjz+GfBIBDz6Ah7JgMf3AY9MwOOXgMdgwOOPgEcp4DE+4DEp4FELeEwLeIwEPOYEPBYEPJYEPFYGPNYGPDYGPLYGPHYGPPYFPA4FPI4FPE4FPM4FPC4HPK4HPG4FPO4HPB4HPJ4FPF4HPN4HPEzAwwUsXuATXuAtXuAVXuAZXuARXuAeXuAmXuAqXuAiXuAsXuAkXuAIXuAAXmAPXmAHXmALXmA9XmA1XmA5XmAxXmA+XmAWXmA6XqCBF5iCF6jgBcbhBcbgBYbwAr/hBbJ4gR/xAim8wABeoBcv8A1e4Gu8wD/wAv+LF+jGC/w3XuD/fv6i9b//+9kL/D94/+3H6/bjhf14//Djfe3HS/jxvvXjDfjxRvnxfvTjfvHjDfpxf/hxJT+u7MdN8uPV/HjT/Hgz/Hhz/Hjz/XhL/Hgr/Xhr/Hgb/Xhb/Xg7/bh9ftwhP+6YH++UH++8H++yH++6H++2H++BH++xH++5H++1H++DH8/4sZ6fj87PG+fnhfPzxPPzwPNz2/m54fxccX7Oe35OOz/HnZ/Dzs8+52eX87PN87PJ+Vnr/Kx0fpY6Pws8P3M8PyOen2men7rnZ5LnZ7zzM9b5+cP5GXR+fnF+fnR+vnd+ks5Pn+cn4fn5p/MTdX7Czs//eH7+2/n5v86P3/nxuwB+147f68bv/S9+70v83tf4vW/we9/i9wbweyn83o/4vV/wuzx+bwi/Nwa/V8bvTcTv1fB70/B7M/B7s/F78/G7xfjdCvxuDX63Eb/bit/bid/bi987hN8dw+9O4ffO4fcu4/eu43e38bsH+L3H+L3n+L3X+L0P+D2D3zr8nzz8bz38Lx3+pw7/Aw//HQ//TQ//FQ//BQ//GYf/hMN/2MO/z+Hf7fBvd/g3OfzrPPyrHP5lDv9CD/9cD/+Ih7/p4a97+Kse/vEe/rEe/qKH/3cPf87h/8nhTzn8SYe/z8P/b4f/nw5/1MP/dw9/yOHvcPiDDr/fEfB7tPs9uvwef/N7fOn3iPk9/uV39Pod3/kdo/yOH/0eWb/Hb36PIb/HGL/HOL9Hxe8xxe9o+B3T/Y7Zfsd8v8div8cKv8cav8cGv8cWv2On32Ov3+OQ3+Oo33HS7zjn97jk97ju97jtd9z3Ox77Hc/8jtd+j/d+D+P3sH6L83/C+d/i+V/i+Z/i+R/h+e/i/Ddx/qt4/os4/xmc/wTOfwTPfwDPvxvn34bzb8L51+H8q3D+ZTj/Qpx/Lp5/BOdv4vx1PP9knH8Czj8W5x+N5x/E+XM4/084fxrn/w/O34fz9+D5/4nzR3H+CJ4/hOfvxPMH8fz/5/MX//9Q6HPr//z/aLX5aHX6aP3NRyvio/WVj9Y3Plq9PloDPlqjfLR+8NHK+mjlfbSGfHijfXjjfLQqPlpTfLSm+WhN99Ga7aM130driY/WSh+tNT5aG320tvpo7fTR2uujdchH65iP1mkfrfM+Wpd9tK77aN3x0Xrgo/XER+ulj9YbH62PPlrWh/N8fGr5eNvy8aLl40nLx8OWj7stHzdbPq60fFxo+Tjt+Tju+Tjs+djX8rGr5WNry8fGlo+1LR8rWz6WtHwsaPmY3fIxo+VjWsvHlJaPSsvHuJaPMS0fQy0fec/HLy0fP7Z8jGr5GGj56G35+Kbl4+uWjy9bPv7W8tHV8tHm+fgvz4ev5cPX+j/4WkF8rU58rRC+1t/xtb7C14rja/Xga32HrzUKX+tHfK0svtZv+FoFfK0x+Frj8LUm4mtNwddq4GsN42vNxteaj6+1GF9rBb7WGnytDfhaW/G1duJr7cXXOoSvdQxf6zS+1nl8rcv4WtfxtW7jaz3A13qMr/UCX+sNvtZHfC3hsx6+Ty1871r4XrXwPWvhe9jCd7eF72YL39UWvostfKdb+E608B1u4dvXwrerhW9bC9+mFr61LXwrW/iWtPAtaOGb28I3o4Wv2cJXa+Gb2MI3voVvTAvfUAvfby18WQ/fjx6+lIdvoIXv2xa+b1r4Yi18/2jh+98Wvu4WvvYWvv/y8Pla/B9fi6CvRYevxf/4WkR8LaK+FnFfix5fi36fxyhfix98LX72tfjV16Lga1H0tRjna1HxtZjsazHV12K6r8VMX4t5vhaLfS2W+1qs8bXY4Guxxddip6/FXl+LQ74Wx3wtTvk8zvlaXPK1uO5rcdvX4r6vxf/La1++PXIkDJ6uT2nyUeoVtVpqWS2nZdmSZVmWZdlVZmZmZmZmZmZmZrfL5bZdbjNTmZmZqSIVGU9GRygz97p+58Nbs9vr6Zk9O7Nn/4n7uj+ZKvhiquDbqYIfpwp+nSqYPXU2s6f+gpj6PWLq14ipnyGmfoiY+h5i6huIqbMQU19ETH2O2VOfREx9BDF1JmLq/cye+jdmT70TMfUWxNQbEVOvQUy9HDH1EsTU8xFTz0ZMPR0x9SRmTz2W2VOPREw9FDH1AMTUfZg9dQ9mT90FMXV7xNStEVM3R0zdEDF1XcTUtRBTV0NMXQExdRnE1Gl2Ln/VVa2YuhhimSFixSFi9SFirSFi/SFikyFi8yFimyFixyFi1yFiryFi3yHioCHisCHi6CHi+CHilCHijCHinCHiwiHi0iHiyiHi2iHipiHi9iHi7iFi+hDxwBDx8BDx2BDx9BDx/BDxyhDx+hDx9hDxwRDx6RDx1RDx7RDx4xDx25DfxJCfxJDvxJAvxZBPxZAPxJC3xZA3xJBZYsgLYsgzYsgTYsg/xJAHxZD7xJB7xJA7xJCbxZDrxJCrxJBLxZALxZBzxZAzxJBTxJDjxZCjxZDDxZCDxZD9xJA9xZBdxZAdxZBtxZAtxJBNxJD1xZC1xZDVxZCVxJDlxJClxJDFxZChmMZQLMNQrMBQrMJQrMVQrMtQbMRQbM5QbM1QbM9Q7MJQ7MFQ7MNQHMBQHMpQHMVQHMtQnMxQnM5QnM1QXMBQXMJQXMlQXMtQ3MhQ3MZQ3MVQ3MtQ/J2heJiheIyheJqheJ6heJmheJ2heJuh+ICh+ISh+JKh+Jah+JGh+JXh7NkMfxEMvxcMvxIMPxMMPxIM3xUM3xAMZwmGLwqGzwqGTwiGjwiGDwqG9wmG9wiGdwiGNwuG1wuGVwuGlwuGFwmG5wqGZwqGpwiGJwiGRwuGhwuGBwuG+wmGewmGuwmGOwmG2wqGWwiGmwiG6wuG6wiGqwuGKwuGywuGSwqGSwimDgVLDwUrDAWrDgVrDgXrDgUbDQWbDwVbDwXbDwU7DwW7DwX7DAUHDAWHDgVHDgXHDgUnDQWnDwVnDwXnDwUXDwVXDAXXDgU3DgW3DQV3DQX3DgV/HwpmDgWPDgVPDQXPDwUvDwWvDwVvDwXvDwWfDAWfDwXfDAU/DgW/DgWzh7MRw18Qw+8Rw68Rw88Rw48Qw/cQwzcRw1cRw5cQw2cRwycRw0cQw4cQwxmI4T2I4Z2I4S2I4Q2I4dWI4WWI4UWI4bmI4ZmI4amI4YmI4TGI4RGI4SGI4f6I4d6I4e6I4U6I4baI4ZaI4aaI4fqI4TqI4eqI4cqI4fKI4VKI4eJ2Ln+llaxYfFHEUgPE8gPEygPE6gPEOgPEBgPEZgPEVgPEtgPETgPE7gPE3gPEAQPEIQPEEQPEcQPESQPEaQPEWQPE+QPEJQPEFQPEtQPEjQPErQPEXQPE9AHigQHi4QHisQHiqQHi+QHilQHi9QHi7QHigwHi0wHiqwHi2wHipwHi1wG/iwE/iwHfiQFfiQGfigEfiAHviAFviAGviAEviAFPiwGPiQEPiwEPiAHTxYC7xYDbxYCbxIBrxYArxYBLxIALxICzxYDTxYCTxIDjxIAjxYBDxYADxIB9xIDdxYCdxYDtxICtxIBNxYANxYB1xYA1xIBVxIDlxYClxIAlxICBGDAQizMQSzIQyzEQKzMQqzMQazMQGzAQmzAQWzIQ2zIQOzEQuzEQezEQ+zMQhzAQRzIQxzIQJzIQpzEQZzEQ5zMQFzMQlzMQ1zAQNzIQtzIQdzEQ0xmIvzMQMxmIxxiIpxiI5xiIlxmI1xmItxmIDxiITxiILxmIbxmInxiIXxn8PpvBL4LBd4LBV4LBp4LBh4LBu4LBG4LBLMHgRcHgacHgCcHgYcHgAcFgumBwt2Bwh2Bwk2BwnWBwlWBwqWBwoWBwtmBwumBwkmBwnGBwtGBwqGBwoGCwr2Cwh2Cwi2CwvWCwlWCwqWCwoWCwrmCwhmCwimCwgmCwtGAwVTAYCIYDwbSBYNmBYMWBYPWBYO2BYP2BYJOBYIuBYNuBYKeBYLeBYO+BYL+B4JCB4IiB4JiB4MSB4NSB4OyB4LyB4KKB4PKB4OqB4IaB4NaB4M6B4N6B4P6BYOZA8OhA8ORA8NxA8NJA8NpA8NZA8P5A8MlA8MVA8O1A8ONA8OtAMHvwO2LwC2LwPWLwNWLwGWLwIWLwLmLwBmIwCzF4ETF4BjF4AjH4B2LwIGJwH2JwN2JwB2JwM2JwHWJwFWJwKWJwIWJwDmJwBmJwMmJwHGJwFGJwKGJwEGKwL2KwB2KwC2KwPWKwNWKwGWKwEWKwLmKwJmKwKmKwAmKwDGIwDTFYzM41WnFFKxZbBLFUH3+5Pv7KfcRqfcTafcQGffxN+/hb9hHb9hE79RG79xF79/EP6CMO6SOO6COO7SNO6iNO7yPO6iPO7yMu6SOu6COu7SNu7CNu6yPu6iOm9xEP9PEf7uM/1kc83Ue80EfM6iNe7yPe6SM+7CM+7SO+6iO+WxTxcx/xe5/fRZ+fRZ/vRZ+vRJ/PRJ8P/T7viD5v+H1m+X1e8Ps8Lfo8Lvr8Q/R5QPS5z+9zt+hzu+hzk9/nWr/PlaLPJWJRLhB9zhZ9Thd9ThJ9jhV9jhR9DhV9DhB99hF9dheLsrPos53os6Xos6nos4Hos47os7ros7Los5zos5TfZwnRZ1HRpy+G9P0l6Yvl6IuV6IvV6Iu16Yv16YuN6Yst6Itt6Ysd6Yvd6Iu96Yv96YtD6Isj6ItjWVScRF+cRl+cRV+cT19cTF9cTl9cQ1/cSF/cRt+/i76YTl/8nb54mL54jL54mr7/PH3xMouK1+mLd+iLD+mLT+mLL+mL7+iLn+mL3+jPFvR/EfS/F/S/FvQ/E/Q/EvTfFfTfEPRnCfovCBZ9RtB/QtD/h6D/gKB/n6B/j6B/h6B/k6B/naB/laB/qU//QkH/bEH/dEH/JEH/OEH/KEH/UEH/QJ/+vj79PQT9nQX97QT9rQT9zQT9DQX9dQWLriHoryzoLy/oLyXoLyHoLyoY9gXT+oJl+4IV+4LV+oK1+oL1+oKN+4It+oJtFxXs2Bfs2hfs1Rfs1xcc0hcc0Rcc0xec2Bec2hec1Rec1xdc1Bdc3hdc3fe5oS+4tS+4sy+4ty+4vy+Y2Rc82hc81Rc83/d5uS94vS94py/4oC/4pO/zZV/wbV/wU1/wW18g+gLR/xXR/x6//zV+/3NE/yNE/z1E/01E/1VE/0VE/xlE/wlE/x+I/oOI/gxE/x5E/w5E/2ZE/zr8/lWI/qWI/oWI/jmI/hmI/smI/nGI/lGI/qGIRQ9E9PdF9PdA9HdG9LdH9LdG9DdDLLohor8uor8Gor8Kor8Cor80or8Eor+onWu0/PLWX3Rh/Kk9/KV7+Cv08Ffp4a/Zw1+vh79xD3+LHv7WPfwdevi79vD36uHv18M/uId/eA//mB7+iT3803r4Z/Xwz+/hX9zDv7yHf00P/4Ye/q09/Lt6+Pf28O/v4c/s4T/Ww3+6h/98D//lHv7rPfx3evgf9vA/7eF/2cP/rof/cw//tx6z/R6/+D2+93t85ff4zO/xod/jXb/Hm36PWX6PF/weT/s9HvN7POz3eMDvMd3vcbff43a/x41+j2v9Hlf4PS7xe5zv9zjb73Ga3+NEv8exfo8j/B6H+D3293vs5ffY1e+xo99jG7/H5n6Pjf0e6/k91vJ7rOr3WMHvsYzfY6rfYzG/R8/v0fMXpecvQc9fmp6/PD1/ZXr+GvT8den5G9HzN6Pnb0XP356evws9fw96/r70/IPo+YfT84+h559Azz+Fnn8mPf88ev5F9PzL6PlX0/NvoOffSs+/k57/N3r+/fT8mfT8R+n5T9Hzn6fnv0zPf52e/zY9/wN6/if0/C/p+d/R83+i5/9Gb7ag96tP7wef3tc+vc98eh/59N7z6b3p05vl03vBp/eMT+8Jn94/fHoP+PSm+/Tu9und7tO7yad3rU/vCp/eJT69C3x6Z/v0TvPpneTTO86nd6RP7xCf3v4+vb19erv59Hby6W3j09vCp7eJT299n97aPr1VfXor+vSW9elN8+kt5tPr+Sza81m857NUz2e5ns/KPZ81ej7r9Hw27Pls2vPZquezfc9n557PHj2ffXs+B/V8Duv5HNXzOb7nc0rP58yez7k9nwt7Ppf1fK7u+dzQ87ml53NHz+dvPZ/7ez4zez6P9nye7Pk81/N5uefzes/n7Z7P+z2fT3o+X/Z8vuv5/NTz+a3nI3oCv/crfu8H/N43+L3P8Xsf4ffew++9id97Fb/3In7vGfzeE/i9f+D3HsDv3Yffuwe/dzt+7yb83nX4vSvxe5fi9y7A752N3zsdv3cyfu84/N6R+L1D8HsH4Pf2xu/tht/bCb+3LX5vS/zeJvi99fF7a+P3VsPvrYTfWxa/Nw2/txh+bxE712jZZa2/yEL4i3fxl+ziL9fFX6mLv3oXf50u/oZd/M26+Ft28bfr4u/cxd+ji79vF//ALv6hXfyjuvjHd/FP6eKf0cU/t4t/YRf/si7+1V3867v4t3Tx7+zi39vF/3sX/6Eu/qNd/Ke6+M938V/p4r/exX+7i/9BF//TLv5XXfzvuvg/dfF/7zLb7/KL3+V7v8tXfpdP/S4f+F3e8bu84Xd5xe/yvN/lKb/Lo36XmX6Xv/td7vW73Ol3udXvcoPf5Wq/y+V+l4v8Luf6Xc70u5zidzne73K03+Uwv8tBfpd9/S57+F129rts73fZyu+yqd9lQ7/LOn6XNfwuK/tdlvO7LOV3Wdzvsqjfpet36fqL0PWHdP0l6frL0vVXpOuvTtdfm66/AV1/E7r+lnT97ej6O9P1d6fr70PXP4Cufyhd/yi6/vF0/ZPp+mfQ9c+l619I17+Urn8VXf96uv4tdP076fp/o+vfT9d/iK7/KF3/Kbr+83T9l+n6r9P136brf0DX/4Su/yVd/zu6/k90/d/pzhZ0f/Hp/uDT/dqn+7lP9yOf7rs+3Td8uq/4dJ/36T7t033cp/uwT/fvPt3pPt27fLq3+XRv8Ole49O9wqd7sU/3PJ/umT7dU3y6J/h0j/HpHu7TPcinu69Pd0+f7i4+3R18ulv5dDfz6W7k013Xp7umT3dln+7yPt2lfbpL+HQX9el2fRbp+izW9ZnW9Vmm67Ni12e1rs9aXZ/1uz4bd3227Pps1/XZqeuze9dn767PAV2fQ7s+R3Z9juv6nNT1OaPrc07X54Kuz6Vdn6u6Ptd3fW7u+tze9bmn6zOj6/NQ1+eRrs+TXZ/nuj4vdX1e6/q81fV5v+vzSdfni67Pt12fn7o+v3V9RHc2fvdX/O4P+N1v8Luf43c/wu++i999E7/7Kn73RfzuM/jdx/G7D+N3H8DvTsfv3oXfvQ2/eyN+91r87hX43Yvxu+fhd8/E756K3z0Rv3sMfvdw/O5B+N398Lt74nd3we/ugN/dGr+7OX53I/zuuvjdNfG7q+B3V8DvLo3fXQK/uyh+d2E7V7DMMlYuvCBysQ5yWge5bAe5Yge5Wge5dge5QQe5aQe5ZQe5XQe5Uwe5ewe5Twd5QAd5aAd5VAd5fAd5Sgd5Rgd5bgd5YQd5WQd5dQd5fQd5Swd5Zwf5tw7y/g5yZgf5aAf5VAf5fAf5Sgf5Rgf5Tgf5YQf5WQf5dQf5fQf5S4fR7A5CdvhVdvhBdvhadvhMdvhQdnhXdnhTdpglO7wgOzwtOzwqOzwsO/xddrhXdrhTdrhVdrhBdrhGdrhcdrhIdjhHdjhTdjhFdjhedjhKdjhMdjhQdthHdthDdthZdthedthKdthUdthAdlhbdlhddlhJdlhWdlhSdlhMdlhEdlhIdujIHh25GB05jY5cho5cgY5cjY5cm45cn47cmI7cgo7clo7ciY7cnY7cm448gI48lI48io48jo48iY48nY48l468kI68jI68io68no68hY68k468l468n46cSUc+Rkc+SUc+T0e+TEe+Tke+Q0d+SEd+Skd+RUd+T0f+QkfOpiMknV8lnR8kna8lnc8knY8knfcknTcknVmSzouSztOSzhOSzsOSzt8lnXslnbsknVslnRsknWsknSsknYslnfMknTMlnVMknRMknaMlncMknQMlnX0knT0knV0kne0kna0knU0lnQ0lnXUkndUlnZUkneUknaUkncUlnb6ks5Bk4Y5k0JFM7UiW6UhW6EhW7UjW7EjW60g27ki26Ei27Uh27Eh260j27kj270gO6UiO7EiO60hO6khO70jO6Ugu6Egu7Uiu6kiu70hu6Uju6Ej+1pHM6Ege6kge6Uie7Eie60he7khe70je7kg+6Eg+7Ui+7Ei+60h+7kh+70hGHR/Z+Q3Z+RHZ+QbZ+RzZ+QjZeQ/ZeRPZeRXZeRHZeQbZeRzZeRjZeQDZmY7s3IXs3Ibs3IjsXIvsXIHsXIzsnIfsnInsnIrsnIjsHIPsHI7sHITs7Ifs7Ins7ILsbI/sbIXsbIbsbIjsrIPsrIHsrITsLIfsLIXsLI7s9JGdhexcwVJLWdltIRdtI6e2kUu3kcu3kau0kWstiFyvjdy4jdy8jdymjdypjdytjdy7jdy/jTykjTyyjTyujTy5jTy9jTynjbygjby0jbyqjbyujby5jby9jbynjby/jXyojXy0jXxyQeRzbeQrbeTrbeTbbeQHbeSnbeTXbeT3beQvbUaizWzZ5hfZ5gfZ5mvZ5jPZ5kPZ5h3Z5g3ZZpZs84Js87Rs86hckIflgvxdLsi9ss2dss2tss0Nss3Vss1lss2Fss25ss0Zss3Jss1xss2Rss2hss0Bss3ess1uss1Oss22ckG2kG02lm3Wl23Wlm1WlW1WkG2Wlm2myjaLyTYLywVpyzZtuRBt2actF6ctl6Itl6MtV6Yt16At16UtN6QtN6Mtt6Ytd6Atd2VBuScLyv1oy4NpyyNoy2NYUJ5IW55GW55NW15AW15CW15JW15HW95MW97OgvJuFpQzaMuHaMtHacsnactnacuXacvXacu3acsPaMtPWFB+RVt+T1v+zIJyNm1f0v5N0v5B0v5G0v5c0v5Y0n5P0n5D0p4lab8oaT8taT8haT8sWfDvkvZ0SfsuSftWSfsGSftqSftySfsiSfs8SfsMSftkSft4SfsoSfswSfsASXsfSXsPSXtnSXs7SXsLSXsTSXsDSXttSXs1SXsFSXsZSXuapL2YpN2TtBeULNSWLNKWDNuSJduS5dqSlduS1duSddqSDdqSzdqSrduS7duSXdqSPdqS/dqSg9uSw9uSY9qSE9qS09qSs9uS89uSS9qSK9qS69qSm9qS29qSu9uS+xaUPNiWPNKWPNGWPNuWvNSWvNaWvNWWvN+WfNKWfNmWfNeW/NyW/N6WjNo+csHfkO0fke1vke0vkO2Pke33kO03ke1Xke0Xke1nkO3HkQs+jGw/gGxPR7bvQrZvRbZvQLavQbYvR7YvQrbPQ7bPRLZPQbaPR7aPQrYPQ7YPRC64D7K9O7K9E7K9HbK9JbK9KbK9AbK9NrK9GrK9IrK9LLI9DdleDNnuIdsL2rnktGl21FmAUb/FaNhitGSL0bItRiu3GK3RYrROi9GGLUabthht3WK0Q4vRri1Ge7YY7dtidHCL0REtRse2GJ3UYnRqi9HZLUYXtBhd2mJ0VYvRtS1GN7UY3d5idE+L0f0tRg+1GD3aYvRki9FzLUavtBi93mL0TovRBy1Gn7YYfd1i9H2L0S8tRrNbCL/Fr6MWP4xafD1q8dmoxYejFu+MWrwxavHKqMXzoxZPjVo8Omoxc9Ti/lGLv41a3DFqccuoxfWjFleNWlw6anHBqMU5oxanj1qcNGpx7KjFEaMWh4xa7D9qseeoxa6jFjuMWmwzarH5qMWGoxbrjlqsMWqxyqjF8qMWS45aLD5q0R+16I5atEYtWqMOrdEitEZDWqNptEbL0hqtRGu0Oq3ROrRGG9AabUprtBWt0fa0RrvQGu1Ba7QvrdFBtEaH0xodQ2t0Aq3RqbRGZ9EanU9rdDGt0ZW0RtfSGt1Ea3Q7rdHdtEYzaI0eojV6hNboSVqjZ2mNXqY1ep3W6B1ao/dpjT6hNfqK1uh7WqNfaI1+pyV8Wr+NaP0wovX1iNZnI1ofjmi9N6L1xojWKyNaL4xoPT2i9diI1swRrftHtO4d0bpzROuWEa3rR7SuGtG6bETrwhGtc0a0Th/ROmlE67gRrSNHtA4Z0dp/RGuvEa3dRrR2HNHaZkRr8xGtjUa01hvRWnNEa5URreVHtJYa0VpiRGvREa3uiFZrRKc1otcasVhrxLTWiGVbI1ZqjVitNWLt1ogNWiM2bY3YqjViu9aInVsj9miN2Lc14qDWiMNaI45ujTihNeLU1oizWiPOa424uDXiitaIa1sjbmqNuK014u7WiPtaIx5sjfhHa8QTrRHPtka81BrxWmvE260R77dGfNIa8WVrxHetET+3RvzeGuG3BKPWb4xaPzJqfcOo9Tmj1keMWu8xar3JqDWLUesFRq2nGbUeY9R6mFHr74xa9zJq3cmodQuj1vWMWlczal3GqHUho9Y5jFqnM2qdzKh1PKPWkYxahzBq7c+otTej1m6MWjsyam3DqLU5o9bGjFrrMWqtyai1CqPW8oxaSzNqLcGotSij1sKMWm07V7DEEla250P2msjFmshpTeQyTeSKTeRqTeQ6TeQGTeQmTeRWTeT2TeQuTeSeTeS+TeRBTeThTeQxTeSJTeSpTeRZTeT5TeQlTeRVTeR1TeTNTeQdTeTfmsj7m8iHmshHm8inmsjnm8hXmsg3msh3msgPm8jPmsivm8gfmshfm0i/iS+b/Cab/CibfCObfCabfCSbvCebvCmbzJJNnpdNnpJNHpVNZsom98smf5NN7pBNbpFNrpdNrpJNLpVNLpBNzpJNTpNNTpRNjpVNjpBNDpJN9pNN9pRNdpFNtpdNtpZNNpNNNpBN1pFNVpdNVpZNlpVNpskmi8kmPdmkI5vML5s0ZZumXJimHNCUU2nKZWjKFWnK1WjKtWnK9WnKTWjKLWnK7WjKnWnKPWjKfWnKg2jKw2nKY2jKE2jKU2nKs2jK82nKi2nKK2nKa2nKm2nKO2jKe2jKGTTlQzTlozTlUzTl8zTlyzTl6zTluzTlRzTlZzTlVzTl9zTlLzTlbJr+iOZvkuaPkuY3kubnkuaHkuZ7kuabkuYsSfNFSfNpSfMxSXOmpHm/pHmvpHmnpHmLpHm9pHmVpHmZpHmhpHmOpHmapHmSpHmcpHmkpHmIpLmfpLmnpLmrpLmDpLm1pLmZpLmhpLmOpLmGpLmypLmcpDlN0hxKmotImh1Jc35JuynpNiWLNiVLNCXLNCUrNiWrNiVrNSXrNSWbNCVbNiXbNiU7NSW7NyX7NiUHNSWHNSVHNyUnNCWnNiVnNSXnNSUXNyVXNCXXNiU3NSW3NSV3NyX3NSUPNiX/aEqebEqea0pebkpeb0rebko+aEo+bUq+akq+b0p+aUpmNyWj5gjZ/B3Z/BHZ/BbZ/ALZ/BjZfA/ZfBPZfBXZfBHZfAbZfAzZfBjZ/DuyeS+yeReyeQuyeT2yeTWyeRmyeSGyeQ6yeTqyeTKyeRyyeSSyeQiyuT+yuReyuSuyuSOyuTWyuRmyuRGyuS6yuQayuTKyuRyyuRSyuTiy2Uc2O8jm/HYuNRzaYIF5CboNgkUbBEs0CJZuECzfIFilQbBWg2C9BsHGDYItGgTbNgh2ahDs3iDYp0FwUIPg8AbBMQ2CExsEpzYIzmoQnNcguLhBcEWD4NoGwc0NgtsbBHc3CGY0CB5sEDzSIHiyQfBcg+CVBsHrDYJ3GgQfNgg+axB80yD4oUHwa4PAb+AHDX4LGvwYNPgmaPB50OCjoME7QYM3gwazggYvBA2eDho8GjSYGTS4P2hwT9Dg9qDBzUGD64MGVwYNLg0anB80ODtocFrQ4MSgwdFBg8ODBgcHDfYLGuwRNNgpaLBt0GDLoMEmQYP1gwZrBQ1WDRqsGDRYOmiwRNBg0aBBN2jQDhrMFzRoBAvQCBaiEfRpBIvTCJaiESxPI1iFRrAmjWBdGsFGNILNaQTb0Ah2pBHsRiPYm0ZwAI3gUBrBUTSC42gEJ9MIzqQRnE8juJhGcDmN4BoawY00gttoBHfRCO6jETxII3iERvAkjeBZGsHLNILXaQTv0Ag+ohF8RiP4mkbwA43gVxqBoDGSNH4PaPwU0Pg2oPF5QOPjgMZ7AY03AxqzAhovBjSeCWg8HtCYGdC4P6Bxb0DjjoDGzQGN6wIaVwU0LgtonB/QOCugcVpA48SAxtEBjcMDGgcFNPYLaOwZ0Ng5oLFdQGPLgMYmAY31AxprBzRWC2isGNBYJqCxREBjENBYOKDRDmjMF7BAI6DTCFikETBsBCzVCFi+EbByI2CNRsA6jYCNGgGbNwK2bgTs0AjYtRGwdyPggEbAIY2AIxsBxzUCTm4EnNEIOK8RcHEj4IpGwDWNgBsbAbc2Au5qBExvBDzQCPhHI+CJRsCzjYCXGgGvNQLebgR80Aj4tBHwVSPg+0bAr40A0QiQjRFB43eCxk8Eje8IGl8QND4maLxH0HiToPEqQeNFgsYzBI3HCRozCRp/J2jcS9C4k6BxM0HjOoLGVQSNywgaFxI0ziJonE7QOImgcRxB4wiCxkEEjX0JGnsRNHYlaGxP0NiSoLEJQWMDgsbaBI3VCBorEjSWIWhMI2gsRtDoETTaBI357FxqscVsMN88BB2PYBGPYOgRTPMIlvUIVvII1vAI1vUINvQINvMItvYIdvQIdvMI9vII9vcIDvEIjvIIjvcITvEIzvQIzvUILvYIrvAIrvEIbvQIbvMI7vEIZngED3oEj3gET3oEz3sEr3gEr3sE73gEH3oEn3sE33gEP3gEv3oEvsco8Pgt8Pgx8Pgm8Pg88Pgo8Hg38Hgj8Hgl8Hg+8Hgq8Hg08Hgo8JgReNwTeNweeNwUeFwbeFwReFwceJwXeJwZeJwSeBwfeBwVeBwaeOwfeOwdeOwWeOwYeGwTeGweeGwUeKwbeKwReKwceCwXeCwZeAwDj0UCj07gMX/gMW/g4QVNvGBBvKCHFyyGF0zDC5bFC1bCC1bHC9bBCzbECzbDC7bGC3bAC3bFC/bCC/bHCw7BC47AC47DC07GC87AC87FCy7CCy7HC67BC27EC27DC+7GC+7DCx7ECx7BC57AC57DC17GC17HC97BCz7ACz7DC77GC37AC37FCwTeSOL9HuD9GOB9E+B9HuB9HOC9F+C9EeC9EuC9EOA9HeA9FuDNDPBmBHh/C/DuCPBuDvCuC/CuCPAuCfDOD/DOCvBODfBOCPCOCvAOC/AODPD2DvB2C/B2CvC2DfC2CPA2CvDWDfDWDPBWCfCWD/CWDPCGAV4/wFsowFsgwJs3oOkFtL2Ahb2AgRcwzQtY1gtY0QtYzQtY2wvY0AvYzAvYygvY3gvYxQvYywvY3ws42As4wgs41gs42Qs4wws4xwu40Au4zAu4xgu40Qu41Qu4ywuY7gU86AU84gU84QU86wW85AW87gW84wV84AV86gV85QX84AX86gUIL0B6IwLvdwLvJwLvWwLvCwLvYwLvPQLvTQJvFoH3AoH3NIH3GIE3k8C7n8D7G4F3B4F3M4F3HYF3JYF3CYF3PoF3FoF3KoF3AoF3NIF3GIF3IIG3D4G3O4G3E4G3LYG3BYG3MYG3HoG3JoG3CoG3PIG3FIG3OIHXJ/AWIvBaBF7DzhUOBlbNNw+q46EW8VBDDzXNQy3roVbyUGt4qHU91IYeajMPtbWH2tFD7eah9vJQ+3uoQzzUUR7qeA91soc6w0Od56Eu9lBXeKhrPdRNHup2D3WPh7rfQz3koR71UE95qOc91CwP9YaHetdDfeShPvdQ33qoHz3Ubx5q5DFSHr8rj5+Ux7fK4wvl8bHyeFd5vKk8ZimPF5TH08rjUeUxU3ncrzz+pjzuUB43KY/rlMeVyuNi5XGe8jhDeZyqPI5XHkcpj0OVx/7KY2/lsZvy2FF5bKM8NlceGymPdZXHGspjZeWxnPJYUnkMlcciyqOjPOZXHp7y8FQTTy2Ip3p4ajE8NQ1PLYunVsJTq+OpdfDUhnhqMzy1NZ7aAU/tiqf2wlP746lD8NQReOo4PHUynjoDT52Lpy7GU1fgqWvx1E146nY8dTeemoGnHsJTj+Kpp/DUc3jqFTz1Bp56F099hKc+w1Pf4Kkf8dRveMrHkwHe7wrvJ4X3rcL7QuF9ovDeV3hvKrxZCu9FhfeMwntc4c1UePcrvHsV3p0K7xaFd53Cu1LhXarwzld4Zym8UxTe8QrvaIV3mMI7UOHtrfB2U3g7KbxtFd4WCm8jhbeuwltT4a2i8JZXeEsqvKHC6yu8hRTeAgrPUzQ9RdtTLOwpBp5imqdY1lOs6ClW8xRre4oNPcVmnmIrT7G9p9jFU+zlKfb3FAd7iiM8xbGe4mRPcYanOMdTXOQpLvcU13iKmzzFbZ7ibk9xn6d40FM84ime9BTPeYqXPcXrnuIdT/Ghp/jMU3ztKX7wFL95Ct9TBF6A8majvJ9R3nco70uU9wnKex/lvYXyXkV5L6K8Z1De4yjvYZT3d5R3L8q7E+XdgvKuR3lXobxLUN75KO8slHcqyjsB5R2N8g5DeQeivH1Q3u4obyeUty3K2wLlbYzy1kN5a6K8VVDe8ihvKZS3OMrro7yFUN4CKG9eO9dkv2/VvH9FteuoheuoQR01tY5apo5asY5avY5ap47aoI7atI7auo7aoY7atY7as47ar446pI46qo46vo46uY46o446t466qI66vI66uo66sY66rY66p46aUUc9WEc9Ukc9WUc9X0e9Uke9Xke9U0d9WEd9Xkd9U0f9WEf9VkeN6oxUnd9VnZ9UnW9Vnc9VnY9UnXdVnTdVnVmqzguqztOqzqOqzkxV535V5x5V53ZV5yZV51pV5wpV52JV5zxV5wxV5xRV53hV5yhV51BVZ39VZy9VZ1dVZwdVZ2tVZ3NVZ0NVZx1VZ3VVZyVVZ1lVZ5qqs5iqs7Cq01Z15lN15lF16qpBXbWoqy51tSh1tQR1tTR1tQJ1tRp1tRZ1tT51tQl1tSV1tT11tTN1tSd1tR91dTB1dQR1dSx1dRJ1dTp1dQ51dSF1dRl1dTV1dQN1dRt1dTd1dR919SB19Qh19QR19Rx19TJ19Tp19Q519QF19Rl19Q119SN19Rt15VOXAfXZivpPivq3ivoXivonivr7ivqbivosRf1FRf0ZRf1xRX2mon6/ov43Rf0ORf1mRf06Rf0KRf0SRf18Rf0sRf0URf14Rf1oRf0wRf0ARX1vRX1XRX1HRX0bRX1zRX0jRX0dRX0NRX1lRX05RX2aor6Yot5T1BdU1JuK+jyKeeuKBeqKbl2xaF2xRF2xdF2xfF2xal2xVl2xfl2xSV2xRV2xXV2xc12xZ12xX11xUF1xeF1xbF1xUl1xel1xdl1xYV1xWV1xdV1xQ11xa11xV10xva54sK54pK54oq54tq54qa54va54p674oK74tK74uq74oa74ra7w64qgHqDqAlX/GVX/DlX/ElX/BFV/H1V/C1V/FVV/EVV/BlV/HFWfiar/HVX/G6p+B6p+M6p+Hap+Jap+Cap+Pqp+Fqp+Kqp+Aqp+NKp+KKp+AKq+N6q+O6q+E6q+Daq+Oaq+Eaq+Lqq+Bqq+Mqq+LKo+DVUfouqLoOodVL2Jqs9j55rsLWLVPHOj5q+hFqqh+jXU4jXUkjXU8jXUqjXUmjXU+jXUJjXUFjXUdjXUzjXUHjXUvjXUwTXUETXUsTXUSTXU6TXUOTXUhTXU5TXU1TXUDTXUrTXU3TXUfTXUgzXUIzXUkzXUczXUKzXU6zXUuzXURzXU5zXUNzXUjzXUbzXUqMZI1fhd1fhJ1fhW1fhC1fhI1XhX1XhD1XhF1Xhe1XhK1XhU1XhI1ZihatytatymatykalyjalyualykapyrapyhapysahynahypahysauynauypauyiamyvamypamyiamygaqypaqyqaqyoaiylaiyuaiyqaiykarRUDU/V+KuqUVMeNdWkphakphahpobU1FLU1HLU1MrU1JrU1HrU1MbU1BbU1HbU1E7U1O7U1L7U1EHU1BHU1DHU1InU1GnU1DnU1AXU1GXU1NXU1A3U1K3U1N3U1H3U1IPU1CPU1JPU1LPU1MvU1OvU1DvU1EfU1GfU1DfU1I/U1G/UlE8tCKjNVtR+UtS+VdS+UNQ+UdTeV9TeVNRmKWrPK2pPK2qPKWoPKWozFLV7FLXbFbWbFLVrFbUrFLWLFLXzFLUzFLWTFbXjFLWjFLVDFLX9FbU9FbVdFLUdFLWtFLVNFbX1FbW1FbVVFLUVFLVlFLXFFbVFFbWuoraAotZQ1GoKr6Zo1hSdmmKRmmJYUyxZUyxfU6xcU6xeU6xXU2xcU2xeU2xbU+xUU+xeU+xbUxxYUxxeUxxTU5xYU5xaU5xdU1xQU1xaU1xVU9xQU9xSU9xZU0yvKR6oKf5RUzxRUzxbU7xUU7xWU7xdU3xYU3xWU3xdU/xQU/xWU/g1RVCTqNpsVO1nVO07VO1LVO0TVO19VO1NVO1VVO0FVO1pVO0xVG0mqnY/qnYPqnY7qnYTqnYtqnYlqnYJqnYuqnYGqnYyqnY8qnYUqnYIqrY/qrYnqrYLqrY9qrY1qrYZqrYBqrY2qrYqqrYCqrYMqjYVVRugagujam1UrYGq/dXOpbsL2/CvfyFsVgkXrDLZqzI5rDK5ZJXJ5aqEK1cJ16gSrlcl3LhKuEWVye2qTO5cZXL3KuG+VcKDqoSHV5k8psrkSVUmT68SnlNl8sIqk5dVmby6SnhjlfC2KuHdVcL7qoQPVQkfrTL5VJXw+SqTr1SZfKNK+F6V8OMq4RdVJr+tEv5UJZxdZTKoEkxWEZNVfpms8t1klS/DKh+HVd4Pq7wVVpkVVnkhrPL0ZJVHJ6vMnKwyI6xyT1jl9rDKjWGVa8Mql4dVLgqrnBtWOX2yyslhlWPDKkeEVQ4Kq+wXVtkjrLLzZJXtJqtsGVbZZLLK+pNV1gyrrBJWWS6sstRklcUnqywSVumEVZphFW+yytxhlWpYpzo5H9VwQaphj2o4pBpOoxouS3VyZaqTa1CdXI9quDHVcAuqk9tSndyJ6uRuVCf3oTp5ENXJw6hOHkM1PIFqeBrVyXOohhdSDS+lGl5NdfIGqpO3UQ3vphpOpxo+QDV8hGr4FNXJ56lOvkw1fINq+C7V8GOqk59TnfyWavgT1fB3qqGkqiapipDqLyHV7yepfjlJ9dOQ6gch1bdCqrNCqi+GVJ8JqT4eUp0ZUp0xSfVvIdU7Qqo3TVK9dpLq5ZNULw6pnjtJ9YxJqidPUj02pHpkSPXgSar7T1LdM6S6c0h1+0mqW01S3XSS6vqTVNcMqa4aUl0+pLpUSHXxkOoik1QXCqnOP0nVm6RaDalXQxrVkHY1ZOFqyKAaMq06yTLVkJWqIatXJ1mnGrJxNWTzasg21ZAdq5PsXp1kn+okB1YnOawacnR1khOrIadVQ86uhlxQDbm0GnJ1NeT6asit1ZC7qiHTqyEPVEP+UQ15sjrJc9WQl6uTvF4Neaca8lF1ks+rId9UJ/mpOsnv1UlkNWSyqpis+kxWfyGsfk9Y/Yqw+ilh9QPC6luE1VcJqy8yWX2GyerjhNWZhNX7Cat/Y7J6B2H1JsLqtYTVKwirFxNWz2OyegaT1ZOZrB5HWD2SyeohTFb3Y7K6J5PVXQir2zNZ3YrJ6qZMVtdnsroWYXVVJqsrMFldisnqEkxW+0xWFyKsLsBk1WOyOredSy+0kA1rZcL5KoTtCuHCFcLFKoTTKoTLVghXqhCuXiFct0K4UYVw8wrhNhXCHSuEu1cI96kQHlghPKxCeEyF8MQK4WkVwrMrhBdUCC+rEF5VIbyhQnhrhfCuCuF9FcIHK4SPVAifrBA+VyF8pUL4RoXw3QrhRxXCLyqE31YIf6oQ/l5hMqgQhBVmhxV+Dit8F1b4IqzwcVjhvbDCW2GFWWGFF8IKT4UVHg0rPBRWmBFWuCescFtY4cawwjVhhcvDCheFFc4JK5wWVjgprHBsWOHwsMJBYYV9wwp7hBV2CitsG1bYPKywcVhhvbDCGmGFlcMKy4YVlgwrDMMKvbDCgmGF+cIK9bDCX8IKlbBGJWxQCVtUwoWohItSCZegEi5DJVyRSrgqlXBtKuGGVMLNqIRbUwl3oBLuRiXcm0p4AJXwMCrhUVTCE6iEp1IJz6ISXkAlvJRKeBWV8Hoq4a1UwjuphNOphA9QCR+hEj5JJXyWSvgylfANKuG7VMIPqYSfUwm/pRL+RCX8nUooqaiQih9S+SWk8n1I5cuQyqchlfdDKm+FVGaFVF4MqTwTUnkspPJQSGVGSOVvIZXbQyo3hlSuCalcHlK5KKRybkjl9JDKSSGVY0Mqh4dUDg6p7BtS2SOkslNIZbuQyhYhlY1DKuuFVNYIqawSUlkupDItpDIMqfRCKp2QSjOkUg+p/CVk7krIvJWQBSohC1VC+pWQJSohS1dCVqyErFoJWbsSsmElZNNKyFaVkB0qIbtWQvauhOxfCTm0EnJUJeT4SsgplZCzKiHnV0IuqYRcWQm5vhJySyXkzkrIvZWQv1dC/lEJeaIS8mwl5KVKyOuVkHcrIR9WQj6vhHxTCfmxEvJbJWRUCZmsKMKKT1j5hbDyA2HlK8LKp4SVDwgrbxFWXiWsvEhYeYaw8jhhZSZh5X7Cyt8IK3cQVm4irFxDWLmCsHIxYeVcwsoZhJWTCCvHEVaOJKwcTFjZj7CyB2FlZ8LKdoSVLQkrmxBW1iOsrElYWYWwsjxhZUnCymKElUUIKx3CyvyElXkIK9U5AMxdJvQqhK0KYbdC2K8QLl4hXKZCuGKFcLUK4doVwg0qhJtWCLeuEO5QIdy1QrjXvwBwdIXwhArhqRXCsyqE51cIL50DwPV/AOCB/wEAH88B4Ls5AMyuEM4BQPwLAF+GFT6ZA8CbYYVZYZnnwzJPhRUeCSs8GFa4L6xw9xwAbggrXB1WuCyscEFY4eywzGlhmRPDMkeHFQ4LKxwYVtg7rLBbWGHHsMLWYZnNwgobhhXWCSusFlZYMaywTFhhibDComGFblihFZaZN6xQC8tUwjKVcG4qoUclXGAOAH0q4eJUwqWphCv8CwAbUAk3pRJuNQeAXamEe80B4JD/BoByeD7l8FLK/0MAnqASPkclfOVfAPj43wJQ/lcAvvsDAG/OAeCFkPJTIeXHQsoPhVTuC6ncE1K5bQ4AV4dULgupXBhSOWcOACeGVI6eA8BBIZV9Qiq7hVR2DKlsE1LePKSyUUhlnZDK6iGVlUIqy4ZUpoZUFg2pdEMq7ZBKI6Ty15BK5T8BmKcSMn8lpDMHgMUrIUtVQpavhKxSCVmrErJBJWSTSsiWlZDt5wCw1xwADqmEHDkHgFMrIWdWQs4rh1xS/k8Arvu/AODlOQC8Uwn56A8A/P5fASgrwvJ/BeD7fwHg/X8B4AXCytOElccIKw8RVmYQVu4hrNxOWLlxDgCXE1YuJKycQ1g5fQ4AxxCWDyesHERY2YewsvscALYhrGxOWNmIsLIuYWV1wspKhJVlCStTCcsDwvLChJU2YaVBWKkTVv5i5/pnp2N19c9or4xuldHdMrpfRi9RRi9dRq9YRq9WRq9dRm9QRm9WRm9TRu9YRu9WRu9dRh9QRh9aRh9dRp9QRp9aRp9dRl9QRl9aRl9dRt9QRt9aRt9dRt9XRj9YRj9aRj9VRj9fRr9SRr9ZRr9XRn9SRn9ZRn9XRv9cRosyWpVRuoyvy/yiy3yvy3yly3yiy7yvy7yly7yqy7ygyzylyzyqyzyky8zQZe7RZW7TZW7QZa7RZS7TZS7UZc7WZU7TZU7UZY7RZQ7TZQ7UZfbRZXbXZXbUZbbRZTbTZTbUZdbRZVbXZVbUZZbRZabqMovqMl1dpqXLzKvL1HSZsi5T1nNT1h5lvQBlvRBl3aesF6esl6asV6CsV6Ws16asN6CsN6Wst6asd6Csd6Ws96asD6CsD6Wsj6KsT6CsT6Wsz6KsL6CsL6Wsr6Ksb6Csb6Ws76Ksp1PWD1LWj1DWT1LWz1PWr1DWb1DW71HWH1PWX1LW31LWP1HWsynrgLIKKfua8i+a8vea8lea8qea8vua8lua8qua8oua8tOa8mOa8kOa8gxN+R5N+XZN+UZN+RpN+TJN+SJN+RxN+XRN+URN+RhN+TBN+SBNeR9NeXdNeSdNeRtNeXNNeSNNeR1NeXVNeSVNeVlNeaqmPNCUu5pyW1NuaMp/1ZQrmrnLmnnKmvnLmk5Z0y9rFi9rliprli9rVi1r1ipr1i9rNi1rtiprti9rdi1r9ipr9i9rDilrjiprji9rTilrziprzi9rLilrriprri9rbilr7iprppc1D5Q1j5Q1T5Y1z5U1L5c1b5Q175Y1H5c1X5Q135Y1P5U1v5c1sqwJyyG6PEKXf0WXf0CXv0aXP0OXP0CX30KXX0WXX0SXn0GXH0eXZ6LL96PLf0OXb0eXb0KXr0GXL0eXL0KXz0WXT0eXT0KXj0WXj0CXD0KX90WXd0eXd0KXt0WXt0CXN0KX10WX10CXV0aXl0WXp6HLi6HLC6PLbXR5PnT5r+hyxc71z3bb6r/8Ce2V0POX0J0Sul9CL15CL1VCr1BCr1pCr11Cb1BCb1pCb11C71BC71pC711CH1BCH1ZCH11Cn1BCn1pCn1VCn19CX1pCX1VCX19C31pC31VC31dCP1BCP1JCP1VCP19Czyqh3yih3y2hPy6hvyyhvyuhfy6hZ5fQqoTSJXxd4hdd4ntd4ktd4hNd4n1d4i1d4lVd4gVd4mld4lFd4iFdYoYucbcucZsucYMucY0ucZkucaEucbYucZoucaIucYwucZgucYAusY8usZsusaMusbUusZkusYEusbYusZousYIusbQusYQu0dclFtIl5tclPF2iqkv8WZco6b9Q0nVKuklJL0hJL0xJDynpJSnp5SjpVSjpNSjp9SnpTSjprSjp7SjpXSjpvSjp/SnpQynpIynp4ynpUyjpMynp8ynpSyjpKynp6ynpWyjpuyjp6ZT0A5T0I5T0k5T085T0K5T0G5T0u5T0x5T0F5T0d5T0z5T0bEo6oDQZUhppSr9qSt9rSl9rSp9qSh9oSm9pSq9qSi9qSs9oSo9pSg9pSjM0pXs0pds1pZs0pWs0pcs0pYs0pXM0pdM1pRM1pWM0pcM1pQM1pX00pd01pR01pW00pc01pQ01pXU0pdU0pRU1pWU0pama0qKa0kKa0gKa0rya0tya0p81lZKmXtLMV9IsWNL0SpphSbNkSbNcSbNKSbNmSbNeSbNJSbNlSbNdSbNLSbNnSbNfSXNISXNkSXN8SXNKSXNGSXNeSXNJSXNVSXNdSXNLSXNnSTO9pHmgpPlHSfNkSfNcSfNySfNGSfNuSfNRSfNFSfNtSfNTSfN7SSNLmrAUoksSXfoVXfoRXfoaXfoMXfoAXXobXXoNXXoRXXoGXXocXZqJLt2PLt2DLt2OLt2ELl2DLl2OLl2ELp2DLp2OLp2ELh2LLh2BLh2ILu2DLu2OLu2ELm2LLm2OLm2ILq2DLq2BLq2ELi2DLk1Flwbo0sLoUgtdmhddqqFLZTvXP1sLWF0uoOtF9HxFdLuI7hXRixXR04ro5YrolYvoNYvo9YrojYvoLYvo7YronYvoPYvo/YroQ4roI4vo44roU4roM4vo84voS4roK4vo64voW4rou4vo+4roB4roR4roJ4voF4roWUX0G0X0e0X0J0X0l0X090X0z0X07CJaFVG6iK+L/KKLfK+LfKWLfKKLvK+LvKWLzNJFXtBFntJFHtNFHtJF7tNF7tZFbtVFbtBFrtZFLtVFLtBFztJFTtVFTtBFjtJFDtFF9tdF9tRFdtFFttdFttRFNtFF1tdF1tRFVtFFltNFltRFhrpITxdZUBeZTxep6yIVXeRPukhRlynqGkXdoKhbFPXCFPWAop5GUS9LUa9MUa9BUa9LUW9MUW9BUW9HUe9MUe9JUe9HUR9MUR9JUR9HUZ9CUZ9JUZ9HUV9CUV9JUV9PUd9CUd9FUU+nqB+gqB+hqJ+gqJ+nqF+hqN+gqN+lqD+iqL+kqL+jqH+mqGdT1AHFyZDiSFP8VVP8XlP8WlP8VFP8QFN8S1OcpSm+qCk+rSk+pik+pCnO0BTv0RRv0xRv1BSv1hQv0xQv0BTP1hRP0xRP0BSP0hQP1RQP0BT31hR31RS31xS30hQ31RQ30BTX0hRX0RSX1xSX0hQX1xQX0RQX1BSbmuI8muJfNMU/af5c1NSKmnmLmlZR0y1qBkXN1KJmmaJmpaJmjaJm3aJmo6Jm86Jm26Jm56Jmj6Jm36Lm4KLmiKLmuKLm5KLmjKLmvKLm4qLmyqLmuqLm5qLmzqLm3qLm70XNP4qaJ4qa54qaV4qa14uad4uaj4uaL4qa74qan4qa34saWdSExUl0cYQu/oou/ogufoMufooufoAuvoUuvoYuvoQuPo0uPoYuPoQuzkAX70EXb0MXb0QXr0YXL0MXL0AXz0YXT0MXT0AXj0YXD0UXD0AX90YXd0UXd0AXt0YXN0UXN0AX10YXV0UXV0AXl0IXF0cX++jiQuji/OjiPOhiFV0s2bnsAvNb8+c8plbAzFfAtAuYhQuYQQEzrYBZroBZuYBZs4BZr4DZuIDZooDZroDZuYDZs4DZr4A5tIA5qoA5voA5pYA5q4A5v4C5tIC5qoC5voC5tYC5q4C5r4B5sIB5tIB5qoB5oYCZVcC8VcC8X8B8UsB8VcB8X8D8WsD4BUxYIDQFRqbAb6bAD6bA16bAp6bA+6bA26bAq6bAi6bA06bAo6bAQ6bADFPgblPgNpPnBpPnalPgMlPgAlPgbFPgVFPgBFPgKFPgUFNgf1NgT1NgF1Nge1NgS1NgE1NgPVNgTVNgFVNgOVNgSVNgMVOgZ/K0TYH5TIG/mgJlU6BoChRMmYKpUTDzUjALUDBdCmZAwUylYJalYFaiYFanYNalYDaiYLagYLalYHamYPagYPajYA4mb46gYI6jYE6hYM6kYM6nYC6hYK6iYK6nYG6hYO6iYKZTMA9SMI9QME9RMM9TMK+QN29SMO9TMB9TMF9SMN9TML9QMD4FM0lBGwrSUPjNUPjRUPjaUPjMUPjQUHjbUHjVUHjJkH/GUHjcUJhpKMwwFO4xFG43FG40FK42FC4zFC40FM42FE4zFE4wFI42FA41FA4wFPYyFHY1FLY3FLYyFDY1FNY3FNY0FFYxFJY3FJY0FBYzFHqGQttQmM9QqBsKFUOhaPhzwTB3wTBvwbBAwdAtGBYtGKYWDMsUDCsVDKsXDOsWDBsVDJsXDNvmDTsVDHsUDPsWDAcXDEcUDMcVDCcXDGcWDOcVDBcXDFcWDNcVDLcUDHcWDNMLhgcKhkcKhicLhucLhlcKhjcKhvcKho8Lhi/zhu/yhl8KBlEwqILhn/l/YgoBpvA7pvAjpvANJv8ZpvAhpvAOpvAapvASpvAMpvA4pjATU7gfU7gHU7gdU7gRU7gGk78cU7gQUzgHUzgNUzgRUzgaUzgMUzgAU9gbU9gVU9gBU9gaU9gUU9gAU1gLU1gVU1gBU1gKUxhiCotgCh1MoYkpzIMp/AVT+JOdyzSb1pRymLnzmEYe08pjunnMonnMtDxm2TxmpTxmjTxm3Txmozxmyzxmuzxm5zxmjzxmvzzm4DzmyDzmuDzmlDzmzDzmvDzmkjzmyjzmujzmljzmzjxmeh7zQB7zSB7zZB7zQh4zK495M495L4/5JI/5Mo/5Po/5JY/x8/xzMs+kyTMyeX41eX4web4yeT41ed43ed42eV41eV40eZ42eR41eR4yeWaYPHeb/L8AkOdSk+cCk+csk+cUk+d4k+cok+cQk2d/k2dPk2cXk2c7k2dLk2djk2c9k2cNk2dlk2dZk2eayTMweRY2eVomT8PkqZk8ZZMnb/LkTYm8qZI3HnkzP3mzEHnTJ2+WIG+WJm9WJG9WI2/WIW82JG82J2+2IW92Im92J2/2JW8OIm+OIG+OJW9OJm/OIG/OI28uJm+uJG+uI29uIW/uJG+mkzcPkDePkDdPkjfPk58DQN68R958Qt58Sd58T978Qt745M0keW3IS0P+d0P+R0P+a0P+c0P+Q0P+bUP+NUN+DgD5xw35mYb8DEP+HkP+dkP+RkP+akP+MkP+QkP+bEP+NEP+BEP+KEP+UEP+AEN+L0N+F0N+e0N+K0N+E0N+fUN+TUN+ZUN+OUN+SUN+MUN+YUO+bcjPZ8j/1ZCvGPIFQylv+EveME/e0MwbOnlDP29YPG9YKm9YIW9YLW9YO2/YIG/YLG/YOm/YMW/YPW/YJ284MG84PG84Nm84KW84PW84N2+4KG+4Im+4Nm+4OW+4I2+4N2/4e97wj7zhibzhubzh5bzhjbzh3bzh47zhizkA/Jw3iLxB5Q3/zGtMPsDkf8fkf8Lkv8HkP8fkP8Tk38HkX8PkX8Lkn8HkH8fkZ2Ly92Py92Dyt2PyN2Ly12Dyl2HyF2LyZ2Pyp2HyJ2DyR2Pyh2HyB2Dye2Hyu2Ly22PyW2Hym2Dy62Pya2Hyq2Dyy2PyS2Lyi2HyPUx+QUx+Pky+jslXMPmincs057PmT1nM3DmMl8PMn8N0cph+DrNEDrNMDrNiDrNaDrN2DrNRDrN5DrNNDrNjDrN7DrNfDnNwDnNEDnNsDnNyDnNGDnN+DnNJDnNlDnNdDnNLDnNnDjM9h3kgh3kkh3kyh3khh5mVw7yZw7yfw3ySw3yVw3yfw/yaw/g5zGSO0OQYmRy/mRw/mBxfmxyfmhzvmxxvmRyvmhwvmBxPmRyPmhwPmhz3mRx3mRy3mhzXmxxXmRyXmBznmxxnmhynmBzHmRxHmhwHmxz7mRx7mBw7mxzbmhxbmBwbmRzrmhyrmxwrmRzLmBxTTY6BydE1ORYwOeY1OeY2Of5scuRMjpz5EzlTJWfmIWfmJ2c65EyfnFmcnFmanFmBnFmNnFmbnNmQnNmMnNmGnNmRnNmdnNmXnDmInDmCnDmWnDmZnDmDnDmPnLmYnLmSnLmOnLmFnLmTnJlOzjxAzjxCzjxJzjxPzrxCzrxJzrxHznxCznxJznxPzvxCzvjkzCQ5bchJQ+43Q+5HQ+5rQ+4zQ+5DQ+5tQ+5VQ+5FQ+5pQ+4xQ+4hQ+4+Q+5uQ+42Q+4GQ+5qQ+5SQ+4CQ+4sQ+5UQ+54Q+5IQ+4QQ24/Q25PQ25nQ25bQ24LQ25jQ25dQ24NQ24lQ25ZQ26qITcw5LqGXMuQm9eQqxlyfzbk8oZizvCXnKGeMzRzhk7OsEjOMMwZlsoZVsgZVs0Z1soZNsgZNs0Zts4ZdswZdssZ9skZDswZDs8Zjs0ZTsoZTs8Zzs0ZLsoZrsgZrs0Zbs4Z7sgZ7s0Z/p4z/CNneCJneC5neDlneCNneC9n+Dhn+DJn+C5n+CVn8HMGlTP8M6cxOYnJ/Y7J/YjJfYPJfYbJfYjJvY3JvYbJvYjJPY3JPYbJPYTJzcDk7sbkbsPkbsDkrsbkLsXkLsDkzsLkTsXkjsfkjsLkDsHk9sfk9sTkdsbktsPktsDkNsbk1sXk1sDkVsbklsPkpmJyA0xuYUyuhck1MLm/YnJlTK5g5xrP17C2+B/YahbrZbHzZ7GdLLafxS6exS6Txa6Yxa6Wxa6dxW6UxW6exW6Txe6Uxe6exe6bxR6UxR6RxR6XxZ6cxZ6ZxZ6XxV6SxV6ZxV6fxd6axd6Vxd6XxT6YxT6SxT6Vxb6Qxc7KYt/KYt/PYj/NYr/KYn/IYn/LYkdZrM6ibZbAZvndZvnRZvnGZvnMZvnAZnnbZnnNZnnRZnnGZnnMZnnIZplhs9xts9xqs9xgs1xls1xqs5xvs5xls5xisxxvsxxpsxxss+xns+xhs+xss2xrs2xhs2xks6xjs6xus6xosyxjsyxhsyxqsyxksyxgs3g2S9VmKdksOZsla/9E1v6FrJ2HrG2StR2ytk/WLk7WLk3WrkDWrkbWrk3WbkjWbkbWbkPW7kjW7k7W7kvWHkTWHkHWHkvWnkzWnkHWnkfWXkzWXknWXkfW3kLW3kXWTidrHyBrHyFrnyJrXyBrZ5G1b5K175O1n5K1X5G1P5C1v5K1I7I2JKst2cCS/d2S/cmS/daS/cyS/dCSfduSfc2SfdGSfcaSfcySfciSnWHJ3mPJ3mbJ3mDJXmXJXmrJXmDJnmXJnmrJHm/JHmnJHmLJ7mfJ7mnJ7mzJbmvJbmHJbmzJrmvJrm7JrmTJLmvJTrVkB5Zs15JdwJKd15Kd25L9syWbsxSzlr9kLfWspZm1LJi1LJK1DLOWpbKW5bOWVbOWtbOWDbKWzbKWrbOWHbOW3bOWfbKWA7OWw7OWY7OWk7KWM7KWc7OWi7OWK7KW67KWW7KWO7OW6VnLA1nLI1nLk1nL81nLrKzlzazlvazlk6zlq6zlh6zl16zFz1omsxaTNdhsgM3+js3+hM1+g81+js1+iM2+jc2+hs2+hM0+g80+hs0+hM3OwGbvwWZvw2ZvxGavxmYvw2YvwGbPxmZPxWaPx2aPwmYPxWYPwGb3xGZ3xma3w2a3xGY3wWbXxWbXwGZXwmaXxWanYrMDbLaLzbaw2QY2Ozc2+2dsNm/nsvN61uZd7F8y2HoGO18G285gexns4hnsUhns8hnsqhnsWhnsBhns5hnsNhnsjhns7hnsPhnsQRns4RnssRnsyRnsGRnsuRnsxRnsFRnsdRnsLRnsnRns9Az2gQz2kQz2qQz2+Qx2Vgb7Zgb7fgb7aQb7dQb7Qwb7awY7ymB1Bm0zBDbD7zbDjzbD1zbDZzbDBzbD2zbDazbDizbD0zbDYzbDQzbDDJvhbpvhNpvhBpvhKpvhUpvhfJvhTJvhFJvhOJvhCJvhYJthX5thD5thJ5thG5thc5thQ5thHZthNZthRZthaZthcZthUZthIZuhaTN4NsNfbIY/2Qz/YTNkbIGMLZOxfyVjG2Rsm4ztkbGLkbFLkrHLkbGrkLFrkrHrk7GbkrFbkbE7kLG7kbF7k7EHkrGHkbHHkLEnkbGnk7HnkrEXk7FXkLHXkbG3kLF3krHTydgHyNhHyNinyNjnydhZZOybZOz7ZOynZOxXZOwPZOyvZOyIjA3J/NOQUZbMbEvmJ0vmG0vmc0vmQ0vmbUvmNUvmJUvmGUvmcUvmIUtmhiVzjyVzuyVzoyVztSVzqSVzgSVzliVziiVznCVzpCVzsCWznyWzhyWzkyWzrSWzuSWzkSWzjiWzmiWzoiWztCWzhCXTt2Q6lsz8lsw8lkzVkvmTJfMflnzGUs5YahlLI2NpZSwLZyyLZSzTMpblMpaVM5Y1M5b1M5ZNMpatMpbtM5ZdM5a9M5YDMpbDMpajM5YTM5bTM5ZzMpaLMpbLM5ZrM5abM5Y7MpZ7M5YHMpZ/ZCxPZizPZyyvZCxvZizvZSyfZCxfZSzfZyy/Zix+xjKZsZiMwWYmsZnZ2MzP2Mx32Mzn2MyH2Mw72Mzr2MxL2Myz2Mzj2MxMbOZ+bOYebOZ2bOZGbOZqbOYybOZCbOYsbOZUbOZ4bOZIbOYQbGY/bGZPbGZnbGZbbGYLbGZjbGYdbGZ1bGZFbGYZbGYqNtPHZjrYzPzYjIfNzI3NlLCZrJ1rPK9nbS6N/bOL/auLnc/Ftlzswi526GKXdLHLudhVXOyaLnZ9F7uZi93axe7gYndzsXu72INc7OEu9hgXe5KLPd3FnudiL3axV7jY61zsLS72Lhc73cU+6GIfcbFPudgXXOwsF/uWi33fxX7qYr9xsT+62N9crHSx2uWf1iWwLr9blx+tyzfW5TPr8oF1edu6vGpdXrQuT1uXR63LQ9blPutyl3W51bpcb12utC6XWJfzrMsZ1uVk63KsdTncuhxkXfaxLrtZlx2ty9bWZTPrsoF1Wcu6rGpdlrcuS1qXoXXpWZe2dZnPuvzVupStS8G6ZKyLa/O49s+4toZrG7i2hWsXxrWL4dppuHY5XLsKrl0T166PazfFtVvh2h1w7W64dm9ceyCuPQzXHoNrT8K1p+Pac3Htxbj2Clx7Ha69BdfeiWun49oHce0juPYpXPs8rp2Fa9/Cte/j2k9x7de49gdc+xuulbg2xP2nwVUWd7bF/cnifmNxP7e4H1rcdyzuaxb3JYv7jMV9zOLOtLgzLO7dFvc2i3uDxb3K4l5icc+3uGda3FMs7nEW9wiLe7DF3dfi7mFxd7K421jczS3uhhZ3bYu7qsVdweIuZXEXt7iLWNwFLe58FrducSsWt2hxM5a8aym5lrldy7yupeVauq5l4FqmupZlXcvKrmVN17Kea9nEtWzpWrZ3Lbu6lr1cywGu5VDXcrRrOdG1nOZaznEtF7mWK1zLta7lZtdyh2u517U84Fr+4VqedC3Pu5ZXXMubruU91/KJa/nKtfzgWn51LSPXEroW4/4T6yqsOxvr/oR1v8W6n2PdD7HuO1j3Naz7EtZ9Bus+hnVnYt0ZWPdurHsb1r0B616FdS/Fuhdg3TOx7ilY9zisewTWPRjr7ot198C6O2HdbbDu5lh3I6y7NtZdDeuuiHWXxrqLY91FsO6CWHd+rDsP1q1g3T9h3aydK/LmseNcmnHFZfxXl/F8LuO2y3hhl/FiLuOlXMbLu4xXcRmv5TJe32W8mct4a5fxDi7j3VzG+7iMD3YZH+EyPtZlfJLL+AyX8fku40tcxle6jK93Gd/qMr7LZXyfy/ghl/GjLuOnXcYvuYxfcxm/7TL+0GX8mcv4G5fxTy7j313GgcvYuJixixq7zB67/Dx2+Xbs8vnY5cOxyztjl9fGLi+NXZ4Zuzw2dpk5dpkxdrl77HLb2OWGsctVY5dLxi7nj13OHLucPHY5buxyxNjl4LHLvmOX3ccuO41dthm7bDZ22WDssvbYZdWxy/Jjl6XGLsOxS2/ssuDYZb6xy1/HLpWxS2Hs4o5d3HEed1zGHddwxw3ccRt3vDDueDHc8ZK44+Vwx6vgjtfCHa+PO94Ud7w17ngH3PFuuON9cMcH4Y4Pxx0fizs+CXd8Bu74PNzxxbjjK3HH1+OOb8Ud34U7vg93/BDu+FHc8dO44xdxx6/ijt/GHX+IO/4Md/wN7vhH3PHvuGOJO/4nrh3jqjHu7DHuz2Pcb8e4X4xxPxrjvjPGfX2M+/IY99kx7uNj3Jlj3PvHuPeMcW8f4944xr16jHvpGPeCMe5ZY9xTx7jHj3GPHOMePMbdb4y7xxh3pzHuNmPczce4G45x1xnjrjbGXWGMu/QYd/Ex7iJj3AXHuM0xbn2M+5cxbnGMmxmTc8f82R0ztztmXndMyx2zsDtm4I6Z5o5Z1h2zsjtmTXfMeu6YTdwxW7ljdnDH7OqO2dsdc4A75jB3zDHumBPdMae7Y851x1zkjrnCHXOdO+YWd8yd7pjp7pgH3TGPuGOecse84I6Z5Y55yx3zgTvmU3fM1+6YH90xv7ljpDvmn+6YsTtm7IaMXcHY/YWx+z1j9wvG7seM3XcZu68zdl9m7D7L2H2csfswY/d+xu49jN3bGbs3MnavZuxeyti9gLF7NmP3FMbu8YzdIxm7BzN292Ps7sHY3Ymxuy1jdwvG7oaM3XUYu6sxdldg7C7N2F2CsbsIY7fD2J2fsTsPY/cvjN0iYzdj54rmqdsoO0FUThPV0kSNNFErTdRNEw3SREumiZZLE62cJlozTbRBmmjTNNFWaaId0kS7pYn2ThMdlCY6PE10bJropDTR6Wmi89JEF6eJrkgTXZcmuiVNdFeaaHqa6ME00SNpoqfSRC+kiV5NE72VJvogTfRZmuibNNGPaaLf00RBmvE/05gojYrSzI7S/BSl+SZK83mU5sMozTtRmtejNC9FaZ6J0jwWpZkZpZkRpbk7SnNblOaGKM1VUZpLojTnR2nOjNKcHKU5LkpzRJTmoCjNPlGa3aM0O0Zpto7SbBql2SBKs1aUZtUozfJRmiWjNMMoTS9K047SzBel+WuUphylKURp3ChNOsqSjkqko7lJRx7paAHSUZd0tCjpaCrpaFnS0UqkozVIR+uRjjYhHW1JOtqedLQr6Wgv0tEBpKPDSEfHkI5OJB2dTjo6l3R0EenoCtLRdaSjW0hHd5KOppOOHiQdPUI6eop09ALp6FXS0Vukow9IR5+Rjr4mHf1IOvqddBSQjv5J2kakJyPSIiL9c0T6u4j0FxHpjyPS70akX49IvxyRfjYi/XhEemZE+v6I9D0R6dsj0jdGpK+OSF8akb4gIn1WRPqUiPRxEekjI9IHR6T3jUjvEZHeKSK9TUR6s4j0hhHptSPSq0akl49ILxWRHkakexHpBSPS80Wk/xqRrkSkCxFpNyKbjvhTOqKajvDSEQukI7rpiEXTEVPTEcumI1ZKR6yRjlgvHbFxOmLLdMT26Yhd0hF7pSP2T0ccmo44Oh1xQjritHTEOemIi9IRl6cjrk1H3JyOuCMdcW864oF0xD/SEU+mI55PR8xKR7yVjng/HfFpOuKrdMQP6Yjf0hEyHaHTEeO0JUqHRGmfKP0LUfo7ovSXROmPidLvEqXfIEq/QpR+lij9OFH6YaL0/UTpe4jStxOlbyRKX02UvpQofQFR+myi9ClE6eOJ0kcSpQ8hSu9LlN6DKL0TUXpbovRmROkNidJrE6VXJUqvQJRemig9JEr3iNILEqWbROk6UbpClC4Qpd05APzHBFEpTTR3mmjeCaL5J4gWmvhPAKamiZZJE62UJlojTbRemmjjNNFWE0TbTxDtOkG01wTRgWmiw9JER6eJTpwgOn1iDgATRFdMEF038Z8A3DnxfwDw6ATRUxNzAJggemuC6IMJos8m5gAwQfT7BFEwQfRvAPj2DwC8Fk3wUjTBM9HEvwAwwd3RBLdGE/8nAM6LJjgzmuDkaIJjowkOjybmADDBbtEEO0YT/zsA688BYJUozXLRBNOiCQZRmoWjNK0ozbxRmrmjCUpRmmyUJh2lmYiyTER/YiKqzgFgftLRQqSjRZmIlmAiWoaJaCUmojWYiNYlHW38LwDsQjrai4noACaiw5iIjmYiOpGJfwPAxBwAJqLpTEQPMvFvAJiIPmDi3wAwEf2TCRsxMRkxMQeAiW8jJr6ImPgoIv1OxMTrERMvRUw8EzHxeMTEzIiJGRETd0dM3BYxcUPExFURE5dETJwfMXFmxMTJEeljIyYOj5g4KCK9T0R6t4iJHSImto5IbxoxsX7ExFoRE6tEpJeLSE+LmFgsYmLhiIlWxEQjIl2LmPhzxEQuYiId8R/piD9NRPxlImKeiYj5JyIWmojoT0QskY5YJh2xYjpi9YmIdSf+E4At0hHbTUTsMhGx58R/H4AL/xWAiYg7JiLunfhPAB6ZiHhyYg4AExFvTkS8PxHx6UTE1xMRP0xE/DYRISfmADBhiSYmiSYE0cTPRBPfEU18QTTxEdHEO0Tp14kmXiKaeIZo4nGiiZlEEzOIJu4mSt9GlL6BKH3V/wHAxJlEEycTTRxLNHEE0cTBROl9iNK7EaV3JEpv858ATKxPNLEWUXoVovRyROkliSaGRBMLE6XbRBPzEU38lWiiTDSRJ5pw7VxJ/a82/o//Qvwnh3huh3heh3gBh3ghh3hRh3iaQ7ycQ7yyQ7yGQ7yeQ7ypQ7yVQ7y9Q7yrQ7yPQ3ygQ3yYQ3yMQ3ySQ3yGQ3yuQ3yxQ3ylQ3y9Q3yLQ3y3Q3yfQ/yQQ/yYQ/y0Q/yiQ/yaQ/y2Q/yhQ/y5Q/yNQ/yTQzzbIVYOsXWwscNk7CBih19ih+9ihy9ih49ih3djh9djh5dih2dih8dih5mxw4zY4e7Y4dbY4frY4arY4ZLY4bzY4czY4eTY4djY4fDY4aDYYZ/YYbfYYYfYYevYYdPYYf3YYc3YYeXYYbnYYVrsMIgdurFDK3aYN3aYO3YoxQ7Z2GEidnDi/8CJ/4QT/wUnngcnnh8nXggn7uPES+DEy+DEK+LEq+PE6+LEG+PEW+LE2+HEu+DEe+HEB+DEh+LER+PEJ+LEp+PE5+LEF+HEV+DE1+HEt+DEd+LE03HiB3HiR3Hip3HiF3DiV3Hit3HiD3Hiz3Dib3Din3Di2TixwokNzjjGCWMcP8b5Jcb5Psb5Msb5OMZ5N8Z5I8Z5OcZ5NsZ5IsZ5OMa5P8a5J8a5Pca5Mca5Osa5NMY5P8Y5K8Y5JcY5LsY5IsY5KMbZN8bZPcbZMcbZOsbZNMbZIMZZK8ZZJcZZLsZZMsZZLMZZOMZpxTjzxji1GOfPMU4uxpmIyTgxRSfmL07MPE7M/E5Mx4npOzFLODHLODErOTGrOzHrOjEbOzFbOjHbOTG7ODF7OTEHODGHOjFHOzEnOjGnOzHnODEXOTFXODHXOTE3OzF3OjHTnZgHnZhHnJinnJgXnJhXnZi3nZgPnJjPnJhvnJifnJjfnZjAifmnExM5EbETEjuC2PmV2Pme2PmS2PmI2HmP2HmD2HmZ2HmW2HmC2HmY2Lmf2LmH2Lmd2LmR2Lma2LmU2Dmf2DmL2DmF2DmO2DmC2DmY2NmP2Nmd2NmR2NmG2Nmc2NmA2FmL2FmF2Fme2FmK2FmM2FmY2GkRO/MRO38ldv5M7OSJHdfOFf+1ZuPMfyEuOcRVh3geh3j+OQAMHOKpDvEyDvFK/wLAxn8AYG+H+IA/AHC6Q3yOQ3yRQ3yFQ3zdHADudIinO8QPOsSPOsRPOcQvOMSv/gsAn/23AETm/wzAz7HDt7HD5/83ALjhDwCc8QcADowd9v4XALaKHTaJHdb7FwCW/QMAC8QO3h8AcGIHJ87gxMU5ANRx4iZO3JkDwOI48dJ/AGAjnHiLPwCw/78B4Jz/HwB46n8egO/+AMDrMc5LcwB4PMaZGePMmAPAbTHODTHOVTHOJXMAODPGOTnGOTbGOXwOAPvEOLvFODvEOFvNAWD9GGfNGGflGGfZGGdajDOIcboxzgIxjhfjzB3jlGKc7H8C4DoxBSem4sT81YmZz4lZ0IlZxIlZ3IlZyolZwYlZzYlZx4nZyInZ3InZ1onZ2YnZ04nZz4k5xIk5yok5wYk5zYk524m50Im53Im5dg4Adzgx9zoxD8wB4Ekn5nknZpYT89YcAD51Yr52Yn78bwAYEzua2Bn9AYBP/h8C4CBiZ98/ALDpHwBYjthZktgZ/gsA8xI7NWKnTOzkiJ0JO1dcq9k4/V+Iiw7xX+YA0HSIOw7xIg7xEnMAWNEhXt0hXmcOAFs6xNs5xLs4xHvOAeBQh/hoh/jE/xsAPP1vAPh3A/jvAPBfB/BO7PBa7PDivwDwUOxwX+xw178M4MrY4eLY4dzY4fTY4cTY4ZjY4bDY4YDYYa/YYdfYYfvYYcvYYePYYd3YYY3YYaXYYZnYYYnYoR87LBQ7zB87zBM7/CV2KMYOmf8dABcnLuDElT8AsMi/AWCdfwPAnv8LAPy7AXz+7wGw/5MA3P1vADjv3wBwYIyz9x8A2CTGWe8PAEyNcRaNcRaKceafA0A1xvlTjPMfMY4Tk3Zi8k5M+Q8A9JyY4R8AWNuJ2fAPAOzxvwDAU/8GgM/+hwCExI5P7PxC7Hw3B4CPiZ13iZ3XiZ2X5gDwOLEzk9iZQezcTezcRuzcQOxcRexcQuycR+ycSeycTOwcS+wcPgeAvYmd3YidHYidrecAsB6xsyaxs/IcAKYRO4sSO11iZ4E5AMxN7PyJ2Mn+JwD/W21um6SnkBRTJH9JkcyTImmmSDopkkVSJEukSJZJkayYIlk9RbJuimSTFMmWKZLtUiS7pEj2SpEcmCI5LEVyTIrkxBTJ6SmS81IkF6dIrkyRXJ8iuTVFcleK5L4UyUMpksdSJM+kSF5KkbyWInknRfJRiuSLFMl3KZKfUyQiRTKZIhmniJIUOkkxSlL8mqT4PknxZZLi4yTFu0mKN5IULycpnk1SPJ6kmJmkmJGkuDtJcWuS4oYkxVVJikuSFOclKc5IUpyUpDg2SXF4kuLAJMXeSYpdkxTbJym2TFJskqRYL0mxRpJipSTFMkmKJZIU/STFQkmK+ZMU8yQp/pKkKCYpMkmK/5KkSCUuqaRAKqmQSuqkkiappEMqWYRUsjipZGlSyYqkktVJJeuSSjYilWxBKtmOVLILqWQvUsn+pJJDSSVHk0pOJJWcTio5l1RyEankClLJdaSSW0gld5FKppNKHiSVPEoqeZpU8iKp5DVSydukkg9JJZ+TSr4llfxMKhGkkklSyZhUlJDSCalRQurXhNT3CakvE1IfJ6TeTUi9kZB6OSH1bELq8YTUzITUjITUPQmp2xJSNySkrkpIXZKQOi8hdWZC6uSE1LEJqcMTUgcmpPZOSO2WkNohIbVVQmqThNR6Cak1E1IrJ6SWTUhNTUgtmpBaKCG1QELKS0hVE1J/Skj9R0LqvySkUwn5VEI5lfDXVMJ8qYQFUwm9VMIwlbBUKmGFVMJqqYR1UgkbpRI2TyVsm0rYOZWwZyph/1TCoamEo1MJJ6QSTkslnJNKuCiVcEUq4bpUwi2phDtTCdNTCQ+mEh5NJTydSngxlfBaKuHtVMKHqYTPUwnfphJ+TiWIVIJKJZhUQpyKSVKaJDUiSf1KkvqBJPUVSeoTktR7JKk3SFKvkKSeI0k9QZJ6mCR1P0nqHpLU7SSpG0lSV5OkLiVJXUCSOpMkdTJJ6liS1BEkqYNJUvuQpHYjSe1AktqKJLUpSWoDktSaJKmVSVLLkaSmkaQWJUl1SVILkKTmJUnNTZIqkaT+gyT1X+xcydzV//8BcE6K5KIUyRUpkutSJLfMAWB6iuTBFMmjKZKnUyQvzgHg7RTJhymSz1Mk3/63AIz/FwC47f9FAFJJilSSJpXkSSVlUslfSSXzkUoWJJX0SCVDUslSpJIVSCWrkUrWmQPA5qSSbUklO5NK9vwDACeQSk4jlZzz/yAAllQUk/pnQkrOAeCHhNRXCalPElLvzQHglYTUcwmpJxJSDyek7p8DwO0JqRsTUlcnpC5NSJ3/BwCOSEgdlJDa5w8AbJqQWv+/A0D3/z0Ajv/vAHDz/zIAEUnqnyQpSZL67d8A8CZJatb/BADH/T8PwP82d9UmE/8fksIUkvIUktoUksYUkvYUkt4UksWnkCw9hWSFKSSrTSFZZwrJhlNItphCsu0Ukp2nkOw5heSAKSSHTiE5agrJCVNITptCcu4UkoumkFwxheS6KSS3TCG5ewrJfVNIHpxC8ugUkqenkLw4heS1KSTvTCH5aArJF1NIvp1C8vMUEjGFZHIKyXgKUTKFMJmCn0zhl2QK3yVT+DyZwsfJFN5LpvBGMoWXkyk8m0zh8WQKM5MpzEimcHcyhVuTKdyQTOGqZAqXJFM4L5nCGckUTkqmcEwyhcOSKRyQTGHvZAq7JlPYPpnClskUNk6msG4yhdWTKayYTGHpZAqLJ1NYJJlCJ5lCM5lCPZlCJZlCIZmCm0whlUxhSpJmSpJnSvJnpiQ1piQNpiRtpiQ9piRDpvx/2bvvdzmKO+/7VdU9XdOn65ARCAQi5wwiiCSyBCYjDMIWwghhQIAQRoBJRoDJWSQBJuecc845ZxDhaGaYGegJnmam6a56Pz+s9lndLLLx7vp+dp9r/4nX9a6e8/2cbF1UthEq2xyVbYPKtkdlu6KyPVHZ3qhsP1R2ECo7DJVNQWXHorKpqOw0VHYOKpuGyqajsqtQ2Y2o7DZUdg8qewiVPYHKnkNlr6Cyt1DZB6jsM1T2FSoro7LvUFkblXVRaYbqZKg4Q32bob7JUJ9nqI8y1DsZ6rUM9UKGeipDPZKh7stQd2SomzPUtRnqigx1cYY6L0OdkaFOzlDHZ6ijMtTkDHVwhto/Q+2TofbKUKMz1I4ZalSG2jJDbZKh1s9Qa2WoVTLUchlqiQy1SIZaIEPNnaHCDOVlBCrDqIz5VMbCKmNxlbGMylhRZayuMoapjA1VxmYqYxuVsb3K2EVl7KEyxqqM8SrjQJUxSWVMURnHqIwTVcapKuNslXGhyrhMZVylMm5QGbeqjLtVxoMq43GV8azKeFllvKUyPlAZn6iML1VGSWXUVUZLZfygMn5UP5KpDpmKydS3ZOobMvU5mfqITL1Dpl4jUy+QqafI1CNk6j4ydSeZuoVMXUumriBTF5Op88jUmWTqZDJ1PJk6ikwdTqYOIVP7k6l9yNReZGp3MrUTmRpFprYkU5uQqQ3I1NpkahUytRyZWoJMLUqmFiRTc5OpPjJV6Ao7aMGuDSR2LoVdQGEXUdihCruswq6ssGsq7HoKu5HCbq6wIxV2R4UdrbBjFHacwk5Q2IMVdrLCHqmwxynsSQp7hsKeq7AXKezlCnuNwt6ssHco7L0K+7DCPqmwzyvsqwr7tsJ+qLCfK+w3Cvutwn6vsH9V5KkitYqOVcRWUbWKAav4wio+sop3rOI1q3jBKp6yikes4j6ruMMqbraKa63iCqu42CrOs4ozrOJkqzjeKo6yislWcbBV7G8V+1jFXlYx2ip2tIpRVrGFVWxsFetZxZpWsYpVLGcVS1jFIlaxgFXMZRVFq1BWoaxGWYOy86Hswii7OMoug7IroewaKDsMZTdE2c1QdhuU3R5ld0XZPVF2b5TdD2UPQtlJKDsFZY9F2akoexrKnoOy01B2OspejbI3ouztKHsvyj6Esk+g7HMo+wrKvoWyH6DsZyj7NcpWUPZ7lP0ryqaoHy0qsaiGRVUtaqZFzbCojy3qXYt63aJetKinLepRi7rfou60qFss6jqLutKiLrGo8y3qTIs6xaJOsKijLepwizrYova3qN9Z1G8saneL2smitrWoLS1qE4ta36LWsqhVLGo5i1rCohaxqAUtam6LCi3KswTKYpRlPmVZWFkWV5ZllGUlZVldWYYpy4bKspmybKMs2yvLrsqyp7LsrSz7KctByjJJWaYoy7HKMlVZTlOWc5RlmrJMV5arleVGZblNWe5RloeU5QlleU5ZXlGWt5TlA2X5TFm+VpaKsnynLG1l6SpLrjKs+gGrGlhVw6oSVs3Aqo+x6l2seh2rXsSqp7HqUay6H6vuxKpbsOo6rLoSqy7BqvOx6iysOgWrTsCqo7HqcKw6BKsOwKrfYdVvsOrXWLUzVm2LVVti1SZYtQFWrY1Vq2LV8li1JFYNwapBWDUPVoVY5f0EgPkVdmGFXVxhl1HYlX4hAPsp7ESFPWw2AKYq7OkKe47CTlPY6Qp7tcLepLC3zwLgIYV9QmGfU9hXFPYthf1AYT9T2K8VtjJnAL61im+s4nOr+HAOANxvFXf+AwBMtIoJvxCAla1iWasYahWDrWL+fwdAMAuAeVF2IZRdDGWXRtkVUXb1XwDAeJQ98P8eAM1/AIC7/i8BsMD/CUBBWSJlmUdZBinLEGVZSllWUJbVlGUdZRmuLCOUZWtl+ZWy7KIseyjLWGXZV1kOUJZDleUIZTlGWU5UllOV5WxluVBZLlOWq5TlhtkAeFBZHleWZ5XlZWV5U1neV5ZPleUrZSnPEYDmLABm/h0AHsCqu7Dq1n8AgP1/IQCrYNVyWLUEVi2KVQti1dz/BkA+aMFuXhDkkSSfT5IvLMkXk+RLS/IVJfkaknxdSb6hJN9Mko+U5NtL8l0k+R6SfKwkHy/JD5LkkyT5FEl+rCSfKslPl+TnSPJpkny6JL9akt8kyW+X5PdK8ocl+ZOS/HlJ/ookf0uSfyDJP5PkX0vyiiT/XpL/VZL3JGku6eSSOJd8m0u+ySWf55IPc8nbueTVXPJCLnkqlzySS+7LJXfkkptyyTW55PJcclEuOTeXnJ5LTsolx+WSI3PJYbnkoFyyXy4Zl0vG5JLdcskOuWRkLtk8l2yUS9bNJWvkkpVyybK5ZPFcMjiXzJ9L+nOJziUyl8i8gMwjZD4vMl8ImS+GzJdG5isi89WR+TBkviEy3wyZb4PMf4XMd0HmeyDzsch8PDI/EJlPQuZTkPmxyHwqMj8NmZ+DzKch8+nI/GpkfiMyvx2Z34vMH0LmTyDz55D5K8j8LWT+ATL/DJl/jcwryPx7ZP5XZN5D/pgjkxzZyJHVHDmQI7/IkR/lyHdy5Gs58sUc+XSOfDRH3p8j78yRN+fIa3PkFTny4hx5Xo48I0eenCOPz5FH5cjJOXJijpyQI8flyDE5crccuUOOHJUjt8iRG+fI9XLkmjly5Ry5bI4cmiMH58j5c2R/jtQ5UuUUZE6fzJlH5gySOUNkzlIyZwWZs5rMWUfmDJc5I2TO1jJnO5mzs8z5tcz5rczZV+YcIHMOlTlHyJxjZM6JMudUmXO2zLlQ5lwmc66SOTfInNtkzj0y5yGZ84TMeU7mvCxz3pQ578ucT2XOVzKnLHO+kzltmdOVOZn8kVwm5LJBLqvkcoBcfkEuPyKX75DL18jli+TyaXL5KLm8n1zeSS5vIZfXkssryOXF5PI8cnkmuTyZXB5PLo8il5PJ5cHkcgK5HEcux5DL0eRyB3I5ilxuQS43Jpfrk8u1yOXK5HJZcjmUXC5CLhcgl/3kskguva5wCy7QdQWB65e4+SVusMQtLnHLSNxKEremxK0ncRtJ3OYSN1Litpe43SRuT4nbW+L2k7iJEneYxB0pccdJ3EkSd4bEnStxF0nc5RJ3jcTdLHF3SNx9EvewxD0lcS9I3KsS95bEfSBxn0nc1xJXkbjvJe6vEvuj5EcnSZyk4SRVJxlwki+c5CMnecdJXnOSF5zkaSd51Enud5I7neRmJ7nWSa5wkoud5DwnOcNJTnaS453kKCeZ7CQTnWSCk4xzkjFOspuT7OAkI51kCyfZ2EnWc5I1nWRlJ1nWSYY6yWAnmd9J+p1EO4l0EukKSBch3bxItxDSLYZ0SyPdiki3OtINQ7oNkW4zpNsG6bZHul2Qbg+kG4t045HuQKSbhHRTkO5YpJuKdKch3TlINw3ppiPd1Uh3I9LdjnT3It3DSPck0j2PdK8g3VtI9wHSfYZ0XyNdBem+R7q/Iu2PyB8dMnHIhkNWHXLAIb9wyI8c8h2HfM0hX3TIpx3yUYe83yHvdMibHfJah7zCIS92yPMc8gyHPNkhj3fIoxxyskNOdMgJDjnOIcc45G4OuaNDjnLILRxyY4dczyHXdMiVHXJZhxzqkIMdcn6H7HdI7ZDKUZCOSDrmlY6FpGMx6VhaOlaUjtWlY5h0bCgdm0nHNtLxK+nYRTr2kI6x0jFeOg6UjknSMUU6jpWOqdJxmnScIx3TpGO6dFwtHTdKx+3Sca90PCwdT0rH89LxinS8JR0fSMdn0vG1dFSk43vp+Kt09KTFyhwnf8DJBk5WcXIAJ7/AyY9w8h2cfB0nX8LJp3HyUZy8HyfvxMmbcfI6nLwSJy/ByfNx8iycPAUnT8DJo3HycJw8BCf3x8l9cHIvnNwdJ3fEyW1xckuc3AQnN8DJtXFyFZxcDieXwMlFcXJBnJwbJ4s4qWYDIJK4+SRuYYlbTOKWlrgVJW4NiVtX4jaUuM0kbhuJ2+EXADBV4k6XuHMkbprETZe4qyXuJom7XeLunQXAkxL3/CwA3pa4DyXuc4n7RuK+lbhY4jp/H4B3neR1J3nxnwTA5k6ykZOs6yRrOMlKTrKMkyzuJAs7yXxOYpwk+D8A6EO6eZBuENINQbqlkG4FpFsN6dZBuuFINwLptka6X/0TAHgV6d5Gug+R7nOk+wbpvkW6GOk6SPcjMnPIHxyy6ZA1h5zpkDMc8mOHfNchX3fIlxzyGYd8zCEfcMi7HPIWh7zOIa90yEsc8nyHPNMhT3HIExzyaIc83CEPdsj9HXIfh9zLIUfPAmCkQ27ukBs55LoOuYZDruSQyzjk4g65sEPO55DGIYv/BkAoHXNLx4LSsah0LCkdy0vHqtKxtnRsIB2bSsdW0rGddOwsHb+Wjt9Kx77ScYB0HCodR0jHMdJxonScKh1nS8eF0nGZdFwlHTdIx23ScY90PCQdT0jHc78IADcLgC5ONnGyhpMzcXIGTn6Mk+/i5Bs4+TJOPoOTj+HkAzh5F07e+p8AYCecHIWTW+Dkxji5Pk6uhZMr4+SyODkUJxfByQVwsn8WAF5X2AUX6FpfYCOJnVdiF5LYIRK7lMSuILGrS+wwiR0usSMkdmuJ3V5id5XYPSR2rMSOl9iDJHaSxE6R2GMldqrEni6x50jsNImdLrFXS+xNEnu7xN4rsQ9L7JMS+7zEviqxb0vshxL7ucR+I7HfSmwssf8KgJUkVtKwkqqVDFjJF1bykZW8YyWvWckLVvKUlTxiJfdZyR1WcpOVXGMll1vJRVZyrpWcbiUnWclxVnKklRxmJQdZyX5WsreV7Gklu1rJ9layjZVsZiUbWskwK1ndSla0kmWsZDErWchK5rMSYyWBlUgrkbaAtH1IOw/SDkLaIUi7FNKugLSrIe06SDscaUcg7dZI+yuk3QVp90DasUg7HmkPRNpJSDsFaY9F2qlIexrSnoO005B2OtJejbQ3Iu3tSHsv0j6MtE8i7fNI+yrSvo20HyLt50j7DdJ+i7Qx0naQNkVmFvmDRTYtsmaRMy1yhkV+bJHvWuTrFvmiRT5tkY9a5P0WeadF3myR11rkFRZ5sUWeZ5FnWOTJFnm8RR5lkZMtcqJFTrDIcRY5xiJ3s8gdLHKkRW5ukRtZ5LoWuYZFrmSRy1jk4ha5sEXOZ5HGIrVFSosvLX3SMo+0DJKWIdKylLQsLy2rSss60rKBtGwqLVtJy3bSsrO0/Fpafist+0rLAdJyqLQcIS3HSMuJ0nKqtJwtLRdKy2XScpW03CAtt0nLPdLykLQ8IS3PScsr0vKWtHwgLZ9Jy9fSUpGW76Xlr9LSk5ZcZlj5A1Y2sbKGlTOxcgZWfoyV72Ll61j5IlY+jZWPYuX9WHknVt6Clddi5RVYeTFWnoeVZ2LlyVh5PFYehZWTsfJgrJyAleOwcgxWjsbKHbFyJFZujpUbYeV6WLkmVq6Elctg5eJYORgr58dKg5VFrPS6gn9WAfySJ8Ajsz0BXpO4dyTuI4n7QuIGJK4qcQ2JSyQuk2RO8oOTNJ2k5iQznWSGk3z836gA/u0JEPwHngC7It2eSLc30u2HdAch3WFIdyTSHYd0JyHd6Uh3LtJdhHSXI901SHcT0t2BdPch3SNI9xTSvYB0ryHdO0j3EdJ9gXQDSFdFugbSJf9nAbRmK4AvHfITh3zPId/4JxXAP/IEmK0A+qRjHukYJB1DpGMp6VhBOlaTjnWkY7h0jJCOrf9JT4BXpeNt6fhQOj6Xjm+k41vpiKWjIx2pdFhpcbKHky2crONkCSe/xMlPcPK9WQXw0n+PAnALLtB1vsAZgZtP4BYWuMUEbmmBW1Hg1hC4dQVuQ4HbTOC2EbgdBG43gdtT4PYWuP0EbqLAHSZwRwrccQI3VeBOF7hzBG6awE0XuKsF7iaBu13g7hW4hwXuSYF7XuBeFbi3Be5Dgftc4L4RuG8FLha4jsD9KMicIHGChhNUnWDACb5wgo+c4F0neN0JXnSCp53gUSe43wnudIKbneBaJ7jCCS52gvOc4AwnONkJjneCo5xgshNMdIIJTjDOCcY4wW5OsIMTjHSCzZ1gIydY1wnWcIKVnGAZJ1jcCRZ2gvmcwDhB4ATSCYTzEa4P4eZBuEEINwThlkK4FRBuNYRbB+GGI9wIhNsa4X6FcLsg3B4INxbhxiPcgQg3CeGmINyxCDcV4U5DuHMQbhrCTUe4qxHuRoS7HeHuRbiHEe5JhHse4V5FuLcR7kOE+xzhvkG4bxEuRrgOwv2IyByi6xAth6g5RMkhvnSITxziPYd4wyFecohnHOIxh3jAIe5yiFsc4jqHuNIhLnGI8x3iTIc4xSFOcIijHeJwhzjYIfZ3iH0cYi+HGO0QOzrEKIfYwiE2doj1HGJNh1jZIZZ1iKEOMdgh5neIfofQDiEdvnD0Ccc8wjFIOIYIx1LCsbxwrCoc6wjHBsKxqXBsJRzbCcfOwvFr4fitcOwrHAcIx6HCcYRwHCMcJwrHqcJxtnBcKByXCcdVwnGDcNwmHPcIx0PC8YRwPCccrwjHW8LxgXB8JhxfC0dFOL4Xjr8KRyocTuQ40cWJFk7UcaKEE1/ixCc48R5OvIETL+HEMzjxGE48gBN34cStOHEdTlyJE5fgxPk4cRZOnIITJ+DE0ThxOE4cghP748Q+OLEXTuyOEzvhxCic2AInNsaJ9XFiLZxYGSeWxYmhOLEITiyAE3PhhMYJ2RV2gQW61hPYSGDnFdiFBHaIwC4lsCsI7OoCO0xghwvsCIHdWmC3F9hdBXYPgR0rsOMF9iCBnSSwUwT2WIGdKrCnC+w5AjtNYKcL7NUCe5PA3i6w9wrswwL7pMA+L7CvCuzbAvuhwH4usN8I7LcCGwtsR2BTwY9WkFhBwwqqVjBgBV9YwUdW8K4VvG4FL1rB01bwqBXcbwV3WsHNVnCtFVxhBRdbwXlWcIYVnGwFx1vBUVYw2QomWsEEKxhnBWOsYDcr2MEKRlrB5lawkRWsawVrWMFKVrCMFSxuBQtbwXxWYKwgsAJhBcL6CNuHsPMg7CCEHYKwSyHsCgi7GsKug7DDEXYEwm6NsL9C2F0Qdg+EHYuw4xH2QISdhLBTEPZYhJ2KsKch7DkIOw1hpyPs1Qh7I8LejrD3IuzDCPskwj6PsK8i7NsI+yHCfo6w3yDstwgbI2wHYVNEZhE/WETTImoWMdMiZljExxbxrkW8bhEvWsTTFvGoRdxvEXdaxM0Wca1FXGERF1vEeRZxhkWcbBHHW8RRFjHZIiZaxASLGGcRYyxiN4vYwSJGWsTmFrGRRaxrEWtYxEoWsYxFLG4RC1vEfBZhLCKwCGnxhCUUlrmFZUFhWVRYlhSW5YVlVWFZW1g2EJZNhWUrYdlOWHYWll8Ly2+FZV9hOUBYDhWWI4TlGGE5UVhOFZazheVCYblMWK4SlhuE5TZhuUdYHhKWJ4TlOWF5RVjeEpYPhOUzYflaWCrC8r2w/FVYesKSiwwrfsCKJlbUsGImVszAio+x4l2seB0rXsSKp7HiUay4HyvuxIpbsOJarLgCKy7GivOw4kysOBkrjseKo7BiMlYcjBUTsGIcVozBitFYsSNWjMSKzbFiI6xYDyvWxIqVsGIZrFgcKwZjxfxYYbBCY4X6JxbASQJ3hsCdK3AXCdzlAneNwN0scHcI3H0C94jAPSVwLwrca7+sAH5wgqYT1JxgphPMcIKP/5sVgPjfAvi/WgArCMdqswpguHCMEI6theNXwrGLcOwhHGOFY7xwHCgck4RjinAcKxxTheM04ThHOKYJx3ThuFo4bhSO24XjXuF4WDieFI7nheNV4XhbOD4Ujs+F4xvh+FY4YuHozCoAK+z/nAJw//oR0AjsfAK7sMAuJrBLC+yKAruGwK4rsBsK7GYCu43A7iCwuwnsngK7t8DuJ7ATBfYwgT1SYI/7JxTAj/9bAP+UAsj/twD+6wsgx4ouVrSwoo4VJaz4Eis+wYr3sOINrHgJK57Bisew4gGsuAsrbsWK67DiSqy4BCvOx4qzsOIUrDgBK47GisOx4hCs2B8r9sGKvbBid6zYCStGYcUWWLExVqyPFWthxcpYsSxWDMWKRbBiAayYa1YB/P/oCfCOFbxmBS9YwVNW8IgV3GcFd1jBTVZwjRVcbgUXWcG5VnC6FZxkBcdZwZFWcJgVHGQF+1nB3lawpxXsagXbW8E2VrCZFWxoBcOsYHUrWNEKlraCxaxgISuY1woiKyj8TwPgx9kAqP43BkD87xPg/79PgBdmPQHeEbiPBO4LgRsQuKrANQQuEbjsf9oToPA/5wnwg0M0Zz0BZjrEDIf42CHedYjXHeJFh3jaIR51iPsd4k6HuNkhrnWIKxziYoc4zyHOcIiTHeJ4hzjKISY7xESHmOAQ4xxijEPs5hA7OMRIh9jcITZyiHUdYg2HWMkhlnGIxR1iYYeYzyGMQwT/+wT4pz4B/vdXgP/9BvA/4RuAJxyhcMwtHAsKx6LCseRsvwKs/d/gV4AfhcP+T/oVIF9ggW7uCfJIkM8ryBcS5EME+VKCfAVBvrogHybIhwvyEYJ8a0G+vSDfVZDvIcjHCvLxgvwgQT5JkE8R5McK8qmC/HRBfo4gnybIpwvyqwX5TYL8dkF+ryB/WJA/KcifF+SvCvK3BfmHgvxzQf6NIP9WkMeCvCPIfxT8mAt+yAXNXFDLBTNzwYxc8HEueDcXvJ4LXswFT+eCR3PB/bngzlxwcy64NhdckQsuzgXn5YIzcsHJueD4XHBULpicCybmggm5YFwuGJMLdssFO+SCkblg81ywUS5YNxeskQtWygXL5ILFc8HCuWC+XGByQZALRC4QuY/I+xD5PIh8ECIfgsiXQuQrIPLVEPk6iHw4Ih+ByLdG5L9C5Lsg8j0Q+VhEPh6RH4jIJyHyKYj8WEQ+FZGfhsjPQeTTEPl0RH41Ir8Rkd+OyO9F5A8j8icR+fOI/FVE/jYi/xCRf47Iv0Hk3yLyGJF3EHmKyHLEDzmimSNqOWJmjpiRIz7OEe/miNdzxIs54ukc8WiOuD9H3Jkjbs4R1+aIK3LExTnivBxxRo44OUccnyOOyhGTc8TEHDEhR4zLEWNyxG45YoccMTJHbJ4jNsoR6+aINXLESjlimRyxeI5YOEfMlyNMjghyhMzxRU6fyJlH5AwSOUNEzlIiZwWRs5rIWUfkDBc5I0TO1iLnVyJnF5Gzh8gZK3LGi5wDRc4kkTNF5BwrcqaKnNNEzjkiZ5rImS5yrhY5N4qc20XOvSLnYZHzpMh5XuS8KnLeFjkfipzPRc43IudbkROLnL+KnFTk5CIjFz+Qiya5qJGLmeRiBrn4mFy8Sy5eJxcvkounycWj5OJ+cnEnubiFXFxLLq4gFxeTi/PIxZnk4mRycTy5OIpcTCYXB5OLCeRiHLkYQy5Gk4sdycVIcrE5udiIXKxHLtYkFyuRi2XIxeLkYjC5mJ9cGHKhyYWa7S8B/6s/Ap4ksGcI7LkCe5HAXi6w1wjszQJ7h8DeJ7CPCOxTAvuiwL72yz4C/mAFTSuoWcFMK5hhBR//f/QRcNnZPgLOP9tHQGkFwhYQNkLYeRF2IYRdDGGXRtgVEXZ1hB323+RnwPwf+wj4gEXcZRG3WMR1FnGlRVxiEedbxJkWcYpFnGARR1vE4RZxsEXsbxH7WMReFjHaIna0iFEWsYVFbGwR61nEmhaxskUsaxFDLWKwRcxvEf0Wof/lI6AvLH3CMo+wDBKWIcKylLCsICyrCcs6wjJcWEYIy9bC8ith2UVY9hCWscIyXlgOFJZJwjJFWI4VlqnCcpqwnCMs04RlurBcLSw3CsvtwnKvsDwsLE8Ky/PC8qqwvC0sHwrL58LyjbB8KyyxsHSEJf0f9xFwwQX+5RqwX5LPL8kHS/LFJfkyknwlSb7mT64Bt5HkO0jy3ST5npJ8b0m+nySfKMkPk+RHSvLjfuE14EOS/AlJ/twvuAZM//Y14Du55LWfXAPen0vuzCU355Jrc8kVueTiXHJeLjkjl5ycS47PJUflksm5ZGIumfB3rgHXyyVr5pKVZ10DDv1PXQMOR+YjkPnW/99dA2azrgGbs10DzsiRH+fId3Pk6z9zDXhXjrwlR16XI6/MkZfkyPNz5Jk58pQceUKOPDpHHp4jD86R++fIfXLkXjlydI7c8WeuAdfKkavkyOVy5BI5cpEcuUCOnCtHFv/lGtD/hdeAm8qcrf4LrwEflDmPy5xnf/E1YEYufyCXTXJZI5czyeUMcvkxuXyXXL7+k2vAB8jlXeTyVnJ5Hbm8klxeQi7PJ5dnkctTyOUJ5PJocnk4uTyEXO5PLvchl3uRy93J5U4/cw24NrlchVwuRy6XIJeLkssFyeXcs64BVVfkCy7QzX1Bbv7GOfAwST5cko+Q5FvPOgfe9b/gHPgfAeAXngM/n0uezCUP/yfOgffOJXvmkl1zyfa5ZJtcslku2XAO58ALzwLA5JLgfxoAv+Qc+IUc+VSOfOS/8Bx4ZI7cPEdulCPX/QXnwPJ/EgC/5Bz4BXL5FLl85L/oHHhHcjmSXG5OLjcil+v9snPg/B8tgJGzCmDXv1MAJ0nyMyT5uZL8Ikl++d/ZA3h1FgAfSvLP/3YBNHJJNZcM5JIvcslHcyiAnwPgP1oAW+SSjf/LC2D2PYDt/3sAMPPvFMB9PwHgP1IA/7oHsGWO3CRHrv83CuAnewCRzJlX5iwkcxaTOUvLnBVlzuoyZ5jM2VDmbCZztpE5v5I5u8icPWTOWJkzXuYcKHMmyZwpMudYmTNV5pwmc86ROdNkzvS/swfwisx5S+Z8IHM+kzlfy5yKzPn+3xVA8g8UwE8B+L9ZAIMW7GYFQdYvyeaXZIMl2eKSbBlJtpIkW1OSrSfJNpJkm0uykZJse0m2myTbU5LtLcn2k2QTJdlhkuxISXacJJsqyU6XZOdIsmmSbLoku0qS3SjJbpNk90iyhyTZE5LsOUn2iiR7S5J9IMk+lWRfSbKyJPtOkrUlWU/SyySdTBJnkm8zyTeZ5PNM8mEmeSeTvJZJXsgkT2WSRzLJfZnkjkxyUya5JpNcnkkuziTnZZIzMsnJmeT4THJUJpmcSSZmkgmZZFwmGZNJdsskO2SSUZlki0yycSZZL5OsmUlWziTLZpKhmWRwJpk/k/RnEp1JVCaRWQGZRchsXmS2EDJbDJktjcxWRGarI7NhyGxDZLYZMtsGmf0Kme2CzPZAZmOR2XhkdiAym4TMpiCzY5HZVGR2GjI7B5ldiMwuQ2ZXIbMbkNltyOweZPYQMnsCmT2HzF5BZm8hs/eR2afI7CtkVkZm3yGzNjLrItMfkZ0M2ciQ1Qw5M0POyJAfZ8h3M+RrGfKFDPlUhnwkQ96XIe/IkDdlyGsy5BUZ8pIMeX6GPCNDnpwhj8+QR2XIyRlyYoackCHHZcgxGXK3DLljhhyVIbfIkBtnyPUy5JoZcuUMuWyGHJohB2fIBTLkXBmymCFVRkFmRDJjXpkxSGYMkRlLyYwVZMZqMmMdmTFcZoyQGVvLjF/JjF1kxq9lxm9lxr4y4wCZcajMOEJmHCMzTpQZp8qMs2XGhTLjMpnxF5lxvcy4VWbcLTMelBmPy4xnZcbLMuNNmfG+zPhUZnwlM8oy4zuZ0ZIZP8iMH+WPZLJDJptkskYmZ5LJGWTyYzL5Dpl8jUy+QCafIpOPkMn7yOQdZPImMnktmbySTF5CJs8nk2eRyVPI5Alk8mgyeTiZPIRM7k8m9yGTe5HJ3cnkjmRyWzK5JZnchExuQCbXJpOrkMnlyOQSZHJRMrkgmZybTBbJpOqKbNCC3awgyfpnGwVdTJEtrchWVGRrKLJ1FdmGimwzRbaNIttBke2qyPZQZGMV2XhFdpAimzSHUdALFdlliuwqRXajIrvtb4yCvq/IPv2ZUdCuovczo6CfZYoPMsXbmeLVTPF8pngyUzycKe7NFLdnihszxdWZYnqmmJYpzs0Up2eKkzLFcZniyExxWKY4KFPslyn2zhR7ZordMsUOmWJkptg8U2yUKdbNFGtkipUyxbKZYmimGJwp5p9tFFRlCpUFqMzMGgVdCJUthsqWRmUrorLVUdkwVLYhKtvsJ6Oge6CysahsPCo7EJVNmsMo6IWo7LJZo6A3/J1R0PdR2ac/Pwram8Mo6IcZ6u2/MQp6U4a6JkNdnqEuylDnzmEUdGKGmpChxmWoMRlqt9lGQbfIUBtnqPUy1JoZauUMtWyGGpqhBs8aBZ0rQxUzlPq3UdB5VcZCKmMxlbH0z4yCjlAZW6uMX802CvpblbGvyjhAZRyqMo6YwyjopSrjLyrj+r8xCvqmynjvb46CpnMYBf3wb4yC3kGmbiZT15Cpy8nURXMYBZ1Mpg4mUxPI1DgyNYZMjZ5tFHQLMrUxmVqfTK1FplYmU8uSqaFkapFZo6BzkamQTPmzCuBfV4EXUGSLKLKhimzZv7EKvOPfWAU+UpEdp8hOUmRnKLJz57AKfK8ie/hvrAJ/rcgqiuz7f1sF/lcA4kzxbab4JlN8PmsV+OcA+Okq8OWZ4qJZAMxpFXhCphiXKcb8J1aB1f+xCjw/KhuMyoaismXnsAo8EpXtgMp2m8Mq8JGo7DhUdhIqO/0nq8BXz1oFvh2V3YvKHv4bq8Bfo7IKKvv+bwPwxRxWgR/NUPdnqDvnsAp8ZoY6JUOdkKGOzlCHz2EVeKcMte0cVoGXz1BLZqhFM9SC//lV4F1Vxp5zWAU+VmVMVRmnzWEV+DaVcY/KeOhnAHhfZXyqMr5SGWWV8d2/WwVOyFSDTFXJ1ACZ+mIOq8CPkqn7/84q8J/J1Alk6ui/sQq8M5nadg6rwMuTqSXJ1BAyNYhMzTMLAO9f/y/ArAL4KQArK7K15gDAaEU25m8AMFWRnabIzp5DAdytyB5UZI8rsmcV2cuK7M3ZCuBLRVZSZHVF1vq3Amj/gwUwJwBO/zsA/K1Z8LUyxSq/aBbc/I1Z8DkVwJ5/pwBORWVnz6EA7kZlD6Kyx1HZs6jsZVT25qwC+ASVfYnKSqisjspaqOyHORfAf3QW/O8BsPt/DQALzQGA4XMogLF/pwDOUhkXzKEAHlAZj6mMZ1TGSyrjjTkUQHMWANkcC+A/Mgt+Bpk65W8A8Bsy9eufAWD9WQCs9jMAzDYL/uNCg7ppoEjn8kgX8EgHe6SLe6TLeKQreaRreqTreqQbeqSbeaTbeKQ7eKS7eaR7eqRjPdLxHulBHukkj3SKR3qsRzrVIz3VIz3bI73QI73MI73KI73BI73VI73bI33QI33cI33WI33ZI33DI33PI/3EI/3SIy15pHWPtOmR/uDRTT3aqcd3qUc59fgq9fg09Xg/9Xgr9Xgl9Xg+9Xgy9Xg49bg39bg99bgx9bg69bg89bgo9Tg39Tg99Tgp9Tgu9Tgq9ZicekxMPSakHuNSjzGpx+jUY8fUY1TqsUXqsXHqsV7qsWbqsUrqsVzqsUTqsUjqsUDqMVfqUUw9vNTDSzVeavDS+fDShfHSxfHSZfDSlfDSNfDSYXjphnjpZnjpNnjp9njprnjpnnjpWLx0PF56IF46CS+dgpcei5eeiJeeipeejZdeiJdehpdehZdej5feipfejZc+iJc+jpc+i5e+jJe+gZe+h5d+gpd+iZeW8NI6XtrESxO8borXTvG+S/HKKd7XKd5nKd4HKd5bKd4rKd7zKd6TKd7DKd69Kd7tKd6NKd41Kd7lKd5FKd65Kd7pKd5JKd7xKd5RKd7kFG9iijchxRuX4u2V4o1O8XZM8UaleFukeBuneOuleGuleKukeMuleEukeIukeAumeHOneGGK56cEXorxUubzUhbyUhbzUpb2Ulb0Ulb3UoZ5KcO9lBFeytZeyq+8lF28lD28lLFeyr5eygFeyqFeyhFeyjFeyoleyp+9lLO8lAu8lEu9lL94Kdd7Kbd6KXd5KQ94KY95Kc94KS95KW94Ke96KR97KTO8lJleSs1LaXgpHS+l5/VIvTap9z2pVyH1vib1PiP1PiD13iL1XiX1nif1niT1Hib17iX1bif1biT1riH1Lif1LiL1ziX1ziD1TiL1jif1jiL1JpN6B5N6E0i9caTeXqTe7qTeTqTeKFJvC1JvY1JvfVJvbVJvFVJvOVJvCVJvUVJvEKk3N6nXR+oVuuLHhRfqplqRzu2RLuiRLuqRLuGRLuuRruyRruWRru+RbuyRbu6RjvRId/RIR3ukYzzSvT3S/TzSiR7pYR7pkR7pcbMAON0jPccjnTYbADd6pLd5pPf8BICXfgaAmR5p7e8D8MFsADyXejyRejz0MwBM/wcA2OsnAGyZemySeqyfeqw1C4DlU48lU49FU48FU4+5U48w9fBTDy8t4qX9eOn8eOlgvHQoXrrsbACsi5duhJdu/jMA7D0HAKb+AwA8g5e+9BMAZuClM/HS2iwAfviPAXBHinfTTwA4L8U7I8U7eRYAR6d4h6d4B6d4+6d4+8wCYPcUb6cUb9sUb8sUb5MUb4MUb+0Ub9UUb/kUb6kUb0iKNyjFmyfF6/sXALSX0u+lzO+lDPZShnopy3opK3kpa3gp63opG3kpm3sp23gp23spu3ope84CYLyXcqCXMslLmTIbAKd6KWd7KRd6KZf9BIC7vZQHfwaA934GgKaXkvy/APyV1PvuFwJwH6l3B6l382wAXEzqnUfqnUnqnTwLgKNJvcNJvUNIvf1JvX1Ivd+Qer8m9XYm9bYl9bYk9TYl9YaTeuuQequSesuTekuReouReguRevPOAsDvinThhbq9okdvLp/egj69RX16S/j0lvPprezTW8unt75Pb2Of3hY+vZE+vR19eqN9emN8env79Pbz6U306R3m0zvSp3esT+9En95pPr2zfXoX+vQu9en9xad3g0/vVp/e3T69B3x6j/n0nvHpveTTe92n965P72Of3gyf3kyfXtWn1/DpJT5Jz6fZ86n1fEo9ny97Pp/0fN7r+bzZ83ml5/Ncz+eJns9DPZ97ej6393xu7Plc3fOZ3vO5qOdzbs/n9J7PST2f43o+R/V8Jvd8JvZ8JvR8xvV89ur5jO757NjzGdXz2bLns0nPZ/2ez1o9n1V6Psv3fJbs+Sza81mw5zN3zyfs+fg9H79XxO/14/fmx+8Nxu8Nxe8ti99bCb+3Bn5vXfzeRvi9zfF72+D3tsfv7Yrf2xO/Nxa/Nx6/dyB+bxJ+bwp+7xj83on4vVPxe2fj9y7A712K3/sLfu96/N4t+L278HsP4Pcew+89g997Eb/3On7vXfzex/i9L/B7A/i9Kn6vgd/r4P/Qw2/18Os9/HIP/6se/qc9/Pd7+G/28F/p4T/Xw3+ih/9QD//eHv7tPfwbe/hX9/Cn9/Av6uGf28M/vYd/Ug//uB7+UT38yT38iT38CT38fXr4e/XwR/fwd+zhb9vD37KHv0kPf/0e/lo9/FV7+Mv38Jfs4S/awx/Uw5+nh9/Xwy/00H6Pfr/HfH6Phf0ei/s9lvF7rOj3WN3vMczvsaHfYzO/x9Z+j1/5PXbxe+zh9xjr99jX73GA3+NQv8cRfo8/+j3+5Pf4s9/jLL/HBX6PS/weV/o9rvN73OL3uMvvcb/f41G/x9N+jxf9Hq/5Pd7xe3zk9/jC7zHg9/jW7xH7Pf7q9+j6XXp+m57/HT2/TM//mp7/GT3/A3r+W/T8V+n5L9Dzn6TnP0zPv5eefwc9/2Z6/jX0/Mvp+RfR88+j559Jzz+Znn88Pf8oev7h9PxD6Pn70/P3oef/hp7/a3r+zvT8ben5W9HzN6XnD6fnr0PPX5WevwI9fyl6/mL0/IXo+fPQ8yN6ftAV3YUX6na1R3dun+6CPt1FfLpDfbrL+nRX8emu6dNd16e7kU93c5/uNj7dHXy6u/l09/Tp7u3THe/TPcinO8mnO8Wne4xP90Sf7mk+3bN9uhf4dC/16f7Fp3uDT/dWn+5dPt0HfLqP+XSf9em+5NN93af7rk/3Y5/uFz7dAZ9u1afb8Ol2fDpdn2bXp9b1mdn1mdH1+aTr817X542uz0tdn2e7Po93fR7s+tzd9bm163ND1+eqrs9lXZ8Luz7ndH1O6/pM7foc2/U5sutzWNfnoK7Pfl2fcV2fMV2f3bo+O3R9RnZ9tuj6bNz1Wa/rs2bXZ5Wuz3JdnyW6Pot0fRbs+szd9Qm7Pn7Xx+8W8btz4XcXwO8Oxu8Oxe8ui99dGb+7Bn53XfzuRvjdzfG72+B3t8fv7orf3RO/uzd+dzx+90D87iT87hT87jH43RPxu6fid8/G716A370Uv/sX/O71+N1b8Lt34XcfwO8+ht99Br/7In73dfzuu/jdj/G7X+B3B/C7VfxuA7/7V/zkB/xWF7/exZ/Zxf+yi/9pF//9Lv6bXfyXu/jPdfGf6OI/1MW/p4t/Wxf/xi7+1V386V38aV38c7r4p3fxT+riH9fFP7KLP7mLP7GLP6GLv08Xf68u/ugu/o5d/FFd/C27+Jt08dfv4q/VxV+1i798F3/JLv6iXfxBXfx5uvh9XXy/i/a79Ptd5ve7LOx3WdzvsozfZSW/y+p+l2F+lw39Lpv5Xbb2u/zK77KL32UPv8tYv8u+fpcD/C6H+l2O8Lv80e/yJ7/Ln/0uZ/ldzve7XOJ3udLvcp3f5Wa/y51+l/v9Lo/6XZ72u7zgd3nN7/KO3+Ujv8vnfpdv/C7f+l1iv0vb7/KDn9D1W3T9Ol2/RNf/kq7/KV3/fbr+m3T9l+n6z9H1n6DrP0TXv4eufxtd/0a6/tV0/el0/Wl0/XPp+mfQ9U+i6x9H1z+Krj+Zrn8wXX8CXX8fuv5edP3d6fo70fVH0fW3pOtvQtffgK6/Nl1/Vbr+8nT9Jen6i9L1B9H156XrR3T9oCvSwQv/SwHM49Mb5NMbMlsBrDqrANabrQBG+fR2+BsFMGVWAUz16Z3u0ztnVgFc5tO7yqd3/WwF8OBPCuCN/1wBvNzzebbn83jP58FZBXBbz+eG2QpgWs/nnJ8UwJH/YAGs3fNZdbYCGNLzGdTzmafn09fzKfR8/F6I35sbv7cgfm8R/N4SswpgZfzemj8pgJH4vR1mK4C98Xv7/d8pgGS2ApjZw//yJwXw8k8K4J4e/m0/KYBp/2AB7PQzBbD2TwpgyL8vgKLfYy6/xwJ+j0X8HkP9Hsv6PVb2e6zp91jX77GR32Nzv8dIv8f2fo9d/R57+j329nuM93sc6PeYNKsAjvF7nOj3OHW2ArjU7/GXnxTAAz8pgNd/UQG06Pn12QrgU3r++/T8N+n5r9Dzn6PnPzFbAdxOz7/xJwVwLj3/DHr+Kb+gAHai5283WwFsMKsAVv9JASxMz5/v3wrgh8ELd5PQJ5mnQDKoQDKkQLJkgWT5AskqBZK1CyTrF0g2LpBsUSDZtkCyY4FktwLJmALJuALJfgWSiQWSwwokUwokxxZITiyQ/LlAclaB5IICySUFkisLJNcXSG4pkNxZILm/QPJogeSpAskLBZJXCyRvF0g+LJB8XiD5ukBSKZB8VyBpF+gkBeKkwLdJgZlJgS+SAh8mBd5JCryeFHg5KfBsUuDxpMCDSYF7kgK3JQVuSApclRSYnhSYlhQ4JylwWlLgpKTAcUmBI5MCk5MCE5MCE5IC45ICeyUFRicFdkwKbJsU2DIpsElSYIOkwNpJgVWTAiskBZZKCgxJCgxKCsybFIiSAoWkQCEJKSRzU0gWoJAsQiFZgkKyHIVkZQrJmhSS9SgkG1FINqeQjKSQbE8h2ZVCsieFZG8KyXgKyYEUkkkUkiMoJMdQSE6kkJxKITmLQnIBheRSCslfKCTXUkhuoZDcSSG5n0LyCIXkKQrJCxSSVykkb1NIPqSQfEYh+ZpCUqGQfEch+SuFTkKhkVCoJRRmJhRmJBQ+SSi8m1B4I6HwckLh2YTC4wmFhxIK9yQUbkso3JhQuDqhMD2hMC2hcG5C4fSEwskJheMTCkclFCYnFA5OKOyfUNgnofCbhMLuCYWdEgrbJRS2SihsmlDYIKGwTkJhtYTCCgmFpRIKiyUUFkoozJtQiBIKQUKxkDBXIWGBQsLgQsLQQsKyhYSVCglrFBLWLSRsVEjYrJCwTSFh+0LCLoWEPQoJYwsJ+xYSDigkTCokHFFI+GMh4U+FhFMKCWcWEi4oJFxaSLiikHBtIeGWQsIdhYT7CgmPFBKeLCQ8X0h4tZDwViHhg0LCZ4WEbwoJ5ULCd4WEduEHkkJCUmiRFKokhZkkha9ICp+QFN4lKbxJUniZpPAMSeEJksJDJIW7SQq3kxRuIilcTVK4jKRwEUnhPJLC6SSFk0kKx5MUjiIpHE5SOISksD9J4Xckhd+QFH5NUtiJpLAdSWErksKmJIXhJIVhJIXVSAorkhSWJiksTlJYmKQwH0nBkBSCrvhhkcHdJCyQzB2QDApIhgQkSwQkywUkqwQkawUk6wckGwckmwckowKSHQKSXQOSPQOSsQHJ+IDkoIBkUkByREByTEAyNSA5NSA5KyC5ICC5JCC5MiC5PiC5JSC5MyC5PyB5JCB5KiB5ISB5NSB5OyD5MCD5PCD5OiCpBCTfBSTtgHYSECcB3yYB3yQBnycBHyUB7yQBrycBLyYBTycBjyYBDyQBdyUBtyQB1ycBf0kCLksCLkwCzkkCTksCpiYBxyYBRyYBhyUBByUBE5KAcUnAXknA6CRgxyRgVBKwZRKwSRKwfhKwdhKwahKwQhKwVBIwJAkYlATMmwRESUCQBARJH0EyN0GyIEGyKEGyBEGyHEGyCkGyFkGyHkGyEUGyOUEykiDZgSDZlSDZkyDZmyAZT5AcSJBMIkiOIEiOIUhOJEj+TJCcRZBcQJBcQpBcSZBcR5DcTJDcSZDcT5A8QpA8RZC8QJC8SpC8TZB8SJB8RpB8TZBUCJLvCJIWwV8Tgjgh+DYh+CYh+CIh+DgheDcheD0heDEheCYheCwheDAhuDshuDUhuCEhuCohuCwhuDAhOCchOC0hOCkhOC4hODIhOCwhmJgQTEgIxiUEeyUEoxOCHROCbROCLROCTROCDRKCtROCVROCFRKCpRKCIQnBQgnBvAlBlBAECWGQMHeQsGCQMDhIWCJIWC5IWClIWCNIWC9I2ChI2CxIGBkkbB8k7Bok7BkkjA0SxgcJBwYJhwYJRwQJfwwS/hQk/DlIOCtIOD9IuCRIuDJIuDZIuDlIuCNIuC9IeCRIeDJIeD5IeDVIeCtI+CBI+CxI+CpIKAcJ9SChGSR0gr+SBDFJUCUJBkiCL0iCj0mCd0mC10mCl0iCZ0iCx0iCB0mCu0mCW0mCG0iCq0iCy0iCaSTBOSTBaSTBSSTBcSTBkSTBZJLgYJJgAkkwjiTYiyTYnSTYkSTYliTYkiTYhCTYgCRYhyRYjSRYgSRYiiRYjCRYmCSYlyQwJIGeBUBfgWSegGShgGSxgGTJgGT5gGS1gGTtWQBsEpBsEZBsG5DsGJDsFpCMCUj2Dkj2C0gmBiSHBSRTApJjA5ITA5LTApKzZwFwaUDyl4Dkup8A8OhsALw2BwC+/3kAvpgDAI/NBsCtswC46hcAMHEOAGw7C4BNk4ANZgGw2mwALJYELJQEzJcEmP8XgIggmYcgGUSQDCFIliRIlidIVp0FwPoEySYEyRYEyahZAOxGkIyZBcB+BMlBswCYMhsAp/4dAO6bDYDnZwPgg1kAfEWQlP8NgHbn3wPwUULwzs8A8MAcAJg2C4DTZwPgqIRgckJwcEKwf0KwzywAdk8IdkoItksItpoFwPCEYJ2EYPWEYMWEYOmEYLGEYOGEYL6EoD8h0Al9QcI8swBYNEhYchYAqwQJa84CYOMgYYufAWDvnwHgmH8aAB2SoDEbADPmAMDjswFw2ywAriYJps8C4FyS4IxZABxPEhw1GwC/Jwn2IQl+QxL8miTYiSTYjiTYiiQYQRJsSBIMIwlWJwlWJAmWJgmGkgSDSYL5SIJ+kqDYFT8suki30xfQmU/TWVjTWUzTWUrTWV7TWVXTWUfT2UDT2UTT2ULTGaXp7KTpjNZ0xmg6e2s6+2k6EzWdwzSdKZrOMZrOiZrOaZrO2ZrOBZrOJZrOlZrO9ZrOLZrOnZrOfZrOI5rO05rOC5rOq5rOW5rOB5rO55rO15pOWdOpazotTauj+a6jqXQ0X3c0n3c0H3Y0b3c0r3U0L3Q0T3c0j3Y093c0d3U0t3Q013c0f+loLu1oLuxozu5oTutopnY0x3Y0R3Y0h3U0EzuaCR3NuI5mr45mdEezU0ezbUezZUezaUezQUezTkezWkezQkezdEezWEezcEczX0djOhrd0ehOhO7Mi+4shO4MQXeWQneWR3dWRXfWRnfWR3c2QXe2QHdGoTs7oju7oTtj0J290Z390J2D0J1J6M4UdOcYdOdEdOdUdOcsdOcCdOcSdOdKdOc6dOdmdOdOdOc+dOcRdOcpdOd5dOdVdOctdOcDdOdTdOcrdKeE7tTRnSa61UF/30FXOuivO+jPO+iPOuh3OujXOugXO+hnOujHOugHOui7OuhbO+gbOuirOujLOugLO+hzOujTO+iTOujjOugjO+jJHfTBHfT+HfQ+HfReHfTuHfTOHfR2HfRWHfSmHfTwDnpYB716B71iB710B714Bz24g56/g+7voHWHPt1hHt1hkO6wqO6wpO6wvO6wiu6wlu6wnu6wse6whe4wUnfYQXfYVXfYU3fYW3cYrzscqDscqjscoTv8UXf4k+7wZ93hTN3hfN3hYt3hCt3hWt3hJt3hDt3hXt3hYd3hSd3hOd3hFd3hTd3hfd3hU93hS92hpDvUdIeG7vBX/Vc6Oqajv6Wjv6Gjv6CjP6Kj36GjX6ejX6Sjn6GjH6OjH6Cj76ajb6Wjb6Cjr6KjL6Ojp9HR59DRp9PRJ9PRx9HRR9HRk+nog+no39PR+9DRv6Gjd6ejd6ajf0VHb0VHj6Cjh9PRw+joNejoFenoZejooXT0YDp6ATq6n44udsVfF12k244C2vNq2gtr2otp2ktp2ito2qtq2uto2hto2pto2lto2qM07Z007dGa9hhNe29Ne4KmfZCmPUnTnqJpH6Np/0nT/rOmfaamfb6mfbGmfYWmfa2mfZOmfbumfa+m/bCm/aSm/Zym/bKm/aam/b6m/Ymm/aWmPVPTrmnaDU2zram3NaW25qu25tO25oO25q225tW25oW25qm25pG25v625s625pa25rq25i9tzaVtzYVtzdltzaltzdS25ti25si25rC2ZmJbM6GtGdfW7NXWjG5rdmprtm1rtmxrNm1rNmhr1mlrVmtrVmxrlm5rFmtrFm5r5mtrTFuj2xrdjtDtedDtQej2EHR7SXR7eXR7FXR7LXR7PXR7Y3R7c3R7JLq9A7q9K7q9J7o9Ft0ej24fiG4fim4fgW7/Ed3+E7p9Crp9Jrp9Prp9Mbp9Bbp9Dbp9E7p9O7p9L7r9MLr9BLr9HLr9Mrr9Jrr9Prr9Cbo9A92eiW7X0O0Y3Wyhv2+jK23012305230B2302230q230C2300230Y230A230XW30LW309W30VW30ZW30hW302W30aW30SW30cW30kW305DZ6Yhu9fxu9Txu9Vxu9exu9cxu9XRu9VRu9aRs9vI0e1kav3kav2EYv00Yv3kYPbqPnb6P72+himz7dZh7dZkHdZlHdZkndZjndZmXdZk3dZj3dZiPdZjPdZhvdZnvdZhfdZg/dZqxus69uc4Buc4hu8wfd5mjd5gTd5hTd5gzd5jzd5iLd5nLd5hrd5kbd5nbd5h7d5iHd5gnd5lnd5mXd5g3d5j3d5mPdZoZuM1O3qeo2sW7T0i3a+nvaukJbf0Nbf05bf0hbv01bv0pbv0BbP01bP0pb309b30Vb30pbX09bX0VbX0ZbT6Otz6GtT6OtT6Ktj6Otj6KtJ9PWE2nr/WnrfWjr39DWu9PWO9PWv6Ktt6KtR9DWw2nrYbT16rT1SrT1MrT14rT1YNp6Adp6Lto67Ir2oot0W1FAaz5NayFNa4imtaSmtbymtZqmtbamtb6mtbGmtYWmNUrT2lHT2k3T2lPTGqtp7adpHahpHapp/UHT+qOm9SdN6xRN60xN6zxN62JN63JN61pN6yZN63ZN6x5N6yFN63FN61lN6yVN6w1N611N6xNNa4amNaBpfatpxZq4pam1NKWWZkZL80lL825L82ZL83JL81xL80RL83BLc29Lc0dLc1NLc21Lc3lLc0lLc0FLc1ZLc2pLc2JLc0xLM6WlmdTSHNTS7NfSjGtpxrQ0o1uaHVuabVuaLVuaTVuaDVqatVua1VqaFVuapVuaxVqahVua+Voa09IUWxrditCtedCtQejWEHRrSXRreXRrFXRrLXRrPXRrY3Rrc3RrJLq1A7q1K7q1J7o1Ft0aj24dgG4dim79Ad36I7p1Arp1Crp1Jrp1Hrp1Mbp1Obp1Dbp1I7p1O7p1D7r1ELr1BLr1LLr1Err1Brr1Hrr1Ebr1Bbo1gG5V0a3v0Y0mutZCz2yhZ7TQn7bQ77fQb7XQr7TQz7XQT7bQj7TQ97XQd7TQN7fQ17XQV7bQl7bQF7TQZ7fQp7bQJ7bQx7bQU1row1rog1roCS30uBZ6rxZ6dAu9Uwu9bQu9VQu9aQu9QQu9Tgu9egu9Ygu9dAu9WAs9uIWev4Xub6GLLfp0i3l0iwV1i0V1iyV0i+V0i1V0i7V0i/V0i410i811i210i+11i110iz10i7G6xb66xQG6xSG6xR90i6N1ixN0i5N1izN0i/N0i4t0i+m6xdW6xY26xW26xT26xYO6xWO6xbO6xYu6xeu6xTu6xUe6xee6xTe6xbe6xXe6RVM3aOkaLV2ipb+kpT+lpd+npd+ipV+hpZ+npZ+kpR+hpe+jpe+kpW+mpa+jpa+kpS+lpS+gpc+mpU+jpU+kpY+lpafQ0ofR0gfR0hNo6XG09F609O609M609La09Fa09AhaejgtPYyWXp2WXpGWXpqWHkpLD6al56el56Klw65oD1m02zSa5vxFmoOLNBcv0ly6SHOFIs3VizTXKdLcoEhz0yLNLYs0RxVp7lSkObpIc0yR5t5FmvsVaR5UpDmpSPOIIs1jijT/VKR5SpHmmUWa5xVpXlykeXmR5tVFmjcWad5WpHlPkeaDRZqPF2k+U6T5UpHm60Wa7xZpflSk+UWR5jdFmt8WaX5fJG4WqTaLDDSLzGgW+bhZ5L1mkTeaRV5uFnmuWeSJZpGHmkXubRa5o1nkpmaRa5tFrmgWubhZ5IJmkbOaRU5tFjmxWeSYZpEpzSKHNYsc1CwyoVlkXLPIXs0io5tFdmoW2bZZZKtmkU2bRYY3i6zTLLJ6s8hKzSLLNIss3iwyuFlk/maRuZpFwmaRYrOfYnM+is2FKTYXo9hcimJzBYrNVSk216bYXJ9ic2OKzS0oNkdRbO5AsbkrxeaeFJt7U2yOp9g8kGLzUIrNP1Bs/pFi8wSKzVMoNs+g2DyXYvMiis3LKTavpti8gWLzNorNeyg2H6TYfJxi8xmKzRcpNl+n2HyHYvMjis3PKTYHKDYrFJvfU4ybFGtNijObFGc0KX7SpPh+k+KbTYovNyk+16T4ZJPiw02K9zUp3tGkeFOT4nVNilc2KV7apHhBk+JZTYqnNilObVI8tknxyCbFw5oUJzYpTmhS3KdJca8mxd2bFHduUtyuSXHrJsURTYrDmxSHNSmu0aS4UpPisk2KQ5sUF2lSXKBJce4mxbCJKTaZt9hkoWKTIcUmSxWbrFBssmqxyVrFJusXm2xcbLJ5scnIYpMdik12LTbZs9hkbLHJ+GKTA4pNDi02+UOxydHFJicUm5xSbHJGscl5xSbTik2mF5tcXWxyQ7HJbcUmdxebPFhs8lixyTPFJi8Wm7xWbPJOscmHxSafF5t8U2xSKTb5rtikUWzQLNZoFmfSLH5Js/gpzeL7NItv0iy+QrP4PM3ikzSLj9As3kezeCfN4s00i9fRLF5Js3gpzeIFNItn0yyeRrN4Es3isTSLR9IsTqZZPJhmcX+axX1oFn9Ds7g7zeLONIvb0SxuTbM4gmZxQ5rFdWkW16BZXIlmcVmaxSVoFhelWVyAZnFumsW+rmgtNqTbMEUa84c0Boc0hoY0lglprBjSWC2ksU5IY4OQxqYhjS1DGqNCGjuGNHYLaYwJaewd0tgvpHFgSOPQkMYRIY0/hjROCGmcHNI4I6RxbkjjopDG9JDG1SGNG0Iat4U07g5pPBDSeCyk8XRI48WQxmshjXdCGh+GND4PaXwd0qiENL4L+a4R8m0j5JtGyBeNkI8aIe80Ql5vhLzcCHm2EfJ4I+ShRsg9jZA7GiE3NUKuaYRc0Qi5uBFyfiPkrEbInxshf2qEHNMImdIImdQIOagRsl8jZFwjZK9GyOhGyE6NkG0bIVs2QjZthAxvhKzTCFm9EbJSI2SZRsjijZDBjZAFGiFzNULCRkjYMISN+QgbCxE2FiNsLEXYWIGwsSphY23CxvqEjY0JG1sQNkYSNnYgbOxK2NiTsDGWsDGesHEAYeMQwsYfCBtHEzaOJ2ycTNg4nbBxLmFjGmFjOmHjasLG9YSNWwkbdxM2HiBsPEbYeJqw8QJh4zXCxtuEjQ8IG58RNr4ibJQJG3XC72PCaoNwoEE4o0H4cYPwvQbhGw3ClxuEzzYIn2gQPtQgvLdBeHuD8KYG4bUNwisahJc0CM9vEJ7VIDy1QXhig/CYBuGUBuGkBuFBDcIJDcJxDcK9GoSjG4Q7NQi3axBu1SAc0SAc3iAc1iBco0G4UoNw2Qbh0AbhIg3CBRqEczcIwwZR2GDesMGgsMGQsMFSYYPlwwarhA3WDBusFzbYOGywedhgm7DB9mGDXcIGe4QNxoYN9g0b/D5scHDY4PCwwVFhg+PDBieFDU4PG5wTNpgWNrgsbHBV2OD6sMGtYYO7wgYPhA0eDRs8FTZ4IWzwatjg7bDBB2GDz8IGX4UNSmGDWtggDmMaYZVGOEAjnEEj/IRG+B6N8A0a4cs0wudohE/SCB+mEd5LI7yDRngzjfA6GuGVNMJLaIQX0AjPphGeSiOcSiM8lkY4hUZ4GI1wIo1wfxrhPjTCvWiEu9MId6YRbkcj3JpGOIJGuCGNcF0a4Ro0wpVphMvSCJegES5KI1yQRjg3jbCvKxqLDenGpkg8X0g8OCRePCReKiReISReNSReJyTeICTeJCTeIiQeGRLvGBLvFhLvGRKPDYn3DYkPDIkPDYn/EBIfHRIfHxKfEhKfERKfGxJPC4mnh8RXhcQ3hMS3hsR3hcQPhMSPhsRPh8QvhMSvhsRvh8QfhMSfhcRfhcSlkLgWUo9DynHI13HIZ3HIh3HIO3HIa3HIi3HI03HIY3HIg3HI3XHIbXHIDXHI1XHI5XHIRXHIeXHIGXHIKXHICXHIH+OQI+KQQ+OQA+OQ8XHI3nHImDhktzhkxzhkVByyZRyyaRyyQRyyThyyWhyyYhyyTByyeBwyOA6ZPw6ZKw4J45Aw7ieM5yOMFyaMFyOMlyKMVyCMVyWM1yaM1yeMNyGMtyCMRxLGOxDGuxLGexLGYwnjfQnjAwjjQwjjPxDGRxPGxxPGJxPGpxPG5xLG0wjj6YTxXwjj6wnjWwnjuwjjBwjjRwnjpwnjFwjjVwnjtwnj9wnjTwnjrwjjEmFcI6zHhJWY8OuY8POY8MOY8N2Y8PWY8MWY8JmY8LGY8KGY8J6Y8LaY8MaY8OqY8IqY8OKY8LyY8MyY8JSY8MSY8JiY8IiYcFJMeFBMuF9MOC4mHBMTjo4Jd4oJt40Jt4oJN40Jh8eEw2LC1WPClWLCZWLCoTHhIjHhAjHh3DFhGNMfxswbxiwUxgwJY5YKY5YPY1YJY9YKY9YLYzYOY7YIY7YJY7YPY3YJY/YIY8aGMfuGMb8PYw4OYw4PY44OY44LY04KY04LY84JYy4MYy4LY/4SxlwXxtwSxtwZxtwfxjwSxjwZxjwfxrwSxrwVxrwfxnwSxnwZxpTCmGoY8334HXFYIQ6/IQ4/Jw4/Ig7fJQ5fJw5fIg6fIQ4fJw4fIg7vIQ5vJw5vJA6vIQ6vIA4vJg7PJw7PJA7/TBz+iTg8hjicQhxOIg4PIg4nEIfjiMO9iMPRxOFOxOG2xOFWxOEI4nA4cTiMOFydOFyJOFyWOBxKHC5CHC5AHM5DHEazAOgvEi8QEi8SEi8REi8TEq8YEq8eEg8LiYeHxJuGxFuGxKNC4p1C4tGzANg7JB4fEh8UEk+aBcAfQ+ITQuKT/wEAnpoNgLf+PQDfzwGAt/8OADf+DABnzgLgT7MBMCkOOSgO2W82AEbHITvFIdvGIVvFISPikOFxyLA4ZPU4ZKU4ZNk4ZGgcskgcskAcMncc0heHhPFchPH8hPHgWQAsTRivSBivRhivMxsAWxLGo34CwN6E8fifAeCEOQBw1RwAeHI2AN765QC8818AwJTZAJgwC4C9ZgNgu1kAjJgFwLox4RqzAFh2FgCLxoQLzgKg718AmD+MWTiMWSyMWTqMWSGMWS2MWTuMWT+M2SSM2TKMGRnG7BDG7BrG7BnG7D0LgAPCmENmA+D4WQCc/jMAXP93AHjjHwLgnb8DwE0/A8BZswA4kTg8dhYAhxGHE2cBsM8sAHYnDncmDrcjDrcmDjcjDjckDtclDtckDleeBcASxOGixOGgfwPg+8UX69bnCqkv2Ed9kT7qQ/uoL9NHfcU+6mv0UR8WUR8eUd80or5lRH1URH2niProiPqeEfW9I+r7RdQP6KN+aET9DxH1oyPqJ0TUT+6jfnof9bP7qF/YR/3SiPqVEfXrIuo3R9Tv6KN+Xx/1hyPqT0bUn42ov9xH/Y0+6u9F1D+OqH8RUR+IqH/bR7Xex8x6HzPqfXxSj3i/HvFmvY+X6xEv1COeqkc8Wo94oB5xV72PW+t93FDv46p6xPR6HxfX+ziv3scZ9T5Oqffxp3rEH+sRR9QjJtUjDqxH7FeP2LseMaYeMboesVM9Ytt6xFb1PkbU+xhe72NYvY816hEr1SOWrUcsUY9YtB6xYD1i7nofffU++upz0Vefn6g+mKi+OFF9afrqK9JXX42ovjZRfQOi+iZE9S3oq48iqu9AVN+VqL4HUX0sUX1f+uq/J6ofQlQ/nL76UfTVj6evfhJ99dPoq59NX/1C+uqXEtWvoK9+LX31m+mr30FUv4+o/hBR/Qmi+rNE9ZeI6m8Q1d8jqn9MVJ9BVB8gqlfoq9XoK9Xp+6pO9Gmd6MM60dt1olfrRC/UiZ6uEz1ap+/BOn131em7rU50Q53o6jrRZXWii+tE59XpO7NO3yl1ohPrRMfUiY6oE02q03dQnWhCnWifOtFedaLRdaKd60Tb1Ym2rtM3ok7fhnWidev0rVmnb+U60bJ1oiXq9C1aJxpUJ5qnThTV6e+rM39fnYWjOotFdZbuq7NCX51V++qsFdVZP6qzSVRni6jOyKjO9lGdXaI6e0R1fhvV+V1UZ/+oziFRncOjOkdGdY6L6pzUV+fUvjpn99W5IKpzSVTnyr461/bVuamvzu1RnXujOg9HdR6P6jwb1XkpqvN6VOfdvjof99X5PKrzTVSnEtWp9dWo95Wo931FPfqMevQh9b63qfe9Rr3vRerRM9Sjx6hHD1CP7qYe3Uo9upF6dA31aDr16GLq0fnUozOpR3+mHv2JevRH6tEU6tFh1KOJ1PsmUO/bh3rfXtT7dqce7Uw92o56tDX1aDPq0YbUo3Wp961JvW9l6tFy1KMlqUdDqEeDqEfzUo9MV3y3+OLd2lx91BaIqC0SURsaUVsmorZiRG31iNo6EbUNImqbRtS2jKiNiqjtGFHbLaK2R0RtbERt34jaARG1QyJqh0fUjoqoHR9ROymidnpE7eyI2oURtUsjaldG1K6LqN0cUbsjonZvRO2RiNqTEbVnI2ovR9TeiKi9G1H7OKL2RUTtm4haJaJSixioRcyoRXxci3ivFvFGLeLlWsRztYgnaxGP1CLuq0XcUYu4pRZxXS3iL7WIy2oRF9YizqlFnF6LOLkWcXwt4uhaxB9qEYfWIg6oRYyvRexdixhTi9itFrFjLWLbWsRWtYgRtYjhtYh1ahGr1yJWrkUsW4sYWotYpBaxYC1inlpEVIuIanMT1RYgqi1CVFucqLY0UW1FotpqRLV1iGobENU2IaptQVQbSVTbgai2K1FtD6LaWKLavkS1A4hqhxDVDieqHUVUO46odhJR7TSi2tlEtQuJapcS1a4gql1LVLuZqHYHUe1eotrDRLUniGrPEtVeIqq9TlR7h6j2EVHtc6La10S1MtG3VaKBGtGMGtHHNaL3a0Rv1oheqRE9VyN6skb0SI3ovhrRXTWiW2pE19eI/lIjml4juqhGdG6N6PQa0Sk1ohNqRH+sEf2hRnRojejAGtF+NaJxNaIxNaLRNaKdakTb1oi2qhGNqBFtWCMaViNao0a0co1o2RrREjWiRWtEC9aI5qkRRTXmjmrMH9UYHNVYPKqxdFRjxajGqlGNtaMaG0Q1NolqbB7VGBnV2CGqsWtUY4+oxm+jGr+Lavw+qnFwVOPwqMaRUY3johpToxqnRjXOimpcENW4JKpxRVTj2qjGTVGN26Ma90Q1HopqPB7VeCaq8WJU4/WoxjtRjQ+jGp9HNb6KapSiGtXoW2rRTGrRDGrRJ9Si96lFb1GLXqEWPU8teopa9Ci16H5q0V3UolupRddTi66iFk2nFl1ELTqXWnQGtegUatEJ1KI/UouOoBZNohYdRC3aj1o0jlo0hlq0O7VoZ2rRttSirahFI6hFG1KL1qUWrUEtWplatCy1aElq0RBq0ULUonmpRaYr6kMX71bn7qM6KKK6aER1iYjqshHVlSKqq0dUh0VUh0dUN42obhlRHRVR3SmiOjqiumdEdWxEdd+I6oER1UMjqodHVI+KqB4fUT0ponpaRPXsiOqFEdVLI6pXRlSvi6jeHFG9I6J6b0T1oYjqExHVZyOqL0VUX4+ovhNR/Sii+nlE9euIaiWiUo34phrxRTXi42rEu9WIN6oRL1UjnqtGPFmNeLgacW814s5qxC3ViOuqEX+pRlxajZhWjTinGnF6NeLkasTx1YijqxF/qEYcWo04sBoxvhoxthoxphoxuhqxUzVi22rEVtWIEdWIDasRw6oRa1QjVq5GLFeNWKIasWg1YlA1Yt5qhKlGRNV5iKoLElUXIaoOJaouQ1Rdiai6OlF1HaLqBkTVTYmqWxJVRxFVdyCq7kpU3ZOoOpaoui9R9QCi6sFE1cOJqkcRVY8jqp5EVD2VqHo2UfVCouqlRNUriarXEFVvIqreQVS9h6j6EFH1caLqM0TVF4mqrxNV3yGqfkhU/Yyo+hVRtUxUqRJ9UyX6okr0cZXovSrRG1Wil6tEz1WJnqwSPVwlur9KdGeV6JYq0XVVoquqRNOrRNOqROdWiU6vEp1SJTqhSvTHKtEfqkSHVokOqhLtVyUaVyUaUyUaXSXaqUq0XZVo6yrRiCrRhlWidatEa1aJVqkSLVclWrJKNKRKtFCVaL4qkakyd1RlgajK4KjK4lGVZaIqK0VVVouqrB1V2SCqsklUZcuoysioyg5RlV2jKntEVX4bVfldVOX3UZWDoyqToypHRlWOi6pMjaqcGlU5K6pyflTlkqjKFVGVa6MqN0VVbouq3BNVeTCq8nhU5ZmoygtRldeiKu9EVT6MqnwWVfkqqlKOqlSjb6lGA1SjGVSjT6hG71ON3qQavUI1eo5q9BTV6BGq0f1Uo7uoRrdQja6nGl1FNZpONbqIanQu1egMqtEpVKM/UY3+SDU6gmo0iWp0ENVoP6rROKrRXlSj3alGO1ONtqMabU012oxqtBHVaD2q0ZpUo1WoRstTjZaiGg2hGi1ENZqPatTfFbWhQ7uVuSMqgwyVRQ2VJQyVZQ2VlQyV1Q2VYYbKcENlU0NlS0NllKGyk6Ey2lDZ01AZa6jsa6j83lA5xFA53FA5ylA5zlCZaqicZqicZaicb6hcbKhcaahca6jcaKjcZqjcbag8ZKg8bqg8bai8YKi8aqi8Y6h8aKh8aqh8aajMNMysGL6qGD6rGD6sGN6pGF6vGF6sGJ6tGJ6oGB6qGO6tGG6vGG6uGK6rGK6sGC6tGC6oGM6pGE6vGE6qGI6vGI6qGP5QMRxaMRxQMYyvGMZWDGMqhtEVw44Vw7YVw5YVw4iKYcOKYVjFsEbFsHLFsFzFsGTFsGjFMKhimLdiMBWDqcyNqSyAqSyCqQzFVJbBVFbEVFbDVNbBVDbAVDbBVLbAVEZiKjtgKrtiKntgKr/FVH6HqfweUzkYU5mMqRyJqRyLqUzFVE7FVM7CVM7HVC7BVC7HVK7BVG7EVG7DVO7GVB7EVB7DVJ7GVF7AVF7FVN7CVD7AVD7FVL7EVAYw5TLm6wrm8wrmowrmnQrmjQrmpQrm2QrmiQrmoQrmvgrmjgrm5grmugrmLxXMZRXMtArmnArm9Arm5ArmhArmjxXMHyqYQyuYAyuY/SqYcRXMmApmdAWzUwWzXQWzdQUzooLZsIJZt4JZs4JZpYJZroJZsoIZUsEsVMHMV8GYCnObCvObCoNNhcVNhWVMhRVNhVVNhbVNhfVNhU1MhS1MhW1Mhe1NhV1MhT1Mhd+aCvuYCvubChNNhcNMhSNNhWNMhRNNhT+bCmeaCuebCheZCpebClebCjeYCreaCneZCg+YCo+aCk+ZCs+bCq+YCm+ZCu+bCp+YCjNMhZmmQsWUqZhvqJgvqJiPqZh3qZg3qJiXqZjnqJgnqZiHqZj7qJg7qZhbqJjrqZi/UDGXUTHTqJhzqZgzqJiTqZgTqJg/UjFHUDGTqJiDqJj9qJhxVMxeVMzuVMzOVMx2VMzWVMxmVMxGVMx6VMyaVMwqVMzyVMxSVMxiVMxCVMx8VEx/V3w7dGi3PHdEeZChvKihvIShvIyhvKKhvLqhPMxQHm4ob2Iob2EojzSUdzSUdzOU9zCUf2so/85Q/r2hfLChPNlQPtJQPtZQnmoon2oon2kon28oX2woX2EoX2Mo32Ao32oo32UoP2goP2YoP2UoP28ov2Iov2Uov28of2IozzCUBwwzy4Yvy4ZPy4YPyoa3yoZXy4YXyoany4bHyoYHy4a7y4bbyoYby4ZryoYryoZLyoYLyoazy4ZTy4apZcNxZcNRZcPhZcMhZcPvy4Z9y4axZcOeZcNuZcOOZcOosmHLsmHTsmF42TCsbFijbFi5bFi2bFiibFi0bBhUNsxbNpiywZTnwZQXxJQXwZSHYsrLYMorYsqrY8rrYMobYMqbYMpbYMojMeUdMOVdMeU9MOXfYsq/w5T3x5QnYsqTMeUjMeVjMeUTMeU/Y8pnYsrnYcoXY8qXY8pXY8o3YMq3Ysp3YcoPYMqPYspPYcrPY8ovY8pvYsrvY8qfYMozMOVvMDNLmK/KmM/KmA/KmLfLmFfLmBfLmGfKmMfKmIfKmHvKmNvLmJvKmGvKmCvLmEvLmAvLmLPLmNPKmKllzPFlzNFlzB/KmEPKmN+XMePLmL3LmDFlzG5lzE5lzLZlzFZlzIgyZngZs24Zs2YZs3IZs1wZs0QZM6SMWaiMma+MMWXmMWUWMGUGmzKLmzLLmDIrmjKrmTJrmzLrmzKbmDJbmDLbmDLbmzK7mDJ7mDK/NWX2MWUmmDIHmTKHmTJTTJljTJk/mTKnmDJnmjLnmTIXmTLTTZmrTJkbTJlbTZk7TZn7TZlHTJmnTJnnTZmXTZk3TZn3TJlPTJkZpsw3pkzJzKRsvqJsPqNsPqRs3qZsXqNsXqRsnqFsHqdsHqJs7qFsbqdsbqJsrqNsrqRsLqVsLqRszqZsTqdsTqJsjqdsjqZsDqdsDqVsDqRsxlM2e1M2Yyib0ZTNTpTNtpTNVpTNCMpmQ8pmXcpmDcpmZcpmOcpmScpmCGWzEGUzH2XT3xXfLjG0W5onorSQobSoobSkobScobSyobSGobSuobShobSpobSloTTKUNrRUNrNUNrDUBprKO1rKB1gKB1iKE02lI40lI41lE40lE41lM40lM43lC42lC43lK4xlG4wlG41lO4ylO43lB41lJ4ylJ43lF4xlN40lN43lD42lL4wlL4xfFMyzCgZPikZ3i8Z3ioZXi0Zni8Zni4ZHi0ZHigZ7i4Zbi0ZbiwZrikZrigZLikZzi8ZzioZTi0ZppYMx5UMR5YMh5cMh5QMB5QM+5YMe5cMY0qG3UqGHUuGbUuGrUqGESXDhiXDuiXDmiXDKiXD8iXDUiXDkJJhUMkwX8nQXzKY0ryY0iBMaVFMaSimtAymtBKmtDqmNAxT2gBT2gRT2hJTGoUp7YAp7Yop7YEp/RZT2hdT+j2mNBFTmowpHYkpHYsp/QlT+jOmdCamdB6mdDGmNB1TuhpTugFTuhVTugtTug9TegRTegpTeh5TegVTegNTehdT+hhT+gJT+hozMBPzZQnzaQnzfgnzVgnzWgnzQgnzdAnzWAnzYAlzdwlzewlzYwlzTQlzZQlzaQlzYQlzdglzWgkztYQ5voQ5uoQ5vIQ5pIQ5sIQZX8LsXcKMKWF2K2F2KmG2K2G2LmFGlDAbljDrlTBrlTCrljDLlTBLlTCLlTALlTDzlTD9JeYxJRY0JRYxJYaaEsuYEiuaEqubEuuYEhuYEpuYEluYEiNNie1NiV1MiT1Mid+aEr8zJfY3JSaaEoeZEkeaEseYEn8yJf5sSpxpSpxvSlxkSkw3Ja4yJa43JW4xJe40Je4zJR42JZ40JZ43JV42Jd4wJd41JT4yJb4wJb42JWaamZTMl5TMp5TMB5TM25TMa5TMi5TMM5TM45TMg5TMPZTM7ZTMTZTMtZTMlZTMpZTMhZTM2ZTMaZTMSZTM8ZTM0ZTMHyiZQymZAymZ/SiZcZTMGEpmNCWzEyWzHSWzNSWzGSWzESWzHiWzFiWzKiWzPCWzFCWzOCUzmJKZn5KZqyvKSy7RHZjHMLBQPwND+hlYsp+B5foZWLmfgTX6GRjWz8DwfgY27Wdgy34GRvUzsEM/A7v2M7BHPwO/7Wfgd/0M/L6fgYP7GTisn4Ep/Qwc08/An/oZOKWfgTP6GTi3n4Fp/Qxc1s/AVf0MXNfPwM39DNzRz8C9/Qw83M/AE/0MPNvPwEv9DLzez8C7/Qx82M/AZ/0MfNXPVwP9fD7Qz0cD/bw70M8bA/28PNDPswP9PDXQzyMD/dw30M9dA/3cNtDPDQP9XDXQz/SBfi4e6OeCgX7OHOjnzwP9nDjQz7ED/Rw10M/kgX4OHujn9wP9/G6gn7ED/ew50M9uA/3sMNDPqIF+throZ8RAPxsO9DNsoJ81BvpZZaCf5Qf6WWqgn8UG+llooJ/5BvqZa6Cf/oF56R8YRP/AovQPLEH/wLL0D6xE/8Dq9A+sQ//ABvQPbEL/wBb0D4ykf2B7+gd2oX/g1/QP/Ib+gX3oH9if/oGD6B+YRP/AFPoHjqF/4E/0D5xM/8Dp9A+cS//ANPoHLqV/4C/0D1xH/8DN9A/cQf/AvfQPPET/wBP0DzxL/8BL9A+8Rv/A2/QPfEj/wGf0D3xJ/zff0P/FAP0fD9D/3gD9bwzQ/8oA/c8N0P/UAP2PDNB//wD9dw7Qf+vA/0NJ//crXeaN/nd3YPHh4Pgc73eWlebvSS0t7YdlllqWpVlWappmZaalZZlmaWlaZmVmlgYWRdFaLSSKGJyImGAGh4EZYTgHNycnJ8Pa60b45+v6Y8937/3P9b0e1/NGPBn4dpSBL0cZiIwy8OkoAx+NMvD+KAOHRxnYP8rA3lEGdo8y8NIoA8+PMvDsKANPjjLw+CgDq0cZWDnKwAOjDNw7ysDiUQYWjDIwb5SBG0cZmDXKwPRRBqaMMnD1KANXjjJw6SgDdpSBwChmYBQ3MMplA6NcNTDKpIFRfjcwysyBUa4fGOWmgVHmD4yycGCUuwdGuW9glBUDozw6MMragVE2DIyyaWCUrQOjvDAwyssDo+wZGOWNgVEODozy7sAoHw6M8vHAKKcGRvl8YJTYwCiDA6OcGRjlx4FRsgOj5AdGKQ2Mcm5glNrAKI2BUVoDo/zXwCj/Ghjlfw78T0YH/pvRgf9kdOCfjA7UGR34K6MDf2Z04I+MDuQYHfg9owMpRgeGGR2IMzoQY3Tgc0YHPmV04CNGB95ndOAwowMHGB3Yy+jAbkYHXmZ0YBujA88yOvAUowOPMzqwmtGBhxkdeIDRgaWMDixmdGABowO3MjpwI6MDsxkdmMHowBRGByYwOnAlowNjGB2wjA4ELl4yOn78xb4ZoK8BRsYGGBkfYGRSgJHfBRi5LkB/ToD+LQH68wP0FwYYuSdAf1mAkYcCjDwaYGRtgJGNAfrPBOg/F6D/QoD+zgD9PQFG3ggwcijAyLsBRo4GGPk4wMipACNfBBiJBRgZDDByJkD/xwD9swH6vwTo/xZgpBygXw3Qrwfo/zPAyH8GGOkF6I0E6PQDtPoBGv0AtZEA5ZEAv40EyI8EODsSID0SINkPMDgS4OuRAF+MBPhsJMCJkQBHRwIcGQlwaCTAmyMBXhsJsHMkwIsjAZ4bCfDMSICNIwEeHwmwaiTAQ/0Ay/oB7hkJsLgf4PZ+gFv6AeaMBLhuJMD0kQCTRwKMHwkwth9ARwLYkQCBkQCBEUOgLwRGxhIYGUdgZBKBkd8RGLmWQP8GAiM3ExiZT2BkIYGRJQRG7icw8iCBkUcJjKwlMPIEgZFNBEa2Ehh5gcDITgIjewj09xHoHyTQf5fAyFECIx8T6J8k0P+cQD9GoD9IoH+GwMgPBEayBEZ+ITDyG4F+mcDIXwmMnCcw8h8ERv6TwEiXwL/+RaAzQqA1QqAxQqA2QuAvIwRKIwTyIwTOjhD4aYTA9yMEvhsh8PUIgS9GCIRHCHwyQuDYCIEjIwQOjRDYP0Lg9T6BXX0CL/YJPDdCYPMIgSdHCDw+QmD1CIGHRggsHyFw7wiBxSMEFowQuGWEwNwRArP6BKb3CUwZITB+hMAVIwQuHSFgRwgERjCBEVygz2WBEcYFRpgU6PO7QJ+ZgT7XB0a4OTDC/MAICwMj3B3oc1+gz4OBPo8G+qwNjLAhMMLTgRG2BkZ4ITDCzsAIrwZG2BcY4a3ACIcDIxwNjHA8MMLJwAinAyN8FejzbaBPItDnh8AIvw+MkAuM8MdAnz8H+vw1MML5wAj/ERihHRihGxihH/gXI4H/YiRwgZHAPxgJ/I1+4C/0AyX6gV8ZCfyBkcBP9APf0w98Rz/wDSOBKCOBMP3AJ/QDx+gH3mMk8DYjgf30A6/TD+yiH9hBP7CNfuBZRgJPMhJ4nJHAakYCDzMSeICRwL2MBBYzEljASOBWRgI3MhKYxUhgOiOBKYwEJjASuJKRwKWMBHxGAt7FS/7n1Vdf7JsA/Us9+ld49K/26E/26F/j0b/Ooz/X41/zPP51u0d/kUf/Ho/+co/+So/+Ko/+Oo/+Ro/+Mx795zz6L3j0d3r09wTovxGgfyhA/90A/aMe/Y89+qcC9L8I0I8F6A969M949H/0+Fc2wL/+HUD/zx79v3r06x79//Dotz36/+3x332P/+x7/LPvUe97VPse5b7Hb/0A+X6As/0A6X6AZN9jsO/xdd/ji77HZ32PE32Po32PI32PQ32PN/ser/U9dvY9Xux7PN/32Nz32Nj3eLzvsbrvsbLvsbzvcW8/wOK+x4K+x7y+x9y+x6y+x/S+x5S+x9V9jyv6Hpf2Pfx+AK8fwOuH8PqX4vXH4vXH4/Un4/Wvwetfh9efg9e/Ba9/O15/EV7/Hrz+Mrz+Q3j9VXj9dQT6G/H6m/D6z+H1X8Dr7yTQ34PX34f3r4N4/Xfx+kfx+h/j9U/i/etzvH4Mrz+I968zeP0f8fq/x+v/gtf/I17/z3j9Cl7/73j9/yDQbxPod/F6/8L7zz7eP/t49T5etU/gXJ9AsY+X7+Od7eP91CfwfZ/Ad328r/t4X/Txwn28T/p4x/p4R/oEDvUJ7O/jvd4nsOtfBF7s4z3fx3u2T+CpPoH1fQKr+3gr+3gP9PGW9vHu6uMt6BOY1ydwYx9vdh9vRp/AlD6Bq/t4V/bxxvTx/D6e18cG+migz1ivzzivz+RAn2sCfa71+tzg9bnZ63O712eR12eJ1+d+r8+D3r94NNBnXaDPE16fTV6frV6fF7w+O70+r3p99nl93vL6HPb6HPX6HPf6nAz0OR3o85XX51uvT8Lr84PX5/den1ygzx8Dff7k9al4ff7u9Wl6fdpen67X51/ev+h7Hfpei77XoO/V6Hvn6Hsl+t6v9L0/0Pd+ou99T9/7jr73DX0vSt8L8y/vE/7lHaPvvUffe5t+YD9973X63i763g763jb63rP0vafoe+vpe2voew/T9x6g7y2l791FP3AH/cCt9L0b6Xuz6Xsz6HtT6Qcm0A9cSd+7jL7n6HvBi5eMXH31xV4oQO9Sj96VHr0JHr0pHr1rPHrXefTmevTmefRu9+gt8ujd49Fb7tFb6dFb5dFb59F7wqP3jEfvOY/eCx69nR69PQF6bwToHQzQOxyg94FH77hH72SA3ukAvS8D9OIevYRH7wePXiZA7+cAvV8D9P7k0at49P7u0fuHR++CR6/r0e15tHsezZ7H33selZ7Hn3oehV6AXC9Athfgx16AMz2PwZ5HrOfxec/jVM/j457H0Z7Huz2Pgz2PN3oee3oer/Q8Xux5PNfzeKbnsbHnsa7nsbrnsbLnsbzncW8vwOKex4Kex7yex9yex6yex/Sex9Sex4Sex5U9jzE9D9sL4PUCeL0QXk/xemPxeuPxepPxer/D612H15uD17sZrzcfr7cQr7cEr3c/Xu9BvN6jeL21BHpP4PU24fW24PW24/VeJtB7Fa+3D6/3Fl7vMF7vfbzeR3i9k3i9CF7vS7xeHK/3HV4vhdfL4PX+gNf7Fa9XxOv9Ba/3N7zePwj0LhDodfD++7/x2j28/+jhne/h/bVHoNwj8FsP75ceXraHl+4RSPYIDPXwvu7hfd7D+6yHd6KH92EP70iPwKEegf09vNd6BF7pEXixh/dcD29zj8CTPQKP9wis7uGt7OE90MNb2sO7q4e3oEdgXo/AjT282T28GT0CU3sEJvTwrurhXdbD83t4Xo9QoIcEelzu9Rjn9Zgc6PG7QI9rvR43eD1u9nrM93os9Hrc7fW4z+uxwuvxaKDH2kCPDV6Pp7weW7we270eL3s9dns99no9Dng9Dns93vd6HPN6fBLoEQn0+NLr8Y3X4zuvx/dej4zX4+dAj18DPYpej3Nej795Pf7h9bjg9eh4Pf7b+2963n/S8/5Jz6vT86r0vDI97zd63i/0vLP0vDQ9L0nPG6LnfU3P+4Ke9xk97wQ970N63hF63tv0Avvpea/T83bR83bQ856n522m5z1Jz3ucnreanvcwPe8Bet5Set5d9AJ30AvcSs+7kZ43m543g543lV5gIr3AVfS8y+h5jp4XvHjJv66++mI3FKB7qUf3Co/u1R7dyR7dazy613l053h0b/HozvfoLvTo3uPRXebRfdCj+6hHd61Hd4NH92mP7laP7naP7sse3d0e3b0e3bc8uu94dN/36H4UpPupRzfi0f3Co/uNR3fYo5vy6P4UpHs2SDfv0S15dP/i0a15dOse3ZZHt+PR6Xpc6Hr8o+vxt67HX7oepa7Hr12Pn7sema5Hqusx3PWIdz2+6nqc7nqc7Hoc73oc7Xoc7nq81fXY1/V4tevxUtfjha7H1q7Hpq7HE12PdV2PVV2Ph7oey7oe93Q9Fnc9bu963NL1mNv1uK7rMb3rMaXrcXXX44qux6VdD7/r4XU9vK7F6ypedyxedzxedzJe9xq87nV43Tl43ZvxuvPxugvxukvwuvfjdVfgdR/B6z6G192A130Kr7sFr7sdr/sSwe5uvO5evO4BvO47eN338brH8Lon8LphvG4Ur/sNXvc7vG4Kr/sTXvcsXjeP1/0Nr3sOr1vD69bxuv/E67bxOv+F1+ri/aOL97cu3l+6eH/q4hW6eD938TJdvFSX4HAX79su3lddvEgX79Mu3vEu3tEu3uEu3sEu3r4u3p4u3s4u3otdvOe6eJu6eBu7eOu6eKu6eA918ZZ38e7t4i3u4i3o4s3r4t3YxZvVxZvexZvaxZvQxbuyizemi+e6eMEuIa+LeF0u97qM87pM9rr8zusy0+tyg9flJq/LfK/LQq/L3V6X+7wuK7wuj3hdHvO6rPe6POV1edbrst3r8rLXZbfX5XWvywGvy9tel/e9Lse8Lp94XT7zukS9Lt94XYa8Lkmvy09el7Nel1+8Lr95Xcpel6rXpe51+Q+vy396Xbref9H1LtD1/kHX+xtdr0LX+xNdr0DX+5mul6Hrpeh6Z+h639L1vqTrnabrnaTrHafrHaXrvUvXO0g3+AZdbw9dbydd70W63nN0vc10vY10vXV0vVV0vZV0veV0vXvpeovpBhfQ9W6l691I15tF15tB15tK15tI17uKrjeGrufoesGLl/z3hAkXO9ajMyZI58ognQlBOlOCdKYH6cwK0pkbpDMvSOf2IJ1FQTr3BOksC9J5KEjn0SCdtUE6G4J0ng7S2RKksz1I56Ugnd1BOnuDdA4E6bwdpPNekM6xIJ1Pg3TCQTpfBOl8E6QzFKSTDNJJB+lkg3R+CdL5LUinHKTz1yCdvwfp/EeQTjtIuxPkn50g9U6QaidIuRPkt06QXzpBznaCpDtBkp0gQ50gX3eCRDtBwp0gn3SCHOsEeb8T5HAnyFudIHs7QXZ3grzcCfJCJ8jWTpBNnSBPdIKs6wRZ1QnyUCfIsk6QezpBFneCLOgEmdcJMrcTZHYnyIxOkKmdIBM6Qa7sBLmsE8R1gphOkGDHJ9gZQ7BzBcHO1QQ7kwl2phPszCLYmUOwczPBznyCnUUEO0sIdu4n2FlBsPMIwc5jBDsbCHaeJth5lmBnG8HOSwQ7uwl2XifYOUCw8zbBznsEO8cIdk4Q7IQJdj4n2IkR7AwR7CQJdn4k2Pk9wU6OYOePBDtlgp2/Euz8nWCnSbBzgWD7Pwn+s0Ow3iFY7RAsdwgWOwTzHYJnOwTTHYLfdwh+1yH4TYdgtEMw0iF4skPwow7BDzoED3cIvtUh+EaH4J4OwZ0dgi90CG7tEHymQ3Bjh+C6DsFVHYIPdQgu7xC8t0Pwrg7BBR2C8zoE53YIzu4QnNkhOLVDcGKH4LgOwcs7BKVD0HTwgx0uDXYYG+wwPthhcrDDNcEO1wU73BDscHOww/xgh4XBDncHO9wX7LAi2OGRYIfHgh3WBzs8FezwbLDDtmCHHcEOu4IdXg922B/s8Haww5Fgh6PBDh8HO3wW7PB5sMNXwQ7fBjucCXb4Mdjh98EOPwc7FIId/hzsUAl2+Huwwz+CHf5HsEMn+J90gv+kE6zTCVbpBM/RCZboBPN0gmfpBH+iE0zRCQ7TCX5DJ/glneBpOsGTdILH6QQ/oBM8TCd4kE7wDTrBPXSCO+kEX6ATfI5O8Bk6wY10guvoBFfRCa6kE1xOJ3gvneBddIJ30AneSid4I53gbDrBmXSC0+gEJ9EJjqMTvJxOUOgEzcVL/mvixIttG6Q9xtC+0tCeYGhPMbSnG9qzDO25hvY8Q/t2Q3uRob3E0L7f0H7Q0H7E0H7M0N5gaD9taG8xtLcZ2i8Z2rsM7dcN7f2G9iFD+4ihfdTQ/tjQPmVof25of2Vof2toJwztHwztjKH9s6FdMLRLhvZfDO2/Gdr/MLT/aWi1Df9oG/7eNlTahj+1DYW2Idc2ZNuGH9uGM23DYNsQaxu+aBs+axtOtA0ftg3vtQ3vtA0H2oa9bcPutuHltmF727ClbXi6bdjQNqxtGx5tGx5qG5a1Dfe0DYvbhgVtw7y2YW7bMKttmNE2TG0bJrQNV7UNl7UNfttg2gbT9jHtSzHtKzDtqzHtyZj2NZj2dZj2HEz7Zkx7Pqa9ENO+G9NeimmvwLQfwbTXYNrrMe2nMO3NmPbzmPYOTHsXpv06pv0mpn0I0z6CaR/FtD/GtE9i2p9j2l9h2t9i2glMO4VpZzDtnzHtAqZdwrTPYdo1TLuBaf8Tc+F/YJptzPk25q9tzJ/bmN/amF/amGwb82Mbk2xjhtqYr9uYL9qYcBvzaRvzURvzXhvzThtzoI3Z18a82sa83MZsb2O2tDGb2piNbczaNubRNuahNmZ5G3NvG7O4jVnQxsxrY+a2MbPbmBltzNQ2ZkIbc1Ubc3kbI22MaWNNGzVtxpo2402byabNNabNtabNDabNTabNfNPmTtPmLtNmqWnzgGnzsGmzxrR53LR50rTZbNo8b9rsMG1eMW1eM23eMG0OmjbvmjYfmDbHTZuTps1p0+ZL0yZu2gybNinT5ifT5g+mza+mTdG0OWfa1EybhmnzT9Pmf5j/Qds0aZvztE2VtinTNr/RNr/QNlna5kfaJknbfEfbfE3bfEHbRGibT2mbj2ib92mbd2ibt2ibfbTNHtrmZdpmO22zlbbZRNtspG3W0TaP0jYraZvltM29tM1dtM0dtM2ttM2NtM1s2mYmbTONtplE24yjbS6nbYS2MRcv+c+JEy9esEFaYwytKw0XJhguTDG0rjG0ZhlacwwXbjFcmG+4sNBwYYmhdZ+htcLQetjQWmNorTe0njK0njW0nje0XjS0XjFceM3Q2m9oHTS03jVc+MDQOm5onTK0ThtaXxpaccOFYcOFlOHCT4YLZw0X8oYLRUPrL4ZW1dCqGy78h+GfLUOjZai1DH9pGUoXDL9eMPzhgiFzwZC6YEhcMHzbMnzZMnzeMpxqGT5uGY62DEdahkMtw/6W4fULhl0tw4sXDNsuGJ69YHiqZVh/wfBYy/BIy/Bgy3D/BcOSlmHRBcPtFwy3tAxzW4bZLcP0lmFqyzChZbiyZRjTMriWwbQMpuVjWpdiWmMxrasxrSmY1jWYC9dhWnMwrZsxrfmY1kLMhbsxraWY1gpM62FMaw2mtR5z4UnMhc2YC89jWi9iWq9gLryGufAG5sJBzIV3MRc+wLQ+wrROYlqnMa0vMa04pjWMaX2Paf2EufAHTOsXTOs3TKuMaVUxrTqm1cS0WphGC1NrYf7SwpRamEIL83MLk2lhUi1MooUZvICJXcCcbmFOtTAftzAftjDvtTCHLmD2tzB7W5jdLcxLLcy2FubZFuapFmbDBczaC5hHWpgHW5hlFzD3tDCLW5jbL2DmXcDc2MLMvoCZcQEz9QJmwgXMVS3MZS2MtDCmhTUt1LQYa1qMNy2mmBbXmAtcay5wg2lxk2kx37S401zgLnOBpabFA6bFw6bFGtPicdPiSdPiGdPiedPiRdNip2mxx7R4w7Q4aC5w2FzgfdPiI9PiU9MiYi4QNS2+MS2+My2SpkXaXOCsafGLafGbaVE2LaqmxXlzgaZp0TItLph/0DJ/o2UqtMyfaJkCLfMzLZPhgvmBljlDywzSMjFa5nMumM9omRO0zIe0zHtcMG/TMgdomb1cMLtpmZdomW20zBZa5mlaZgMts5YL5lEumIdomWW0zD20zGJaZgEtcystcyMtM5uWmcEFM5ULZiItcxUXzGVcMELLhC5e0p408WLLD9K6zNC6ytCaaGhNMbSm/zuAuYbWPEPrdkNroaG1xNC639B60NB65P89gNZrhtab/yeA1v8TwElDK/J/AmgNG1rfG1ppQ+usofWLofWbofVnQ+uvhtZ5Q6tp+I9/GuotQ7VlKLcMxZYh3zKcbRl+ahm+bxmGW4b4vwM43TKcbBmO/zuAd/8dwJstw2stwystw46WYVvL8Gzr3wG0/lcAj/4/AbT+HUDLsKBlmPfvAGa1DDNahmktw8SW4aqW4bKWQVqGUMtgWg7TugzTuvL/CmA6pjXr/wrg9v8VQGsJpnXfvwN45P8E0HoK09qMaf1fAbRew7TewLQOYlqHMa0PMK3jmNanmFYE04piWt9gWt9hWklMK41pnf13AH/EtP6Maf0V0zr/vwL45z8x9Ram2sKc+3cAv7Ywf2hhfvq/Aoi3MLHW/wngxL8DONLCHGr9fw/g6RbmiRZmbQvz6L8DuL/1fwXQwsxrYea2MLNbmBktzLQWZmILM66Fufz/BOBMizGmxRWmxdX/DmC6aXGdaTHHtLj53wEsNC3uNi3u+/8jgH2mxUHT4rBp/e8APjEtwqZF1LT42rQYMi3OmBZp0yJrWuRMiz+aFn82Lf5qWvzdtPiHafFP809apk7L1GiZv9AyJVrm1/8dQMv8QMskaJlv/3cALXOKlvn43wEcoWUO/e8AWv8vAbTMo7T+fwYwk5aZRstMomXG0TJjaRn9XwFcmDTxYtMFaV5uaF5laE4M0Zwaojk9RHNWiOZcQ3OeoTnf0FxoaC4xNO8P0XzQ0HzY0FxjaK4P0XzS0NxkaD5naL5oaO40NPcYmnsNzQOG5juG5vuG5keG5qchmp+FaH5uaMYMzUFDM2Fo/mBoZkI0fw7R/NXQLBmafzE0ayGaDUOjafhb01BpGv7UNBSahp+bht83Q/zYDJFsGoaaIb5uGr5oGsJNw6dNw/Gm4YNmiHeaIQ42Q7zRNLzWDPFK07CjaXi+aXi2aXiqaVjfNDzWNDzSNDzYNNzfDLGkGWJR03B70zCvaZjbNMxuhpjRNExrGiY2DeOaIcY2DdI0hJqGUNNhmmMwzSsxzasxzSmY5nRM8zpCzRswzZsxzfmY5kJM825M8z5M8wFMcyWmuRrTXIdpPoFpbsI0txBqbifUfJlQ81VMcy+muR/TfAfTfJ9Q8xih5seEmqcINb8g1PyKUHOQUDNBqJki1MwQav5MqPkroWaRUPMcplnDNOuYRhPz9yam0iT0pyahPzYJ/dIklG0S+rGJSTYx3zUx3zQx0SYm0sR82sR81MR80MS828QcbGLebBJ6vYnZ1cTsaGKeb2KebRJ6uklofZPQY01CjzYJPdgkdH8Tc08Ts7hJ6PYmoXlNzNwmZnYTM7NJaFoTM7GJGdckdHmTkDYJhZo402SMaXJFqMnVoSZTTJNrTJNrTZMbTJObTJP5psmdpsldoSZLQ00eCDVZGWqyKtRkbajJE6Emz4SabDFNtpsmL5smu02TvabJftPkHdPkPdPkQ9PkhGnyWajJ56bJV6bJt6ZJwjT5wTTJmCZ/CDX5NdSkGGpyzjSpmibnTZOm+QdN83eapkLT/Jmm+Y1m6BeaoSxNk6ZpvqcZ+o6m+Yam+ZKmidAMnaQZ+pimOUrTvEvTHKRp3qRpXqcZ2kXTvETTbKNpttA0T9M062max2iaR2mGHqIZWkbT3EPTLKZpFtAM3UrT3EgzNJtmaCbN0DSaock0zXiaZixNozRN6OIl/zFp0sWGMzQuD9G4KkRjYojGlBCN6SEas0I05oZo3BKiMT9EY2GIxt0hGveFaDwQorEyRGN1iMa6EI2NIRrPhGhsCdHYHqLxUojG7hCNvSEa+0M0DoVoHAnR+DBE40SIxskQjdMhGl+GaMRDNIZDNL4P0UiHaJwN0ciHaBRDNP4covHXEI3zIc43QlQbIcqNEMVGiHwjxNlGiJ8aIVKNEIlGiG8bIb5qhDjdCHGqEeJEI8SxRoj3GiEONUIcaITY2wjxaiPEy40QLzRCbG2EeKYR4slGiMcbIdY0LA83LA80QtzXCLGkEWJRI8TtjRC3NELMbYSY1QgxoxFiaiPExEaIqxohLm+E0EaIUCNEqCGEGpcRalxFqDGBUGMKocZ0Qo1ZhBpzCDVuJtSYT6hxJ6HG3YQaSwk1lhNqrCTUWEWosY5QYyOhxiZCjS2EGtsJNV4m1NhNqPE6ocZ+Qo1DhBpHCDWOEmp8TKhxklDjNKFGlFAjTqjxHaHG94QaaUKNs4QavxBq/JFQ48+EGn8l1Pg7ofMNQrUGoXMNQsUGoV8bhH5uEMo0CKUahBINQoMNQrEGoc8b2M8ahE40CH3YIPReg9A7DUIHGoT2NQjtaRDa2SD0YoPQcw1CzzQIbWwQerxB6LEGoYcbhFY0CN3fILSkQWhRg9DtDULzGoRubBCa3SA0s0FoWoPQxAahcQ1CYxuEtEHINnChBmNCDa4MNbg61GBKqMH0UIPrQg1uCDW4OdRgfqjBnaEGd4UaLA01WB5qsDLUYFWowdpQgydCDZ4ONdhiG2wLNdgRarDLNnjNNngz1OBQqMG7oQZHQw2OhxqcDDWIhBpEQw2+CTX4LtTg+1CDdKhBNtTgl1CDP4Ya/DnUoBJq8LdQg3roPI1QjUboHI1QiUaoQCP0M41QhkboBxqhMzRCgzRCMRqhL2iEPqMR+oRG6CMaofdphN6hETpAI7SPRmgPjdBOGqEXaYSeoxHaTCP0JI3QehqhNTRCj9AIPUgjdD+N0BIaoUU0QgtohG6lEbqRRmg2jdBMGqFpNEKTaITG0wiNpRG6lEbIXrykOXnyxfMSon55iPpVlvpEy/mplvp0S32WpT7XUp9nqc+31Bdazt9tOX+fpb7CUl9pqa+21NdZ6hst9Wcs9a2W89st51+y1Hdb6nst9f2W+iFL/YilftRS/9hSP2mpRyz1Ly31b0LUhyz1pKWettSzlvovlvofLfU/WeoVS71m+Vs9RKUe4s/1EH+sW345b8nWLem65fu65bu6JV63fFm3nK5bTp23fHze8uH5EEfqlrfrlgPnLXvPW3bXLS/VLdvrlufqlmfqlo11y7q6ZXXd8nDdsuK85b7zliV1y6K65fa6ZV7dMrdumV23zKhbptUtE+uWcXXL2POWS+sWW7fYumDrl2PrV2HrE7D1qdj6dGx9FrY+B1u/GVufj60vxNbvJlRfSqi+nFB9Jba+Gltfh61vxNY3YetbsPXt2PpL2PpubP11bP1NbP0Qtv4utv4Btn4cW/8UW49g61Fs/WtsfQh7/gz2/I/Y81ls/RdC9T8Sqv+JUL2CrdewfzuP/WsdWz6P/e089pc6oWyd0E91Qt+fxw6fx35bx35Vx56uY0/VsSfq2GN17HvnsW/XsQfq2L117Kt17M469oU69rk69pk69sk6dn0du6aOfbiOXVHH3lfHLqljF53H3l7HzqsTmlsnNLtOaGYdO62OnVTHjj+PHVvHXlrH2jpi61xm61xlz3O1rTPFnme6Pc91ts4cW+dmW2e+rXOnrXOXrbM0VGd5qM7KUJ1Vts7a0HmesHWetnW22DrbbJ0dts4uW+c1W+dNW+egrXPY1vnA1jluz/OprRO2db6wdb62dYZsnTO2zo+2zu9tnZ/teQq2zp9snb/YOrVQnfP279TtX6nbMnX7G3Wbpx76A/XQT9RD31O3Cer2W+qhr6iHPqduT1G3n1C3x6jb96jbd6jbA9TtPup2D3W7k/P2Rer2Oep2M3X7FHW7nrp9jHroEeqhBzlv7+e8XULdLqZuF1C3t1K3N3LezqZuZ1K306jbydRD46nbsdTtpdStvXhJffLkizUJURtrqV1lqU201KZaatMttVmW2lxLbZ6lNt9Su9NSu9tSW2qpPWCprbTUVllq6yy1Jyy1TZbaFkttm6W2w1LbZam9bqm9Yam9ZakdttTet9Q+stQ+sdQ+s9S+sNRiltqgpZaw1FKW2k+W2h8stV8ttaKlVrbU/mqp1iznapZizfJrzfJzzZKpWVI1y5maZahm+aZmidYskZrlZM1yvGY5WrO8W7Mcqln21yyv1yy7a5aXapbtNcvWmmVTzbKxZllXs6yuWR6uWR6oWe6rWe6uWRbWLLfXLPNqlrk1y6yaZUbNMq1mmVSzjKtZxtYsl9YstmaxNcHWLsPWrsLWJmBrU7C16djaddjaDdjazdjabdjandjaXdjavdjacmztIWxtFba2FlvbgK09ja09i609j629iK3txNb2YGtvYGtvYWuHsbX3sbVj2Non2Npn2NoX2FoMW/sWWxvG1r7H1tLY2h+wtTy2VsTWythaBVurYf9Sw5Zq2EIN+3MN+/sa9sca9kwNO1TDflPDflnDRmrYkzXs8Rr2gxr23Rr2UA27v4Z9vYbdXcO+XMNur2G31rDP1LBP1rCP17BratiHa9gVNez9NeySGnZRDXt7DTuvhp1bw86uYWfWsNNq2Ek17PgadmwNO6aGtTWcrTHG1rjS1rja1phia1xja1xra9xga9xka9xma9xhayy2Ne61NZbZGg/ZGo/aGo/ZGhtsjadsjWdtjedsjRdsjZ22xh5b4w1b4y1b4x1b431b40Nb44StccrW+NzWiNkacVtj2Nb43tZI2xpnbY1fbI3fbI0/2xoVW6Nma9RshZr9EzX7R2r2F2o2S82mqdkkNfsdNfsNNRulZk9Tsyep2Y+p2Q+p2SPU7NvU7H5qdi81+yo1+zI1+wI1u5WafYaafZKafZyaXUPNPkzNrqBm76dml1Czi6jZ26nZW6nZG6nZ2dTsTGp2GjU7mZodT82OpWbHULP+xUvOT558sSohqpdZqldZqhMt1SmW6nRL9TpLdY6leoulepuleqelepelutRSXW6pPmSprrJU11qqT1iqT1uqmy3V5y3VFy3VVyzVPZbqPkv1LUv1HUv1fUv1mKV6wlL9zFL93FKNWarfWqrfWarfW6ppS/WspfqLpfpHS/XPlmrFUqlaylXLb1VLvmo5W7X8VLWkqpbhquXbqiVWtXxetXxWtXxStRyrWt6vWt6pWt6qWt6oWvZULa9ULTuqlm1Vy7NVy9NVy4aqZW3VsqpqeahqWV61LK1a7qpa7qxa5lctt1Qtc6qWWVXLjKplatUysWoZV7VcXrVo1WKrFlsVbPUybPUqbHUCtjoFW70GW70OW70BW70JW70NW70DW12Mrd6DrS7DVh/EVh/FVh/DVtdjq09hq5ux1eex1Rex1Z3Y6qvY6j5s9QC2+g62+h62+iG2egJbPYWtnsZWv8RW49jqd9jq99hqGlvNYqu/YKt/xFb/hK3+BfvXv2LLVexvVWy+ij1bxf5Uxf5QxSaq2G+r2FgV+0UVG65iP6liP6piP6hiD1exB6vYN6rY16rYV6rYHVXstip2SxW7qYrdWMWuq2JXV7Erq9gHqtj7qti7q9iFVez8KvaWKnZOFTurip1RxU6tYidWseOq2Mur2EurWFvF2SpjbJUrbZWrbZUptso1tsq1tsoNtspNtspttsodtspiW+UeW2WZrfKQrfKorfKYrbLeVnnSVtlsqzxnq7xgq+y0VV61VfbZKgdslbdtlfdslaO2yglb5ZStctpW+dJW+cZW+c5WSdoqP9oqWVslZ6v80Vb5k63yF1vlr7ZC1f6Zqi1Stb9StT9TtRmqNkXVJqjaQar2a6r2C6o2TNV+StUep2o/oGrfpWoPUrVvUrWvU7W7qNqXqNrtVO0WqnYTVbuRql1H1a6maldStQ9QtfdRtUuo2oVU7e1U7Tyqdi5VO5uqnUHVTqNqJ1G146jasVTtpVStf/GSv02efLEiISpjLZVxlsokS2WqpTLdUpllqcyxVG6xVG6zVO60VO62VO61VJZbKg9ZKo9aKo9ZKhsslactlc2WyvOWyouWyk5L5VVLZa+lcsBSecdSOWKpfGipfGypnLRUIpbKl5ZK3FIZslTOWCo/WCq/t1RylsqvlkrRUjlnOVexlCqWQsWSq1h+X7H8WLEkK5bvKpZ4xfJlxRKpWE5VLCcqlg8rlvcqlncqlrcqln0Vy56K5ZWK5cWK5fmK5dmK5emKZUPFsrZiWVWxrKxYllcsSyuWuyuWhRXL/IrlloplbsUyq2KZUbFMq1gmVSzjK5YrKpZLKxa/YrEVxVYux1bGYSsTsZWp2MoMbGUWtjIHW7kZW7kNW7kTW7kLW7kXW1mGrTyIrTyKrTyGrazHVp7CVjZjK89hKy9gKzuxlVexlb3YygFs5W1s5Qi2chRb+RhbOYmthLGVL7GVb7CVIWwlga2ksJXfYys/Yyu/YitFbKWM/UsF+6cKtlDB/lLBnq1gf6xgkxXsdxVsvIL9qoL9vIL9rII9UcEeq2Dfq2DfqWDfqmDfqGBfq2B3VbA7KthtFeyzFezTFeyGCnZtBbu6gl1ZwT5QwS6tYO+uYBdWsPMr2HkV7I0V7OwKdmYFO62CnVzBXl3BXlHBXlrB+hXEVrjMVrjKVphgK0y1FabbCtfZCjfYCjfZCrfZCnfYCotthXtthftthYdshUdshTW2wnpb4UlbYbOt8Jyt8IKtsNNW2G0r7LUV9tsKh2yFI7bCB7bCR7bCp7ZC2FaI2gpf2wqDtkLCVkjZChlb4Q+2wq+2wm+2wp9thb/Yv1CxJSq2QMX+QsWepWLTVOz3VOx3VGyciv2Kiv2civ2Mij1BxR6jYt+nYg9TsW9RsW9Qsa9Rsbuo2B1U7DYq9lkqdhMVu5GKXUfFrqZiV1KxD1Cx91Gxd1OxC6nY26nYeVTsjVTsbCp2JhU7jYqdTMVeTcVeQcWOoWL9i5dUp0y5WBZLeaxPeZxPeaJPearPuRk+5et8ynN8yjf7nLvN59wdPuW7fMr3+pSX+ZQf9Ck/4lNe41Pe4FN+0qf8jE95q095u8+5l3zKu3zKr/mce9Pn3CGf8mGf8gc+5Y98yp/4lD/zKX/hU/7a59y3PueGfc5973PuJ59zf/Ap/+JT/qNP+U8+fy77/Fb2yZd9/lD2+emcT+qcT+Kcz2DZ5+uyzxdln3DZ59Oyz/Gyzwdln3fP+Rw657P/nM/ecz6vnvPZWfZ58ZzPc2WfzWWfp8o+G8o+j53zefScz0Nln2Vln6Vln7vP+dxZ9plf9rm57DOn7DPrnM+Mss+0ss/Ess/4ss/Ycz6Xln38so9/TvHPXY5fvgq/PAG/PAW/PB3/3HX4527AL9+Ef+5W/HN34JcX45fvwS/fj19egV9+BL+8Br/8OH55I355E355K355O/65l/DPvYJ/7jX88pv45YP45w7jn3sf/9xH+Oc+wT93Cr/8BX45hn/uW/xz3+GXk/jln/DLZ/HLv+CX/4hf/hN+uYxfLOPnz+H/fA7/92X81Dn8RBl/sIz/dRk/WsaPlPFPlvGPl/GPnsM/cg7/7TL+/jL+3jL+q2X8nWX8F8v4z5XxN5fxnyrjbyjjP1bGX1XGf6iMv/wc/tJz+HeX8e8s488v499Sxp9bxp99Dn9mGX9aGX9SGX98Gf+KMv6YMr5fRvxzXOaf4yq/zNV+mSl+mWv8Mtf6ZW7wy9zol7nVL7PAL7PIL7PEL3OfX2aFX+Zhv8xqv8w6v8yT/jk2+efY4pfZ5pd5yT/HK/45XvPP8YZf5i2/zGG/zHt+mWP+OU745zjln+Nzv8xXfpm4X+Y7v0zSP0faL5P1y/zilyn4ZUp+mbJfpuwXOefnOef/TNn/PWX/B8r+Gcr+IGX/a8p+lLJ/mrJ/knP+cc75Ryn7Ryj7b3PO3885fy9l/1XK/iuU/R2U/ecp+5sp+09R9p+g7K/jnP8o5/yHKPsPUPaXUvbvpuzfyTl/Puf8eZzz51L2Z1P2Z3LOn8Y5fzLn/PGU/Sso+2Mo++7iJX+ZMuViSS2ly31K43xKE31KU3xK031Ks3xKN/iUbvYp3epTusOntNindI9PaZlPaYVP6WGf0hqf0jqf0kaf0iaf0haf0naf0g6f0i6f0h6f0j6f0ls+pXd8Su/5lD70KX3sUzrlU4r4lL70KX3jUxryKZ3xKf3gU8r4lP7gU/rVp/SbT7Hk82vJ5+eST6bk82PJ50zJZ6jkEy/5fFnyOV3yOVXyOVHyOVbyea/kc7jk81bJ542Sz2sln10ln5dKPttKPltKPs+UfDaWfB4v+awp+TxS8nmw5LOs5HNvyWdxyeeOks9tJZ+bSz5zSj6zSj4zSj5TSz4TSz7jSj5jSz6Xlnz8ko9fUvzS5filq/BLE/BLU/BL0/FL1+GXbsAv3YRfuhW/tAC/tAi/tAS/dD9+aQV+6WH80mr80jr80hP4pU34pS34pW34pR34pZ34pT34pX34pbfwS+/gl47glz7EL32MXzqJX4rgl6L4pa/xS4P4pTP4pR/wSz/hl/6AX/oVv/QbfrGEXyjh/1zC/30J/8cSfrKE/10JP17C/6qEf7qEf6qEf6KEf6yE/34J/3AJ/2AJ/80S/msl/N0l/JdL+NtL+FtL+M+U8J8s4a8v4a8p4T9awn+whL+8hL+0hH9XCf/OEv5tJfxbSvhzS/izSvgzS/jTSviTSvjjS/hjS/hjSvh+CfVLXO6XuMovMcEvMcUvcY1f4lq/xA1+iZv8Erf6JRb4JRb5JZb4Je7zS6zwS6z0S6zyS6zzS2zwSzztl3jWL/G8X2KHX2KnX2KPX2KfX+KAX+Jtv8QRv8RRv8Rxv8SnfomwXyLql/jaLzHol0j4JVJ+iZ/8Emf9Er/4Jf7olyj5JUr+Hyn5P1Pyf0/J/5GSn6TkD1Py45T8GCX/c0r+KUr+J5T8Y5T8Dyj571LyD1Ly36Tkv0bJ303Jf5mSv52S/xwl/xlK/pOU/PWU/DWU/Ecp+Q9S8pdT8pdS8u+i5N9Jyb+Nkn8LJX8uJX8WJX8mJX8aJX8SJX88Jf8KSv4YSr67eMm5qVMvFtWneIWjOM5RnOQoTnUUpzuK1zmKcxzFWxzF2xzFOxzFuxzFexzFZY7iCkfxYUdxjaO4zlHc6Cg+7Sg+6yg+7yjucBR3OYp7HMV9juJbjuLbjuIRR/Goo3jcUTzpKIYdxS8cxZij+K2jmHAUU47iT47iWUfxF0fxj44/Fh2/FB1/KDoyRccPRUei6Pi26Pi66IgWHZGi42TR8XHR8WHR8V7R8U7RcaDo2Fd0vFZ07Co6Xio6thUdW4qOTUXHxqLj8aJjTdHxSNHxYNGxrOi4t+i4q+i4s+iYX3TcUnTMKTpmFR0zio5pRcekomN80XFF0TGm6HBFhyteiiuOxRXH4YoTccWpuOJ0XPE6XPEGXPEmXPE2XPEOXHExrngPrng/rrgCV3wYV1yNK67DFZ/AFZ/GFZ/FFZ/HFXfgiq/gintwxb244gFc8W1c8V1c8SiueBxX/BRXDOOKn+OKMVzxW1wxgSumcMU0rngWV/wFVyzgfivifini/lDEZYq4VBF3pogbLOK+LuKiRVykiDtZxH1cxH1YxL1fxL1TxL1VxL1ZxO0p4l4p4l4q4rYXcVuLuE1F3JNF3Poibk0R92gR92ARt7yIW1rE3VXELSzi5hdxtxRxc4q4WUXczCJuWhE3uYi7uoi7ooi7rIhzRdQVudwVGeeKTHBFproi17gi17oiN7giN7kit7kiC1yRRa7IPa7Ifa7ICldkpSuyyhVZ54pscEWedkU2uyLPuSIvuiI7XZE9rsheV2S/K/K2K/KuK3LUFfnIFfnEFQm7Ip+7IjFX5FtXZNgVSbkiaVfkrCuSc0UKrkjR/UbR/UrR/YGiy1B0P1B0Zyi6IYrua4ruS4ruNEV3kqI7QdF9SNG9T9EdpujeoujeoOheo+h2UXQvUXTbKbqtFN0miu5Jim49RbeGonuUonuQoltO0S2l6O6i6BZSdPMpunkU3VyKbhZFN5Oim0bRTaborqborqDoLqPo3MVL/jx16sWC+hSucBTGOQqTHIWpjsJ0R2GWo3CDo3Czo3Cro3CHo7DIUVjiKNzvKDzgKDzsKKx2FNY5Ck84Ck87Cs86Cs85Ci86Cjsdhd2Owl5H4U1H4ZCjcNhReN9ROOYonHAUPnMUTjsKXzoK3zgKQ45C0lH4wVHIOAo/Owp5R77g+Lng+H3B8WPBkSw4hgqOeMERKzi+KDjCBcenBcfxguNowXGk4Hi74DhQcOwrOF4tOF4pOHYUHNsKji0Fx6aC44mC4/GCY3XB8XDBsaLguL/guKfgWFxw3FFw3FZw3FxwzCk4ZhUcMwqOaQXHpIJjfMFxRcExpuBwBYcrXIorXI4rXIUrTMAVpuIK03GF63CFG3CFm3CFW3GFBbjCIlxhCa6wFFdYjiusxBVW4QprcYUNuMJTuMJmXOE5XOEFXOElXGE3rvA6rvAmrnAQVziMK7yHKxzDFU7gCidxhdO4QhRX+AZXGMIVzuAKP+IKP+EKP+MKeVyhgMsVcNkCLl3AfV/ADRdw3xZwsQLuiwIuXMB9WsAdL+COFnBHCri3C7gDBdwbBdyeAm5nAfdiAfd8AbelgNtUwD1RwD1ewK0p4B4p4FYUcMsKuHsLuMUF3J0F3G0F3C0F3NwCblYBN7OAm1bATS7gri7grijgLivgXAF1BS53Ba5yBSa4AlNdgWtcgWtdgetdgZtcgXmuwO2uwEJXYIkrcJ8r8IArsNIVWOUKrHUF1rsCT7kCz7gCW12B7a7Ay67AblfgdVfgTVfgLVfgsCvwnitwzBU44QqcdAVOuwJRV+BrV2DQFUi4Aj+4Aj+5An9wBX5xBX51BQruFwouS8H9RMGlKLhhCm6QgotRcFEKLkLBfUrBfUzBHaXg3qPg3qHg3qLg9lJweyi4XRTcSxTcNgpuCwW3iYJ7koJ7nIJbQ8E9QsE9SMEto+DupeDuouDupOBuo+BuoeDmUnCzKLiZFNw0Cm4yBXc1BXcFBXcZBecuXlKcOvViXn3yVzjy4xz5iY78VEd+uiN/nSN/gyN/kyM/z5G/3ZFf6MgvceTvc+SXO/IPOfKrHPm1jvwGR/4pR36zI7/VkX/BkX/Zkd/lyL/myL/pyL/lyL/tyL/nyB915I878icd+bAj/4UjH3Pkv3Xkhx357x35tCOfdeRzjlzecTbv+CnvSOUdibxjMO/4Ou+I5h2RvONk3vFx3vFh3vFe3vFO3nEw73gz73g979idd7ycd7yQd2zNOzbnHU/lHRvyjnV5x6q8Y2Xe8UDecV/esSTvWJR3LMg7bs07bso7bsg7rss7pucdU/OOSXnH+LzjirxjTN7h8g6XvxSXvxyXvwqXn4jLT8Xlp+Py1+LyN+DyN+Hy83D5Bbj8Qlz+blx+KS6/HJd/CJd/FJdfi8tvwOWfxOU34/JbcfntuPwOXH4XLv8aLv8GLv8WLv82Lv8uLv8hLn8cl/8Ul4/g8l/g8l/h8t/i8sO4fBKXT+PyWVz+Z9wvedzZPO6nPC6VxyXyuME87us87ss8LpLHnczjTuRxH+Zx7+dx7+Rxb+Vxb+Rxr+dxu/K4l/O4F/K45/K4Z/K4p/O4DXnc2jxudR73cB63Io+7P4+7J49bnMfdkcfdlsfdnMfNyeNm5XEz8rhpedykPG58HndFHjcmj3N51OW53OW5yuWZ4PJMdXmucXmudXmud3nmujzzXJ7bXZ6FLs/dLs+9Ls9yl+dBl+cRl+cxl2e9y/Oky7PJ5dni8mxzeV5yeXa5PHtcnr0uzwGX522X512X56jLc9zl+cTlCbs8n7s8MZfnW5dn2OVJujxpl+f3Ls/PLk/e/ULe/YG8+4m8+4G8O0PeDZJ335B3UfLuNHl3irw7Qd4dI+/eI+/eJe8OkndvkHevk3e7yLuXybsXyLvnybtnybunybuN5N068m4VefcwefcAeXc/eXcPebeIvLuDvLuNvLuZvJtD3s0m72aQd9PIu0nk3Xjy7kry7jLyzl285I9Tp17MXerIXSHkxgm5SUJuqpCbLuRmCbkbhNxNQm6ekLtdyC0ScncLuaVCbrmQe0jIPSLk1gi59UJuo5DbJOSeFXLbhNwOIfeKkHtVyO0TcvuF3CEh966Q+0DIfSTkPhFynwm500LuSyH3jZAbEnJnhNwPQi4j5P4g/CEnZHLCjzkhmRO+ywnxnPBVTvg8J3yWEz7JCR/lhA9ywpGccCgnHMgJ+3LCnpzwSk7YkRO254QtOWFTTngyJ2zICY/lhEdzwkM54YGccF9OWJITFuWEBTnh1pxwU06YkxNm5YQZOWFaTpiUE67OCVfmhMtyguQEyY1BcmOR3DgkNxHJTUVy05HcdUjuBiR3E5Kbh+QWILmFSO5uJHcvkluG5B5Cco8iuTVIbj2S24jkNiG5Z5Hc80huB5LbieR2I7m9SG4/kjuI5A4juQ+Q3DEkdwLJfYbkTiO5KJL7BskNIbkEkvsByWWQ3Fnk5xzy+xzyYw5J5pDvckg8h3yVQ77IIZ/lkE9zyPEccjSHHMkhh3LIgRyyL4fsySGv5JAdOWR7DtmaQ57JIU/mkA055LEcsiqHrMwhD+SQ+3LIkhyyKIfckUNuyyE355A5OWRWDpmZQ6blkMk55OoccmUOuTyHSA6VHJdLjnGSY6LkmCI5rpEc10qO6yXHjZJjnuSYLznulBx3SY57JccyyfGg5HhEcqyRHOskx5OSY5PkeFZyPC85XpQcOyXHbsnxuuR4U3IclByHJcf7kuOY5DghOU5JjtOS40vJ8Y3kGJIcCcnxg+T4SXKclRw5+ZmcZMnJj+Tke3IyTE7i5CRGTj4nJ2Fy8ik5+ZicHCUnR8jJ2+TkADnZR05eJye7yMlL5GQ7OXmOnDxDTp4kJxvIyVpyspqcrCQnD5CT+8nJPeRkETm5g5zcRk5uISdzyMkscjKTnEwjJ5PJydXk5Epychk5kYuX/Dp16sXspY7slUJ2nJCdJGSnCmenC2dnCdnrheyNQnaekL1dyC4SsncJ2XuF7DIh+6CQfVjIrhayjwvZJ4Ts00J2s3D2OSH7opB9WcjuErKvC9k3hOxBIfuOkD0iZD8UsseF7EkhGxaynwvZr4RsXMh+J2STQvZH4WxGyJwVfswK32eF784K32aFr84Kn2eFSFY4eVb4OCt8mBXeywqHs8LBrLA/K7yeFXafFXZmhRezwrazwrNZ4emzwsazwrqssCYrPJIVHswKy88KS7PC3VlhUVZYkBVuzQo3ZYU5WWHWWWFGVpiWFSadFa7OCldmhcuygpwV5OwY5OxYJDsOyU5EslOQ7DVI9lokez2SvRHJ3oJk5yNn70SydyFn70XO3o9kVyBnH0ayq5HsOiT7BHL2aeTsZiT7HJLdjmRfRrKvINnXkOwbSPYAkn0HOfsukj2KZI8jZz9Bzn6GZE8j2a+Qs3Hk7BCSPYNkf0SyGeT3WSSdRb7PIsNZZDCLfJ1FolkkkkVOZpETWeTYWeS9LHI4ixzMIvuzyN4ssjuL7DyLvJhFtmWRLVlkUxbZmEXWZ5E1WeTRLPJQFll+FrkviyzJIouyyIIscmsWuSmL3JBFZmWRGVlkWhaZnEWuziJXZpHLziKSRSXL5ZJlnJxlgmSZIll+J1lmSpbrJctcyTJPstwmWe6QLHdJlnskyzLJ8oBkWSlZVkuWtXKWDZLlKcmyWbJslSzbJctLkuUVybJHsuyTLAcky9uS5V3J8oFkOS5ZPpEsYcnyuWT5UrLEJcuQZDkjZ/lBsvwkWbKSJSs/kZXvyUqCrAySla/JypdkJUJWTpGVE5yVj8jK+2TlXbJyiKzs56zs46y8ylnZyVnZwVnZxlnZSlY2cVY2clbWc1YeIyuPclZWkpXlZOU+srKErCwiK3eQlVvJys1kZQ5ZmUVWZnJWppGVyWTlarJyFVm5nKzIxUtyU6dezFzqyFwhZMYJmYlCZoqQuUbIXCdkZguZuULmFiEzX8gsFDKLhcw9QuZ+IbNCyKwUMquEzDohs0HIPCVknhEyW4XMdiGzQ8i8ImReEzL7hMwBIXNIyLwrZI4KmY+EzCdC5pSQOS1kvhIyXwuZQSGTEDIpIZMWfsoIqYxwJiMMZYSvM8JXGeHzjPBZRvgkIxzPCEczwpGM8HZGOJAR3sgIr2WEXRnhpYywPSM8lxE2Z4SnMsITGWFdRlidER7OCCsywrKMcG9GuCsjLMwIt2eEeRnhxoxwfUa4LiNMzwhTM8LEjDA+I1yREcZkBMkIkhmDZMYimXFIZiKSmYJkrkEy1yKZ2UhmLpK5BcnMRzJ3IpnFSOYeJHM/klmBZFYimVVIZi2S2YBknkIyzyCZrUhmO5LZgWReQTJ7kMw+JLMfyRxCMu8imQ+QzEdI5gSSOYVkTiOZKJL5Gsl8i2QSSCaFZNLITxnkhwxyJoMMZZBvMshXGeTzDPJZBvkkgxzPIEczyJEM8nYGOZBB3sggr2WQ3RnkpQzyYgZ5PoNsziBPZ5AnMsjjGWRNBnk4gzyYQZZlkHszyN0ZZGEGWZBB5mWQmzLIDRnkugwyI4NMzSCTMsj4DHJFBrksg0gGlQyXS4ZxkmGCZJgiGX4nGWZKhtmSYY5kuEUy3CYZ7pAMiyXDEslwv2R4QDKslAyrJMNjkmGDZNgoGTZJhq2SYZtk2CEZdkqGVyXDPsmwXzIckgyHJcP7kuEjyXBCMpySDBHJEJUMX0uGbyVDQjJ8LxnSkiEjP5GRH8nIGTLyHRmJk5GvyMgXZOQzMvIpGTlORo6SkffIyNtk5CAZeZOMvEZGdpORl8jIi2TkeTKymYw8TUaeICOPk5E1ZORhMvIQGVlGRpaSkbvJyEIysoCMzCMjN5GRG8jIdWRkBhmZSkYmkZHxZOQKMnI5GZGLl/w8derF9KWO9JVCepyQniSkpwrp6UL6WiF9vZC+SUjfIqTnC+mFQnqxkL5HSN8vpFcI6ZVCepWQXiuk1wvpJ4X0ZiG9VUhvF9I7hPQrQvpVIb1XSL8ppA8K6cNC+gMh/aGQ/lhInxTSESH9hZCOCelvhfR3QjoppH8QfkwLybQwnBa+TQtfp4VoWoikhVNp4URa+CgtfJAW3k0Lh9LC/rSwLy3sSQuvpIWX0sL2tLAlLWxOC0+lhQ1pYW1aWJ0WHk4LK9LC/WnhnrRwV1pYmBZuTwvz0sKNaeGGtHBdWpieFqalhUlpYXxauDItXJYWJC1IegySHoukxyHpiUh6KpKejqSvRdLXI+kbkfQtSHo+kr4DSS9G0vcg6fuR9AokvRJJr0LSa5H0eiT9JJJ+BklvQdLbkPQOJP0Kkn4VSe9F0m8i6YNI+h0k/T6S/hBJH0fSnyLpz5D0F0g6hqTjSPo7JJ1E0j8gP6aR79PIcBr5No18nUaiaeR0GjmVRk6kkY/SyAdp5EgaOZRG9qeRfWlkTxrZlUZeSiPb08hzaWRzGnk6jWxII2vTyOo08nAaeTCNLEsjS9PI3WlkYRpZkEbmpZGb0sgNaeS6NDIjjUxLI5PTyNVp5Mo0clkakTSXSpqxkmacpJkgaaZKmt9JmpmS5npJM1fS3CJpbpM0d0iaxZJmiaS5T9IslzQPSZpVkuYxSbNe0myUNJskzRZJ87yk2SFpXpY0uyXNXknzpqQ5KGnekTTvSZoPJc1xSXNS0nwmaT6XNDFJE5c030maM5LmB0mTljRpSZGWBGkZJC1fk5YoaTlNWk6Rlk9Iy0ek5QPScoS0vE1aDpCWN0jLa6RlF2l5ibS8QFqeIy2bScvTpOUJ0rKOtKwmLQ+TlgdJyzLSspS03E1aFpKWBaRlHmm5mbTMIS3XkZYZpGUaaZlMWq4mLVeRlstJi1y8JDtt2sXUGEfqSiU1TkhNElJThdR0IXWtkLpeSM0VUrcIqduE1AIhtVhJLRFS9wmp5ULqISH1qJB6TEk9rqQ2KqmnldRmJfWcktqupF5SUq8oqT1Cap+QOiCkDgmpI0rqfSF1TEidEFKnhFRESEWF1NdC6lshNaykksr3KWE4JQymhK9Typcp4XRKOJVSPkkpH6WEoynlSEp4OyW8lRLeSAmvp4RdKeHllPBiSng+JWxJKU+nhI0pZX1KeSylrEopK1PCAynh/pRwT0q4KyXckRLmp4R5KeHGlHBDSrguJUxPCdNSwqSUMD4lXJkSLksJkhIkNQZNjUVS45DURCQ1BUldg6ZmIqnrkdSNSOpmJHUbklqAphajqSVoaimaegBJPYSkHkVSjyGpx5HUE0jqKST1LJJ6DkltR1MvoalXkNQeJLUXSe1HUgeR1LtI6gMkdQxJfYykPkVSESQVRVIxJPUtkhpGUknk+xSSSCFDKfTrFPplCjmdQj5LIZ+k0I9SyNEUciSFvJNC3kohb6SQ11PI7hSyM4W+mEKfTyFbUsimFPJkClmfQtam0FUpZGUKeSCF3p9C7kkhi1PoHSl0fgqZl0JuSiE3pJDrUsiMFDIthUxOIVenkCtTyOUpVFNcKinGSopxmmKCppiiKX4nKWZKitmSYq6kuFlS3CopFkiKRZpiiaZYKimWS4qHJMUjkmKNpFgrKTZoiqckxWZJ8Zyk2C4pXtIUuyTFHkmxV1LslxQHJcVhSfG+pDgmKU5Iik8lRVhSRDVFTFN8Kym+kxRJSZGSFCk5Q0qGSMk3pORLUvI5KfmMlH5CSo+TkqOk5D1S8g4peYuUvElKXiclr5KSl0nJi6RkGynZQko2kZInScl6UrKWlKwiJQ+TkgdI6f2k5F5SspiULCQl80nJPFJyEym5gZRcR0pmkJJppGQyKbmalF5FSi4nJXrxksy0aRfPXOpIjhXOjBOSE5XkFCV5jZCcKSRnC8m5SvJm5cxtSuIO4cxC4czdQvJeIblMSD4oJB8WzqwWkmuF5AYh+aSQfEZIbhUS24TkDiG5UzjzqpDcqyTfVJIHleRh4cwR5cxR4cxxIfmJcOaUkDwtJL8Ukt8IySHhTEIYTiiDSeWbM0r0jPB5QggnlU/PCB8nhQ+TwvtJ5fAZ4eAZ5c2ksjep7Ekqr5wRdiSF7Ulha1LZfEZ4KilsSArrEsLqpPBIUngwqSw7o9yXVJYkhUVnhDuSwm1J4ZakMDcpXJ8Urk0K1ySFqUlhYlIYf0a4IqmMSSiaUCQ5BkmORZLjkOREJDkFSV6DJGciydlIci6SvBlN3oacWYAkFyKJu5HkUiS5DE2uQJIPI8nVSGItktyAJJ9EzzyDJLcgyW1IcgeS3IkkdyPJ15Hkm0jyAJJ8B0m+hySPIsmP0OQJJPkZkvwcSX6JJL9GkoPImWEkkUS+O4N8k0S+SiJfJNHwGeRkAvk4iXyYRN9PooeT6KEksj+B7E0ge5LIK2eQl5LI9iSyNYlsTqJPJZEnksi6JLomiTySRB5KIg+cQe5LIvckkcVJ9I4kMj+J3JJEbjyDXJ9Ark0i088gU5PIpCQyPoFckUAuSyKa5FJJMlaSjJMzTJAEUyTJNZpkpiaZpUlu0CQ36xnmSZLbJclCPcNdkuReSXC/JFghZ3hYkqySJGslyeOSZKMkeUaSbJEk2yTJi5JkpyR5VZK8LknekCQHNMnbcoYjkuQDSXJcEpzQJKckyWlJEpUkX8sZ4pLkOznDGR0mqd9xRr8hoV+RlC84I2ES8ilJ+ZikHOOMvE9CD3NGD3FG95OUfSRlD0l5haS8RFK2k5TnSMpmkvIUSXmCpKwjKWtIyiOc0ZUk9QGSch9JuYeELCapd5DU+STlFpJyI0m5nqRcS1Kmk5SpJGUSSRnPGb2KM3o5Z0QvXvLjtGkXE5cKibFCYpyQmKQkpiiJa4TETCExW0nMVYZvVoZvU4bvUBILhcTdQuJeIbFMSKxQEg8LidVCYq2QWC8kNgrDm4TEFmH4eSHxopB4WRjeLSReVxJvKMNvKcNvC8PvKsMfCIljwvAJYfikkIgoiagyHFOG40JiSPhuWIknlFhC+SIhRIaFU8PKiYTwUUL4ICEcSShvDwtvJZQ3E8rrCeXVhPJyQnhxWNg2LGxJKM8MC08OC+sTwrphYfWw8Miw8GBCWZZQlg4rdyeERQnhjoRw27Bwy7Awd1i5fli5NiFckxCmJoSJCWF8QrgyoVyWUGRYkeExSGIskhiHJCYiiSlI4hokMRNNzEaH56DDN6OJW9HE7cjwQmT4biRxLzK8DB1egSRWIsOrkOHHkOH1yPBGNLEJSTyLJJ5HEi8iwy8jiV3I8OvI8D4kcQAdPoQk3kWG30cSx9DECSRxEkmEkeEokvgKScSRxBDy3TD67TD69TASHUZOD6OnEsgnw8hHw8gHw+iRYfSdBHowgb45jLw+jLyaQHYmkB3DyLYEsmUYeWYYfSqBbkig6xLommHkkWHkwQSyPIEsTSBLEsiiBHpHArktgdwyjNw4jF4/jFw7jEwfRqYmkIkJZPwwemUCvTyB6DCXyjBjZZhxkmCCDDNFElyjw8zUYWZpght0mJs0wTwZ5nYZZqEmuEsS3CvD3C/DrJBhVkqCVTLMYzLM45JgoybYJAmelQTPSYIXJMHLmmCXJnhNE+zTYfbrMId0mHdlmPclwTEd5mMd5lMdJiwJvpBhvtIE38gwQzLMsA6T0G9J6NcM65cMy2kScoph+YSEHGdYjjIs7zGsh0noWyT0TRK6l4S+yrDsZFh2kJDtJOQ5hnUzCXmKYXmChKwjIWsYlkdI6IMM63ISspSELCEhixnWO0jofBJ6Cwm9kWG9nmG5loRMJyFTGZbJDMvVJPQqEno5CdWLl6SmTbs4NEYYulIZGqcMTVSGpihD1yhD1yqDs5TBOcrgTcrQrcrQAmXoTmXoLmXwHmXwfmXwAWVopTK0Shlcoww9rgxuVIaeUoY2K4NblaHtytAOZegVZWiPMrRXGXpTGXpLGXpbGTqiDH2gDH2kDJ1Qhk4pg6eVwagy+LUy9K0yOKh8Pah8OaicHlQ+G1I+HVKODyofDirvDSqHB5VDg8r+QWXfkLJnSNk1pLw0pGwfUp4fUp4dUjYNKRuHlMeHlLVDyqpBZeWgsmJIuX9IuXdIuXtIWTSkLBhUbh1Ubh5U5gwqs4eUaweV6YPK1EFl4pAyfki5cki5bFDRQUUHx6CDY9Ghq9DBCejQZHTod+jQTHRoFjp0Azp0Ezp0Kzp0Ozp4Jzq4GB28Bx28Dx1ajg4+hA4+ig6uQQfXoUMb0MGn0MFn0KGt6NB2dGgHOvgKOvgqOvg6OvQmOvQWOvgOOngEHTqKDn2EDp1AB0+hgxF06At0KIYOxdHBIfSbIfTLIfTzQTQ8hH46iB4fRD8cQt8bQg8PoYcG0f2D6L4h9LUhdNcg+tIg+sIg+vwQumUI3TSEbhxE1w+hjw2hq4bQhwfRFUPosiF06RC6ZBBdNIguGEJvG0JvHkJvHESvH0SvHUKnD6FTh9CJQ+j4QfTKIfTyIVSHuFQHGatDjNNBJuggk3WQaTrEDB1ilg5xgw5xow5xiw4yX4e4UwdZrIPco4Ms1UGW6yAP6RCP6BBrdIi1OsQGHeJpHWSzDrJVh3heh3hRh9ipQ+zWIfbqEG/oIG/pIG/rEO/qEEd1iGM6xAkd4pQOEdFBvtBBvtJB4jrIoA4ypHGG9EuG9AuGNMygfsqgnmBIP2RI32dIDzOkbzOkBxjSfQzpawzpLgb1JQb1BQb1eQZ1K0P6DIO6kSHdwJA+xqCuZkgfZkhXMKjLGdR7GdIlDOkiBnUBQ3obQ3ozQ3ojQ3o9g3otgzqdQZ3KoE5mSK9mSK9kUMcypHrxkjPTpl2MXyrExyrxcUp8ohKfosSvUeIzlPgsJT5Hid+oxOcp8flK/E4lfpcSX6LElyrxZUr8QSX+iBJfrcTXKvH1SvxJJf6MEn9WiW9T4i8o8ZeV+G4l/poS36fE9yvxQ0r8XSX+nhI/psSPK/FPlXhYiX+uxL9U4l8rX8eVL+PK53ElHFdOxpWP48qHceX9uPJuXDkUV/bHlTfiymtxZXdc2RlXdsSVbXFlS1x5Jq48FVc2xJV1cWVNXHk0rjwUV5bHlfviyj1xZXFcuTOu3B5X5sWVm+LKnLgyO67MjCvXxJWpcWViXBkXV66IK5fFFY0rGh+Dxsei8avQ+AQ0PhmN/w6Nz0Djs9D4HDR+Ixqfh8bno/E70PgiNL4EjS9F48vQ+INo/BE0vhqNr0Xj69H4k2h8Exp/Fo0/j8ZfQOMvo/FdaHwPGt+Hxvej8YNo/F00/h4a/xCNH0fjn6Lxz9D4aTQeReMx9Js4+lUc/SKOhuPoyTj6cRw9Fkffj6OH4+ihOHogjr4RR1+Lo7vj6M44+mIc3RZHt8bRZ+LoU3H0iTj6eBxdE0cfiaMPxdEH4uj9cfSeOLo4jt4ZR2+Po7fG0Zvi6Nw4OjuOzoyj18TRKXF0UhwdH0eviKOXxVGNc6nGGatxxmmcCRpnisaZpnFmaJzrNM71GudGjXOzxrlN49yhcRZpnCUaZ6nGWaZxHtQ4D2uc1RpnrcZZr3Ge1DibNM4WjfO8xnlB47yscXZpnD0aZ6/GeVPjHNI4hzXOexrnqMY5rnE+0TinNM5pjRPVODGN841+Q1y/JK5fENcIcT1JXD8mrseI6wfE9V3i+jZxPUBc3yCurxPXV4nrTuK6g7huJ65bietm4voUcd1IXB8nrmuI66PE9SHi+gBxvZ+43ktc7yKudxLX24nrrcT1ZuI6l7jOJq4ziet04jqVuE4iruOJ6xXE9XLiqhcvGZ427WJsjBC7UomNU2ITldgUJXaNEpupxGYpsTlK7EYlNk+J3a7E7lBii5XY3UrsPiW2TIk9qMQeUWKrlNhaJbZBiW1UYpuU2LNK7Dkl9oIS26HEXlFiryqxvUrsTSX2lhJ7W4m9q8Q+UGLHlNjHSuykEgsrsc+V2JfKlzHli5gSiSknY8qJmHIspnwQU47ElHdiysGY8mZMeT2m7Ikpu2LKyzFle0x5LqY8G1M2xZQnY8r6mLI2pqyOKY/ElIdiyvKYcl9MuTumLI4pd8aU22PKvJhyU0y5IabMiikzY8rvYsqUmDIxpoyPKVfGlMtiisYUjY1BY2PR2Dg0NhGNTUZjv0NjM9DYLDQ2B43diMbmobH5aOwONLYIjd2NxpaisWVobAUaW4nGVqGxx9DY42hsIxp7Go1tRmPPobHtaGwHGtuJxnajsdfR2Bto7AAaO4TG3kVjH6CxY2jsYzT2KRr7DI19jsa+RL+KoV/E0EgMPRlDT8TQj2Lo0Rh6JIa+E0MPxtA3Y+jeGLonhu6KoS/H0Bdi6PMxdEsM3RRDn4yhG2Lo2hi6OoY+GkMfiqHLY+h9MXRJDF0cQ++MobfH0Ftj6E0xdG4MnR1Dr42h02PolBg6MYaOj6FXxtDLY6jGGKMxrtAY4zTGBI0xRWNM0xgzNMZ1GuN6jXGjxrhZY9ymMe7QGIs0xhKNsVRjLNMYKzTGSo2xSmM8pjEe1xgbNcbTGmOzxtiqMbZrjB0aY6fG2K0xXtMYb2iMAxrjkMY4rDHe0xgfaozjGuMTjfGZxjitMb7UGDGNEdMoMY0Q01PE9AQx/YiYHiWmR4jpYWJ6kJjuJ6b7iOkeYrqbmL5MTF8gps8T0y3EdBMxfZKYPkFM1xHT1cT0UWL6EDFdTkzvI6b3ENO7iOlCYno7Mb2VmN5ETOcS09nEdCYxnU5MpxLTScR0PDG9ipheTkz14iWD06ZdjI4Rolco0XFKdIISnaxEf6dEZyjR65To9Up0rhK9RYnepkQXKNGFSvQuJXqvEr1fiT6gRFcq0UeV6Bol+rgS3aBEn1KizyjRLUp0mxJ9QYm+rERfUaJ7lOg+JfqmEj2oRN9WokeU6AdK9JgSPaFETyrRiBL9XPk8qkSiyqmociKqfBRVjkaVI1HlcFQ5FFUORJV9UWVPVNkdVXZGlR1RZVtU2RJVnokqT0WVJ6LKuqiyJqo8GlVWRpUVUeX+qHJvVLk7qiyKKndElduiyi1R5caockNUmRVVZkSVaVFlSlSZGFXGR5UrosqYqKJRRaNj0OhYNHoVGr0ajU5Go9PQ6HQ0ei0anY1G56LRm9HorWh0ARpdiEbvQqP3oNH70OgDaPQhNPoIGl2NRtei0Q1o9Ek0ugmNbkGjz6PRF9DoS2j0FTS6B43uRaNvotG30OghNHoEjX6ARo+h0Y/R6Ek0Gkajn6NfRNFIFD0VRT+Josej6NEo+l4UPRxFD0XRA1F0XxR9LYrujqI7o+iOKLotim6Nopuj6FNR9Iko+ngUXRNFV0XRh6Poiii6LIoujaJ3R9FFUfSOKDo/is6LojdG0TlRdFYUnRlFr4miU6LoxCg6PopeGUUvj6Ia5VKNMlajXKVRrtYokzTKVI0yXaNcq1Fma5S5GuUmjXKrRlmgURZqlLs0yhKNcp9GWa5RHtQoj2iU1RplrUbZoFGe1CibNMqzGuV5jfKCRnlJo7yiUV7VKHs1ypsa5S2N8rZGeVejfKBRjmmUjzXKSY3ymUb5XKNENUpUTxPVU0T1U6J6nKgeJarvEdXDRPVtonqAqO4jqq8R1d1E9RWiuoOobiOqW4nqZqL6NFF9gqiuJ6qPEdVVRPVhorqCqC4nqkuJ6t1EdRFRvYOozieq84jqjUT1BqI6i6jOJKrXENUpRHUiUR1PVK8kqpcTVb14ydfTpl2MXCpErlAiVymRCUpkkhL5nRKZrkSuVSLXK6fnKKdvViK3KZHblchCJbJYiSxRIkuVyDIl8qASeViJrFIia5XI40pkoxJ5WolsViJblcg2JfKiEnlZOb1LOf2aEtmnRPYrkYNK5B0lckSJHFVOf6RETiiRk0okrEQiysmIciKifBRRjkaU9yLKOxHlUEQ5EFHeiCivRZRdEWVnRNkRUbZHlOciyuaIsimibIwo6yPK2oiyKqI8ElEejCjLI8p9EWVJRLkroiyMKAtOK7dFlJsjytyIcn1EuS6izIgov4soUyLKhIgyLqJcEVHGRBSNKBoZg0bGopGr0MgENDIJjUxDI9PRyLVoZDYamYNGbkIj89DI7WhkIRpZjEaWoKeXopFlaGQFGnkYjaxCI4+hkfVoZCMaeRqNbEYjW9HINjTyIhp5GY3sQiN70Mg+NLIfjRxEI++gkSNo5Cga+QiNnEAjJ9HTYTQSQU9F0E8i6PEI+uFp9L0IejiCHoqgByLoGxH09Qi6O4LujKAvRdDtEfS5CPpsBN0UQZ+MoOsj6NoIujqCPhJBH4ygyyPo/RH03gh6VwRdFEEXRNDbIugtEXRuBL0+gl4XQWdE0N9F0MkRdEIEHRdBr4igl0VQjXCpRhirEa7SCFdrhEkaYapGmK6nuVYjzNYIczTCjRphnka4XSPcqREWa4S7NcJSjbBMI6zQCA9rhEc1wmMa4XGN8IRGeFojbNYIWzXCNo3wokZ4WSO8ohH2aIR9GmG/RjioEd7WCEc0wgca4ZhG+FgjfKoRwhohohEieoqIfkJEjxPRD4noe0T0MBE9REQPENE3iOjrRPRVIrqTiO4gotuJ6HNE9FkiuomIPklE1xPRdUR0Naf1EU7rQ0T0ASJ6HxG9h4jeRUQXEdEFRHQ+Eb2FiM4lojcQ0euI6Awieg0RnUJEJxLRcUT0SiJ6ORHVi5fEpk27GL5UCF+hhK9SwhOU8GQl/DslPF0JX6uEr1fCc5TwTUp4nhK+XQkvVMKLlfASJbxUCS9Twg8q4ZVK+FElvEYJP66ENyrhp5TwM0p4ixJ+Xgm/qIRfUsK7lPBrSnivEt6vhN9Swm8r4XeV8PtK+JgSPq6EP1XCnymfhZVPwsrHYeVYWPkgrBwJK2+HlYNhZX9Y2RdWXgsru8LKy2HlxbCyLaxsDSvPhJWnw8rGsLI+rDwWVh4NKw+HlQfDyvKwcl9YWRJW7gorC8PKgrBya1i5KazMDSvXh5XrwsqMsPK7sDIlrEwIK+PCyhVhZUxY0bCi4TFoeCwavgoNT0DDk9HwNDQ8HQ1fi4Zno+E5aPgmNDwPDd+Ohu9Ew4vR8BI0vBQNL0PDK9DwSjT8KBpeg4YfR8NPoOGn0PAzaHgLGn4eDb+Ahl9Cw6+g4VfR8F40/CYafgsNv42G30XD76PhD9HwcTT8CRo+hYbD6Kdh9OMweiyMfhBGj4TRd8LowTC6P4zuC6OvhdFdYfTlMPpiGN0WRreG0c1h9OkwujGMrg+jj4XRVWH04TD6YBhdHkbvC6P3hNG7wuiiMLogjN4aRm8Oo3PD6PVh9LowOiOM/i6MTgmjk8Lo+DB6RRi9LIxqmEs1zFgNc5WGuVrDTNYw0zTMdA1zrYaZrWHmaJgbNcw8DXO7hrlTwyzWMHdrmKUaZpmGWaFhVmqYRzTMGg3zuIZ5QsM8pWE2aZgtGuZ5DfOChnlJw+zUMK9qmL0a5k0N85aGOaRh3tUw72uYDzXMcQ1zQsOc0jBhDRPWk4T1Y8L6EWH9gLAeIazvENaDhPUAYd1HWF8nrLsJ68uEdQdh3UZYtxLWzYT1acL6JGFdT1gfI/z/oaLfttO69kXdNw8wUaO22mr963w+n89CSMjI2NhCyMjIyNjYRMTERCTEJFI6kUIiWe6mDxhDGoMZViill13KfkxffvsiWnOvdfEr3xt8kubfkuLfcsS/JcG/Jc6/ZY9/S5R/yyP+LWH+Ldv8Wzb5t6zzb1nl37LIv2WOf8sM/5YJ/i1j/FuG+Lf08W/p4d8iX776/05NfWl3Wtp9wl8DQntEaI8J7SnhrxmhvSC0l4X2mtAOCO0t4a8dob0rtB8J7SdCOya0D4T2odA+EtrHQvuV0H4jtL8W2t8I7W+F9ndC+3uh/aPQLgvtc6FdEdq/Ce3fhfZH4a9PQrsmtOtC+1Zo/7fQ/lNo/1/C/9UW/ndb+J+2cPuX0GgL/2oL/2wL123hj7+Ey7bwa1s4bws/t4UPbeGHtvBdW/j2L+GbtvB1W3jTFl61hZdt4agtPG8Lz9pCrC08aQuP2sJuW9hpC1t/CYG/hLW2sNwWFtrCbFuYagvjbWHkL2GwLfS1ha62IH8J0u5C2r1IewBpDyPtUaQ9ibRnkPY80l5C2qtIewP5K4i0Q0g7jLQjSDuKtPeQv+LIXwmknUTaKeSvNNJ+g7RPkHYOaeeRdgFpF5F2CWn/hLTPkXYFaV8i7d+R9hXy1yekXUPa/4G0/xP56w5pN5F2C/mrjfzZRv6njdy2kUYb+Vcb+Wcb+dhGfm8jv7WRX9vIL23kpzbyoY380EZO28j7v5B3beSkjbxtI6/byMu/kBdtJNlGnrWR/TbytI08/gt52EYetJHtNrLZRtbbyEobWWwjs38hU21kvI2MtJHBNtLXRrrbiLTplDa90qZf2gzJX4zJX0xKmxlpMy9tlqTNqrTZkDZBaROSNmFpE5E2UWmzJ23i0iYhbZLSJiV/kZY2GWmTlTY5aZOXvyhIm6K0+UHalKXNmbT5h7S5lDa/S5traXMjbWrSpi5t/kva/Le0aUqblrRpS5u2tGjL/9CWO9rSoC3/oi3/pC03tOUP2lKlLb/yl/yDtvxMWz7Qlh9oyylteU9b3tGWE9rylr/kNW15SVte0JbntOUZbdmnLU9py2Pa8pC2POAv2eYv2aQt67RlhbYs8pfM8pdM05Zx2jJKWwZpSz9t6aEt8uWr/8/U1JdWp6XVK/w5ILRGhNaY0JoS/pwRWnNCa0lorQitdaG1Kfy5LbTCQuuh0HostPaEVlxoPRNaSaH1QmilhVZGaL0VWjmh9U5oFYTWqdAqCa0PQutnofWL0PpVaFWF1h/Cn9dC65PQqgmthtD6L6H1P0KrKfzvP4X/aQm3LaHxp/CvlvDPlvCxJVy1hOqfwq8t4ZeW8FNL+NASSi2h2BLet4R3fwonLeFtS3jdEl62hBct4XlLeNYS9lvC05bwuCU8bAnhlhBqCcE/hfU/hdWWsNQS5lvCTEuYbAljLWHkT2GwJfS1hK6WIH8K0upCWr1IawBpjSCtMaQ1ibRmkNYc0lpEWitIax35cxNpbSOtB0jrIdJ6jLSeIn/uI38+Q1rPkdYL5M+XSOs10nqLtL5GWu+Q1rdI6xRpfY+0PiCtn5DWOdKqIK1LpPUH8uc10vqEtGpIq478+V9I67+R1v9G/ncL+Z8WcttCGi3kXy3kny3kYwv5o4X81kIuWsg/WshZCym3kFILKbaQQgvJ/4nkWki2hWRaSLqFpP5EjlpIooXEW8heC3nSQiJ/IrstZKeFBFvIRgtZbSFLLWS+hcz8iUy1kLEWMtJCBltIXwvpbiHSolNa9EqLAWkxLH8yKn8yIS1mpMWctFiUFivSYk1abEqLbWnxQFo8lBaPpMVTabEvLZ5Ji+fS4oX8yUtp8VpavJEWJ9LinfzJe2lxKi2+lxYfpMVP0uIXafGrtPhNWvwuLa6kxSdpUZMWdWnxX9LiTlr8b2nRkj9pSZOW3NKS/6Ql/6Il/6QlH2nJFS35nZZc0JIKf8oZLSnTkh9pSZGWFGhJnpZ8Q0u+piUZ/pRXtCRFS45oSYKWHNCSPVryhJZEaMkuLQnxpwT5UzZoySotWaYl8/wps/wp07RknJaM0pJBWtJPS3poiXz56t9TU1/+p8vS7BWaA0JzWGiOCc1JoTkjNOeE5pLQXBGa60JzU2huC80HQvOh0HwsNJ8IzZjQPBCah0LzSGgeC81XQvON0PxaaOaEZl5oFoTm90KzJDTLQvNMaP5DaF4IzarQ/ENofhSa/xSa/yE0G0Lzv4Tmfwv/0xRum8J/NoX/aAqfm8JNU/ijKfzeFC6bQqUpnDWFclMoNYXvm8J3TeHbpvBNU/i6KbxpCq+awnFTOGoKh03hWVPYbwpPm8LjpvCwKTxoCttNIdgU1pvCalNYagrzTWG2KUw2hbGmMNwUBppCX1PoagrSFKTZhTR7keYA0hxGmmNIcxJpziDNOaS5iDRXkOY60txEmltIcwdp7iLNR0jzCdKMIc0DpHmINJNIM4U000gzgzSzSDOHNPNIs4A0i0izhDQ/IM2fkeYvSPNXpFlFmn8gzY9I859I819Is4E0/wtp/jfSbCJ3TeQ/m8h/NJHPTeSmiVw1kWoTuWwilSZy3kR+aiI/NpHvm8h3TeTbJvJNE/m6ibxpIq+ayHETOWoiz5vIQROJNZGnTeRxE4k0kQdNZLuJBJvIRhNZbSJLTWS+icw0kakmMt5ERprIYBPpayLdTUSadEqTXmnSL02GpMmoNJmQJtPSZE6aLEqTFWmyJk02pcmWNNmRJrvS5JE0eSJN9qRJXJokpElSmqSkSVqaZKRJVprkpElemhSkSVGalKTJB2nyszT5hzS5kCa/SZPfpclHafJPafIvadKQJrfS5L+lSVOaNOW/acp/0pQ6TflMU25oyhVN+Z2m/EZTKjTlF5ryE035kab8QFO+oynf0pRvaMoJTXlLU17RlJc05QVNeU5TntGUfZrylKZEaUqEpoRpSoimBGnKBk1ZpSnLNGWepszSlCmaMk5TRmnKIE3ppyk9NEW+fPXX1NSXu07LXZ9wOyjcDQt3o8LtpHA7LdzOC7eLwt2KcLcm3AWEu23hbke42xXuHgm3UeF2T7iNC3cJ4S4p3L0Qbl8Kt6+FuzfC7dfC7TfC7bfCXUG4Kwq3JeGuLNz+LNz+Itz+Ktz+Jtz9Idx+FG7/KdzVhLu6cPdfwn/dCvU74V93wqc74eOd8Met8NudcHEr/ONWOLsTfroTfrwTvr8TvrsTvr0V3t0JJ7fC21vh9a3w8lZ4cSc8vxWe3QnxO2HvTnhyJ0TuhN1bYedW2LoVNu+EtTth5U5YvBPmboWZO2HiThi7E4bvhIFboe9O6LoT5FaQ207krge57Uduh5C7UeR2ArmdRm5nkbsF5HYZuV1F7jaQ2yByF0LuwshdBLmLIndPkbt95PYZcnuI3B0hd8fI3Wvk7g1y+zVy+w1y+y1y9x1yV0RuS8htGbn7Gbn7Bbn7Fbn9Dbn9A7m7Ru4+IXc15LaO3P4ncnuLNO6Qf90hn++Qmzvk6g6p3iIXd8g/7pCzO+SnO+THO+T7O+T0Dnl/i7y7Q07ukLd3SOYOSd8iqTskeYckbpH4LbJ3hzy5Qx7dIbt3yM4dsnWLbN4h63fIyh2yeIvM3SIzt8jkHTJ2h4zcIYN3SO8t0nWHyB2dckeP3NEvdwzJLSNyy5jcMiW3zMotC3LHstyxKncE5I6g3BKSO8Jyy0O55bHc8VTu2Jc7nskdh3LLC7nlWG55JXdk5I6s3JKTW/JyS0HuKModP8gdH+SWn+WOX+SOX+WOS7njd7njWu74JHfU5I663PKfcsut3HIn/8md/Is7+cyd3HArV9zJ79zJBXdS4U7OuZOfuJMP3MkP3Mkpd/KeO3nHneS4lbfcSoZbSXMnKe7kiFtJcCtx7mSPO3nCnTziVh5yKw+4lS3uZJM7WedOVriTJe5knjuZ4U6muJMx7mSEOxniTvq5kx7uRL589efU1Jd6l6XRKzT6hcaQ0BgVGuNCY0pozAqNRaGxJDRWhcaG0NgSGiGh8UBoRITGY6HxRGjsC40DofFcaBwJjZTQeCU0MkLjrdA4ERrvhEZBaHwnNH4QGj8KjbLQOBca/xAal0KjKjT+EBo3QuOfQqMmNOpCoyH8qyF8bggfG8JVQ/i9IVw0hEpDOGsIPzWEDw3h+4Zw2hDeN4R3DSHXEN42hDcN4VVDOG4IRw0h0RAOGsJ+Q3jaEKIN4WFDCDeEUEMINoRAQ1htCMsNYaEhzDaE6YYw0RDGGsJwQxhoCL0NoashSEOQRhfS6EUa/UhjCGmMIo1xpDGFNGaRxgLSWEIaq0hjA2lsIo1tpPEAaTxEGo+RxhOkEUMaB0gjgTSSSCOFNNJII4M03iKNE6TxDmm8RxrfIY3vkcaPSKOMNM6Qxj+QxgXSqCKNP5DGR6TxCWnUkMZ/II0G8q8G8rmB3DSQqwZSbSAXDaTSQM4byE8N5McG8n0DOW0g7xvIuwaSayDZBpJpIOkGkmogRw3ksIEcNJD9BvK0gTxuIA8bSLiBhBrIVgMJNJC1BrLcQBYayGwDmW4gEw1krIEMN5CBBtLbQLoaiDTolAY90qBfGgxKgxFpMC4NpqTBjDSYlwZL0mBFGmxIg01psC0NHkiDXWnwSBo8kQYxaXAgDRLSICkNUtLgpTR4LQ3eSIMTafBOGryXBt9Jg6I0+FEa/CQNzqTBP6TBr9LgN2nwhzS4lgb/lAY1aVCXBg1p0JD/oCH/pCE3NOSKhvxOQy5pSIWG/EJDfqIhH2hIiYac0pACDfmWhuRoSJaGvKEhr2nIMQ05oiHPacgzGhKnIU9pyGMaEqEhYRqyQ0OCNGSDhqzRkGUaskhD5mjINA2ZpCFjNGSEhgzQkD4a0kXj/x7Af3RZ6r1CvV+oDwn1UaE+LtSnhPqcUF8Q6ktCfU2obwj1TaG+LdR3hPpDof5IqEeFekyox4V6Qqg/F+ovhPqxUH8l1DNC/Wuh/k6ofyvUC0K9KNRLQv2DUP9ZqJ8L9V+F+qVQrwr1a6F+I9T/KdT/JfyrLnyuCzd14bou/F4XfqsLlbrwS104qwvluvBjXSjWhe/qwvu68K4ufF0X3taF13UhXRdSdSFZFxJ14aAuxOrCk7rwuC7s1oUHdSFUF4J1YaMurNaF5bqwUBdm68J0XZioC2N1YbguDNSF3rrQVRekLki9E6n3IPV+pD6E1EeQ+jhSn0Lqs0h9AakvIfVVpL6B1DeR+jZS30Hqu0j9EVKPIvU9pB5H6s+QehKpv0DqL5H6K6SeQepZpP4NUv8WqReQehGpl5D6B6T+M1I/R+oVpH6J1H9H6ldI/Qapf0bq/4HU60itjnyqIx/ryB915Lc68msd+aWOnNWRch0p1ZFiHfmujryvI+/qyEkdeVtHXteRdB1J1ZGjOnJYRw7qyH4deVpHHteRh3UkXEdCdSRYRwJ1ZK2OLNeRhToyW0em68hEHRmtI8N1ZKCO9NaRrjoidTqlTo/U6Zc6Q1JnVOqMS50pqTMjdealzpLUWZE661InIHW2pM6O1AlLnUdS54nUiUmduNRJSJ2k1DmSOsdS57XUeSN1slLnG6nzrdQpSJ1TqVOSOh+kzk9S5xepU5E6l1Lnd6lzJXVupM4/pc6/pE5d/oO6fKYun6jLR+ryB3X5jbr8Sl3+QV3OqUuZupSoS5G6fEdd3lOXPHU5oS5vqUuGuryiLinqckRdDqnLAXXZpy571OUxdXlIXcLUZYe6BKlLgLqsUZdl6rJIXeapyzR1maAuY9RlmLoMUpde6tJNXeTLV/8zNfXlc5el1ivU+i21IUttxPJ5QqhNCrUZ4fOCpbZk+bxsqa1bagFLbVuo7Qi1XUstYvn82FLbE2r7Qu1AqCWEWtJSe2GppS2115baG8vnE+HzN5Za3lIrWGpFS+0HoVa21H6y1H6x1CqW2oVQ+12o/SHUboTaP4V/1iw3NeGqJvxeEy5rwq+fLeefLT/XLOWapVQTvq9ZCjXh25rwrmY5qVmyNcvrz5ZXny3HNctRzXJYsxx8FmI1Ya8mRGuWRzXLbs0Sqlm2a5bNmrD22bJSsyzVLAs1YbYmTNWE8ZplpGYZ/mwZ+Cz01oTOmkVqFvu5E1vrwX7uw9aGsJ9HsLVRpDaB1KaR2hxSW8DWVrC1dWwtgP28ha2FsLUH2FoEW3uMrT3Ffo5ha3GklkBqSWwtha2lsbXX2NobbO1r7OdvsJ+/xdYK2FoRW/sBW/uArf2ErZ0jtQq2domtVbGfr7C1G2ztM7ZWw376jL2uYf+oYX+rIb/WkF9q2LMa9qfP2B9r2O9r2O9qyLc17LvP2JMaNvsZm6lhX9WwxzXsUQ15XkOe1ZD9GvK0hn1cw0Y+Y3dr2J0adruG3awhGzVktYYs1rALNexsDZmqIeM1ZLSGDNWQ/hq2t4btqiG2Rqet0WNr9MlnBqXGsNQYszUmbI0Z+cy8fGbB1liyNVZtjQ1bI2hrhKRG2NaI2BqPpMYTqRGzNeK2RsLWSNrPpGyNl/Yzr22Nt7bG17bGN7ZG3tZ4b2sUbY0f7Gd+tDV+lhrnUqNia1xKjap85kpqfJQa/7Q1avYzNXtDzV7z2f7BZ/sbNfsrNfsLNXtGzf5Ezf7IZ/s9NXtKzb6nJu+oyQk1m6Vm31Czr6jJMTU5omafU7MJPts4NRujJlE+2wg1+5CafUDNblOzm9TsBjW7Rs0uUbPz1Owcn2WKmoxTs6PU7DA1O0BNevlsu6lZ+fLVf01Nfbmxhk89lpt+4WbQ8mnY8nHc8mnScjNt+ThnuVmwfFy23KwKN+uWT0HLp23LpweWj7uWj4+EmyeWmz3LTdxy88xyc2i5ObLcHFtuXlk+ZSwf3wofTyw331huvrXcfGe5KVo+lSw3HyyffrZ8OrfcVCw3l5abquXmD8unj5aPnyx/3Fh+v7Fc3lgqN5ZfPlp+/mgp31h+vLF8f2M5/SR8+8ny7saSu7FkbyxvPlnSHy0vP1pe3Fie31gSN5b9j5a9G8uTG8ujG8vDT5YHnyyhT5bgJ8vGJ8vajWXpxrJwY5n7ZJn+ZJm8sYx9sozcWAY+Wvo/WnpvLJ03FntjsR87kZse7Mc+7M0g9uMw9mYUezOBvZnGfprD3ixgPy1hb1axN+vYj5vYT1vYTzvIp13sTQR7E8V+fIq9iWFvDrCfDrE3R9hPx9ibNPYmg715i/14gv34DfYmj70pYG+K2E8l7KcP2JufkZtfsDcV7M0F9mMVe/MH9uYj9uYT9uoG+/sn7OUNtnKD/eUT9ucbbPkG++NH7Pc32NNP2Pc32Hc32NxHJPsJ++YG++oG+/IG++IG+/wTNvEJG7/B7n3CPrnBPvqEPLzBPrjBbt9ggzfYjU/YtRvs8ifs4ifs3A12+gaZ/ISM3SAjN8jQDbb/Btt7g+26wdobxH6ix36iz35k0N4wbD8xam+YsDdM24/M2o8s2E8s2U+syg3r9oZNe8OW3LAjN+zaGyL2E1F7w1P7iZj9xDP7kUP7kSP7iZT9SNrekLE3vLWfOLGf+MbekLef+M7eUJSPlOSGD3LDT/YT5/YTFXvDpf1I1d7wh73h2t5wY2+4sdfc2N/5KL/xUX7lxv7Cjf2ZG1vmxv7Ijf2ej/aUG1vgxr7jxua4sVk+2Tfc2Nfc2Jd8si/4JM+5sQlu7AEfbYwb+5Qb+4iP9iE3NswnG+JGtriRDW5kjU+ywo1d5MbOc2On+WgnubHj3NgRbuwQN3aAT7aHj7aLG2u/fFWfmvryR6fhqke46rNcDVquhy1XY5arCcvVtOV61nI1b7laslyvWq7WheuA5XrLcr1juQ5briKWq8eWqyeWq5jl+kC4SliukparF5arl5brV5arjOUqa7k6sVy9s1x9K1wVLNdFy/UPlusPluufLFfnwnXFcnVhubq0XP1uubqy/HZlubyyVK4s51eWn68t5WtL6cry/ZXlu2vL+2vLu2vLyZXl6yvLm2vLqyvLyyvLi2tL8sqSuLIcXFliV5an15bolSVyZdm9tuxcW7avLZvXlvVrYfXKsnxlWbiyzF1Zpq8sk1eWsWvLyJVl8MrSd23pubJ0XlvkyiJXnchVD/aqD3s1iL0exl6NYq8msFfT2OtZ7NUC9moJe72KvVrHXgew11vY6xD2Ooy9eoi9foy9eoK9imGvDrDXCez1c+z1C+z1MfbqFXKVwV5lsVcn2KtvsFffYq8L2Osi9voH7NUH7NXP2Ktz7NU/sNe/Yq9/w179jv3jCvvbNfbyGlu5wv5yhf35GvvhGlu6wn5/hf3uCvv+GvvuGpu7xn59hby5wr6+xr68wqausEdX2MMr7ME1NnaNfXqNjV5jI9fY3WvszhV2+wq7eYXduMauXWGXr7EL19i5a+z0NTJ5hYxdY0eusINX2P5rbO8VtusKa6/otFf02Cv67DWD9ophe82ovWJCrpi2V8zaK+btNYv2mhW5Ys1eEbDXbNkrQvaKsL1i117zyF7xxF6xZ6+J22sS9orn9poX9oqUvSJtr8jYa7L2mhN7zTf2mm/tFQV7RdFe84O94kd7xU/2ijN7xT/sNb/aK36zV/xur7iyV1zZ37myl1zZX7myv3Btf+bKlrmyJa7t91zJd1zZAtc2z5XNcWW/5sq+4dq+5sqmubIpruwR1/aQK/uMa7vPtTzl2ka5shGu7S5Xdocru821DXJlN7iSNa5lmWtZ5MrOcWWnubKTXNlxrmSEazvEtR3g2vZwbbu4svbLV/+anPzymxiq3ZZqr6U6aKkOWapjluqEpTplqc5aqnOW6pKlumKprlmqAUs1aKmGLNUHluqupfrYUo1aqnuW6r6lemCpHlqqzy3VlKX60lJ9Zam+sVTfWqo5S/WdpfqtpfqdpXpqqZYs1Q+W6k+W6rml+g9L9VdL9dLyW9VyUbVUqpZfqpafq5YPVcsPVcv3Vct3Vcv7qiVfteSqlq+rlrdVy+uqJV21pKqWo6rledXyrGrZr1qeVi1PqpbHVcvDquVB1bJdtWxVLZtVy3rVslK1LFYt81XLbNUyVbVMVC2jVctw1TJYtfRVLT1Vi9yz1U5stRtb7cVWB7DVIWx1FFudwFansNUZbHUOW13AVpex1TVsNYCtBrHVLWx1B1vdxVYj2GoUW32Krcaw1Ti2msBWk9jqC2z1GFt9ha2+wVaz2OoJtvoOW/0WW/0OWy1iqz9gqx+w1Z+w1XNs9R/Y6q/Y6m/YahV7UcVWqthfqtifq9hyFVuqYr+vYk+r2PdVbL6KzVWxX1exb6rY11VsuopNVbFHVezzKvZZFbtfxe5VsU+q2MdV7MMqNlzFhqrYrSp2s4pdr2JXqtjFKnahip2tYqeq2IkqdryKHa5iB6vY/iq2p4rtrGJtlU5bpdtW6bVVBmyVIVtlxFYZt1WmbJUZW2XOVlmwVZZtlVVbZcNWCdoq27bKjq0StlUitkrUVnlqq8RslQNbJWGrJG2VF7ZKylZ5ZatkbJWsrXJiq7yzVb61VQq2StFW+cFWKdkqP9kqZ7bKP2yVX22V32yVqq1StZdUbYWq/YWq/ZmqLVO1P1K131O1p1Rtgar9lqrNUbVfU7VvqdoMVfuSqk1RtUdU7XOq9hlVG6dqY1TtE6r2MVX7kKoNU7U7VG2Qqt2kajeo2hWqdomqnadqZ6naGap2gqodo2pHqNohqnaAqu2haruoWvvlq39OTn75VQyX3ZaLXsvFoOViyHIxarkYt1xOWi5nLJdzlst5y+Wy5WLVcrFuuQhYLrYtFzuWi7DlImK5fGy5jFou9ywX+5bLhOXy0HJ5ZLlMWS5eWi5eWy7eWC6+tlzkLBfvLBfvLZffWS6+t1yULJcfLBc/WS7OLRf/sFz8avn1wvKPS8vZheWnC0v5wlK6sBQvLd9dWt5fWN5dWHKXlq8vLW8uLa8vLC8vLKkLy9GF5fDS8uzSEr+0xC4tTy8sjy8skUtL+MKyc2nZurRsXljWLy1rl5blC8vCpWX+0jJzaZm8tIxfWEYvLMMXloFLS9+FpfvSIpcWe2mxl4K97MZe9mIvBrCXg9iLEezFOPZyEns5g72Yw14sYC+WsJcr2Mt17EUAexnEXoSwlw+wlw+xl4+wl0+wl3vYy33sxTPsxSH2Iom9SGEvXmIvXmMv3mAv3mIvTrCX77AX77GX32EvitjLEvbiA/byJ+zlGfbyF+xFBfvrBbZygT2/wP58if1wiS1dYouX2O8usYVLbP4Cm7vAZi+xby6xry+w6Qvs8SX26AJ7eIlNXGLjl9jYBfbJBfbxBTZyiQ1fYncusNuX2M1L7MYldu0Su3yJXbjAzl1iZy6xk5fY8Uvs6CV2+AI7eIntu8R2X2I7L7D2ErGXdNsLeu0lA/aSIXvBiL1g3F4waS+ZsZfM2UsW7CVL9pIVe8G6vSRgLwnaS0L2kgf2kof2kkf2kqi9ZM9esG8veGYvObSXJO0FKXvBS3vJa3tJxl6StZfk7AXv7CXv7SUFe8mpveAHe8EHe8lP9oIze8kv9pJf7SUX9pIL+ysX9hcu7c9c2jKXtsSl/YEL+x2XtsClzXNhc1zYEy7tWy7say5smkt7zKV9waV9zqVNcGnjXNoYl/YpF/Yxl/YRFzbMpd3hwm5zYTe5tBtc2lUu7TKXdoELO8+lneXCTnJpx7mwY1zaYS7tIJe2j0vbw4Xt4sLaL1/dTE5++YcYKt2WSq+lMmCpDFkqo5bKmKUyYalMWyqzlsqCpbJkqaxYKuuWSsBS2bJUQpbKA0vloaXyyFJ5bKk8tVRilsqBpfLMUnluqRxZKilLJW2pvLZU3loqX1sqOUslb6l8a6l8Z6kULZUfLJUPlspPlsqZpfKL5R8Vy1nF8lPF8qFiKVUs31cs31Us7yuWfMWSq1i+rljeViyZiiVdsRxXLC8qlucVS6JiOahY9iuWvYrlccXyqGJ5WLE8qFhCFUuwYtmsWNYrltWKZbliWahY5iqWmYplsmIZr1hGKpbhimWgYumtWLorFqlYbMViK4KtdGMrvdhKP7YyhK2MYCtj2MoEtjKFrcxiK/PYyhK2soKtrGErAWwliK2EsJUH2EoYW4lgK4+xlafYSgxbOcBWnmErh9jKEbaSwlbS2MprbOUNtvI1tpLDVt5hK99iKwVspYit/ICtfMBWfsJWzrCVX7D/qGB/qWB/rmA/VLClCvb7Cva7CvZ9BZuvYL+pYE8q2LcVbKaCfVXBHlewLyrY5xVsooI9qGD3K9i9CjZawT6qYB9WsOEKNlTBblWwGxXsegW7WsEuV7ALFexcBTtTwU5WsOMV7EgFO1zBDlawvRVsdwXbWcHaCmIrdNsKvbZCv60wZCuM2ArjtsKkrTBlK8zaCvO2wpKtsGIrrNkKAVshaCts2wo7tsKurRCxFR7bCk9thZitELcVntkKh7bCka2QshXStsIrWyFjK2RthRNb4Z2t8K2tULAVirbCD7bCj7bCT7bCz7bCL7ZCxVao2F+o2J+p2A9UbImK/Z6KPaVi31Ox31Kx31CxX1OxWSo2Q8W+omKPqdgXVGySik1QsQdU7D4Vu0fFPqFiH1GxD6nYXSo2RMUGqdgAFbtOxa5SsctU7CIVO0fFzlCxU1TsBBU7RsUOU7GDVGwfFdtDxXZRsfbLV1eTk1/OxOO823DWazjrt5wPWs6HDeejhvNxw9mU4XzGcD5nOF8wnC0bzlctZ+uWs03D+ZbhPGQ4CxvOHhrOI5bzqOHsqeF833AWN5wlDGfPDWdHhrNjw1nacP7KcPbWcv614TxnOXtnOcsbzgqGs1PD+Q+G85LhvGw4/9nw87mlfG748cxQOjcUzwzfnRu+PTfkzw25M8PX54a3Z4bX54ZXZ4bjc8OLc0PyzJA4txycWfbPDHtnhifnhkfnhodnhvC5IXRu2TozbJ4bNs4Ma+eG5XPD4rll/twyc2aZOjNMnBnGzg3D54bBM0PfuaH3zNB9ZpAzi/k/ujDnPdjzPuz5AOZsCHM+ijkfx5xPYs6nMWezmPMFzPkS5nwVc7aOOQ9gz7ewZyHM+QPM2S7mPII5i2LPnmLP9rHnccz5M+z5c8zZEeb8GHOWxpy/wpy/wZxlMecnmPNvMGd5zFkBc3aKOf8Bc/Yj5qyMOfsZ8/M55qczzI9nmNIZpniG+e4c8/4Mkz/HfHOO/foc8/YM8/oc8+oc8/Ic8+IckzzHJM4wB2eY/TPM3jnmyRnm0Tnm4TkmfI7ZOcdsn2E2zzAb55i1c8zyOWbxDDN/hpk9w0yfYybOMWNnmOEzzOA5ZuAc03uO6T7HdJ5h7DnWnNNlzukxZ/SZMwbMOUPmnFFzxrg5Z8qcMW3OmTXnLJhzlsw5K+aMdXNGwJyxZc4ImTN2zBm75oyIOSdqznlqz4iZM+LmjGfmnOfmnCNzxrE546U555U54405I2vOyZkz3plz8uacgjmjaM74wZxTMmd8MGf8ZM44M2ecm584Mz9yZkqcmSLn5pRz855zk+fcfMOZ+Zoz85Zzk+HcpDk3LzkzLzg3Sc7NIef2gHOzz5nZ49w84dw85sw85NyEOTc7nNltzkyQM7PBuVnj3CxzZhY5Mwucm1nOzTRnZoJzM8a5GeHcDHFuBjg3vZybbs5NJ+fGfvnqt8nJLx/Eo9xtKPcYyn2G8oChPGwojxjKY4bypKE8bSjPGcrzhvKSobxs+LBmKG8YypuG8pbhw46h/MBQfmgoPzJ8iBrKTw0fYoYPB4ZywvDhueHDkaGcMpTThg+vDeU3hnLW8OHEUP7GUM4byu8N5e8M5aKhXDKUPxh+LBt+KBu+/2A4LRsKZcO3ZcM3ZcNJ2ZAtG96UDa/LhpdlQ6pseFE2PC8bEmXDQdkQKxv2yoboB8OjsuFh2RAuG3bKhu2yIVg2bJQNax8Mq2XDUtmwUDbMlg3THwyTHwzjHwyjZcNw2TBYNvSVDT0fDF0fDPaDwZQNpiyYD52Ycjem3IcpD2A+DGHKI5jyOKY8iSlPYz7MYsrzmPIipryC+bCGKa9jypuY8hamHMJ8CGPKu5gPjzDlKObDU0w5hinHMeVnmPIhpnyEKacw5WNM+RWmnMGU32LKX2M+5DAf8pjye0z5FFMuYj78gPnwI+ZDGfNjGfP9B8xpGfP+AyZfxnxTxpyUMdky5k0Z8/oD5mUZkypjXpQxz8uYRBlzUMbslzF7ZcyTMuZxGfOwjAmXMTtlzHYZEyxjNsqYtTJmtYxZKmMWypi5Mma6jJksY8bLmNEPmOEyZrCM6S9jesqYrjKms4wxZcSU6TJlekyZPlNmwJQZMmVGzQfGTZlJU2bKlJkxZeZNmUVTZtmUWTMfWDdlNk2ZLVMmZMo8MGV2TZmIKfPYlHlqPhAzZfZNmWemzKEpc2Q+kDJljs0HXpkPvDFl3poyX5syOVMmbz7w3pT5zpQpmg/8YMr8aD5QNh8omx/5YL7ngznlgylQNt9SNu8omxPKJkvZvOGDeU3ZvKRsjimbF3wwzymbBGVzQNnsUzZ7fDBPKJvHlE2EDyZM2exQNtuUTZCy2aBs1imbVcpmibJZoGzmKJsZymaKshmnbEYpm2HKZpCy6adseimbbsqmk7IxX766mJz88oN4lLoNpR5Dqc9Q6jeUhgylEUNpzFCaNJSmDKVZQ2neUFo0lJYNpVVDacNQChhKQUMpZCjtGEq7hlLEUHpsKD0xlJ4aSvuG0oGhlDCUkobSkaGUMpReGkoZQ+mNoZQ1lE4MpW8Mpbyh9N5Q+s5QKhpKPxh+KBmKJcNpyfC+ZPi2ZHhXMnxdMmRLhjclw+uSIV0ypEqGFyXD85LhsGR4VjLslwx7JUO0ZHhUMkRKht2S4UHJsF0ybJUMmyXDWsmwWjKslAxLJcNCyTBbMkyXDJMlw1jJMFIyDJUMAyVDf8nQWzJ0lwy2ZDAlgykJptSFKfVgSn2Y0gCmNIgpjWBKY5jSBKY0hSnNYErzmNIiprSMKa1gSmuY0gamFMSUtjGlHUwpjClFMKXHmNITTOkpphTDlA4wpQSm9BxTOsKUUpjSS0zpNab0BlPKYkonmNI3mFIeU3qPKX2HKRUxpR8wpRKmWMKcljCFEiZfwnxTwnx9700J87qESZcwqRLmRQmTLGESJcxBCRO796SEeVzCREqYcAnzoIQJlTBbJcxmCbNewqyWMMslzFIJM1/CzJYw0yXMZAkzXsKMljBDJcxACdNfwvSUMF0lTGcJY0pYU6LTlOgxJfpMiX5TYtCUGDElxkyJCVNiypSYMSXmTYlFU2LZlFgxJdZMiYApETQltk2JHVMibEo8NCUemRJPTIk9UyJmSuybEglT4rkpcWRKpEyJl6bEK1MiY0q8NSW+NiW+MSXypsR7U+I7U6JoSvxgSpRMiZL5npI5pWQKlMy3lMw7SuaEkvmaknlLybymZNKUTIqSeUHJJCmZBCVzQMnEKZkYJfOUkolSMo8omYeUzANKJkTJbFMyAUpmg5JZo2RWKJklSmaBkpmjZKYpmUlKZoKSGaFkhimZQUqmn5LpoWS6KBmhZMyXryqTk1++sx6nXYZit6HYZyj2G4pDhuKwoThqOJ0wnE4aTmcMp3OG4oKhuGw4XTEU1wyn64bipqG4ZSiGDKcPDKdhw+lDQ/GxofjEcPrUcLpvKB4YignD6XPD6ZHhNGUovjQUXxuKb/52+rWhmDMU84bTbw3FguH01PBd0VAoGr4tGvKnhm9ODV+fGt4WDZmi4fWpIV00pIqGo1PD86LhsGh4VjTsnxpiRUP01PCoaHh4atgtGh4UDdtFw1bRsFk0bBQNa0XDStGwVDQsFA1zp4aZomGqaJgoGsaKhpGiYahoGCga+k4NPaeGrqLBnhrMqcEUDea0E3PajSn2Yk77MKdDmNNhTHEUUxzHFCcxxWlMcRZTnMcUlzDFZUxxFVNcxxQDmOIWpriNKe5gimFM8SGm+AhTjGJOn2JOY5hiHHOawBQPMadJTDGFOT3GFNOY4htM8S2m+DWmeII5fYcp5jHF95jTAub0FPPdKebbU0y+iPmmiDk5xWSLmMwp5nUR8/IUkypijoqY5Cnm8BRzcIrZL2JiRcyTIubxKebRKWa3iAmfYkKnmO0iJljEbBQx66eY1SJmqYhZOMXMnWJmipipImbiFDN2ihkpYoZOMQNFTF8R03OK6Spi5BRjihhTpNMU6TZFes0pfabIoCkybIqMmFPGzSmTpsiUOWXWnDJnTlk0RZbNKSumyLo5JWCKbJki2+aUHVMkbIrsmlMemSJRc8pTc0rMnBI3pzwzRQ7NKUlTJGVOeWmKvDJFMqbIW1Pka1PkxBR5Z4rkzSnvzSmn5pSiKXJqvqNovuXUfMup+YZTc0LRfE3RZO695NSkKJoXFM1zTk2CU/OMoolTNHsUzVOK5glFE6FoHlI0YYomRNFsUTSbFM0Gp2adU7PCqVnm1CxwauYomlmKZoqimeDUjHFqRiiaIU7NAKemj1PTQ9F0cWqEU2O+fHU+MfHlvacpdHoUug2FXkOh31AY8CgMeRRGPQpjHoVJj8KUR2HWUJjzKCwYCsuGwopHYdWjsGEobBoK24bCjkfhgUdh11CIGAqPDIUnHoWnHoWYoRA3vH9meP/cUDjyKKQ8Ci89Cq88Cq89Cm8Nha8NhZyh8I2h8K1HoeDxvmDIFwzvCoZvCh5fFzzevvfIFDxeFTxeFjxSBY+jguF5wXBYMDwrGPYLhr2Cx5OCx6OCR6RgCBcMDwqGUMEjeC9Q8FgrGFYLHssFj8WCx3zBMFMwTBcMkwXDWMFjtOAxXPAYKHj0Fzx6Cx7dBY/OgsEWDOb/6MQUujGFXkyhD1MYwLwfwiuM4BXG8QoTmMIUXmEGrzCHV1jEKyzhFVYwhTW8wgZeIYhX2MIrhPAKD/AKu3iFh3iFx3iFJ3iFPUxhH69wgFdI4BWe4xWSeIUXmMIxpvAKU3iNV3iDKWTxCjm8wju8Qh6v8C3mfQGTf4959x7zTQHv6wLmbQGTKWBeFfDSBbxUAXNUwHtewEsU8J4V8OIFvFgB70kBL1rAixTwdgt4Dwp4ofd4WwW8YAGzUcCsFTArBcxSAbNQwJsr4M0U8KYLmMkCZryAN1LAGyrgDb7H6ytgegt43QW8zgKefY9nChhTQEyBLq9Aj1egzysw4BUY8gqMmPeMmfdMeAWmvAIzpsCs954Fr8DSvVWvwIYpsOkV2PIKhEyBHVMg7BV46BV47BWIegX2TIF9r8CBVyDhvee5KZD0CrwwBY5NgbRX4LVX4K0p8LVX4MQr8I1XIG8KvDcFCqbAe/MtBe8d771vKJgTCiZLwbyhYF5R8NIUvBQF74iCl6RgEhTMMwomTsHEKJinFMxjCuYRBe8hBROmYHYomG0KZpOCt0HBrFMwaxTMMu/NEgVvnoI3S8GboeBNUvAmKHijFMwwBTNEweun4PVR8LopeJ0UPKFgzJevfpqY+PLOaPKdHvkuTb7HI9/nkR/wyA955Ec88mOa/IQmP6nJT2vysx75eY/8okd+2SO/4pFf0+QDHvmgR37bIx/S5B9o8rua/ENN/rEmH9Xkn3rk9z3yBx75hEf+0COf9Mi/8Mgfe+TTmvxrTf6NRz7rkT/xyH/jkc975POab/Kak7wmm9e8yXu8znuk8x7HeY9U3uMo7/E875HIezzLe8TzHnt5jyd5zeO8RyTv8TDv8SDvEcprtvKazbwmkNes5zWrec1y3mMx7zGf95jNe0znPSbzmom8ZiyvGcl7DOU9BvIefXmP3rxHd96jM68xeY2X13h5g5cXvHwXXr4HL9+Hl+9H54fw8iN4+VG8/Dg6P4HOT+PlZ/Hy83j5Bbz8El5+BZ1fQ+cD6PwmOr+Flw+h8w/Q+V10PoKXf4zOP0Hn99D5fXT+AJ1/hs4fovNJvPwLvPwxXv4lXv41Xv4NXj6Llz9B53Po/Du8fB7vXR4vl0d/nUe/yeO9zuOl83gv83ipPN5RHu95Hi+RxzvI4+3n8WJ5vKd5vMd5vEd5vId5vAd5vFAebyuPF8zjbebx1vN4q3m85TzeYh5vPo83m8ebzuNN5tHjefRYHj2SxxvK4w3k8fryeL15dHce3ZlH2zzay2O8POLl6fLy9Hh5enWefp1n0Msz7OUZ8/KMe3kmvTzTXp5ZL8+cl2fBy7Ok86zoPOteng0vz6aXZ8vLE9J5Hnh5wl6eiJfnkZfniZdnz8uz7+U58PI88/IcenmSXp4XXp5jL0/ay/Pay/NG53mr83yt8+S8PO+8PHkvT957R17nyOuvyXtvyevX5HWavH5J3kuR947Ie8/Jewny3gF5L07ei5H3npL3ouS9CHnvIXkvTN7bIe9tk/eC5L0AeW+dvF4jr1fI60XyeoG8N0teT5PXU+T1BHk9Tt4bIe8NkfcGyes+8rqXvO4m73WS9yx5z/vy1Y8TE19OPE1OPHJdmpMej1yvx0m/JjeoyQ1rcqOa3JgmN6HJTXnkZjS5Oc3Jgia36JFb9sitanLrHrmAR27T42RLcxLSnDzQ5HY1uYgm90iTi3rknnjk9jxycY/cgSaX8Mg998gdeZy80OReak5eeZy89si98chlPXInHic5TTaneZPTvM5pXuU0L088UjmPo5xHMudxeKJ5lvOI5zSxnOZpThPNaR6faCI5j92cx4Ocx07OYzunCeY0gZxmPadZy2lWcprFnMdCzmM+5zGb85jOeUzkNOM5zWhOM5zzGMx59Oc8+nIePScenScektOYE43OaXTOoHMWL9eJl+vBy/Xi5frQuUG83DDeyQhebgydG8fLTaFPZtAnc+jcPF5uES+3jM6tok/W0LkAOreJl9tC50Lo3AN0bhcv9xCde4Q+iaJzT9EnMXRuH507QJ8k8E6e4+WO8HIv8HIv8XKv8HKv8XJv0Lm36NzX6FwOnc2h356gMzn0qxzeyxxeKoc+yqGTOfRhDv0sh47n0LEcei+HfnKC9ziHjuTQuzm8cA5vJ4fePkEHT9CBHN5GDm/tBG8lh7eYQy/k0HM59GwOPZ1DT+TQ4zn0aA49coI3lMPrz+H15fB6TtDdJ+jOHNrk0N4JxsthdY5OL0e3l6NX5+jXOQa9HMM6x4jOMaZzTHg5prwTZrwT5rwc816OBX3Csj5h1cux5p2w4eXY1Dm2dI6QPmFH59jVOR56OR55OR57OZ54Ofa8HPtejgMvxzPvhEMvx5GX44WX46WX45U+4bU+4Y3O8dY74WvvhJzOkdNfk9NvyekMJ/oVOf2SnE6R0y/IeUlO9CE57xk5L07Oi5Hz9sh5TzjxHpPzHnHiPSTnhTnxdsh5IXLeFjkvQM7bIOetcaJXyOlFcnqBnJ7nxJslp6fI6QlyepwTPcaJN8yJN0jO6yen+8jpHnK6m5zXyYlnOPG8L199Pz7+5a12yYom26l526PJ9miy/ZrsgCY7pMmOaN6OarITmuyUJjutyc5qsnOa7KImu6TJrmiya5q365psQJPd1GS3NNkdTfaBJvtQk32kyT7WZJ9osk812Zgmu6/JHmiyh5rsc002qcmmNG9fat6mNdnXmuwbTTareZvVvMlqMllNOqt5mdWkspqjrCaZ1SSymoOsJp7VxLKavawmmtU8ymoiWc1uVhPOakJZzVZWE8xqAlnNRlazltWsZjXLWc3CW818VjOb1cxkNVNZzXhWM5bVjGQ1w1nNYFbT91bTm9V0ZzVdWY1kNV5Wo7ManfXQWYvOdqKz3ehsDzrbh84OoLND6OwIOjuKzo6hs5Pot9PotzPot3Po7AI6u4TOrqCzq+jsOjobQGeD6Ow2+m0InX2Azu6isxF09jE6G0Vnn6KzMXQ2jn77DP02gc4+R2eP0NkUOvsSnU2js6/R2Td/e5tFv8miM1l0Oot+mUWnsuijLDqZRR9m0c+y6HgWHcui97LoJ1n04yw6kkXvZtHhLHoni97Oorey6EAWvZFFr2XRq1n0cha9kEXPZ9GzWfRMFj31Fj3+Fj2WRY9k0cNZ9GAW3Z9F92bRPVl0VxZt36K9t2idxdNZrM7SqbN06yw9OkufzjKgswzpLMM6y6jOMq6zTOos0zrLjM4yq7PM6yxLOsuKzrKqs6zrLAGdJaizbOssIZ3lgc6yq7NEdJbHOktUv+WpzrKns8R1lmc6S0Jnea6zHOksKf2WlzpLWmd5rbO80Vne6ixZnSWr35DVGbL6FVn9kqw+JqtfkNVJsvqQrH7GWx3nrd4nq/fI6idk9WOyOkJWPySrw2T1Dlm9TVYHyepNsnqDrF4jq1fJ6mWyepGsnuetniWrZ8jqKd7qCbJ6jKweIauHyepBsrqfrO4lq3vI6i6yWshqQ1brL199Nz7+5bXnkhFNplOT6dZkejSZPk2mX5MZ1GRGNJlRTWZck5nUZKY0mRlNZlaTmddkFjWZZU1mRZNZ02TWNZmAJrN1b+feribzUJN5pMlENZmnmsyeJrOvyRxoMs80mUNNJqnJvNBkUprMS03mlSaT0bzOaF5lNC8zmlRG8yKjSWY0hxnNs3v7GU0so3ma0TzJaB5nNJGMZjejCWc0oYxmO6MJZjSBjGY9o1nLaFYymuWMZjGjWcho5jKamYxmOqOZzGjGM5rRjGYkoxnKaAYymr6Mpjej6c5oOjMam9F4GY3OaHTGQ2csOtOJznSjMz3oTB8604/ODKIzI/fG0JkJdGYKnZlBZ2bRmXl0ZgGdWUJnVtCZNXRmHZ0JoDNBdGYLnQmhMw/QmV105iE68widiaIzT9GZPXRmH505QGeeoTMJdOY5OnOEzqTQmZfoTBqdeY1+nUG/yqCPM+hUBv0ig05m0M8z6Gf34hl0LIN+mkE/yaAfZdCRDHo3gw5n0KEMejuDDmbQgQx6PYNey6BXM+ilDHoxg17IoOcy6JkMejqDnsygxzPo0Qx6JIMeyqAHMuj+DLo3g+7OoDszaJtBexm0zuDpDFZn6NQZunWGHp2hT2fo1xkGdYZhnWFEZxjTGSZ0hkmdYUZnmNUZ5nWGBZ1hUWdY1hnWdIZ1nWFDZwjqDNs6Q0hneKAz7OoMD3WGiM4Q1Rme6gx7OsO+znCgMzzTGRI6Q1JneKEzpHSGY50hrTO81hkyOkNGvyaj02T0MRn9gow+IqMPyehn9+Jk9D4ZvUdGPyGjo2R0hIzeJaPDZPQOGR0io7fI6CAZvUFGr5HRq2T0Mhm9SEYvkNFzZPQMGT1NRk+S0eNk9CgZPUJGD5LRA2R0PxndS0Z3k9GdZLQlow0Zrb989X58/MtL7ZI2mrRo0l2adM+9fk16UJMevjemSU/cm9KkZzTpOU16QZNe0qSXNekVTXpNk17XpAOadFCT3tKkQ5r0jia9ey+iST/WpKOa9FNNOqZJ72vSB5p0QpN+rkknNekjTfpYk05rXqY1x2nNi7QmmdYcpjWJtObgXiyteZrWPElrHqc1j9Kah2nNblrzIK0JpTXbaU0wrdlMazbSmtV7S/fm05rZe9NpzVRaM5HWjKU1w/cG05r+tKYvrelOazrTGrnnpTX6/zDotKDTXeh0Nzrdg073odMD6PQgOj2CTo+i0+Po9AQ6PYVOz6DTc+j0wr0ldHoFnV5Fp9fR6QA6vYlOb6HT2+j0DjodRqd30ekIOv0YnY6i00/R6Rg6vY9OH6DTCXT6OTqdRKeP0OkUOv0S/TKNPk6jX6TRR2n08zQ6kUY/S6PjafR+Gr2XRj9Jox+n0Y/S6EgavZtGP0ijQ2n0dhodTKM30+iNNHr13nIavZRGL6TRs/em0+jJNHo8jR5Lo0fS6OE0ejCN7k+je9Po7jS6K43uTKNtGm3SaP03o9OITtOl03TrND06TZ9OM6DTDOk0wzrNqE4zptNM6DSTOs2MTjOr0yzcW9RpVnSaVZ1mXafZ0Gk2dZotnWZbp9nRacI6za5OE9FpHus0UZ3mqU4T02n2dZoDnSah0zzXaZI6zZFOk9JpXuo0aZ0mrY9J6xRpfURaPyetE6T1M9L6gLTeJ633SOunpHWUtH5EWkdI613S+gFpHSKtt0nrIGm9SVpvkNar95ZJ6yXSepG0niOtZ0nradJ6irSeJK3HSevhe4OkdT9p3Udad9/rIq2FtDaktf7y1TdjY19SruLYc0lZl1Sny3GXy3GPy3Gvy3G/y/GgS2rYJTXikhp1SY27pCZdjqdcjmdcUnMuqQWX1KLL8ZLL8YrL8apLat0lFXBJbbocb7kcb7ukdlxSYZfjXZdUxOX4kUsq6pJ66nK853K873Icd0k9c0kduhw/dzk+cjl+4fLi2OXo2CV57HJ47JJIuRwcu+wfu8RSLk+PXZ4cuzxOuUSOXR6mXMLHLg+OXULHLlvHLsFjl8Cxy8axy1rKZSXlspxyWUy5LKRc5o5dZlIu0ymXyZTLxLHLWMpl5NhlOOUymHIZSLn0Hbv0pFy6Uy6dxy5y7GJSLjrl4qZc3JTGTXm4KYub6sQ97sJN9eAe9+Ie9+OmBnFTQ7jHw7ipUdzjcdzjCdzUFO7xDO7xLG5qHje1iJtawk0t46bWcFMbuKkAbmoTNxXETW3jpnZwjx/gpnZxUxHc1CPcVBQ39RT3eA/3eB/3+AD3OIE+PkSnnuMeH+Eev8B9kcI9SuEmU7iHx7iJY9yDFO5+CjeWwt1L4T45xn18jBs5xn2Ywt1N4T5I4YaOcbdSuMEUbiCFu5HCXUvhrhzjLh/jLh7jLhzjzqVwZ1K40yncyWPciWPcsRTuyDHuUAp3MIU7kMLtS+H2pnC7U7idKVxJ4ZpjXJ3CdY/R7jGee4x1U4h7TLebosc9ptc9pt9NMeAeM+SmGHZTjLopxt0UE26KKTfFtJti1k0x7x6z4B6z5KZYdlOsuinW3RQbbopN95igm2LbTbHjHvPATbHrHhNxUzxyU0TdFE/dFHvuMfvuMXH3mGduikP3mOduiiM3xQv3mGP3BSn3iJSb5Nh9zrGb4Ng9IOXuk3JjpNw9Uu4Tjt0ox+4jUu5DUm6YlPuAlBvi2N0i5QZJuQFS7gbH7hrH7gopd5mUu0jKXeDYnSflzpJyp0m5kxy7Exy746TcUVLuMCl3kJQ7wLHbR8rtIeV2c+x2cuwKKddw7HqkXPfLVydj41+eK0XSKJJWkexUHHUpjnoUyT5Fst/laMAlOahIDiuSI4rkmCI5oUhOKpLTiuSsS3JOkZxXHC0okkuK5LIiuapIrrskAy7JTUUyqEhuuyRDLkcPXJK7imREkXzkkoy6HD1xOXrqktxTJPcVyQOXo2cuR4eK5KHL86Ti8Mjl2ZHLQdJlP+myl1Q8PVJEk4po0iWSdHmYVISPXB4kXXaSLqGky1ZSsXmkCCQV60cua0nFSlKxlFQsJhXzScVcUjGTVEzdm0y6jCVdRpOK4aTLUFIxmFT0JxW9SZeepEt3UtGZVEhSYZIKnXRxkwqVdHGTHippUUlBHXWhkt2oZA/qqA83OYCbHMRNDqOSI6ijMdzkOG5yEjc5hZucQSXnUMkF3OQibnIJlVzGTa7iJtdxkxu4yQAqGUQlt1FHO7jJ8L1dVDKCSkbvPcFN7qGS+7jJA9zkM9xkAjd5iPs8iTpMop4lcQ+SuPEkbiyJeppEPUmioknUoyTq4REqnEQ9SKJ2kqjtJCp4hLuZRAWSqPUkai2Ju5LEXTpCLR6hFpKo+STubBJ3Ook7lURNJFHjSdzRI9RwEncoiTuYxO1P4vYmcXuSqO4kqjOJkiTKHKF0EuUm0eoITyWx6ghRR3SpJN0qSY9K0ucmGXCPGHSTDLlJRtwkYyrJuEoyqZJMqSQzKsmsOmJeHbGgkiypJCsqyZpKsq6SbLhJNt0kQZVkWx0RUkkeqCRhN8lDN0lEJXmkkkRVkicqyZ5KEnOTxN0jnqkkCZXkuUpypJIk1SFJ9xlJ94CkinPkxkiqPZLqCUkVJakec6QiJFX4b+4OSTdE0t3mSAVJqgBJd4Oku8aRWuXIXSapFkm6CyTdOY7UDEk1TVJNkVQTJN1xjtxRku4wSTVEUg2SdPtJql6Sbg9J1U1SdXLkWo5cj6SrSbrul6/ejo1+eab8JLTi0CgORXHYqTjsVhz2KA77FIcDisNBxeGQ4nBYkRhVJMYViQlFYkqRmFEczioSc4rEgiKxqEgsKxIrisSq4nBNcbihSAQUh0HF4bYiEVIc7igOw4rEQ0Uiokg8vvdEcbinSMQUh3HF4bO/JQ4Vzw4VB4eK2KHi6aHiyaHi8aHicUIRSSh2E4rwoWIn8bfthGIrodg8VAQOFRuHitVDxfKhYimhWEwoFg4Vc4eK2UPFdEIxlVBMJhTjCcVYQjGSUAwlFEOHiv5DRe+hoieh6Eooug4VnYcKk1B4hwo3oVCHCnXoohIadWhQCYtKCCrRhUp0ow57UYd9qMQAKjGEOhxGHY6iEmOoxDgqMYk6nEIlZlCJWVRiHpVYRB0uoRIrqMQqKrGOSmygDjdRia17IVRiB5UIoxIPUYkI6vAR6vAxKvEEldhDJWKoxD4qcYA6fIY6TKAOEqh4ArWfQMUOUU8TqGgC9TiBepRARRKo3QTqQQK1k0BtJ1Bbh6jNQ9RGArVxiFo7RK0mUMuHqIXE3+YTqLkEaiaBmkrgTiZQEwnUWAI1cogaSqAGE6iBQ1TfIarnENWVQHUdouQQZQ9R3iHKTaBUAlcl0OoQoxLIvS6VoFsl6FUJ+tUhA+qQQXXIsDpkRCUYvzepDplSh8yoQ+ZUgnmVYFElWFIJVtQhqyrBmkqwoRIEVIKgSrCtEoRUgh11SFgd8lAd8lAleHzviTpkz00Qcw+Jq0MOVIJnKkFCJUioZxy6BxyqfRIqxqHa41A94VBFSahHJFSEhHrIoQqTUDsk1DaHaouECpJQG/fWSKhVEmqZQ7XIoVrgUM1zqGY5VNMk1BSHapJDNU5CjXHojnCohjlUQyTUAAnVR0L1cKi6OVRdJJRwqCwJZThUmkOlvnz1emz0S0z5OXAVcU+xbxVxUcS7FPFuRbxXsd+vOBhQxAcVB8OK+Ihif1QRH1fEJxT7U4r4zL05xcG8Ir6oiC8pDpYV8RVFfFURX1fsbyriQUV8SxHfVuzvKOIPFAdhRfyh4uCh4uCRIh5VxJ8oDp4q4jHF/r4ivq+IxRVP44oncUU0rngcVzyKKyJxRXhfET5Q7MQVobhie1+xGVdsHCjW439bO1CsHCiW44rFfcX8vmJuXzEbV8wcKKbiism4YjyuGD1QjMQVw3HF0L5iIK7o31f0HSh6DhRdB4rOuEIOFPZA4cUVblyh4goVd1EHGhU3qH2LOhBUvBMV70Yd9KLifah4P+pgABUfQh2MoOJjqP1xVHwCFZ9EHUyjDmZQ8VlUfA4VX0AdLKHiy6j9FdTBGupgHRUPoA6CqPgWKr6N2n+Aij9AxcOo+ENUPII6eISKR1HxJ6j43t/2Y6h4HLUfR+3FUU/3UdF91OM4KnJvdx/1YB8ViqNCB6hgHBU8QAXiqI191HoctXaAWjlALcdRi3HUfBw1t4+aPUBN76Mm91ETcdT4AWo0jhrZRw3FUYNx1MA+qi+O6o2juuOorgNU5wFK9lF2H+Xto9w4SsVxVRyt4hh1gKg4og7oVPt0qX161AF9Kk6/2mdAHTCkDhhR+4yqOGNqnwl1wKQ6YErFmVUHzKl95lWcBRVnUcVZubeqDlhXBwTUAUEVZ0vF2Vb7hNQ+O+qAXXXArtonouJE1T6P3ThRN85TFSem9ompfeIqzr7aJ+4+5UA9Ia6i7KvHxNUj4uohcbVLXD3gQD0grkLE1TZxtUVcbbKvNoirdfbVKvtqhX21zL5aIq4WiKs5DtQsB2qafTVJXE0QV+McqDHiaoS4GiauhoirAQ5UPweqj7jqIa66iKtO4soSV4Z95RFXLnGlvnyVHhn58tTvEHP97Hl+9qziqSj2OhWxbsVer5+9PkWsXxEb8BMb9LM3rNgbUcTG/MTG/exN+IlN+Xk67Sc2o4jNKWLzfmKLfmJL91b8xFb97K37eRpQxAKKvaBib1sRC/mJ7fiJhf3EworYQz97jxSxR35iUT+xp3729v72JKaI7ike7/l5FPPzcM/P7p6fcMzPzp6fUEyxtecnGPOzuecnEPOzHvOzFvOzEvOzHFMsxRSLMcX8nmJuTzG752cm5mcqppiIKcb3FGMxxUhMMRxTDMX8DO4pBmKK3j1Fz56f7pifrpgfiSlMzI8X8+PtKVRMofb8f4u5+GMe/j2Ligkq1ok/1ok/1oOK9aFiffhjA/hjg6jYMP69Efx7o/hj4/hjE6jY1N/2ZlB7s6jYPP7YIv6ni6i9ZVRsBRVbxb+3jtoLoPY2UXtB1N4W/lgI/94O/tgDVCyMij3EvxfBv/cY9TSKP/YE/94eau8p6uke/id7+KN7qEd7qIcxVHgPFY6hHuyhQnv4t/bwB2P4N/fwb8Twr8dQ63uolRhqOYZaiqEWY6j5PdRsDDW7h386hn8yhprcQ43H8I/F8I/E8A/voYb2UIN7qP49VF8M1RvD372HvyuGX2Iou4cyeyi9h3L38KsYSsXQKoZRMax/D/HH6FR7dPv36PXH6FV79Pn3GPDHGPTHGPbvMaJijPr3GPfHmFAxplSMaRVjxr/HrD/GvD/Gov8pS/49lv0xVvwxVv0x1lWMDRUjoGIE/Xts+fcI+WPsqBgP/Hvs+mM89O8RUTEeqRhRtccTtcdT/x4xf4w9/x4xf5SY/zEx/yP21ENiapc9FSamHhBTIWL+EDH/FjH/JjF/gJjaIOZfY0+t3FtmTy2y518g5p9jzz9LzD9DTE2xpyaIqXFiapSYf5SYf4SYGibmH2TP30/M30fM38ue6uap6iKmOon5LTG/Yc/vEfO7xPzqy1epkeEvj50Ooq6fqOcnau6Jn2inn2i3n2iPn2ivn2i/n+ign+iQn+iwn+iIn+ion+iYn+iEn+ikn+iUn+iMn+isn+i8n+iCn+iin+iSn+iyn+iqn+i687eAQzToJ7rlJ7rtJxryE93xE33gJ7rrJ/rQTzTiJ/rYIRp1iEb9PI76eRT1E4n6Cd97EPUTivrZjvrZivrZjPoJRP1sRP2sR/2sRv2sRP0sRf0sRv0sRP3MR/3MRv3MRP1MR/1MRf1MRP2MRf2MRv2MRP0MR/0MRf0MRP30R/30Rf303OuK+pGoHxv1Y6J+vKgfN+qgog7+qIM/6scfVfijGn/U4I9a/FHBH+3EH+3GH+2514c/2o8/Oog/OoQ/Oow/Ooo/OoY/OnFvCn90Gn90Fn90/t4i/ugS/ugy/ugq/ug6/ujGvU380S380W380RD+6A7+6AP80V380Yf4oxH80Uf4o9G/PboXieJ/GMUfjuLfieIPRfFvR/FvRfFvRvEHovg3ovjXovhXo/hXoviXovgXo/gXovjnozizUZyZKM50FP9kFP9EFP9YFP9oFP9IFP9wFP9QFP9AFH9/FH9vFH/Pva4ofonit1H8Jorfi+J3o/hVFL8/ivJHce95/ijGH8X6o3T6o3T7o/Tc6/NH6fdHGfRHGfJHGfZHGfVHGfNHmfBHmfRHmfJHmfZHmfVHmfdHWfBHWfRHWfJHWXairPqjrPmjrPujbPijbPqjbPmjbPujhPxRdvxRHvij7DpRHjpRIk6UR/4oj/1Rov4oUf9jov5HRP0Rov6HRP27RP0PiPp3iPq3ifq3iPqDRP2bRP0bRP3rRP1rRJ0Vos4SUf8SUf8iUf88Uf8sUf8MUWeaqDNF1D9B1D9G1D9K1D9C1D9M1D9E1D9A1N9P1Okl6vQQ9Xff6yTqt/c8on5N1K+I+v1fvkqODH956HQQcR0i2iHiOUSMQ8Q6RMQh0uUQ6XaI9DhEeh0i/Q6RwXtDDpERh8ioQ2TMITLuEJl0iEw5RKYdIjMOkTmHyLxDZMEhsugQWXaIrDhEVh0iaw6RdYdIwCGy6RAJOkS2HCLbDpEdh8gDh0jYIbLrEIn87WHEYTfi8CDisBNxCEUctiMOwYjDZsQhEHFYjzisRRxWIw4rEYfliMNixGEh4jAfcZiLOMxGHKYjDlMRh8mIw0TEYTziMBpxGIk4DEcchiIO/fd6Iw4997ojDp0RBxtxMPfciIOKOPgjDs7/oXAiGifi4UQMTsTiRAQn0oUT6caJ9OBEenEifTiRAZzIIE5kCCcyjBMZwYmM4UTGcSKTOJEpnMg0TmQGJzKHE5nHiSzgRBZxIss4kRWcyCpOZA0nso4TCeBENnEiQZzIFk5kGyeygxN5gBMJ40R2cSIPcSIRnN17DyI4OxGcUARnO4ITjOBsRnAC99YiOKsRnJUIznIEZzGCsxDBmY/gzEVwZiI4U/cmIzgTEZzxCM5oBGckgjMcwRmK4AxGcPojOH0RnN4ITk8EpzuC0xnBsREcc09HcFQExx/BcSL4nQjKiaDvGSeCdSKIE6HLidDtROhxIvQ6EfqcCANOhEEnwpATYdiJMOJEGHMijDsRJp0IU06E6XtzToR5J8KCE2HRibDsRFhxIqw6EdacCOtOhIATYdOJEHQibDkRtp0IO06EB06EsBNh14nw0IkQcSJEnIdEnF0iTpiIs3Nvm4gTJOJsEnEC99aJOKtEnBUizjIRZ5GIs0DEmSfinyPizBJxpok4U0ScSSLOBBFnnIgzRsQZIeIME3GGiDiDRJwBIk4fEaeXiNNDxOkm4nQScew9Q8TRRBxFxPETcfxfvkoMDX950NFB2HUIa4ewd886hMUh3OUQ7nbY7XbY7XEI9zqE+x3CAw7hQYfw8L1Rh/C4Q3ji3qRDeNohPOOwO+uwO+ewO+8QXnQILzmElx3CKw7hVYfw+r2AQ3jTIRx0CG/fCznsPnDYDTuEww4Pwg47YYdQ2GE77LAVdtgMOwTCDhu7Duu7Dmthh5Www3LYYSnssBh2mA87zN2bDTtMhR0mww4T98bDDiNhh+Gww9C9wbDDQNihL+zQc6877NAVdpCwg7nnhR102EGFHZz/F4UT1jhh757BCQtOuOteN064ByfcixPuxwkP4IQHccJDOOFhnPAoTngMJzyOE57ACU/i7E7jhGdwwrM44Tmc8DxOePHeMk54BSe8ihNev7eBE97ECQdxwts4u9s4uyGc8AOc3TBOOIzzIIyzE8YJhXG2wzjBXZzNME4gjLMRxlkP46yFcVZ2cVbCOMthnMUwznwYZ+7ebBhnOowzGcaZuDcexhkN4wyHcYbuDYZxBsI4fWGc3jBOTxinO4zTFcaRMI4N45gwjhfG0WEcFcbxh3GcvyknjHZ20U4YzwljnTDihOm61+2E6XHC9Dph+p0wA84ug84uQ06YYSfMqBNmzAkz7oSZcMJMOmGmnTAzTphZJ8ycE2beCbN4b9kJs+rssurssu6EWXfCbDhhNp0wQSfMthMmdO+BEyZ8b9d5wK6zQ9gJEXa2CTtBws4mYSfArrPBrrNO2Fkl7KzcWyLsLBJ25gk7c/dmCTvT7DqThJ2Je+OEnVF2nWHCzjBhZ4iwM0jYGSDs9BF2egk7Pew63ew6XYQdIexYwo4h7HiEHU3YcQk7fsKO8+Wr+PDwl+2ODkLKYcd12NEOIc8hZBxC4rDT6RDqutftEOpxCPU5hPodQgMOO4MOoeG/7Yw6hMbujTuEJh1CUw6hGYfQtENozmFnzmFn3iG06LCz6LCz5BBacQit3Vu/F3AIbTqEthxC2w6h0P/fVsghGHLYDDls3FvbcVjdcVjecVjZcVgKOSzsOMzvOMyFHGZDDrM7DtOhv02GHMbvjYUcRnccRkIOQyGHwXv9Ow59IYfekENPyKEr5NC149AZcrA7DmbHwQs56JCDG3LwhxyckIOz83/z4+y4OCHvnsEJWZwdixPquteNE+rBCfXhhPpxQgM4oUGc0BBOaAQnNIoTGrs3jhOaxNmZxgnN4IRmcUJzODuzOKGFe4s4oSWc0ArOzhpOaA0ntI4T2sAJBXB2NnF2gjihbZxQ6G/b94IhnM0QzkYIZ2MHZz2EsxrCWbm3vIOzEMKZvze3gzMTwpm+NxnCmQjhjO/gjIZwRu4Nh3CGQjiDIZyBEE5/CKc3hNMTwukO4XSFcDpDODaEY3b+pkM4bgjHf8/5m98J4Toh9D3PCWGcENYJ0XWv2wnR44Toc3bod0IMODsMOjsMOTsMOyFGnBCjTogxJ8S4E2LC2WHS2WHGCTHthJhxQsw6Oyw4IRacHRadEEtOiGUnxNr/w7oTIuCE2HR2CDohtpwQ206IHSdEyNm+FyTkbLLjbBBy1gk5a4ScVULOCjvOMiFniZCzQMiZv+8cIWfm3hQhZ5IdZ5yQM07IGb03wo4zTMgZYscZIOT0E3L62HF62XF6CDndhJwuQk4nIccScsw9Tchx7/kJOc6Xr2JDQ182O3wE/R0Ele9v2kfQ6yBoHILSQbCz4+92OQR7HII9PoJ9PoL9PrYGfASHOggOOgSHOwiO+AiO+giO+QhO+AiO+whO+ghOdRCccgjO+AjOdhCc87E17yO44CO45CO43PG3FYfgqo/gegfBjQ6CAYfgZgfBYAebQR+bQR+BoI+NoI/1YAerwQ5Wgz5Wgj6Wgw6LQR+LQR/zQYf5LYe5oMNs0GE62MFU0MdksIOJoMN40Mdo0MdosIPRYAcjWx0MBzsYDHbQH/TRH/TRu9VBT9ChJ9hBd7CDzmAHstWBDfowQQcd7MANdqDu+YM+nKCPjqCPjqCDL+jHF/TTEdT4gi6+oKYjaOkIWnxBuddJR7ALX7AHX7CPjmA/HcGBe0N0BIfpCI7gBEfpCI7iC47jC07gC07SEZyiIzhNR3D23hwdwXmc4AIdwSU6gsv3VvAF1+gIrt8L0BHcpCO4iW8ziC8QpCMQxNkI4qwH6Vi9txLEtxzEtxSkYzGIM/+3jrkgvtkgvuktfJNBfBNBOiaCdIwH8Y0F8Y0G8Q0H8Q0F6RgK4gwG6RgI4usP4usL0tEbxNcbxNcdpKPzngnimCAd3t8cN4hPBXFUEMcJ0uEEcTqC+H1B1D3tBPGcIMYXxPqCdPqCiBOk0wnS3RGkx7dFry9Iny/IQEeQoY4gg84WQ74gwx1BRp0g4x1BxnxBxn1BJn1Bpjr+NtOxxWxHkLmOIPMdQRY6gix1BFn2/W2lI8hqR5B1J8hGR5BAR5CAE2TTFyTYESTYsUnQFyDYESDYsU7QWSPorBL0rRDsWCboLBF0Fgk68wQ75gl2zBHsmCXomybom/xbxwRB3zjBjtF7wwQ7Rgj6hgj6Bgl2DBDs6CPo6/1bRy9Bp5tgRxdBn9yzBH2GoM8j2KEJdrgEO/z3nC9fPR0c/LLxv/4XAcdHwO8joO65PgLaR8D4CFgfAbnX6SPQ7SPQc6/PR2DAx8aAj8Cgj8CQj8Cwj8CIj8DYvXEfgQkfgUkfgSkfgWkfgRkfgVkfgTkfgQUfgUUfgSUfG8s+Ass+Aqs+NtZ8BNZ9bGz4CAT+thHwsR7wsRbwsXJvOeBjKeBjMeBj/t7sxt9mAj6mAz4mAz4mAj7GN3yMBf42EvAxfG8o4GMw4KM/4KMv4KM34KMn4KPrXmfAhwR82A0fJuBD33MDPlTAhz/gwwn46Aj48AV8+AId9/z4AgrfhosvoO95+AL2Xue9bnyBHnyBXnyBPnyBfnyBAXyBQXyBIXyB4Xuj+AJj+DbG8QUm8AUm8QWm783gC8ziC8zhCyzgCyziCyzhCyzj21jBF1jFF1jDF1jHF9i4F8C3fm8tgG81gG9lA99yAN9SAN9iAN9CAN/cBr7ZwN9mAvimA/gmA/gmAvjG740F8I0E8A0H8A0F8A3e6w/g6wvg6w3g67nXFcDXGcAnAXz2nhfApwP43AA+dc8J4OsI4PP9rcMXwO8LoHwBXF8A7Qvg3bO+AOIL0Hmv+16PL0CfL0C/L8CAL8CgL8CQL8CwL8CIL8CoL8CYL8C4L8DEvSlfgGlfgBlfgFnfBnO+AAu+AIv3lnwBln0BVn0B1nwB1u9t+AIEfAECvg0CvvV7awR8KwR8ywR8SwR8iwR8C/fmCPhmCfhmCPimCfgmCfgmCPjG740R8I0Q8A0T8A3dGyTg6yfg6yPg6yXg67nXRcDXScAn9wwBn0fApwn4XAI+RcDnJ+BzCPg6CPh8X756PDj4ZfV//S/WHB9r/r+tKh9rro9V7WPN/G3V+FgTH2td97p9rPX4WOv1sdbnY7Xfx9qAj7VBH2tD90bujfpYG/OxNu5jbcLH2uT/r516WU5d6cJ0bdAhlSMzQYA4CSRkMEyOC5amiYqK2LFjR+z7v5fquXpvNZTYMtNr/RdQf+OJb4yRKYEtUo77wXE/Nt7PjvvFcf/L8X5t3P92vNeO99+O93fH+91xf2/q33dH/e74+91xuzuud8dfd8fFO90dx3fH4d2xvzd23vbd8XZvrO+Oylu9N4p3x+LeyL3Z3TG9OyZ3x+TdMX53jO6OQUt6d/TuDvdN30tx7wPcfYC7D3H3DHcf494nuPsUd5/j3udN3he4+xJ3L3D3EndfeRXu/oq7r3H3De79DXff4u473P0X7r7H3Q+4+xF3P+HuZ9z9grv/hbtfce833PvfuHuNu//23nH3O+79jvt9x9V33N933PUdd73j/nrHXbzTO+5wb+y9nbe9497uuM0dt77jXu+46o5becUdt7zjFndc7s3uuOk7bnJvjO+40R03vOMGd1zq9e44945zd5y703Pv9N2d1N0ZuDtDd2fk7mTuztjdmbg7U3dn5u7M3Z3c3Vm4O0t3p/BW7k7l3nl1d17dnbW7s3F33tydrbuz8/buzsHdObp3ju6ds7tzcXf+cu9c3Z2bu3Nz79Tund/uzrt3d3fu7p27+83d1dzd396Vu/uLu7twd2fu7sTdHbm7A3e393belrt789bc3St3V3F3K+6u9Jbc3YK7y70Zdzf1Jry7MXc34u6G3oC7S7m7nuc+Xv7/qvr421pqZ6n7ljr1BpZ6aKkzSz32Jo7fU0c9s9Rz11g46oWlLhx1YalLS72y1JWlXlvqtWtsHL/fHPXO++Wo94764KhPtnG21BdL/Zelvno3S/23pa4bf9eWm/dXbfmrdlxqy/l341RbDrVlX1t+1ZZftWNXO95qy8Zb/3asf1tea8vKK2tLUVsWtSOvLXltmdeWaW2Z/rZMflsmtWVcW7LaMqwtAy+tLf3a4mqHra3nsHUPW/exdYqtU1w9wNVD7O8MW2fYeoytJ9h6iq1nXu4tsHXhldh6ha0rbL1u2WB/v2HrnfcLW++x9QFbn7wztr5g67+8K/b3Dfv7b2xde7+xf9fYW4271ti/auylxp5r7Pk39vQbe6ix+xr7y9vV2Lcau/HWXlVjV15ZY4sau6ixuTevsbMaO62xkxo3qbHjGpvV2GGNHXhpje3XWOfZ31j7G2dr+q6RupqBrRnamsw1xq5mYmumtmbmama2Zm5rcluzsDWFV9qala2pbM26ZWNr3mzNzvtla/a25mBrTvY3J1tzdjUXW/OXd7U1f3v1p7+9G7W9ehfvTG1P1PZAbffUbk9tf1HbHbV9o7YbftsNv+2a2q75bV+pbUVtV9S2pLYFv92C2i6obU5t59R2Rm2n1HbijaltRm2H3oDaptS27/Worft4+f+q1cfVGq49y7VvufUt17RxG1huI8t1ZLlm3thynVquM8tt5rjNHbfccls4rkvLtbBcS8uttFxXlmtlub56G8t147huHyy3neW6t9z2luvBcjtarkfL9WS5ni23i+V2tVy9v27e1XK6Wo7e4WrZ3xq7a+Pt1theLZurZX21vF4t1c1SXS2rq6W8WoqrZXm15DdLfnXkV8v8ZpldLdOrZXK1TG6W7PpleLMMrpb0ZulfLf2bxd0s9mqxn+mwtz72ln5y1wH2NsJeM2+MvU6wtyn2OsPeZtjbHHvLsbdF47rEXgvstcReV95r47bGXtfY27Zx3WKvO+x1j73tsdcD9nr0TtjbGXu7NK5X7PWG/euGvdyw5xv2fMWebtjjFXu4Yg837P6K3d2wuyv27YZ9u2I3N+z6hl1fsdUVW92wq2ujuGLLK3Z5xS68+Q07v2JnV+z0hp1eseMbNrs2Rlfs8IYdXBvpFdu/Yt0N665Ye8XaG85e6dsbfXul764M7I2BvTFyVzJ3JbM3MntjbK9M3ZWZvTKzN+b2xtxeWXhLe6WwV0p7pbQ3Knvl1d5Ye2/2+mlnr+zsjV/2yqHl6J3tlYu98pd3szeu9srVXrjZM1d79nnkZg9c7YGr3XO1O252x81tudo3rvaNm91wtRuu9tWruNqyZcnVLri6BVebc7VzbnbGzU652Qk3O+ZmM2/E1Q652QFXm3KzKTfb52p7XK3jZu3Hy/+7Wn2cRHN2lnPPcnHCuSdcUsM5FS5D4TI0nIeGcyacx8JlKpynhsvEcJnZxtxwzg3nheG8tFyWlkthuJSGy8pwqQyXV+G8No2N5fxmuLwJ561w3gmXX4bL3nLeG84H4XIUzkfhfBYuZ8P5bDldhNPFcLpYDhfL4Wz4dTbsLoZfF8PuYnm7GN7OhvXFsD4b1hfL68WwuhhWZ8vqYigvluXFsDwbFmfL4mLIL4b52TI7G2YXy8QbXwzjiyG7GEYXw/AsDC6G9GLpXSy9i8FdhN5FsGeDuRjMxWIuFrk47LmPvfSxlxR7STGXAeYy8jLMZYy9jDGXSeM8w5xnyGWOucwx5xx7WSKXAnMuMJcCey4x5xXmUmEvr8h5jVw2yOWtcd5izjvM+Rf2vMee95jLATkfsecT9nzCnC/I+Yw5nTHHC3K8IMcz5nDB7M/IrzP21xm7uyBvF8zbGdlckPUFWZ8xr2dMdUbKhikumPKCLM6YxRmzuGDyCzY/I9MLMj0j0zN2fMGOz9jsjMnOmNEFGVwwwzNmcMakF0z/gvQuGHfBuDPGnjHmgjUXenKhJ2f69kzfXEjNhYG9MDJnRnIhMxcye2Fsz0zkzNRcmNkLM3smt2fmciE3Z5b2zNKcKeyFwpxZyZlKzryaC69y4dWe2ciZNznzZi5szYWdvbAzF36ZC3tz4WAvHOTMyVw42QtnuXA2F87mzNmeuNgjF3vkLEcusudsf3GxO++Ni3njbDdczJqLrDmbNRepOJsVF1tysSVnU3A2iy8252xyLnbG2c642CkXM+FsJlxM1pARFzPkbAdczICz7XOxPS62z9k6LsZwFvPx8v+UxcdBNEdnODrh4IRjTzj2DIdUOKTCaWA4Dg3HoXAcCaexcBwbThPDaSKNmeE4MxznhmNuOOSG49JwWhpOheFUGk4r4Vh5r8JxbTisheObcHgTTlvhsDUcfxmOv4TTXjjsheNBOB2F49FwOAi/TsL+JOxPht3RsD0atifD9mB4Owrrk7A+Cq8HoToKq5NhdRLKk6E4GoqTYXkw5EchPxrm3uwgTI/C5ChMDsL4ZMhOwuhkGJ0Mw5NhcDIMjkJ6EnonQ+9kcEfBHQR7EMzRYE6myYNFjg5zdJhTDzn0kWMfc0gxpwHmNMScRl7mjTGHCeY4QQ4z5DhDjnPklGNOC+S4RE5L5LjEHAvkUCLHFXKskNMrclo3jm+Y4xvmuMUcd8hxh5x+Icc9cjhgDgfkeMQcj5jDEbM/IPsDsj9idifM9oh45u2ErI+N1wNSnZDqgFkdMOURKRpmeUIWJyQ/IvMjZn7CzA7I7IhMDsjkiIyPSHbEjE6Y0REzPGIGJyQ9YvoHTP+I6Z0w7oS4E8YdMebhgDUnnJxwcqQnR/rmQCoHUnNkYI4M5eCdyORIJkfGcmAiB6ZyZGqOzOXEXI7k3lIOLOVIIUdKObAyR1ZyopIjr3JkLUfWcmAjR97kxNY0dubELznxS44c5MTBnDjKiYM5cjQHjubAwew5mT1H+cVJdhxly8m8cZI3TrLmKGuO8srJVJyk4igVJyk5SsFJlpzMkqMsOUrOUeYczZyjzDjKjJNMOJoJBzPmYDJOJuNohhzMkIMMOMqAk6QcpM9RehxMj4NxHMVxEsNRzMfL/yyKj19as7fmizPse4Z9X9inwn7QMmrJhP1Y2E+8qbCfeXNhnwv7hbf0CmFfCvuVVwn7V2G/9jbem2G/FfY7Yf+r8Wvf2O2F7V548zZ7Ye297oVqb1jthdVeKL1iLyz3wqJlvhdm3tSb7IXxXsi80V4Y7oVBS9/reW4v2L1g9oJ8Y1scsu8h+z5mn2L2A8x+gOwHyH7UkiH7sTdB9lNkP/PmyD5H9gtviewLr0T2K69C9q/Ifo3s15j9BrPfIPs3zH6L2e+Q/Q7Z/8L82mN+7ZHdHtnukTdv3fK6R6o9svpiyj1S7JHlHlnsMYsmZb5HZt60YSZ7zHiPZN7IG7Ske6S/R3p7xHnWky9G9ljPyS96sqfvpbJn4A1lz8jLZM9Y9kzMnonsmcqemTeXPbnsWciehdmzlEYhe0rZs/Iqb+1tZM/G7HmTPVvZszN7fkljL7+8HXvZspc3b8Ne1t6rt2op2cvSW3wxc/Yy45fM2MuMvUzZy4S9jL2MvYy8gZc2TJ+96TXEecaTj5f/URQfbzpha4SdEXbW6wnbnrDra7bpg7AbCLthYzvyxsJ2rJucCNuZZjfTbGea7VwauWaba3ZLr/BKzW4ljaqxXQvbTWO3EXZbzW4rbHfC21bYeOut8LrTvG4b1U5Y7YRyKxQti61msdPk28Z8K8x3wmwnzLaa6U4z2QnjnZDtNKOW4VYz3GlSr7/T9LdCbyf0tpreVmO3GrMVzE6QnUa2Gr0VZGuQnUXvLHprka1Ddj1k20e2KbJr6N0AvR02diNkN0J2Y2T7MEFvp+jtFNnN0LsZejdHdgtku0B2S2S7RHZL9K5A70r0doXeVsinNbLdILs1stsg2zf0dos8vO2QzQ693qFft94OXe3Qqy1SeoW33CKLHZJvkXyHzHfIfIuebdHTHTLdIZMderxFxltk1NCjLTLcoYc79GCLpF5/i3Y7tNs27BaxO8TsENmhZYfWO0TvMLLF6B1O73DS6OstfdnRly2pbBnIjqHeMpQtIy+TLWO9Zax3TGXHVLbM9Ja57JjJjrnsyGVHrncsZctSb1nKlkK2lLJlpXdUsv30KjvWsmUtW95ky5vseNM7drJlJ2/s9BtbvWGr12z1K1v9yk6/spOKrazY6RU7XbDTBVtdsNVLtnrBTi/Y6ZydzNnJnK3M2MqUnUzZyYSdTNjqCVvJ2EnGTo/Y6SFbGTR0ylZSttJnK67FstWWnRi2WtiKfLy8F8uPdZKw0ZqN0aytsLaatWtsel6/sU4164GwGQjrobAZCZuRZp1pNplmM9ZsJpr1RLOZajYzYTMT1nPdyDWbhWbtbZaaTaHZlJrNSrNZCeuVsKk060qzfm1sXoX1RvO6EdYbYb0WqrVQbTSrtabcaIq1plxrio2w3AjLtWa51uQbzXytmW80s41mthEma2Gy0UzWmvFaM95oso0wWmuGm8ZgoxmsNelG0183emtNby24teA2GrvRmI3GbATZ6MZao9cavRZkY9Br67nGpofe9BvrPno9aGyGjfUQWWfoTYZsxsh6jF5PGpspejPz5shmjl7n6M3CW6LXRZObEr0ukXWJ3pTIukLWr8jmFe3JetN4XSPVGl1t0Kt1o9ygyzW6XCPFBr3cIIsNkq/R+RqZb5D5Bj3zpmv0ZI0ebxBPZxv0aIMerdFDb7BGDzbodI2ka3S/IW6Ddhu03aDNGm3WiGwQ2aBlg9YbRG8wssboDVZvcLLGyZq+/pLqNaneMNAbBrJhqBsjvWH8ac1Yr5nqDRPZMJU1U71mptfMvYXesNAblnpDIRsKvabUG2/NSq9ZyYZX2fCq1y0bNnrNWr+2VKz1io0u2UjBRko2umCtl6z1grVestE5Gz1jrec+Z6xlykZPWOsJaz1mrTM2krHRGWs9YqNHbPSQjR6wkZSNbqx1n7X0WGv3aaNNQwxrLay1/nipl8uPVZJQaU1lNJUkrKymsprKJlS9FqebTDVVmlANElZDTTXUVKOEaphQZS1jTTXRVJOEaqobs6QxT6hyTbVoWWqqZUJVaKrSWz0krKo2TVlpikqz9BZeXmnmVfJpViVMK8200kxWmkmVMKk040qTVQmjKmFUaUaVZrjSpCvNoGqklaZfaVyl6bW4SmM9qRLEp650i5CsDLoy6MqSVJakciSVRVcOXfXQlSOp+iSrlKRKSaoBukrR1YCkGqGrEXqVkVQZSTUmqTJ0NUGvpujqYebNSaocXeUk1YKkWqA/lV7hc0VSrdCrCr2qSFYVuvSWK/SyQi8rkkVD5xXJfIWer0jmFXpWoaetnFQkkwqdVeiRz6xCDyv0aIUertCDCj2oSFKvV5H0V+hehe5VJG5FYldoW5FIQ3tJUqF1wyQVRlfYFqcrel4/aaTJilRXDHRFmlQMkophUjFKKka6kemKsV4x1hVjXTHVFTNdMdcrZnrFXFfk3kJXLPTKq1jqilKvKJOKVUuVVFTJyiup9MOyZcFK51Q69zmn0nNWekqlp6ySKVUypdJjVklGlYyokoxKD6n0yBs0fTKgSvpUSZ9V0qN60D0q7VhpS6UtlTZUWjzNSuuPl9ti8VGomEIrSp1QSEIhilIUpW0UXum8nqLsJZT9hCJVFGlCMVCNYUIxVJQjRZEllJmizBLKcUIxVpQTRTFRFFNFMUso5gnlPKGYK4pcUeYJ5UJRLBOKpaIsFEWRNMrGskhYFIq8VD4T8lIxLxPmRcKsVEyLhGmZMC0Uk1IxLhrZMiErErJCMSoThqViWCQMSsWgUKRev1T0C0WvTHClwpUKWyTYQmFLhSkUUiqk8EpFUiqSQqFKhSoSVKlJigdBFYIqrec+Mykcquihyh6q7JOUfZIiJSkHqHKAKoaNctQoMlQxbpRjVDEhKSeoYooqZqhyRlLOSco5qpyjyhxVLlHlolEUqPKhRBUFybJEPeSFV6LmBWpeomYlarZETZck05JkUqLGJWpSkowLVFagspJkVJIMS9SwRA0L1KAgGRSotED1C5J+SdIrSdxDQWJLEluiTEEiSxIpSKRASYFKClRSopKSRBUkSYlWDVEFogqMKjBJiVUFVhU4VeKSkp5a0lMFfVWQegNVMFAlA1Uy9EaqIPPGqmSiSqaqYJIsmSYFM1UyUyVzVTBXS+aqIFcFi6RgoZYsk4KlKihUSZGUlKqkTArKZNlQS4pkSZnkDZVTqDmFmlEmnppSqCmFGlMk4ybVmKXKKJIRZTKkTIYUyYBSDShVSqFSSpVSqj6F6lEkjmXiKJWl8MrEUiSGQgmlVyjdSJKPl8ti8ZGrmDxRLLQi9xaiWJhG7i2MYuGe9BR5vyVV5ANvqMhHisWoyTzz9bhlolhMm8ynvp4p8rkizxULn3muyBeNee4tFLNcMc0Vs4ViulBMcsV4oRjniqxl5A2fpAvFwGeaK/q5ordQ9HKFyxVu0aRdKGyuMLnCLBSSK8Sn9pKFIskVaqFQeeJp1EKjcvmyMN/lDrWwqIXzeqi8j1o8DFC5txig8iFqMUTlI9RihMoz1GLcZD5G5RPUwsunqMUUtZihFnPUIkflPh/yHDVffJnlqGmOmi2anOaoyQI1yVHjHDVeoLIclS1QowVqmKNGOWqQo4YL1GCBSheoNPcWqN4C1cubdAuUbTELlMlRskBJ3tCeylFq4TW9VgsvR1SOUTlGLXw2rMpxKsepBU7l9FROX+WkKqevFqRqweBTzlAtGKmc0WfmZCpnrHImrZyonKlaMFP5p7mXqwW5z4XKWag5ectCzcjVjIWakqsJCzVmoSbkKiNXY3KVsVAZuRqxUENyNfqsF2pArgbkKiVXKQvVJ1c9FqpH/smRK8tCWXJlyJVhoQwLJeRKyJVuSVio5OPllOcf0zhmlqgvukVajGJmPfek19L3Bk+G3sjLvHHLxJu2cqqYzRrTJxNv3JK1jLzhTDF4ks4U/Zae51qsZzzxdEviqR8lLdoTz7TYFoea9bx+S/pkgJoNUbNRS9YybpmgZtOW2Z+m3mSGGrdkT0Yz1NAbtKRev6U3Q7kndoYyT2SG0k+SGUr9KfF0i3imxbY4NaP3pK9mpGrG4B+MnmRqxtibeNMnsz9MvUnLmJnKmKlRKx+GzNTgScpM9Vt6nmuxLYaZEk+3JC2KmVIfL4c8/5jEMVOlmCaebpEW02IVU9fKh55i2vfSVg58Dr1Bq85+MPYmium0MWnlZKrIpoqxz7aRN5wqBj5TXw+min5Lz3MttsV48kR7SYv6UfJEe/LEPnEtvZa+l6KmA2/4gxFqmrWMWyatnKIm06/MvLE38v2oZThFDaao1OejTqeo/hTVa2VvinKebTGetGgvaVF/Sp5oNUWeGDXFPnFqSu8H/Za0ZdgyUlOylpGaMlZTJj+YqilTNWkZM1WZz0edMVUjb9iSMlUDL2Wq+kxVr5UPjqmyrTSetFKYKu0lTxRTpT5efs3mH6MwIlMxWRyTJU90RKZ9LTGZKDKtmtp4NvpK9xCT9Xw6RdaLyfoxWV/5jMjSiGwQkw0isuFDTDZUZCOfWUyWRWRZzCiLWmKGWcQwixhkMYNMfUqzmDRT9DNFP4vpZRH9LKKfKXqZwmUxLouxT0wWYzOFfIrRWYRkiiSLSbIY7T362FOtOs5ioiwmyhRRplBP4iwhzjRxpomyhCjTxJmgMtNiiTNDnFnizBFljsjXTd8jyvpEWZ846xNnKXGWEmUpcTb4TJUNiLMBcTZsyYiyjCgbEWcZapR95TAjfuTDICNKv8RpRtzz+k1GnrIZsc2IXEb84GdKMpTJiE1GLBmxzlBJk7HOiBNPZcRxRhQ3+agfvYozkjgjUV903EhUho4yJMqQOMPEGcbXNs6wUYaLMlycYVUzcy29B5XRjzPSOCNVGYPIp8oYqoxh3Bj5fhR/yaKMLBqRxSMy5cXDlkFDpWRxShb3yaKHXit7ZJEjix1ZbD1fK0umDFlsyCJDFguZErJYN3UsZJEmi5JGrL7OuPr08bKdzT8GYcgwihjGLepJ0qJbpMU8sU9cS8/re+mTgTf8bvAkHUb0n/Q859P62vn6wTyRFt2StKiW+En0o7hF/SAhGuon0mJ+YD3XGDiiYa+l/yRtGRANh98NWvmQtvSHRL0nrsW2mB+Ip1uSFvUkbon+FLfFQ1T0JWnRT8QzT2yLe9Lz+l7aMniqB9GQ4afBk7SlxzDq+2xzLfZLbBrRg3j6SfJEtcRPIoZR9PGymc8++mFAGoWkccggCpva95+SkIEKSVVEqpq+EZHqiFSHPiNSiUhNRCphw4ZNb5+FpK6lF5L2Q9Je1Og/hE2mIf2HQUgvDek90nNpiEsjXBpiW0watYSYNETS6JNOQ/RnhiSfIlQaehEqjYjTkDiNiAYhURp+ZRr9IXwSpTFRqghTRThQhGnyA02UasJvhOgbQzSwhJ/54J70iLww7ROlKVHaJ0z7hGna6KdE/ZSoPyDqp4S9RtRLCV0jerAtJiX0ogdJCSUl1ClRW5IStkTqT6FKCeOUMGoJU6JwQBimn6KHqBFHKXHYUOEAFaYkYUoSDUii9JMOU3SUIlGKhN6jjgbYMMU8RANcmGK9nudadS9M6UUp/TClHw7o+3oQpaRRnzT8Moh6DKIe6SfHIHKkkSMNbSOypKH5LjKkkTRCL9KkoSYNk1YmpKH6EinSKCYNYwZhRBpGpGHsRaRhSBqFHy+v0+mHCwJ6YUgvCnFe7yF+okKc1/Nc8l1P/wPxTMNJiLMPAT0bNHMb0nN/ci7EuhDb+xcuxPS+E0+3a9f0D0kvJHEhqvenuM01GbqQyIVEPc+FhL0fuJDQRYQ9z0WEvfiJIuwpAi/sJX9yutF7kO9cuzaEvQfb4gida7LnCF2v6Z0jtI++19TWEdoeoel9pfj0dSA9Au3n0iPUPcLEESa979QP4h9E7k9hjzB030Q/iD/1PqmwhwodKuyRfONIQocOey0OCRo6dEjYQz6zhwl72B+4sIcN3WfdCx29sIcLHS50vnf0QvsDg/N6n/Qn961O6LW4UNFrcaHChfEfemH0JMR5vTD8eFlNpx8mCLBh+MmEITZ6En/nHrVqSVrZpp/qBwmx/kVgJfjkTIi1zdon2zBeu5dWimlStzz6pDV71MpLrH8BtGbxDyIvfNSmEdo/BZ919CT+YjwbE3xShH9ICF1CaHVLQmgTAqs9aeZOE1p5YgmteWK/DruxBF5oHaE4QrFfdIsYQm0JkkaYGM8RaEegLKF6zCxhbAmVJYwNYWwIYj+L/SwyhJH1h962Dr8jCK33/BKwLU3fvADsJ+U99w33WevQokOHDmwjtIify9PswXg2NJ/118ziQocNTYu08lFrrD/oTS2YMMF+0q06wYbKa14AX31z2K3XHP4IF0aYMMJ+0xx+G4bYIPx4KSaTj6TbQcIACbrosCFhFwlDdBggYYCO2h7rXXTcRcd+roKmVgGiwqaOA0QFSNzQ6rEeoJMuOukiSRedBEgSoHVDdID+7MOWrhegpZFIM3uk1l3UgzTzWALiz3lIrAMvJJYQJSFKAr8vINIBoQ4+r4ukS6S7BBISSJfQrzf7unR1l0AHBLrbErQy9BkQ6Ki5j44IdESoIwId+7mn46/UMV2tCMSnjgml6RuKQCsCSeiK+up10qLpiqarE7oiBKIJtCbQQqiFrq+DROj6DBK/JxG6ShN4YSKEiRDE+k9KCGJPSXNNLASRJgw1YSgEoaYbarqRJgg1QaAJAiEIm33dQOiGQjdo9gahJugK3YdACANNGLSuDTRhVwi7QtQVwlCIAyEONFHYiLuaKBCiQBN35UsgqEBQgSYJNElXo7qC6ja98us6kGY90OiuNAJN4uc61OhAI4FGAkF3NdIVpKvRQdLMu8kn3VV+/kV3Yz9XTXZjdBD7PkJ3YyRo6CBGujEShEgYoYMQHURIEDWzIEB3A6T7WAuQIEC63Uf98bKYjD9Up0PS7X4J/oPwi2rVSdQWPPVdkvgfqH+R/Ew9U191/IOolf8mTLqEymdL8C+6berPvtkX0P1X4T+I6KqoyUfd7pOIbhLTVXGT36in+iH5TrWyLX6qn0WtfBK0+/BBteqEbvAvuv9Z8C/CT6pVN6KuIuomP4q7CbFfj5+oh0B91d2mTv6V+kGM6sYkP4q+UZ912Mp/EvyDENUNvp/zbvfjJR+PP6KXDnG3S9x9ZKsOOr5uCbqf1GfdIQ47TQZd4rDb9G1RM1Pt/g8dr0sct3WI4y6q1Udxlyju+NpTX3X4TferVl+CuPvF9+Fn/1j72tf9B53PukNXdVprjzpoXg5+b6d9rerSiYNmz5OOCpo1FdKNQzpPun+I6MYRnTiio1qpIjpxTEfFdGJFN44/deJm1oljOlFMN1J0opjOZ8afa50wpvsQKbphTCdUzd6w2d8NFd0g/tQJmus6rVm3G9PtKrqBatZ93+nG33Q7jU5XeV+zbicm6Da6nZjgMe+qZt6JCX0+Cx+6MVFXEXZj/7JozxpRN/4Ut+qoExN/Ul+6MerbWnOd6kbE3Zi4GzU6EepbHxJ3fHYfddOrTkDc1v1Bp4vqBl9nuBOgul2/1qzHnS6q0yXudIg7nY+XaTb+CF5eCDsdwk6HwHv0YfdL4IX/IHjyuRZ47fpJ4IVBhzDsELSE4df8WfAs+HPW9Z7Xum3BU/+81tIJO3QCL2z14Q+Cf1gLOnSCLp3wB8E/1QGd8CePPcF/EH4JWnV7LXiaBS1d73n2vP6vgu9956nuBF74D4If93T/o8D7cy3ohASdwGfbP89Cr70W/uDnteA/6P7D7GeBF7bya611lv/08TLJso/OywvdTodu6wXw3H/Oun7eOuhdL3jMO1+zZz9d920t+KF+vCDa9eNAdr/qx57H/DN/qh+H2esGHTrdxmNvp+2xt/svgn/J4Id7fa53W/u7rfXuD5/pXwqPDFp9W+BfGN2uP3TBVx886lbf3tcNvq4Pgtb9f9j3jd/X6TYH9FsdfM3a807g6/bs39b/03Xf174Oftf7fv/uN90nwR990Lpn8K3/2vc17z7Nu398XvA57/7Y/7yn4/vON8Hn2vcz+1Pv8+NlPM4+Xl5eeOh4Ly8dXl46dDovn146HT9/ofPS8f68tuOv/drfae3ptD7jcc/W9Z127T/r8R3adefr2qb+2vN5z077c77XL51OK3397Ts9vlendc1Pf2vr/9Vp3f/pe3y//9f3/b7/+/pPn/nH5zzu1Xoencff2+n88Dxaf/NL5+kZdJ6ee8d7+f6/+OH5dl7a+16+PfNv9/3xd9J+Pp3W/7f1G/v2v+h8/z+8vHx+z06n8/R/7/zwXf78Xt8/9+Ufnn3n+9l4/M2dzg/P4s9n2Pn22+o8/c6//9a/7Xk8g+ff+ZPPM/Lyfc/z7/Rr3vl4ybLvL4D/+q//+r/Gx0uWZf/r5eXlf7+8vHz813/91/81/vfLy8v/+j8HvRJqG7MILAAAAABJRU5ErkJggg==';

// Initializes VCI when DOM Document is ready
$(document).ready(function () {    
    VCI.init();
});