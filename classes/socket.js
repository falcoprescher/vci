var Socket = function (callback) {
	var io = undefined;

	// Socket.IO init and config
	if (config.SSL) {
		var key = fs.readFileSync('private-key.pem');
		var cert = fs.readFileSync('public-cert.pem');
		io = socketio.listen(server, {
			key: key,
			cert: cert
		});
	} else {
		io = socketio.listen(server); // without ssl
	}


	io.configure('production', function () {
		io.enable('browser client etag');
		io.set('log level', 0);
		io.enable('browser client gzip');
		io.disable('browser client cache');
		io.set('transports', [
			'websocket',
			'flashsocket',
			'htmlfile',
			'xhr-polling',
			'jsonp-polling']);
	});

	io.configure('development', function () {
		io.disable('browser client cache');
		io.set('transports', [
			'websocket',
			'flashsocket',
			'htmlfile',
			'xhr-polling',
			'jsonp-polling']);
	});

	io.set('authorization', function (data, accept) {
		/*// to detect which session this socket is associated with, we need to parse the cookies
		if (!data.headers.cookie) return accept('Session cookie required.', false);

		// verify the signature of the session cookie
		var signedCookies = connect.utils.parseSignedCookies(cookie.parse(data.headers.cookie), config.APPKEY);

		if (!vci_sessions[signedCookies['connect.sid']]) return accept('No valid session.', false);

		data.vci = vci_sessions[signedCookies['connect.sid']]

		*/
		return accept(null, true);
	});

	// set socket events
	io.on('connection', function (socket) {

		// create session
		// ssid - socket session id, this temp unique id is given in vci html page
		socket.on('ssid', function (ssid) {
			console.log(ssid.red.bold, vci_sessions[ssid]);

			if (vci_sessions[ssid]) {
				async.series({
					prepare: function (callback) {
						// save important data in sessionstore
						socket.vci = vci_sessions[ssid].vci;
						socket.uid = vci_sessions[ssid].uid;
						socket.ssid = vci_sessions[ssid].ssid;
						socket.name = vci_sessions[ssid].name;

						// delete temp ssid
						delete vci_sessions[ssid];

						callback(null, null);
					},
					dbData: function (callback) {
						// load all vci data(chat+whiteboard) async from database
						// and send it back to the client
						async.parallel({
							chat: function (callback) {
								var chatMessages = new Array();
								ChatMessages.find({
									'vci.$id': socket.vci
								}, function (err, cursor) {
									cursor.toArray(function (err, result) {
										for (var i in result) {
											var chatMessage = result[i];

											chatMessages.push({
												name: chatMessage.name,
												ts: chatMessage.timestamp,
												msg: chatMessage.message
											});
										}
										callback(null, chatMessages);
									});
								});
							},
							vciData: function (callback) {
								var vciData = new Object();

								VCIData.find({
									'vci.$id': socket.vci,
									type: 'LAYER'
								}, function (err, cursor) {
									cursor.toArray(function (err, layerResult) {
										if (layerResult.length > 0) {
											var getObjects = new Array();
											for (var layerIndex in layerResult) {
												var layer = layerResult[layerIndex];

												vciData[layer._id] = layer.properties;

												var getObject = function (layerID) {
													return function (callback) {

														VCIData.find({
															'layer.$id': layerID
														}, function (err, cursor) {
															if (err) logger.error(err);

															cursor.toArray(function (err, objectResult) {

																if (objectResult.length > 0) vciData[layerID].o = new Object();

																for (var objectIndex in objectResult) {
																	var object = objectResult[objectIndex];

																	vciData[layerID].o[object._id] = object.properties;
																}

																callback(null, layerID);
															});
														})

													}
												};

												getObjects.push(getObject(layer._id.toString()))
											}

											async.parallel(getObjects, function (err, results) {
												callback(null, vciData);
											});
										} else callback(null, null);
									});
								});
							}
						},

						function (err, result) {
							callback(err, result);
						})
					},
					post: function (callback) {
						socket.join(socket.vci);

						callback(null, null);
					}
				},

				function (err, results) {
					var initData = new Object();

					if (results.dbData.chat.length > 0) {
						initData.chat = results.dbData.chat;
					}
					if (!core.isEmpty(results.dbData.vciData)) {
						initData.vciData = results.dbData.vciData;
					}

					socket.emit('init', initData);
				});
			} else socket.disconnect();
		});

		socket.on('disconnect', function (data) {
			console.log(socket.name, 'disconnected');
		});

		// System messages
		socket.on('sys', function (data) {
			util.log(socket.id + ': ' + data);
		});

		// Client Ping
		socket.on('ping', function (data) {
			socket.emit('ping', data);
		});

		// VCI-Broadcast
		socket.on('vbc', function (data) {
			io.sockets.in(socket.vci).emit('sys', data);
		});

		// Chat messages
		socket.on('chat', function (data) {
			util.log(socket.id + ': ' + data);

			if (socket.vci) {
				if (!vcis[socket.vci]) {
					console.log('#########################################');
					vcis[socket.vci] = new VCI(socket.vci);
					console.log(vcis[socket.vci]);
					console.log('#########################################');
				} else {

				}


				var timestamp = new Date();
				var message = vcis[socket.vci].chat.newMessage(data, timestamp, socket.vci, socket.uid, socket.name);

				console.log('++++++++++++++++++++++++++++++++++');
				console.log(socket.name + ':', message);
				console.log('----------------------------------');

				io.sockets. in (socket.vci).emit('chat', {
					name: socket.name,
					ts: timestamp,
					msg: message
				});
			} else socket.disconnect();
		});

		// Client SnapShot
		socket.on('ss', function (data) {
			if (socket.vci) {
				if (!vcis[socket.vci]) {
					console.log('#########################################');
					vcis[socket.vci] = new VCI(socket.vci);
					console.log(vcis[socket.vci]);
					console.log('#########################################');
				}

				vcis[socket.vci].processSnapShot(socket.id, data);
			} else socket.disconnect();
		});

	});

	callback(null, io);
}

module.exports = Socket;