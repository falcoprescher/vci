/**
Cache Files and minify javascript, avoid disk access

@class FSCache
@constructor

**/

var FSCache = function () {
	/**
	Filecache self destroy timeout

	@property fsTimeout 
	@type Number
	@default "1800000"
	**/
	this.fsTimeout = 1800000; // 30 minutes

	var jsp = uglifyjs.parser;
	var pro = uglifyjs.uglify;

	/**
	Cached files

	@property cache 
	@type Object
	**/
	var cache = new Object();

	/**
	Returns a destroy funtion with timeout

	@method selfDestroy
	@return {Function} _selfDestroy
	**/
	var selfDestroy = function (fs) {
		var _selfDestroy = function (_this) {
			console.log('_selfDestroy:'.red.bold, _this.path.toString().green.bold);

			if (_this.timeout) clearTimeout(_this.timeout);

			if (_this.watcher) _this.watcher.close();

			delete _this;
		};

		return _selfDestroy(fs);
	};


	/**
	Cache a File ...

	@method fileCache
	**/
	var fileCache = function (path, mimeType, data) {
		if (config.FSCACHE) {
			cache[path] = new Object();
			cache[path].path = path;
			cache[path].mimeType = mimeType;
			//cache[path].timeout = setTimeout(cache[path]);
			cache[path].data = data
	
			// create fsWatcher ...
			var watcher = function (fs, fileLoad) {
				return function (event, filename) {
					console.log('event:'.yellow.bold, event.cyan);
					console.log('watcher:'.red.bold, fs.path.toString().green.bold);
					fileLoad(fs.path, function (mimeType, data) {});
				};
			};

			fs.watch(path, watcher(cache[path], fileLoad));
		}
	};


	/**
	Load a File and minify scripts

	@method fileLoad
	**/
	var fileLoad = function (path, callback) {
		fs.readFile(path, function (err, data) {
			if (err) {
				logger.error(err);
				callback(null, null);
			} else {
				var mimeType = mime.lookup(path);

				if (config.FSMINIFY) {
					switch (mimeType) {
						case 'application/javascript':
							var ast = jsp.parse(data.toString());

							ast = pro.ast_mangle(ast);
							ast = pro.ast_squeeze(ast);

							var final_code = pro.gen_code(ast);

							console.log('[' + 'FSCACHE'.red + ']', path.toString());
							callback(mimeType, final_code);

							fileCache(path, mimeType, final_code);
							break;
						default:
							console.log('[' + 'FSCACHE'.red + ']', path.green);
							callback(mimeType, data);

							fileCache(path, mimeType, data);
							break;
					}
				} else {
					console.log('[' + 'FSCACHE'.red + ']' + ' loaded:', path.green);
					callback(mimeType, data);
					fileCache(path, mimeType, data);
				}
			}
		});
	};


	/**
	File request to cache

	@method fileLoad
	**/
	this.load = function (path, callback) {
		path = rootdirname + '/public/' + path;

		var fs = cache[path];

		if (fs) {
			//fs.timeout = selfDestroy(fs);
			callback(fs.mimeType, fs.data);
		} else {
			fileLoad(path, callback);
		}
	};

	this.isCached = function (path) {
		path = rootdirname + '/public/' + path;

		if (cache[path]) {
			return true;
		} else {
			return false;
		}
	};

	/**
	precache

	@method precache
	**/
	this.precache = function(pathList, callback) {		
		if (pathList.length > 0) {
			var jobs = new Array();
			var job = function(_this, path) {
				return function(callback) {
					_this.load(path, function(mimeType, data) {callback(null, null);});
				}
			}
	
			for(index in pathList) {
				var path = pathList[index];

	
				jobs.push(job(this, path));
			}
	
			async.parallel(jobs, function (err, result) {
				callback(err, result);
			});
		} else {
			callback(null, new Array());
		}
	};

}

module.exports = FSCache;