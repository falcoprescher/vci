/**
Webserver

Features:
	SSL-Support(key & cert)
	parallel: HTTP and HTTPS

@class Server
@constructor

**/

Server = function (callback) {
	require('../config/enviroment.js')(app_primary, express);
	require('../config/routes.js')(app_primary);


	app_secondary.get('*', function (req, res) {
		var port = config.SSL ? config.HTTPSPORT != 443 ? config.HTTPSPORT : "" : config.HTTPPORT != 80 ? config.HTTPPORT : "";
		var protocol = config.SSL ? 'https' : 'http';
		var server = protocol + '://' + req.host + ':' + port + '/'

		logger.log('app_secondary: ' + req.url);
		res.redirect(server)
	});


	if (config.SSL) {
		console.log("   info  - ".cyan + "SSL activated");


		options = {
			key: fs.readFileSync('private-key.pem').toString(),
			cert: fs.readFileSync('public-cert.pem').toString()
		};

		server_http = http.createServer(app_secondary);
		server_https = https.createServer(options, app_primary);

		server_http.listen(config.HTTPPORT);
		server_https.listen(config.HTTPSPORT);

		var ldlg = 'https/wss';
	} else {
		server_https = http.createServer(app_primary);

		server_https.listen(config.HTTPPORT);

		var ldlg = 'http/ws';
	}

	logger.blank("   info  - ".cyan + "listen on %s://%s:%s", ldlg, config.LISTEN, config.SSL ? config.HTTPSPORT : config.HTTPPORT);


	callback(null, server_https);
}


module.exports = Server;