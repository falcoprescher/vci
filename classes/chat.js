/**
Save message to database and check scripting in message

	// examples:
	// simple-text
	// <strong>html-formatted</strong>
	// <script>alert('javascript');</script>
	// <div>text und <script>alert('javascript');</script></div>
	// <img src="fdsg" onerror="alert('hallo')" />
	// <img src="fdsg" onerror="var test;" />

@class Chat
@constructor

**/

var Chat = function () {
	// recursivly check all dom elements

	var checkNode = function (node) {
		for (var i in node.childNodes) {
			if (node.childNodes[i].nodeName) {

				// check all blocked events(defined in config/base.js) and delete this if exists
				if (node.childNodes[i].getAttribute)
					for (index in config.CHATBLOCKEDEVENTS) {
						if (node.childNodes[i].getAttribute(config.CHATBLOCKEDEVENTS[index]))
							node.childNodes[i].setAttribute(config.CHATBLOCKEDEVENTS[index], '/*blocked event*/');
					}

				// the scriptblock will be wrapped with a function, this will avoid the execution
				// and a button will be added; this calls the function
				if (node.childNodes[i].nodeName.toUpperCase() == 'SCRIPT') {
					var script_src = node.childNodes[i].innerHTML;
					var script_id = uuid.v4().toString().replace(/-/g, '');

					node.childNodes[i].innerHTML = 'var f' + script_id + ' = function(){' + script_src + '}';


					var tag_a = node._ownerDocument.createElement('a');
					tag_a.href = 'javascript:f' + script_id + '();';
					tag_a.innerHTML = script_src;

					node.childNodes.splice(i + 1, 0, tag_a);
				} else { }
			} else {
				// check all events, but not here ... :P
			}
			// if there are child elements then the childs will be checked
			if (node.childNodes[i].childNodes) {
				checkNode(node.childNodes[i]);
			}
		}

	};

	this.newMessage = function (message, timestamp, vciID, userID, name) {
		var document = jsdom(message); // parse message and build dom

		checkNode(document); // delete events and wrap scripts in anchor tag

		message = document.outerHTML // copy back new message

		// create and save vci and user reference in the database document
		// async database access !!!

		var dbVCI = new bson.DBRef(VCIs.collectionName, vciID);
		var dbUser = new bson.DBRef(Users.collectionName, userID);

		ChatMessages.insert([{
			vci: dbVCI,
			uid: dbUser,
			name: name,
			message: message,
			timestamp: timestamp
		}], function (err, docs) {
			if (err) logger.error(err);
			/*else
				docs.forEach(function(doc) {
					console.dir(doc);
				});*/
		});

		return message;
	};

};

module.exports = Chat;