var VCI = function (vciID) {
	console.log('vci constructor'.red);
	this.vciID = vciID;
	this.dbVCI = new bson.DBRef(VCIs.collectionName, this.vciID);
	this.historyStates = 0;
	this.histories = new Object(); // history and rollback per user
	this.rollbacks = new Object();

	this.currents = new Object(); // working on this state only

	this.fullvci = new Object();

	this.chat = new Chat();


	// custom upsert (recursive, partial update objects)
	var dbUpsert = function (dataType, dbVCI, dbLayer, dbObjectData, objectID) {
		var _dbVCI = dbVCI;
		var _dbLayer = dbLayer;
		var _objectID = objectID;
		var _dbObjectData = dbObjectData;
		var _dataType = dataType.toUpperCase();

		return function (err, cursor) {
			if (err) logger.warn(err.message);

			cursor.toArray(function (err, items) {
				if (err) logger.warn(err.message);

				if (items.length > 0) {
					for (index in items) {
						var item = items[index];

						item.vci = _dbVCI;
						if (!_dataType == 'LAYER') item.layer = _dbLayer;

						item.properties = core.deepExtend(item.properties, _dbObjectData);

						VCIData.save(item, function (err, doc) {
							if (err) logger.warn(err.message);
						});
					}
				} else {
					VCIData.update({
						_id: _objectID
					}, {
						$set: _dataType == 'LAYER' ? {
							type: _dataType,
							vci: _dbVCI,
							properties: _dbObjectData
						} : {
							type: _dataType,
							vci: _dbVCI,
							layer: _dbLayer,
							properties: _dbObjectData
						}
					}, {
						upsert: true
					}, function (err) {
						if (err) logger.warn(err.message);
					});
				}
			})
		}
	}

	this.processSnapShot = function (user, data) {

		//console.log('\033[1;33m');
		//console.dir(data);
		//console.log('\033[0m');

		if (!this.currents) this.currents = new Object();

		this.currents[user] = new Object();
		for (layerID in data) {
			var _layerID;
			var originLayerID;
			var dbLayer;
			var updateLayer = false;
			var dbLayerData = new Object();
			var layerCMD;

			try {
				_layerID = bson.ObjectID(layerID);

				dbLayer = new bson.DBRef(VCIData.collectionName, layerID);
			} catch (e) {
				originLayerID = layerID;
				_layerID = new VCIData.pkFactory();

				dbLayer = new bson.DBRef(VCIData.collectionName, _layerID);
			}

			if (!this.currents[user][_layerID]) this.currents[user][_layerID] = new Object();
			if (!this.fullvci[_layerID]) this.fullvci[_layerID] = new Object();

			if (originLayerID) this.currents[user][_layerID].originID = originLayerID;
			else delete this.currents[user][_layerID].originID;

			for (layerKey in data[layerID]) {

				if (layerKey != 'o') {
					this.currents[user][_layerID][layerKey] = data[layerID][layerKey];
					this.fullvci[_layerID][layerKey] = data[layerID][layerKey];
					dbLayerData[layerKey] = data[layerID][layerKey];
					updateLayer = true;

					if (layerKey.toLowerCase() == 'command') {
						layerCMD = data[layerID][layerKey].toUpperCase();
					} else {
						dbLayerData[layerKey] = data[layerID][layerKey];
					}
				} else {
					if (!this.currents[user][_layerID]) this.currents[user][_layerID] = new Object();
					if (!this.fullvci[_layerID]) this.fullvci[_layerID] = new Object();
					if (!this.currents[user][_layerID].o) this.currents[user][_layerID].o = new Object();
					if (!this.fullvci[_layerID].o) this.fullvci[_layerID].o = new Object();
				}
			};

			if (updateLayer || true) { // TEST - "|| true" - always save layer in db !!
				switch (layerCMD) {
					case 'DELETE':
						VCIData.remove({
							'layer.$id': _layerID
						}, function (err, removed) {
							if (err) logger.error(err);
							else VCIData.remove({
								_id: _layerID
							}, function (err, removed) {
								if (err) logger.error(err);
							});
						});
						break;
					default:
						var layerUpsert = dbUpsert('layer', this.dbVCI, null, dbLayerData, _layerID)
						VCIData.find({
							_id: _layerID
						}, layerUpsert);
						break;
				}
			}

			for (objectID in data[layerID].o) {
				var dbObjectData = new Object();
				var originObjectID;
				var _objectID;
				var objectCMD;

				try {
					_objectID = bson.ObjectID(objectID);
				} catch (e) {
					originObjectID = objectID;
					_objectID = new VCIData.pkFactory();
				}

				if (!this.currents[user][_layerID].o[_objectID]) this.currents[user][_layerID].o[_objectID] = new Object();
				if (!this.fullvci[_layerID].o[_objectID]) this.fullvci[_layerID].o[_objectID] = new Object();

				if (originObjectID) this.currents[user][_layerID].o[_objectID].originID = originObjectID;
				else delete this.currents[user][_layerID].o[_objectID].originID;

				for (objectKey in data[layerID].o[objectID]) {
					this.currents[user][_layerID].o[_objectID][objectKey] = data[layerID].o[objectID][objectKey];
					this.fullvci[_layerID].o[_objectID][objectKey] = data[layerID].o[objectID][objectKey];

					if (objectKey.toLowerCase() == 'command') {
						objectCMD = data[layerID].o[objectID][objectKey].toUpperCase();
					} else {
						dbObjectData[objectKey] = data[layerID].o[objectID][objectKey];
					}
				}

				if (core.isEmpty(this.currents[user][_layerID].o[_objectID])) {
					delete this.currents[user][_layerID].o[_objectID];
				}

				switch (objectCMD) {
					case 'DELETE':
						VCIData.remove({
							_id: _objectID
						}, function (err, removed) {});
						break;
					default:
						var objectUpsert = dbUpsert('object', this.dbVCI, dbLayer, dbObjectData, _objectID)
						VCIData.find({
							_id: _objectID
						}, objectUpsert);
						break;
				}
			}

			if (core.isEmpty(this.currents[user]) || this.currents[user].length == 0) {
				delete this.currents[user];
			}
		};
	};

	this.redo = function () {
		// redo ...
	};

	this.undo = function () {
		// undo ...
	};

	this.broadcast = function (_this) {
		_this = _this || this;

		if (!core.isEmpty(_this.currents)) {
			socket.sockets. in (_this.vciID).emit('ss', _this.currents);
			delete _this.currents;
		}
	};

	this.broadcaster = setInterval(this.broadcast, config.FRAMETIME, this);
};

module.exports = VCI;