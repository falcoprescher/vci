/**
Database abstraction and access

@class DB
@constructor

**/

var DB = function (callback) {
	var mdb = new mongodb.Db(config.DBNAME, new mongodb.Server(config.MONGODBHOST, config.MONGODBPORT, {
		auto_reconnect: true
	}, {}), {
		safe: false
	});

	var collectionExists = function (collections, collectionName) {
		for (index in collections) {
			var collection = collections[index];
			if (collection.name == config.DBNAME + '.' + collectionName) return true;
		};
		return false;
	}

	var job = function (modelName, collectionName, collections) {
		var _collectionName = collectionName;
		var _modelName = modelName;
		var _collections = collections;

		return function (callback) {
			if (collectionExists(_collections, _collectionName)) mdb.collection(_collectionName, function (err, collection) {
				this[_modelName] = this[_modelName] || collection;
				callback(null, logger.conditionalFormat(_collectionName, this[_modelName]));
			});
			else mdb.createCollection(_collectionName, function (err, collection) {
				if (err) logger.error(err);
				this[_modelName] = this[_modelName] || collection;
				callback(null, logger.conditionalFormat(_collectionName, 2));
			});
		};
	}

	var loadModels = function () {
		mdb.collectionNames(function (err, collections) {
			var jobs = new Array();

			for (index in this.MODELS)
			for (key in this.MODELS[index]) {
				var collectionName = this.MODELS[index][key].toString();
				var modelName = key.toString();

				this[modelName] = undefined;

				jobs.push(job(modelName, collectionName, collections));

				break;
			}

			async.parallel(jobs,

			function (err, results) {
				console.log("   info  - ".cyan + "collections: " + results.join(', '));
				callback(null, mdb);
			});
		});
	}


	mdb.open(function (err, data) {
		if (data) {
			console.log("   info  - ".cyan + "Database opened");

			if (config.MONGODBUSER != '') data.authenticate(config.MONGODBUSER, config.MONGODBPASSWORD, function (err2, data2) {
				if (data2) {
					console.log("   info  - ".cyan + "Authentification successful");
					loadModels();
				} else {
					console.error(err2);
					process.exit(1);
				}
			});
			else loadModels();
		} else {
			console.error(err);
			process.exit(1);
		}
	});
}


module.exports = DB;