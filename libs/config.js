this.PROCESS_NAME = 'VCI - NodeJS';

this.MONGODBPORT = 27017;
this.MONGODBHOST = '127.0.0.1';
this.MONGODBUSER = '';
this.MONGODBPASSWORD = '';
this.DBNAME = 'vci';

this.LISTEN = '0.0.0.0';
this.HTTPPORT = 3000;
this.HTTPSPORT = 3001;
this.SSL = false; // http & ws > ssl
this.APPKEY = '0D9DCDA5-ACB0-4019-82CF-34D63F9E80B2'; // app uuid
this.RESTRICTION = /^((?!(\/user\/(login|register)|index|^\/)\/?$).)*$/; // urls, which requires login
this.FRAMETIME = 100; // 100ms interval
this.COOKIEEXPIRES = 14; //two weeks(14 days)

this.LOCALS = ['en', 'de']; // supported site languages

this.FSCACHE = true;
this.FSMINIFY = true;
this.PRECACHE = [
	'/favicon.ico',
	'/stylesheets/style.css',
	'/libs/jquery-1.8.2.js',
	'/libs/jquery.mobile-1.2.0.css',
	'/libs/jquery.mobile-1.2.0.js',
	'/libs/images/ajax-loader.gif',
	'/libs/kinetic-v4.0.5.js',
	'/libs/jquery-ui-1.9.1.custom.js',
	'/javascripts/vci.js',
	'/javascripts/client.js'

];

this.CHATBLOCKEDEVENTS = ['onerror'];

if (!fs.existsSync(rootdirname + '/config/base.js')) {
	logger.error('config file does\'t exists, empty file will be created!')
	if (!fs.writeFile(rootdirname + '/config/base.js', ''))
		logger.error('can\'t create config file')
}

if (fs.existsSync(rootdirname + '/config/base.js') || true)
	with (this) {
		eval( require('fs').readFileSync(rootdirname + '/config/base.js').toString() );
	}
else
	process.exit(1);

exports = this;