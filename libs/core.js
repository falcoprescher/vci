exports.isEmpty = function(ob){
	for(var i in ob){ return false;}
	return true;
}

exports.deepCopy = function(p, c) {
	var c = c||{};
	for (var i in p) {
		if (typeof p[i] === 'object') {
			c[i] = (p[i].constructor === Array)?[]:{};
			deepCopy(p[i],c[i]);
		} else
			c[i] = p[i];
	}
	return c;
}

exports.clone = function (obj) {
	if (null == obj || "object" != typeof obj) return obj;
	var copy = obj.constructor();
	for (var attr in obj) {
		if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
	}
	return copy;
}

exports.deepMerge = function(obj1, obj2) {
	for (var p in obj2) {
		try {
			if ( obj2[p].constructor == Object ) {
				obj1[p] = deepMerge(obj1[p], obj2[p]);
			} else {
				obj1[p] = obj2[p];
			}
		} catch(e) {
			obj1[p] = obj2[p];

		}
	}
	return obj1;
}

exports.extend = function(destination, source) {
  for (var property in source)
    destination[property] = source[property];
  return destination;
};

exports.deepExtend = function(destination, source) {
  for (var property in source) {
    if (source[property] && source[property].constructor &&
     source[property].constructor === Object) {
      destination[property] = destination[property] || {};
      arguments.callee(destination[property], source[property]);
    } else {
      destination[property] = source[property];
    }
  }
  return destination;
};

exports.isCircularObject = function(node, parents){
	parents = parents || [];

	if(!node || typeof node != "object"){
		return false;
	}

	var keys = Object.keys(node), i, value;

	parents.push(node); // add self to current path      
	for(i = keys.length-1; i>=0; i--){
		value = node[keys[i]];
		if(value && typeof value == "object"){
			if(parents.indexOf(value)>=0){
				// circularity detected!
				return true;
			}
			// check child nodes
			if(arguments.callee(value, parents)){
				return true;
			}

		}
	}
	parents.pop(node);
	return false;
}

exports.print_object = function(o) {
	var cache = [];
	var res = JSON.stringify(o , function(key, value) {
		if (typeof value === 'object' && value !== null) {
			if (cache.indexOf(value) !== -1) {
				// Circular reference found, discard key
				return;
			}
			// Store value in our collection
			cache.push(value);
		}
		return value;
	});
	cache = null; // Enable garbage collection

	return res;
}

exports.regexKeys = function(obj, regexp) {
	var founds = {};

	for(var key in obj) {
		if(regexp.match(key)) {
			founds.push(key);
		}
	}

	return founds;
}

exports.clrscreen = function() {
	process.stdout.write('\u001B[2J\u001B[0;0f');
}