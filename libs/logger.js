logger = {
	_forward: function (method, args, s) {
        args = Array.prototype.slice.call(args);

        // process the first argument for tags

        if (s){
        	if (args)
        		args = vsprintf(args[0], args.slice(1));
        	else
        		args = "vsprintf(args)-error";
        	method(args);
        } else{
	        for(var c = 1; c < args.length; c++)
				args[c] = this._stringify(args[c]);
	
			method.apply(console, args);
		}
	},

	_stringify: function(e, t) {
		var str = [];
			tab = [];
		t = t?t:0;

		 switch(typeof e){
			case "string":
				str.push(("\""+e+"\"").cyan);
			break;
			case "number":
				str.push(e.toString().yellow);
			break;
			case "object":
				var sub = [];
				for(var p in e)
					sub.push(this._stringify(e[p], t));

				if (Object.prototype.toString.apply(e[p]) === '[object Array]')
					str.push("["+sub.join()+"]");
				else
					str.push("{"+this._stringify(p, ++t)+": "+sub.join(",")+"}");
			break;
			default:
				if (e)
					if (e.toString)
						str.push(e.toString().red);
		}

		//for(var c=0;c<t;c++)
			//tab += ".".red;

		return tab + str.join();
	},

	log: function() {
		this._forward(console.log, arguments);
	},

	info: function() {
		this._forward(console.info, arguments);
	},

	warn: function() {
		this._forward(console.info, ["[".white + "WARNING".yellow.bold + "] ".white + arguments[0]].concat(Array.prototype.slice.call(arguments, 1)), true);
	},

	error: function() {
		this._forward(console.info, ["[".white + "ERROR".red.bold + "] ".white + arguments[0]].concat(Array.prototype.slice.call(arguments, 1)), true);
	},

	dir: function() {
		this._forward(console.dir, arguments);
	},

	blank: function() {
		this._forward(console.info, [arguments[0]].concat(Array.prototype.slice.call(arguments, 1)), true);
	},

	loading: function() {
		this._forward(console.info, ["[".white + "LOADING".green + "] ".white + arguments[0]].concat(Array.prototype.slice.call(arguments, 1)), true);
	},

	setup: function() {
		this._forward(console.info, ["[".white + "SETUP".yellow + "] ".white + arguments[0]].concat(Array.prototype.slice.call(arguments, 1)), true);
	},

	system: function() {
		this._forward(console.info, ["[".white+"SYSTEM".red.bold+"] ".white + arguments[0]].concat(Array.prototype.slice.call(arguments, 1)), true);
	},

	conditionalFormat: function(string, state) {
		switch (state){
			case 0:
				return string.red;
			case 1:
				return string.green;
			case 2:
				return string.yellow;
			default:
				if (state)
					return string.green;
				else
					return string.red;
		}
	}
}

module.exports = logger;