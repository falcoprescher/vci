module.exports = function(app, express) {
	app.configure(function() {
		//app.use(express.static(__dirname + '../../public'));
		app.use(express.compress());
		app.use(express.logger());
		app.set('views', __dirname + '../../views');
		app.set('view engine', 'jade');
		app.use(express.bodyParser());
		app.use(express.cookieParser(config.APPKEY));
		app.use(express.session({
			secret: config.APPKEY,
			store: sessionStore,
			maxAge: new Date(Date.now() + 3600000)
		}));
		app.use(i18n.init);
		app.use(app.router);

	});

	//development configuration
	app.configure('development', function() {
		app.use(express.logger('dev'));
		app.use(express.errorHandler({
			dumpExceptions: true,
			showStack: true
		}));
	});

	//production configuration
	app.configure('production', function() {
		app.use(express.errorHandler());
	});

};