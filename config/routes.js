module.exports = function (app) {

	var redirect = function (req, res, to) {
		req.url = req.url == '/user/login' ? '/user/vcis' : req.url;
		req.session.redirect_to = req.url;
		res.redirect(to);
	};

	var getRQPort = function(req) {
		return ( req.headers.host.match(/:/g) ) ? req.headers.host.slice( req.headers.host.indexOf(":") + 1 ) : config.SSL ? 443 : 80;
	};

	// modify header and set language
	app.all('*', function (req, res, next) {
		res.setHeader('X-Powered-By', 'VCI-Server');
		res.setHeader('Cache-Control', 'no-cache');

		i18n.setLocale(req.headers["accept-language"]);

		next()
	});

	// locks defined areas(in /congig/base.js) for unauthorized users
	app.get(config.RESTRICTION, function (req, res, next) {
		if (fsCache.isCached(req.url))
			next();
		else{
			Sessions.find({
				usid: req.signedCookies.usid
			}, function (err, cursor) {
				cursor.toArray(function (err, result) {
					if (result.length > 0) {
						var exdate = new Date();
						exdate.setDate(exdate.getDate() + config.COOKIEEXPIRES);
	
						res.cookie('usid', req.signedCookies.usid, {
							signed: true,
							expires: exdate
						});
	
						next();
					} else {
						redirect(req, res, '/user/login');
					}
				});
			});
		}
	});

	app.all('/user/logout', function (req, res) {
		Sessions.remove({
			usid: req.signedCookies.usid
		}, function (err, removed) {
			res.redirect('/user/login');
		});
	});

	app.post('/user/login', function (req, res) {
		var sessionHash = crypt.createHash('sha512').update(req.sessionID).digest('hex');

		var passwordHash = crypt.createHash('sha512').update(req.body.user.password).digest('hex');
		var email = req.body.user.email;

		var result = [];
		Users.find({
			email: email,
			passwordHash: passwordHash
		}, function (err, cursor) {
			cursor.toArray(function (err, result) {
				if (result.length > 0) {
					Sessions.insert([{
						user: new bson.DBRef(Users.collectionName, result[0]._id),
						tsid: req.sessionID,
						usid: sessionHash,
						created_at: new Date()
					}], function (err, docs) {
						docs.forEach(function (doc) {
							//console.dir(doc);
						});
					});

					var exdate = new Date();
					exdate.setDate(exdate.getDate() + config.COOKIEEXPIRES);

					res.cookie('usid', sessionHash, {
						signed: true,
						expires: exdate
					});

					res.status(200);
					res.send(req.session.redirect_to ? req.session.redirect_to : '/user/vcis');

					console.log('[' + 'LOGIN'.cyan.bold + ']',req.body.user.email.green + ':' + req.body.user.password.green);
					console.log('   passwordHash  -'.cyan, passwordHash);
					console.log('   sessionID  -'.cyan, req.sessionID);
				} else {
					res.status(404);
					res.send('wrong username or password');

					console.log('[' + 'LOGIN'.cyan.bold + ']',req.body.user.email.red + ':' + req.body.user.password.red);
					console.log('   passwordHash  -'.cyan, passwordHash);
					console.log('   sessionID  -'.cyan, req.sessionID);
				}
			});
		});
	});

	app.post('/user/register', function (req, res) {
		var sessionHash = crypt.createHash('sha512').update(req.sessionID).digest('hex');
		var passwordHash = crypt.createHash('sha512').update(req.body.user.password).digest('hex');
		var email = req.body.user.email

		var userAgent = uaParser.parse(req.headers['user-agent']);

		var user = {
			name: email,
			email: email,
			passwordHash: passwordHash,
			language: req.acceptedLanguages[0],
			charset: req.acceptedCharsets[0],
			rights: {
				role: 'USER',
				vci_restrictions: {
					max_vcis: 3,
					max_layers: 10,
					max_objects: 999
				}
			},
			info: {
				register: {
					ip: req.ips ? req.ips : req.ip,
					header: req.headers,
					user_agent: {
						family: userAgent.family,
						major: userAgent.major,
						minor: userAgent.minor,
						patch: userAgent.patch,
						operation_system: userAgent.os
					},
				}
			}
		}

		Users.insert(user, function (err, result) {
			if (err){
				res.status(404);
				res.send('error');

				console.log('[' + 'REGISTER'.cyan.bold + ']',req.body.user.email.red + ':' + req.body.user.password.red);
				console.log('   passwordHash  -'.cyan, passwordHash);
				console.log('   sessionID  -'.cyan, req.sessionID);
			} else {
				Sessions.insert([{
					user: new bson.DBRef(Users.collectionName, result[0]._id),
					tsid: req.sessionID,
					usid: sessionHash,
					created_at: new Date()
				}], function (err, docs) {
					if (err) {
						res.status(404);
						res.send('error');

						console.log('[' + 'REGISTER'.cyan.bold + ']',req.body.user.email.red + ':' + req.body.user.password.red);
						console.log('   passwordHash  -'.cyan, passwordHash);
						console.log('   sessionID  -'.cyan, req.sessionID);
					} else {
						var exdate = new Date();
						exdate.setDate(exdate.getDate() + config.COOKIEEXPIRES);

						res.cookie('usid', sessionHash, {
							signed: true,
							expires: exdate
						});

						res.status(200);
						res.send(req.session.redirect_to ? req.session.redirect_to : '/user/vcis');

						console.log('[' + 'REGISTER'.cyan.bold + ']',req.body.user.email.green + ':' + req.body.user.password.green);
						console.log('   passwordHash  -'.cyan, passwordHash);
						console.log('   sessionID  -'.cyan, req.sessionID);
					}
					docs.forEach(function (doc) {
						//console.dir(doc);
					});
				});
			}
		});
	});

	// ---------------------------------------------------------------- //

	app.post('/vci/:vciName/create', function (req, res) {
		Sessions.find({
			usid: req.signedCookies.usid
		}, function (err, cursor) {
			cursor.toArray(function (err, session) {
				console.log(session);
				if (session.length > 0) {
					var dbUser = session[0].user;

					VCIs.insert([{
						name: req.params.vciName,
						creator: dbUser,
						users: [dbUser]
					}], function (err, docs) {
						if (err) logger.error(err);

						docs.forEach(function (doc) {
							console.log("-----------------------------------");
							console.dir(doc);
							console.log("-----------------------------------");

							res.status(200);
							res.send(doc._id);
						});
					});
				} else {
					res.status(404);
					res.send('');
				}
			});
		});
	});

	app.all('/vci/:vciID/delete', function (req, res) {


		Sessions.find({
			usid: req.signedCookies.usid
		}, function (err, cursor) {
			cursor.toArray(function (err, session) {
				VCIs.remove({
					_id: bson.ObjectID.createFromHexString(req.params.vciID) /*, creator: session.user*/
				}, function (err, removed) {
					if (err) logger.error(err);

					if (removed) {
						// remove vci_data and chat_messages ...
					}
				});
			});
		});

		res.status(200);
		res.send('');
	});

	app.all('/vci/:vciID/:userName/addUser', function (req, res) {
		Sessions.find({
			usid: req.signedCookies.usid
		}, function (err, cursor) {
			cursor.toArray(function (err, session) {
				try {
					Users.find({
						name: req.params.userName
					}, function (err, cursor) {
						cursor.toArray(function (err, users) {
							if (users.length > 0) {
								var dbUser = new bson.DBRef(Users.collectionName, users[0]._id);

								VCIs.update({
									_id: bson.ObjectID.createFromHexString(req.params.vciID),
									creator: session[0].user,
									users: {
										$ne: dbUser
									}
								}, {
									$push: {
										users: dbUser
									}
								});

								res.status(200);
								res.send('');
							} else {
								logger.error('unknown user: ' + req.params.userName);
								res.status(404);
								res.send('unknown user');
							}
						});
					});
				} catch (e) {
					logger.error(e);
					res.status(404);
					res.send(e);
				}
			});
		});
	});

	// ---------------------------------------------------------------- //

	app.get(['/index', '/welcome'], function (req, res) {
		var args = {
			name: req.ip,
			host: req.host,
			port: getRQPort(req)
		};

		res.render('index', args);
	});

	app.get('/vci/:vciID', function (req, res) {
		Sessions.find({
			usid: req.signedCookies.usid
		}, function (err, cursor) {
			cursor.toArray(function (err, result) {
				try {
					VCIs.find({
						_id: bson.ObjectID.createFromHexString(req.params.vciID),
						users: result[0].user
					}).count(function (error, count) {
						if (error) {
							console.error(error);

							res.status(404);
							res.render('error/404', {
								url: req.url
							});
						} else {
							if (count > 0) {
								var port = getRQPort(req);
								port = config.SSL ? port != 443 ? ':' + port : "" : port != 80 ? ':' + port : "";
								var protocol = config.SSL ? 'https' : 'http';
								var args = {
									ip: req.ip,
									host: req.host,
									port: config.SSL ? config.HTTPSPORT : config.HTTPPORT,
									server: protocol + '://' + req.host + port + '/',
									vciID: req.params.vciID,
									ssid: uuid.v4()
								};

								res.render('vci', args);

								vci_sessions[args.ssid] = new Object();
								vci_sessions[args.ssid].vci = req.params.vciID;

								db.dereference(result[0].user, function (err, user) {
									console.log('+++++++++++++++++++++++++++++');
									console.dir(user);
									console.log('-----------------------------');


									vci_sessions[args.ssid].uid = user._id;
									vci_sessions[args.ssid].name = user.name;
								});
							} else res.redirect('/user/vcis');
						}
					})
				} catch (e) {
					res.redirect('/user/vcis');
				}
			});
		});
	});

	app.get(['/', '/user/vcis'], function (req, res) {
		var port = getRQPort(req);
		port = config.SSL ? port != 443 ? ':' + port : "" : port != 80 ? ':' + port : "";
		var protocol = config.SSL ? 'https' : 'http';

		Sessions.find({
			usid: req.signedCookies.usid
		}, function (err, cursor) {
			cursor.toArray(function (err, session) {
				VCIs.find({
					users: session[0].user
				}, function (err, cursor) {
					cursor.toArray(function (err, result) {
						var jobs = new Array();
						var vcis = new Array();
						var user = new Object();

						var dereferenceUserName = function (vci, dbref) {
							return function (callback) {
								db.dereference(dbref, function (err, user) {
									vci.users.push(user.name);
									callback(null, null);
								});
							}
						};

						jobs.push(function (user, dbref) {
							return function (callback) {
								db.dereference(dbref, function (err, result) {
									user.name = result.name;
									callback(null, null);
								});
							}
						}(user, session[0].user));


						for (i in result) {
							var vci = {
								id: result[i]._id,
								name: result[i].name,
								users: new Array()
							}

							vcis.push(vci);

							for (key in result[i].users)
							jobs.push(dereferenceUserName(vci, result[i].users[key]));
						}

						async.parallel(jobs, function (err, users) {
							var args = {
								name: user.name,
								host: req.host,
								port: getRQPort(req),
								vciID: req.params.vciID,

								server: protocol + '://' + req.host + port + '/',
								vcis: vcis
							};

							res.render('user/vcis', args);
						});
					});
				});
			});
		});
	});

	app.get('/user/account', function (req, res) {
		Users.find({
			email: email,
			passwordHash: passwordHash
		}, function (err, cursor) {
			cursor.toArray(function (err, result) {
				if (result.length > 0) {
					var args = {
						user: {
							name: result.name,
							email: result.email
						}
					};

					res.render('user/account', args);
				} else res.redirect('/user/login');
			});
		});
	});

	app.get('/user/login', function (req, res) {
		var port = getRQPort(req);
		port = config.SSL ? port != 443 ? ':' + port : "" : port != 80 ? ':' + port : "";
		var protocol = config.SSL ? 'https' : 'http';
		var args = { server: protocol + '://' + req.host + port + '/' };
		res.render('user/login', args);
	});

	app.get('/user/register', function (req, res) {
		var port = getRQPort(req);
		port = config.SSL ? port != 443 ? ':' + port : "" : port != 80 ? ':' + port : "";
		var protocol = config.SSL ? 'https' : 'http';
		var args = { server: protocol + '://' + req.host + port + '/' };
		res.render('user/register', args);
	});

	// static files(file cache)
	app.get('*', function (req, res, next) {
		fs.exists(rootdirname + '/public/' + req.url, function (exists) {
			if (exists) fsCache.load(req.url, function (mimeType, data) {
				res.contentType(mimeType);
				res.send(data);
			})
			else next();
		});
	});

	// return error if no route match the request
	app.get('*', function (req, res) {
		res.status(404);
		res.render('error/404', {
			url: req.url
		});
	});
};